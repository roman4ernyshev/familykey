<?php
use common\widgets\Alert;
use frontend\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

\frontend\assets\DeutschAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en">
<!--<![endif]-->
<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title)?></title>
    <link href="<?= Html::encode(Url::canonical()) ?>" rel="canonical"/>
    <?= $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<style>
    body{
       /* background-color: rgb(125, 37, 27); */
        background-color: rgba(34, 37, 41, 0.95);
    }

    h2, h3, h4,p{
        color: white;
        text-align: center;
        line-height: 1.5;
    }

    .container{
        background-color: rgb(70, 74, 79);;
        padding: 5px;
        color: white;
    }

    .btn-success {
        color: rgb(255, 255, 255);
        background-color: rgba(42, 46, 50, 0.95);
        background-image: none;
        border-color: rgba(63, 68, 72, 0.95);
        font-size: 20px;
    }
    .btn-success:active {
        color: rgb(255, 255, 255);
        background-color: rgba(42, 46, 50, 0.95);
        background-image: none;
        border-color: rgba(118, 128, 137, 0.95);
    }
    .btn-success:hover{
        background-color: rgb(60, 63, 65);
        border-color: rgb(32, 27, 29);
    }

    .alert-success {
        color: rgb(231, 228, 214);
        background-color: rgb(34, 33, 35);
        border-color: rgb(175, 172, 173);
    }
    .form-control{
        background-color: rgb(8, 10, 12);
        color: white;
        height: 50px;
    }


    button{
        width: 100%;
        height: 60px;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        margin-bottom: 5px;

    }
    .active-button{
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }

    button p{
        overflow: hidden; /* Обрезаем все, что не помещается в область */
        text-overflow: ellipsis; /* Добавляем многоточие */
        height: 55px;
    }



    i.fa-soccer-ball-o, i.fa-bus, i.fa-language, i.fa-music {
        color: rgb(30, 144, 255);
        font-size: 40px;
        transition: 0.2s;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
    }

    i.fa-play {
        color: rgb(189, 217, 233);
        font-size: 40px;
        transition: 0.2s;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
    }


    i.fa-soccer-ball-o:hover, i.fa-bus:hover, i.fa-language:hover, i.fa-music:hover, i.fa-play:hover {
        color: orange;
        font-size: 35px;
    }

    .icons{
        cursor: pointer;
        padding-top: 10px;
    }

    .form-inline .form-group {
        width: 100%;
    }

    .jp-playlist ul {

        font-size: 1.3em;

    }
    #content {
        min-height: 1600px;
    }


    div.jp-type-playlist div.jp-playlist a {
        color: rgb(66, 139, 202);
        text-decoration: none;
    }

    @media (min-width: 992px){
        .container {
            width: 800px;
        }
        .form-inline .form-group {
            width: 100%;
        }
    }

    @media (min-width: 768px) {

        .form-inline .form-control {
            display: inline-block;
            width: 100%;
            vertical-align: middle;
        }
    }


</style>


<div class="container">

    <div class="container" >

            <div id="top-links" class="nav pull-right">
                <ul class="list-inline">

                    <li class="dropdown"><a href="" title="My Account" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"></span>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <?php if (Yii::$app->user->isGuest): ?>
                                <li><a href="<?= Html::encode(Url::to(['/auth/auth/login'])) ?>">Вход</a></li>
                                <li><a href="<?= Html::encode(Url::to(['/auth/signup/request'])) ?>">Регистрация</a></li>
                            <?php else: ?>
                                <li><a href="<?= Html::encode(Url::to(['/cabinet/default/index'])) ?>">Кабинет</a></li>
                                <li><a href="<?= Html::encode(Url::to(['/auth/auth/logout'])) ?>" data-method="post">Выход</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                </ul>
            </div>
    </div>

    <?= Alert::widget() ?>
    <?= $content ?>


</div>
<?php $this->endBody() ?>


</body></html>
<?php $this->endPage() ?>
