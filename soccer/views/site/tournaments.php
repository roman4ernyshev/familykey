<?php
/**
 * @var $country_id integer
 */
?>

    <div id="search-form">
        <p>
            <input type="text" class="form-control" id="tournament" placeholder="Турнир">
        </p>

    </div>

    <button type="submit" class="btn-success" onclick="getTeam()" id="get-matches">Команда</button>




<script>
    $(document).ready(function() {

        $('#tournament').focus(
            function () {
                $(this).select();
            });

        $('#tournament').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("tournament/all/<?= $country_id ?>", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

    });

    function getTeam() {

        var tournament = $("#tournament").val();
        $.ajax({
            type: "GET",
            url: "tournament/get-teams-of-tournament",
            data:  'name=' + tournament,
            success: function(res){
                $("#matches").html(res);
                $("#get-matches").hide();
            }
        });
    }


</script>

