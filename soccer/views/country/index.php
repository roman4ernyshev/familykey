<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $tournaments \core\entities\Soccer\Tournament[] */
$this->title = 'Футбол';
?>

<?php foreach ($tournaments as $tournament) : ?>

    <?php $form = ActiveForm::begin(
            [
                'id' => 'tournament-form-'.$tournament->id,
                'action' => Url::to(['/tournament/'.$tournament->id])
            ]);
    ?>

    <div>
        <?= Html::submitButton( $tournament->name . '<img src="/images/'.$tournament->hash_image[$tournament->hash].'" />',
            ['class' => 'btn btn-success btn-lg btn-block', 'name' => 'tournament-button-'.$tournament->id]) ?>
    </div>

    <?php ActiveForm::end(); ?>


<?php endforeach;