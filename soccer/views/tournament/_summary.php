<?php
/* @var \core\entities\Soccer\TeamSummary[] $summary */
/* @var integer $tournament_id */
/* @var string $team_name */
?>
<style>
    .table_blur {
        background: #363b3f;
        border-collapse: collapse;
        text-align: left;
        font-size: 20px;
    }
    .table_blur th {
        border-top: 1px solid #777777;
        border-bottom: 1px solid #777777;
        box-shadow: inset 0 1px 0 #999999, inset 0 -1px 0 #999999;
        background: linear-gradient(#9595b6, #5a567f);
        color: white;
        padding: 10px 15px;
        position: relative;
    }
    .table_blur th:after {
        content: "";
        display: block;
        position: absolute;
        left: 0;
        top: 25%;
        height: 25%;
        width: 100%;
        background: linear-gradient(rgba(255, 255, 255, 0), rgba(255,255,255,.08));
    }
    .table_blur tr:nth-child(odd) {
        background: #768089;
        background-color: rgb(47, 46, 42);
        background-image: linear-gradient(to bottom, rgb(20, 19, 19), rgb(255, 215, 0));
        background-repeat: repeat-x;
    }
    .table_blur th:first-child {
        border-left: 1px solid #777777;
        border-bottom:  1px solid #777777;
        box-shadow: inset 1px 1px 0 #999999, inset 0 -1px 0 #999999;
    }
    .table_blur th:last-child {
        border-right: 1px solid #777777;
        border-bottom:  1px solid #777777;
        box-shadow: inset -1px 1px 0 #999999, inset 0 -1px 0 #999999;
    }
    .table_blur td {
        border: 1px solid #e3eef7;
        padding: 10px 15px;
        position: relative;
        transition: all 0.5s ease;
    }
    .centr{
        text-align: center;
    }
    .red{
        color: rgb(255, 235, 59);
    }
    /*
    .table_blur tbody:hover td {
        color: transparent;
        text-shadow: 0 0 3px #a09f9d;
    }
    .table_blur tbody:hover tr:hover td {
        color: #444444;
        text-shadow: none;
    }
    */
    @media (max-width: 768px) {
        .table_blur {
            font-size: 15px;
        }
        .table_blur td {
            padding: 3px 2px;
        }
        .firsts td{
            cursor: pointer;
        }
    }
</style>
<table class="table_blur">
    <tbody>
    <tr>
        <td>м</td>
        <td class="fix_width">к</td>
        <td class="centr">и</td>
        <td class="centr">в</td>
        <td class="centr">н</td>
        <td class="centr">п</td>
        <td class="centr">рм</td>
        <td class="centr">о</td>
    </tr>
    <?php $i=1; foreach ($summary as $sum) :
        //echo $team_name.'<br>'.$sum->row_team; exit;
        ?>
        <?php if($sum->row_team == $team_name) : ?>
        <tr class="firsts red" id="t_<?=$i?>">
        <?php else : ?>
        <tr class="firsts" id="t_<?=$i?>">
        <?php endif; ?>
        <td><?=$i?></td>
        <td width="30%" style="cursor: pointer;"
            onclick="getTournamentMatchesOfTeam('<?=$sum->row_team ?>',
                    '<?=$tournament_id?>', 't_<?=$i++?>');"><?= $sum->team ?>
        </td>
        <td class="centr"><?= $sum->games ?></td>
        <td class="centr" style="cursor: pointer;" title="Показать победы"
            onclick="getTournamentMatchesOfTeam('<?=$sum->row_team ?>',
                '<?=$tournament_id?>', 't_<?=$i?>', 1);"><?= $sum->victories ?> </td>
        <td class="centr" style="cursor: pointer;" title="Показать ничьи"
            onclick="getTournamentMatchesOfTeam('<?=$sum->row_team ?>',
                    '<?=$tournament_id?>', 't_<?=$i?>', 2);"><?= $sum->ties ?></td>
        <td class="centr" style="cursor: pointer;" title="Паказать поражения"
            onclick="getTournamentMatchesOfTeam('<?=$sum->row_team ?>',
                    '<?=$tournament_id?>', 't_<?=$i?>', 3);"><?= $sum->defeats ?></td>
        <td width="20%" class="centr"><?= $sum->gets ?> : <?= $sum->lets ?></td>
        <td class="centr"><?= $sum->balls ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>