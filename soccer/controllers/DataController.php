<?php

namespace soccer\controllers;

use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\RadioItem;
use Yii;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;

class DataController extends Controller
{
    function getCurrentTrackText()
    {
        //$file = html_entity_decode(strip_tags(file("http://88.212.253.193:8000/status.xsl?mount=/test")[68]));
        //if(isset(file(\Yii::getAlias('@radio_test_tags'))[68]))
        //    $file = html_entity_decode(strip_tags(file(\Yii::getAlias('@radio_test_tags'))[68]));
        //var_dump(file('http://88.85.67.159:8000/test.xspf'));
        //var_dump($this->parseXmlTitle('http://88.85.67.159:8000/test.xspf'));

        $title = $this->parseXmlTitle(\Yii::getAlias('@radio_test_tags'));
        //exit;
        //else $file = "";
        $item = RadioItem::find()->where(['like', 'audio', trim($title)])->one();
        //var_dump($item);
        if($item) return $item->title . '<br>';
        //echo html_entity_decode(strip_tags(file("http://88.212.253.193:8000/status.xsl?mount=/test")[68])). '<br>';
        return $title;

    }

    public function actionInvertRandImg()
    {
        $item = Items::find()
            ->where('cat_id = 259')
            ->orderBy(['rand()' => SORT_DESC])
            ->limit(1)
            ->one();
        //\core\helpers\Helper::invertImageColor($item->img);
        return '<img class="pic css-adaptive" src="' . Url::to('@static/'.$item->img) . '?cash='.time().'"/>';
    }

}