<?php

return [
    'class' => 'yii\web\UrlManager',
    'hostInfo' => $params['radioHostInfo'],
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        '' => 'site/index',
        //'notion'=> 'notion/notion',
        '<_a:login|logout>' => 'auth/<_a>',

        'notion/category/index' => 'notion/category/index',
        'notion/category/create' => 'notion/category/create',
        'notion/category/update' => 'notion/category/update',
        'notion/category/view' => 'notion/category/view',

        'product/category/index' => 'product/category/index',
        'product/category/create' => 'product/category/create',
        'product/category/update' => 'product/category/update',
        'product/category/view' => 'product/category/view',


        'notion/<category:[\w\-]+>' => 'notion/notion/index',
        'notion/<category:[\w\-]+>/<_a:[\w-]+>' => 'notion/notion/<_a>',


       // '/notion/' => '/notion/notion/',
      //  '<_c:[\w\-]+>/deutsch' => '<_c>/index',


        '<_c:[\w\-]+>' => '<_c>/index',
        '<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
        '<_c:[\w\-]+>/<_a:[\w-]+>' => '<_c>/<_a>',
        '<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_c>/<_a>',
        //'rockncontroll/<_c:[\w\-]+>/<_a:[\w-]+>/<id:\d+>' => 'rockncontroll/<_c>/<_a>',


    ],
];