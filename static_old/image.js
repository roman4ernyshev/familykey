/**
 * @type {HTMLImageElement}
 */
var image = new Image();
image.onload = function () {
    //ctx.drawImage(image,10,10,40,40);
    //ctx.save();
    setInterval(move,100);
    /*


    ctx.save();
    ctx.translate(40,-10);
    ctx.rotate(30*Math.PI/180);
    ctx.drawImage(image,0,0);
    ctx.restore();

    ctx.save();
    ctx.rotate(-30*Math.PI/180);
    ctx.translate(100,100);
    ctx.scale(0.4,0.4);
    ctx.drawImage(image,0,0);
    ctx.restore();
    */


};
image.src = 'ball.png';

var x = 150;
var y = 150;

function move() {
    //console.log(x);
    ctx.save();
    clearCanvas();
    ctx.scale(0.1,0.1);

    if (x<5000 && y<5000) {
        x += 5;
        y += 5;
    }
    else {
        x = 50;
        y = 50;
    }
    ctx.rotate(10*Math.PI/180);
    //ctx.clearRect(0,0,canvas.width,canvas.height);
    //clearCanvas();

    ctx.translate(x,y);
    ctx.drawImage(image,0,0);

    ctx.restore();


}

function clearCanvas() {
    ctx.clearRect(0,0,canvas.width,canvas.height);
    ctx.strokeStyle = 'white';
    ctx.lineWidth = 3;
    ctx.fillStyle = 'green';
    //ctx.shadowBlur = 10;
    //ctx.shadowColor = 'black';

    ctx.rect(10,10,500,300); // x,y,width,height
    ctx.fill();
    ctx.stroke();

    function renderGoal(obj) {
        ctx.strokeStyle = obj.color;
        ctx.strokeRect(obj.x,135,obj.width,obj.length);
        ctx.stroke();
    }
    renderGoal({x:10,width:10,length:30,color:'blue'});
    renderGoal({x:490,width:10,length:30,color:'blue'});
}