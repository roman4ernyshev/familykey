<?php
return [
    'cookieValidationKey' => 'Zr5rjfpqB1a28aDNjNt6AxSjxXJoFDhC',
    'cookieDomain' => '.radiorooma.ru',
    'frontendHostInfo' => 'https://radiorooma.ru',
    'backendHostInfo' => 'http://backend.radiorooma.ru',
    'staticHostInfo' => 'http://static.radiorooma.ru',
    'staticPath' => dirname(__DIR__) . '/../static',
    'mailChimpKey' => '343ae33acd77c9ae8d736a425e355b63-us18',
    'mailChimpListId' => '',
    'smsRuKey' => '',
];
