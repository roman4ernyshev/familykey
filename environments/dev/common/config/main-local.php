<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=family',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'db_rockncontroll' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=plis',
            'username' => 'root',
            'password' => 'vbifcdtnf',
            'charset' => 'utf8',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'robokassa' => [
            //dates from robokassa cabinet
            'sMerchantLogin' => 'ggggg',
            'sMerchantPass1' => 'jfkjkfjjf',
            'sMerchantPass2' => 'ioiodipidpoip',
        ]
    ],
];
