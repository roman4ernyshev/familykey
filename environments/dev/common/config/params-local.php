<?php
return [
    'cookieValidationKey' => '',
    'cookieDomain' => '.myframework.loc',
    'frontendHostInfo' => 'http://myframework.loc',
    'backendHostInfo' => 'http://backend.myframework.loc',
    'staticHostInfo' => 'http://static.myframework.loc',
    'staticPath' => dirname(__DIR__) . '/../static',
    'mailChimpKey' => '',
    'mailChimpListId' => '',
    'smsRuKey' => '',
];
