<?php
/**
 * /* @var $project core\entities\Project\Project */
?>
<?php foreach ($projects as $project): ?>
<div class="col-md-3 col-sm-3 col-xs-12">

            <div class="team animated hiding" data-animation="fadeInUp" data-delay="0">

                <div class="team-photo">

                    <img src="<?= \yii\helpers\Html::encode($project->photos[0]->getThumbFileUrl('file', 'thumb')) ?>" alt="" class="img-responsive" />

                    <a href="<?= $project->link ?>" target="_blank" class="team-overlay">

                        <span class="top"><?= \yii\helpers\Url::to($project->description) ?></span>

                        <span class="bottom">Перейти на сайт</span>

                    </a>

                </div>

                <div class="team-inner">

                    <h3>Сайт</h3>
                    <p><?= $project->title ?></p>

                </div>

            </div>

        </div>
<?php endforeach; ?>