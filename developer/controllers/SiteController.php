<?php
namespace developer\controllers;

use core\readModels\ProjectReadRepository;
use core\useCases\manage\ProjectManageService;
use yii\web\Controller;


/**
 * Site controller
 */
class SiteController extends Controller
{
    public $projects;

    public function __construct($id, $module, ProjectReadRepository $projects, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->projects = $projects;
    }


    public function actionIndex()
    {

        $this->layout = 'base';

        return $this->render('index', ['projects' => $this->projects->getAll()]);
    }


    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

}
