var i = 0;
var stop = 0;

function playSound() {
    console.log(songs);
    $('#audioPlayer').hide();

    $(document).ready(function() {

        var myAudio = new Audio();
        myAudio.src = songs[i].url;
        $('#currSong').html('<button type="button" class="btn btn-default btn-sm"  onclick="stopAudio()" style="cursor: pointer">Остановить после: '+songs[i].name+'</button>');

        $(myAudio).bind('ended', function()  {
            i++;
            if(songs.length == i) return;
            if(stop == 1) {  $('#currSong').hide(); $('#audioPlayer').show(); return; }
            myAudio.currentTime = 0;
            myAudio.src = songs[i].url;
            $('#currSong').html('<button type="button" class="btn btn-default btn-sm" onclick="stopAudio()" style="cursor: pointer">Остановить после: '+songs[i].name+'</button>');
            myAudio.play();
        });

        myAudio.play();

    });


}

function stopAudio(){
    stop = 1;
}