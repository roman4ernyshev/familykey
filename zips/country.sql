-- MySQL dump 10.13  Distrib 5.5.61, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: plis
-- ------------------------------------------------------
-- Server version	5.5.61-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `iso_code` varchar(24) NOT NULL,
  `soc_abrev` varchar(24) NOT NULL,
  `soccer_code` int(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=237 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Абхазия','','','',0),(2,'Австралия','uploads/icoflags/australia.png','au','(Авс)',24),(3,'Австрия','','at','(Авт)',25),(4,'Азербайджан','','az','(Азе)',26),(5,'Азорские острова','','','',0),(6,'Албания','','al','(Алб)',17),(7,'Алжир','','dz','(Алж)',18),(8,'Американские Виргинские острова','','','',0),(9,'Ангилья','','','',0),(10,'Ангола','','ao','',20),(11,'Андорра','','ad','(Анд)',19),(12,'Антарктика','','aq','',10),(13,'Антигуа и Барбуда','','ag','',28),(14,'Аргентина','','ar','(Арг)',22),(15,'Армения','','am','(Арм)',19),(16,'Аруба','','aw','',0),(17,'Афганистан','','af','',0),(18,'Багамские острова','','bs','',0),(19,'Балеарские острова','','','',0),(20,'Бангладеш','','bd','',0),(21,'Барбадос','','bb','',0),(22,'Бахрейн','','bh','',0),(23,'Беларусь','','by','(Беи)',31),(24,'Белиз','','bz','',0),(25,'Бельгия','','be','(Бел)',32),(26,'Бенин','','bj','',0),(27,'Бермудские Острова','','bm','',0),(28,'Болгария','','bg','(Бог)',41),(29,'Боливия','','bo','(Бол)',36),(30,'Босния и Герцеговина','','ba','(Бос)',37),(31,'Ботсвана','','bw','',0),(32,'Бразилия','','br','(Бра)',39),(33,'Британские Виргинские острова','','io','',0),(34,'Бруней','','bn','',0),(35,'Буркина-Фасо','','bf','',0),(36,'Бурунди','','bi','',0),(37,'Бутан','','bt','',0),(38,'Ватикан','','va','',0),(39,'Великобритания','','gb','',0),(40,'Венгрия','','hu','(Вен)',91),(41,'Венесуэла','','ve','',205),(42,'Восточное Самоа','','','',0),(43,'Восточный Тимор','','','',0),(44,'Вьетнам','','vn','(Вье)',206),(45,'Габон','','ga','',78),(46,'Гаити','','ht','',0),(47,'Гайана','','gy','',0),(48,'Гамбия','','gm','',79),(49,'Гана','','gh','',82),(50,'Гваделупа','','gp','',0),(51,'Гватемала','','gt','',85),(52,'Гвинея','','gn','',86),(53,'Гвинея-Бисау','','gw','',0),(54,'Германия','','de','(Гер)',81),(55,'Гибралтар','','gi','(Гиб)',304),(56,'Гондурас','','hn','',90),(57,'Гренада','','gd','',0),(58,'Гренландия','','gl','',0),(59,'Греция','','gr','(Гри)',83),(60,'Грузия','','ge','(Гру)',80),(61,'Дания','','dk','(Дан)',63),(62,'Демократическая республика Конго','','cd','',56),(63,'Джибути','','dj','',64),(64,'Доминика','','dm','',0),(65,'Доминиканская Республика','','co','',0),(66,'Египет','','eg','',69),(67,'Замбия','','zm','',209),(68,'Западная Сахара','','eh','',0),(69,'Зимбабве','','zw','',210),(70,'Израиль','','il','(Изр)',97),(71,'Индия','','in','',93),(72,'Индонезия','','id','(Ина)',228),(73,'Иордания','','jo','',101),(74,'Ирак','','iq','',95),(75,'Иран','','ir','(Ирн)',94),(76,'Ирландия','','ie','(Иря)',96),(77,'Исландия','','is','(Исл)',92),(78,'Испания','','es','(Исп)',176),(79,'Италия','','it','(Ита)',98),(80,'Йемен','','ye','',208),(81,'Кабо-Верде','','cv','',48),(82,'Казахстан','','kz','(Каз)',102),(83,'Каймановы острова','','ky','',0),(84,'Камбоджа','','kh','',0),(85,'Камерун','','cm','(Кам)',46),(86,'Канада','','ca','(Кан)',47),(87,'Канарские острова','','','',0),(88,'Катар','','qa','(Кат)',156),(89,'Кения','','ke','',103),(90,'Кипр','','cy','(Кип)',61),(91,'Киргизия','','kg','',0),(92,'Кирибати','','ki','',0),(93,'Китай','','cn','(Кит)',52),(94,'Кокосовые острова','','cc','',0),(95,'Колумбия','','co','(Кол)',53),(96,'Коморские острова','','km','',0),(97,'Коста-Рика','','cr','',57),(98,'Кот-д\'Ивуар','','ci','(Кот)',58),(99,'Куба','','cu','',0),(100,'Кувейт','','kw','',107),(101,'Лаос','','kw','',0),(102,'Латвия','','lv','',110),(103,'Лесото','','ls','',112),(104,'Либерия','','lr','',113),(105,'Ливан','','ly','',0),(106,'Ливия','','','',114),(107,'Литва','','lt','(Лит)',116),(108,'Лихтенштейн','','li','(Лих)',115),(109,'Люксембург','','lu','(Люк)',117),(110,'Маврикий','','mu','',127),(111,'Мавритания','','mr','',126),(112,'Мадагаскар','','mg','',0),(113,'Майотта','','yt','',0),(114,'Македония','','lt','(Мад)',118),(115,'Малави','','mw','',120),(116,'Малайзия','','my','(Маз)',121),(117,'Мали','','ml','',123),(118,'Мальдивские острова','','mv','',0),(119,'Мальта','','mt','(Млт)',124),(120,'Марокко','','ma','(Мар)',134),(121,'Маршалловы Острова','','mh','',0),(122,'Мексика','','mx','(Мек)',128),(123,'Мелилья','','','',0),(124,'Микронезия','','fm','',0),(125,'Мозамбик','','mz','',135),(126,'Молдова','','md','(Мол)',130),(127,'Монако','','mc','',0),(128,'Монголия','','mn','',0),(129,'Монтсеррат','','ms','',0),(130,'Мьянма','','mm','',0),(131,'Намибия','','na','',136),(132,'Науру','','nr','',0),(133,'Непал','','np','',0),(134,'Нигер','','ne','',142),(135,'Нигерия','','ng','',143),(136,'Нидерландские Антильские острова','','an','',0),(137,'Нидерланды','','nl','(Нид)',139),(138,'Никарагуа','','ni','',0),(139,'Новая Зеландия','','nz','(Нзл)',140),(140,'Новая Каледония','','nc','',0),(141,'Норвегия','','no','(Нор)',145),(142,'Норфолк','','nf','',0),(143,'ОАЭ','','ae','(ОАЭ)',196),(144,'Оман','','om','',0),(145,'Остров Гуам','','gu','',0),(146,'Остров Рождества','','','',0),(147,'Остров Ян-Майен','','','',0),(148,'Острова Кука','','','',0),(149,'Острова Свальбард','','','',0),(150,'Пакистан','','pk','',147),(151,'Палау','','pw','',0),(152,'Палестинская автономия','','','',0),(153,'Панама','','pa','(Пан)',149),(154,'Папуа - Новая Гвинея','','pg','',0),(155,'Парагвай','','py','(Пар)',151),(156,'Перу','','pe','(Пер)',152),(157,'Польша','','pl','(Пол)',154),(158,'Португалия','','pt','(Пор)',155),(159,'Республика Вануату','','vu','',0),(160,'Республика Конго','','cd','',0),(161,'Реюньон','','re','',0),(162,'Россия','','ru','(Рос)',158),(163,'Руанда','','rw','',159),(164,'Румыния','','ro','(Рум)',157),(165,'США','','us','(США)',200),(166,'Сальвадор','','sv','',70),(167,'Самоа','','ws','',0),(168,'Сан-Марино','','sm','(Сан)',163),(169,'Сан-Томе и Принсипи','','st','',0),(170,'Саудовская Аравия','','sa','(Сау)',165),(171,'Свазиленд','','','',180),(172,'Северная Корея','','kp','',0),(173,'Северные Марианские острова','','mp','',0),(174,'Сейшельские острова','','sc','',0),(175,'Сенегал','','sn','',166),(176,'Сент-Винсент и Гренадины','','vc','',0),(177,'Сент-Китс и Невис','','kn','',0),(178,'Сент-Люсия','','lc','',0),(179,'Сербия','','rs','(Сер)',167),(180,'Сеута','','','',0),(181,'Сингапур','','sg','(Син)',170),(182,'Сирия','','sy','',183),(183,'Словакия','','sk','(Сла)',171),(184,'Словения','','si','(Сло)',172),(185,'Соломоновы острова','','sb','',0),(186,'Сомали','','so','',0),(187,'Судан','','sd','',178),(188,'Суринам','','sr','',0),(189,'Сьерра-Леоне','','sl','',169),(190,'Таджикистан','','tj','',184),(191,'Таиланд','','th','(Таи)',186),(192,'Тайвань','','tw','',0),(193,'Танзания','','tz','',185),(194,'Того','','tg','',187),(195,'Тонга','','to','',0),(196,'Тринидад и Тобаго','','tt','',0),(197,'Тувалу','','tv','',0),(198,'Тунис','','tn','(Тун)',190),(199,'Туркмения','','tm','',0),(200,'Турция','','tr','(Туц)',191),(201,'Тёркс и Кайкос','','tc','',0),(202,'Уганда','','ug','',194),(203,'Узбекистан','','uz','(Узб)',202),(204,'Украина','','ua','(Укр)',195),(205,'Уругвай','','uy','(Уру)',201),(206,'Фарерские острова','','fo','(Фар)',231),(207,'Фиджи','','fj','',0),(208,'Филиппины','','ph','',153),(209,'Финляндия','','fi','(Фин)',76),(210,'Франция','','fr','(Фра)',77),(211,'Французская Гвиана','','gf','',0),(212,'Французская Полинезия','','pf','',0),(213,'Хорватия','','hr','(Хор)',59),(214,'Центральноафриканская республика','','cf','',0),(215,'Чад','','td','',0),(216,'Черногория','','me','(Чрн)',133),(217,'Чехия','','cz','(Чех)',62),(218,'Чили','','cl','(Чил)',51),(219,'Швейцария','','ch','(Шва)',182),(220,'Швеция','','se','(Шве)',181),(221,'Шри-Ланка','','lk','',0),(222,'Эквадор','','ec','(Эка)',68),(223,'Экваториальная Гвинея','','gq','',0),(224,'Эритрея','','er','',0),(225,'Эстония','','ee','(Эст)',73),(226,'Эфиопия','','et','',74),(227,'ЮАР','','za','(Южн)',175),(228,'Южная Корея','','kr','(Кор)',106),(229,'Южная Осетия','','','',0),(230,'Ямайка','','jm','(Ямк)',0),(231,'Япония','','jp','(Япо)',100),(232,'Уэльс','uploads/icoflags/wales.png','gb','(Уэл)',207),(233,'Англия','','gb','(Анг)',198),(234,'Северная Ирландия','','gb','(СИр)',144),(235,'Шотландия','','gb','(Шот)',199),(236,'none','','','',0);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-26 11:46:09
