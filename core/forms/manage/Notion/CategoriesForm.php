<?php

namespace core\forms\manage\Notion;

use core\entities\Notion\Category;
use core\entities\Notion\Notion;
use yii\base\Model;
use yii\helpers\ArrayHelper;


class CategoriesForm extends Model
{
    public $main;
    public $others = [];

    public function __construct(Notion $notion = null, array $config = [])
    {

        if($notion){
            $this->main = $notion->category_id;
            $this->others = ArrayHelper::getColumn($notion->categoryAssignments, 'category_id');

        }
        parent::__construct($config);
    }

    public function categoriesList(): array
    {
        return ArrayHelper::map(Category::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->asArray()->all(), 'id', function (array $category) {
            return ($category['depth'] > 1 ? str_repeat('-- ', $category['depth'] - 1) . ' ' : '') . $category['name'];
        });
    }

    public function rules()
    {
        return [
            ['main', 'required'],
            ['main', 'integer'],
            ['others', 'each', 'rule' => ['integer']],
        ];
    }



}