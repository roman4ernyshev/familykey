<?php

namespace core\forms\manage\Notion;

use core\entities\Notion\Notion;
use core\forms\CompositeForm;
use core\forms\manage\MetaForm;

/**
 * Class NotionCreateForm
 * @property MetaForm $meta
 * @property CategoriesForm $categories
 * @property PhotosForm $photos
 * @property AudiosForm $audios
 *
 * @package core\forms\manage\Notion
 */
class NotionCreateForm extends CompositeForm
{
    public $body;
    public $question;
    public $answer;
    public $description;

    private $_notion;

    public function __construct(Notion $notion = null, array $config = [])
    {
        if($notion) {
            $this->body = $notion->body;
            $this->question = $notion->question;
            $this->answer = $notion->answer;
            $this->description = $notion->description;

            $this->meta = new MetaForm($notion->meta);
            $this->photos = new PhotosForm();
           // $this->categories = new CategoriesForm($notion);
            $this->_notion = $notion;
        }
        else{
            $this->meta = new MetaForm();
            $this->photos = new PhotosForm();
            $this->audios = new AudiosForm();
            //$this->categories = new CategoriesForm();
        }
        parent::__construct($config);

    }


    public function rules(): array
    {
        return [
            [['body', 'answer'], 'required'],
            [['body', 'answer'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['body'], 'unique', 'targetClass' => Notion::class,
                'filter' => $this->_notion ? ['<>', 'id', $this->_notion->id] : null],
        ];
    }


    protected function internalForms(): array
    {
        return ['meta', 'photos', 'audios'];
    }

}