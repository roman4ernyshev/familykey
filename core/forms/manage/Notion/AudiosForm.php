<?php

namespace core\forms\manage\Notion;

use yii\base\Model;
use yii\web\UploadedFile;

class AudiosForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;

    public function rules(): array
    {
        return [
           // ['files', 'each', 'rule' => ['mp3']], // for each elements of the array
            //[['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'mp3'],
        ];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {

            $this->files = UploadedFile::getInstances($this, 'files');
            return true;
        }
        return false;
    }

}