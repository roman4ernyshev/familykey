<?php

namespace core\forms\manage\Notion;

use core\entities\Notion\Example;
use core\entities\Notion\Notion;
use core\forms\CompositeForm;

/**
 * Class ExampleCreateForm
 * @property Notion $notion;
 *
 * @property PhotosForm $photos
 * @property AudiosForm $audios
 *
 * @package core\forms\manage\Notion
 */
class ExampleCreateForm extends CompositeForm
{
    public $body;
    public $description;
    public $notion;

    private $_example;

    public function __construct(Notion $notion, Example $example = null, array $config = [])
    {
        if ($example) {
            $this->notion = $notion;
            $this->body = $example->body;
            $this->description = $example->description;

            $this->_example = $example;
        } else {
            $this->notion = $notion;
            $this->photos = new PhotosForm();
            $this->audios = new AudiosForm();
        }
        parent::__construct($config);

    }


    public function rules(): array
    {
        return [
            [['description'], 'required'],
            [['body'], 'string', 'max' => 28],
            [['description'], 'string', 'max' => 512],
            [['body'], 'unique', 'targetClass' => Example::class,
                'filter' => $this->_example ? ['<>', 'id', $this->_example->id] : null],
        ];
    }


    protected function internalForms(): array
    {
        return ['photos', 'audios'];
    }
}
