<?php
namespace core\forms\manage\Product;

use core\entities\Product\ProductsConst;
use core\forms\CompositeForm;
use core\forms\manage\MetaForm;

/**
 * Class ProjectCreateForm
 * @property MetaForm $meta
 * @property PhotosForm $photos
 *
 * @package core\forms\manage\Product
 */
class ProductConstCreateForm extends CompositeForm
{
    public $name;
    public $description;
    public $carbohydrates;
    public $fats;
    public $squirrels;
    public $kkal;
    public $ferrum;
    public $magnesium;
    public $cuprum;
    public $iodum;
    public $fluorum;
    public $zincum;
    public $cobaltum;

    private $_product;

    public function __construct(ProductsConst $product = null, array $config = [])
    {
        if($product) {
            $this->name = $product->name;
            $this->description = $product->description;
            $this->carbohydrates = $product->carbohydrates;
            $this->fats = $product->fats;
            $this->squirrels = $product->squirrels;
            $this->kkal = $product->kkal;
            $this->ferrum = $product->ferrum;
            $this->magnesium = $product->magnesium;
            $this->cuprum = $product->cuprum;
            $this->iodum = $product->iodum;
            $this->fluorum = $product->fluorum;
            $this->zincum = $product->zincum;
            $this->cobaltum = $product->cobaltum;
            $this->meta = new MetaForm($product->meta);

            $this->_product = $product;
        }
        else{
            $this->meta = new MetaForm();
            $this->photos = new PhotosForm();
        }
        parent::__construct($config);

    }


    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string', 'max' => 255],
            [
                [
                    'carbohydrates',
                    'fats',
                    'squirrels',
                    'kkal',
                    'ferrum',
                    'magnesium',
                    'cuprum',
                    'iodum',
                    'fluorum',
                    'zincum',
                    'cobaltum'

                ],
                'double'],
            [['name'], 'unique', 'targetClass' => ProductsConst::class,
                'filter' => $this->_product ? ['<>', 'id', $this->_product->id] : null],
        ];
    }

    protected function internalForms(): array
    {
        return ['meta', 'photos'];
    }

}