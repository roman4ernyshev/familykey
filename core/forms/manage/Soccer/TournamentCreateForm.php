<?php
namespace core\forms\manage\Soccer;

use core\entities\Country;
use core\entities\Soccer\Tournament;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class TournamentCreateForm extends Model
{
    public $name;
    public $hash;
    public $countryId;
    public $season;
    public $description;

    private $_tournament;

    public function __construct(Tournament $tournament = null, array $config = [])
    {
        if($tournament) {
            $this->countryId = $tournament->country_id;
            $this->name = $tournament->name;
            $this->hash = $tournament->hash;
            $this->season = $tournament->season;
            $this->description = $tournament->description;

            $this->_tournament = $tournament;
        }
        parent::__construct($config);

    }

    public function rules(): array
    {
        return [
            [['name', 'hash', 'countryId', 'season'], 'required'],
            [['description', 'name'], 'string', 'max' => 255],
        ];
    }

    public function countriesList(): array
    {
        return ArrayHelper::map(Country::find()
            ->orderBy('name')
            ->asArray()
            ->all(), 'id', 'name');
    }

}