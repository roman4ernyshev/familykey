<?php

namespace core\forms\manage\Project;

use yii\base\Model;
use yii\web\UploadedFile;

class PhotosForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;

    public function rules(): array
    {
        return [
            ['files', 'each', 'rule' => ['image']], // for each elements of the array
            //[['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {

            $this->files = UploadedFile::getInstances($this, 'files');
            return true;
        }
        return false;
    }
}