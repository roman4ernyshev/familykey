<?php
namespace core\forms\manage\Project;

use core\entities\Project\Project;
use core\forms\CompositeForm;
use core\forms\manage\MetaForm;
use core\forms\manage\Notion\PhotosForm;

/**
 * Class ProjectCreateForm
 * @property MetaForm $meta
 * @property PhotosForm $photos
 *
 * @package core\forms\manage\Project
 */
class ProjectCreateForm extends CompositeForm
{
    public $title;
    public $link;
    public $description;

    private $_project;

    public function __construct(Project $project = null, array $config = [])
    {
        if($project) {
            $this->title = $project->title;
            $this->link = $project->link;
            $this->description = $project->description;

            $this->meta = new MetaForm($project->meta);
            $this->_project = $project;
        }
        else{
            $this->meta = new MetaForm();
            $this->photos = new PhotosForm();
        }
        parent::__construct($config);

    }


    public function rules(): array
    {
        return [
            [['title', 'description', 'link'], 'required'],
            [['title', 'description', 'link'], 'string', 'max' => 255],
            [['title'], 'unique', 'targetClass' => Project::class,
                'filter' => $this->_project ? ['<>', 'id', $this->_project->id] : null],
        ];
    }


    protected function internalForms(): array
    {
        return ['meta', 'photos'];
    }


}