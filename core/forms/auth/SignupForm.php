<?php
namespace core\forms\auth;

use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $reCaptcha;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\core\entities\User\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\core\entities\User\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [
                ['reCaptcha'],
                \himiklab\yii2\recaptcha\ReCaptchaValidator::class,
                //'secret' => '6LdpzGUUAAAAAGyoBca5-Sj3gAZgajs_AC9q6wsN', //radiorooma
                //'secret' => '6LeSnG0UAAAAAIS-Wx-Q-UwgygDN2FggdjCtrv8-',
                //'uncheckedMessage' => 'Please confirm that you are not a bot.'
            ]

        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Ваш Ник в системе',
            'password' => 'Пароль',
            'email' => 'Email'
        ];
    }


}
