<?php
namespace core\forms\auth;

use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $reCaptcha;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            [
                ['reCaptcha'],
                \himiklab\yii2\recaptcha\ReCaptchaValidator::class,
                'secret' => \Yii::$app->params['secret'],
                'uncheckedMessage' => 'Please confirm that you are not a bot.'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Ваш Ник в системе',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить'
        ];
    }

}
