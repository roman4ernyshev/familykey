<?php
namespace core\forms\article;

use yii\base\Model;


class SearchForm extends Model
{
    public $title;

    public function rules()
    {
        return [
            [['title'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название ресурса',
        ];
    }


}