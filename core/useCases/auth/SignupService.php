<?php

namespace core\useCases\auth;

use core\access\Rbac;
use core\dispatchers\EventDispatcher;
use core\entities\User\User;
use core\forms\auth\SignupForm;
use core\repositories\UserRepository;
use core\services\newsletter\Newsletter;
use core\services\RoleManager;
use core\services\TransactionManager;

class SignupService
{
    private $users;
    private $roles;
    private $transaction;
    private $newsletter;

    public function __construct(
        UserRepository $users,
        RoleManager $roles,
        TransactionManager $transaction,
        Newsletter $newsletter
    )
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->transaction = $transaction;
        $this->newsletter = $newsletter;
    }

    public function signup(SignupForm $form): void
    {
        $user = User::requestSignup(
            $form->username,
            $form->email,
            $form->password
        );
        $this->transaction->wrap(function () use ($user) {
            $this->users->save($user);
            $this->roles->assign($user->id, Rbac::ROLE_USER);
        });
    }

    public function confirm($token): void
    {
        if (empty($token)) {
            throw new \DomainException('Empty confirm token.');
        }
        $user = $this->users->getByEmailConfirmToken($token);
        $user->confirmSignup();
        $this->users->save($user);
        $this->newsletter->subscribe($user->email);
    }
}