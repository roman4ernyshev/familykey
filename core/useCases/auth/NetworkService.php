<?php

namespace core\useCases\auth;

use core\access\Rbac;
use core\entities\User\User;
use core\repositories\UserRepository;
use core\services\RoleManager;
use core\services\TransactionManager;

class NetworkService
{
    private $users;
    private $roles;
    private $transaction;

    public function __construct(UserRepository $users, RoleManager $roles, TransactionManager $transaction)
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->transaction = $transaction;
    }

    public function auth($network, $identity): User
    {
        if($user = $this->users->findByNetworkIdentity($network, $identity)){
            return $user;
        }

        $user = User::signupByNetwork($network, $identity);

        $this->transaction->wrap(function () use ($user) {
            $this->users->save($user);
            $this->roles->assign($user->id, Rbac::ROLE_USER);
        });
        return $user;
    }

    public function attach($id, $network, $identity): void
    {
        if($this->users->findByNetworkIdentity($network, $identity)){
            throw new \DomainException('Network is ready sighed up.');
        }
        $user = $this->users->get($id);
        $user->attachNetwork($network, $identity);
        $this->users->save($user);
    }

}