<?php

namespace core\useCases\manage\Product;

use core\entities\Meta;
use core\entities\Product\ProductsConst;
use core\forms\manage\Product\PhotosForm;
use core\forms\manage\Product\ProductConstCreateForm;
use core\repositories\Product\ProductConstRepository;

class ProductConstManageService
{
    private $products;

    public function __construct(
        ProductConstRepository $products
    )
    {
        $this->products = $products;
    }

    public function create(ProductConstCreateForm $form): ProductsConst
    {

        $product = ProductsConst::create(
            $form->name,
            $form->description,
            $form->carbohydrates,
            $form->fats,
            $form->squirrels,
            $form->kkal,
            $form->ferrum,
            $form->magnesium,
            $form->cuprum,
            $form->iodum,
            $form->fluorum,
            $form->zincum,
            $form->cobaltum,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );

        foreach ($form->photos->files as $file){
            $product->addPhoto($file);
        }

        $this->products->save($product);

        return $product;

    }

    public function edit($id, ProductConstCreateForm $form): void
    {
        $product = $this->products->get($id);

        $product->edit(
            $form->name,
            $form->description,
            $form->carbohydrates,
            $form->fats,
            $form->squirrels,
            $form->kkal,
            $form->ferrum,
            $form->magnesium,
            $form->cuprum,
            $form->iodum,
            $form->fluorum,
            $form->zincum,
            $form->cobaltum,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );

        $this->products->save($product);

    }

    public function remove($id): void
    {
        $product = $this->products->get($id);
        $this->products->remove($product);
    }

    public function addPhotos($id, PhotosForm $form): void
    {
        $product = $this->products->get($id);
        foreach ($form->files as $file){
            $product->addPhoto($file);
        }
        $this->products->save($product);
    }

}