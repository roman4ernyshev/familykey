<?php

namespace core\useCases\manage;


use core\entities\Books\Author;
use core\entities\Books\Book;
use core\repositories\BookAuthorRepository;
use core\repositories\BookRepository;
use yii\db\StaleObjectException;

class BookManageService
{
    private $books;
    private $authors;

    public function __construct( BookRepository $books, BookAuthorRepository $authors)
    {
        $this->books = $books;
        $this->authors = $authors;
    }

    /**
     * @param array $params
     * @return Book
     *
     */
    public function create(array $params): Book
    {
        $book = Book::create($params);
        $this->books->save($book);
        return $book;
    }

    /**
     * @return BookRepository
     */
    public function getBooks(): BookRepository
    {
        return $this->books;
    }

    /**
     * @param array $params
     *
     * @return Book|null
     */
    public function updateBookAuthor(array $params)
    {
        $book = $this->books->findById((int)$params['book_id']);
        $book->editAuthor((int)$params['author_id']);
        $this->books->save($book);
        return $book;
    }

    public function deleteAuthor($author_id): ?string
    {
        $author = Author::findOne($author_id);
        try {
            $author->delete();
            \Yii::$app->getResponse()->setStatusCode(204);
        } catch (\Exception $e) {
           return $e->getMessage();
        }
        return null;
    }

}