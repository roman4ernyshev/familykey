<?php

namespace core\useCases\manage;


use core\entities\Meta;
use core\entities\Project\Project;
use core\forms\manage\Project\PhotosForm;
use core\forms\manage\Project\ProjectCreateForm;
use core\repositories\ProjectRepository;
use core\services\TransactionManager;


class ProjectManageService
{
    private $projects;
    private $transaction;

    public function __construct(
        ProjectRepository $projects,
        TransactionManager $transaction
    )
    {
        $this->projects = $projects;
        $this->transaction = $transaction;
    }

    public function create(ProjectCreateForm $form): Project
    {

        $project = Project::create(
            $form->title,
            $form->description,
            $form->link,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );

        foreach ($form->photos->files as $file){
            $project->addPhoto($file);
        }

        $this->projects->save($project);

        return $project;

    }

    public function edit($id, ProjectCreateForm $form): void
    {
        $project = $this->projects->get($id);

        $project->edit(
            $form->title,
            $form->description,
            $form->link,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );

        $this->projects->save($project);

    }

    public function remove($id): void
    {
        $notion = $this->projects->get($id);
        $this->projects->remove($notion);
    }

    public function addPhotos($id, PhotosForm $form): void
    {
        $notion = $this->projects->get($id);
        foreach ($form->files as $file){
            $notion->addPhoto($file);
        }
        $this->projects->save($notion);
    }


}