<?php
namespace core\useCases\manage;

use core\entities\Soccer\Tournament;
use core\forms\manage\Soccer\TournamentCreateForm;
use core\repositories\Soccer\TournamentRepository;

class SoccerManageService
{
    private $tournaments;

    public function __construct( TournamentRepository $tournaments)
    {
        $this->tournaments = $tournaments;
    }

    public function create(TournamentCreateForm $form): Tournament
    {
        $tournament = Tournament::create(
            $form->countryId,
            $form->name,
            $form->hash,
            $form->season,
            $form->description
        );

        $this->tournaments->save($tournament);

        return $tournament;
    }

    public function edit($id, TournamentCreateForm $form): void
    {
        $tournament = $this->tournaments->get($id);

        $tournament->edit(
            $form->countryId,
            $form->name,
            $form->hash,
            $form->season,
            $form->description
        );

        $this->tournaments->save($tournament);

    }

    public function markParsed($id): bool
    {
        return $this->tournaments->markParsed($id);
    }

}