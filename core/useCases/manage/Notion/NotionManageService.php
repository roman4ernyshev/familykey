<?php

namespace core\useCases\manage\Notion;

use core\entities\Meta;
use core\entities\Notion\Example;
use core\entities\Notion\ExampleAssignment;
use core\entities\Notion\Notion;
use core\forms\manage\Notion\ExampleCreateForm;
use core\forms\manage\Notion\NotionCreateForm;
use core\forms\manage\Notion\PhotosForm;
use core\repositories\Notion\CategoryRepository;
use core\repositories\Notion\ExampleRepository;
use core\repositories\Notion\NotionRepository;
use core\services\TransactionManager;

class NotionManageService
{
    private $notions;
    private $examples;
    private $categories;
    private $transaction;

    public function __construct(
        NotionRepository $notions,
        ExampleRepository $examples,
        CategoryRepository $categories,
        TransactionManager $transaction
    )
    {
        $this->notions = $notions;
        $this->examples = $examples;
        $this->categories = $categories;
        $this->transaction = $transaction;
    }

    public function create(NotionCreateForm $form, $current_category): Notion
    {

        //$category = $this->categories->get($form->categories->main);

        $category_id = $this->categories->getCategoryIdByName($current_category);

        $notion = Notion::create(
            $category_id,
            $form->body,
            $form->question,
            $form->answer,
            $form->description,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );

        /*if ($form->categories->others) {
            foreach ($form->categories->others as $otherId){
                $category = $this->categories->get($otherId);
                $notion->assignCategory($category->id);
            }
        }
        */

        foreach ($form->photos->files as $file){
            $notion->addPhoto($file);
        }

        foreach ($form->audios->files as $file){
            $notion->addAudio($file);
        }

        $this->notions->save($notion);

        return $notion;

    }

    public function edit($id, NotionCreateForm $form): void
    {
        $notion = $this->notions->get($id);

        $notion->edit(
            $form->body,
            $form->question,
            $form->answer,
            $form->description,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );

        $this->notions->save($notion);

    }

    public function remove($id): void
    {
        $notion = $this->notions->get($id);
        $this->notions->remove($notion);
    }

    public function addPhotos($id, PhotosForm $form): void
    {
        $notion = $this->notions->get($id);
        foreach ($form->files as $file){
            $notion->addPhoto($file);
        }
        $this->notions->save($notion);
    }

    public function addExample($notion_id, ExampleCreateForm $form)
    {

        $example = Example::create($form->body, $form->description);

        foreach ($form->photos->files as $file){
            $example->addPhoto($file);
        }

        foreach ($form->audios->files as $file){
            $example->addAudio($file);
        }

        $this->transaction->wrap(function () use ($example, $notion_id){
            $this->examples->save($example);
            $notion_example_assignment = ExampleAssignment::create($notion_id, $example->id);
            $notion_example_assignment->save();
        });

        return $example;

    }

    public function isCategoryExists($category): bool
    {
        return Notion::isCategoryExists($category);
    }


}