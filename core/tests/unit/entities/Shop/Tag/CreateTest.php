<?php

namespace core\tests\unit\entities\Shop\Tag;

use Codeception\Test\Unit;
use core\entities\Shop\Tag;

class CreateTest extends Unit
{
    public function testSuccess()
    {
        $tag = Tag::create(
            $name = 'Name',
            $slug = 'slug'
        );

       // $tag->setMeta(new Meta($title, $description, $keywords);

        $this->assertEquals($name, $tag->name);
        $this->assertEquals($slug, $tag->slug);

    }

}