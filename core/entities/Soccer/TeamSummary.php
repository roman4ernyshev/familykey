<?php

namespace core\entities\Soccer;

/**
 * Class TeamSummary
 * @package core\entities\tablo
 *
 * @property string $team
 * @property integer $games
 * @property integer $victories
 * @property integer $ties
 * @property integer $defeats
 * @property integer $gets
 * @property integer $lets
 * @property integer $balls
 *
 * @property string $row_team
 */
class TeamSummary
{
    public $games=0;
    public $victories=0;
    public $ties=0;
    public $defeats=0;
    public $gets=0;
    public $lets=0;
    public $balls=0;

    public $team;
    public $row_team;
    private $matches;

    public function __construct($matches, $team)
    {
        $this->matches = $matches;
        $this->team = mb_strcut($team,2);
        $this->row_team = $team;
    }

    public function sumTeam(): self
    {
        /**
         * @var Match $match
         */
        foreach ($this->matches as $match){
            //$this->team = preg_replace('/\s*\([^)]*\)/', '', mb_strcut($team,2));
            $host = mb_strcut($match->host,2);
            $guest = mb_substr($match->guest, 0, -1);
            if($host == $this->team ||
                preg_replace('/\s*\([^)]*\)/', '', $host) === $this->team){
                $this->games++;
                $this->gets += $match->gett;
                $this->lets += $match->lett;

                if($match->gett > $match->lett){
                    $this->victories++;
                    $this->balls += 3;
                }
                elseif($match->gett == $match->lett){
                    $this->ties++;
                    $this->balls++;
                }
                else $this->defeats++;
            }
            elseif($guest == $this->team || preg_replace('/\s*\([^)]*\)/', '', $guest) === $this->team){
                $this->games++;
                $this->lets += $match->gett;
                $this->gets += $match->lett;

                if($match->gett < $match->lett){
                    $this->victories++;
                    $this->balls += 3;
                }
                elseif($match->gett == $match->lett){
                    $this->ties++;
                    $this->balls++;
                }
                else $this->defeats++;
            }
        }

        return $this;
    }


}