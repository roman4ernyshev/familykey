<?php
namespace core\entities\Soccer;

use core\entities\Country;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * Class Match
 * @property integer $id
 * @property string $date
 * @property string $time
 * @property string $tournament
 * @property string $host
 * @property string $guest
 * @property integer $gett
 * @property integer $lett
 * @property string $stay_h
 * @property string $stay_g
 * @property string $yel_kart_h
 * @property string $red_kart_h
 * @property string $yel_kart_g
 * @property string $red_kart_g
 * @property string $substit_h
 * @property string $substit_g
 * @property string $goul_h
 * @property string $goul_g
 * @property string $pen_miss_h
 * @property string $pen_miss_g
 * @property integer $onehalf_h
 * @property integer $onehalf_g
 * @property string $prim
 * @property string $info
 * @property string $stra_h
 * @property string $stra_g
 * @property integer $ud_h
 * @property integer $ud_g
 * @property integer $ud_mim_h
 * @property integer $ud_mim_g
 * @property integer $offside_h
 * @property integer $offside_g
 * @property integer $falls_h
 * @property integer $falls_g
 * @property integer $ud_v_stv_h
 * @property integer $ud_v_stv_g
 * @property integer $corner_h
 * @property integer $corner_g
 * @property integer $saves_h
 * @property integer $saves_g
 * @property integer $yelkar_h
 * @property integer $yelkar_g
 * @property integer $ballpos_h
 * @property integer $ballpos_g
 * @property integer $shtraf_h
 * @property integer $shtraf_g
 * @property integer $outs_h
 * @property integer $outs_g
 * @property float $bet_h
 * @property float $bet_n
 * @property float $bet_g
 * @property integer $tournament_id
 *
 * @package core\entities\Soccer
 */
class Match extends ActiveRecord
{
    public $tournament_id;
    public static function create($match, $date, $time, $bets, $tournament_id): ?self
    {
        if(is_array($match)){
            $self = new static();
            ($date) ? $self->date = $date : $self->date = '';
            ($time) ? $self->time = $time : $self->time = '';
            ($tournament_id) ? ($self->tournament_id = $tournament_id) : $self->tournament_id = null;
            (isset($match['tournament'])) ? $self->tournament = $match['tournament'] : $self->tournament = '';
            (isset($match['host'])) ? $self->host = $match['host'] : $self->host = '';
            (isset($match['guest'])) ? $self->guest = $match['guest'] : $self->guest = '';
            (isset($match['gett'])) ? $self->gett = $match['gett'] : $self->gett = '';
            (isset($match['lett'])) ? $self->lett = $match['lett'] : $self->lett = '';
            (isset($match['stay_h'])) ? $self->stay_h = $match['stay_h'] : $self->stay_h = '';
            (isset($match['stay_g'])) ? $self->stay_g = $match['stay_g'] : $self->stay_g = '';
            (isset($match['yel_kart_h'])) ? $self->yel_kart_h = $match['yel_kart_h'] : $self->yel_kart_h = '';
            (isset($match['yel_kart_g'])) ? $self->yel_kart_g = $match['yel_kart_g'] : $self->yel_kart_g = '';
            (isset($match['red_kart_h'])) ? $self->red_kart_h = $match['red_kart_h'] : $self->red_kart_h = '';
            (isset($match['red_kart_g'])) ? $self->red_kart_g = $match['red_kart_g'] : $self->red_kart_g = '';
            (isset($match['substit_h'])) ? $self->substit_h = $match['substit_h'] : $self->substit_h = '';
            (isset($match['substit_g'])) ? $self->substit_g = $match['substit_g'] : $self->substit_g = '';
            (isset($match['goul_h'])) ? $self->goul_h = $match['goul_h'] : $self->goul_h = '';
            (isset($match['goul_g'])) ? $self->goul_g = $match['goul_g'] : $self->goul_g = '';
            (isset($match['pen_miss_h'])) ? $self->pen_miss_h = $match['pen_miss_h'] : $self->pen_miss_h = '';
            (isset($match['pen_miss_g'])) ? $self->pen_miss_g = $match['pen_miss_g'] : $self->pen_miss_g = '';
            (isset($match['onehalf_h'])) ? $self->onehalf_h = $match['onehalf_h'] : $self->onehalf_h = '';
            (isset($match['onehalf_g'])) ? $self->onehalf_g = $match['onehalf_g'] : $self->onehalf_g = '';
            (isset($match['prim'])) ? $self->prim = $match['prim'] : $self->prim = '';
            (isset($match['info'])) ? $self->info = $match['info'] : $self->info = '';
            (isset($match['stra_h'])) ? $self->stra_h = $match['stra_h'] : $self->stra_h = '';
            (isset($match['stra_g'])) ? $self->stra_g = $match['stra_g'] : $self->stra_g = '';
            (isset($match['ud_h'])) ? $self->ud_h = $match['ud_h'] : $self->ud_h = '';
            (isset($match['ud_g'])) ? $self->ud_g = $match['ud_g'] : $self->ud_g = '';
            (isset($match['ud_mim_h'])) ? $self->ud_mim_h = $match['ud_mim_h'] : $self->ud_mim_h = '';
            (isset($match['ud_mim_g'])) ? $self->ud_mim_g = $match['ud_mim_g'] : $self->ud_mim_g = '';
            (isset($match['offside_h'])) ? $self->offside_h = $match['offside_h'] : $self->offside_h = '';
            (isset($match['offside_g'])) ? $self->offside_g = $match['offside_g'] : $self->offside_g = '';
            (isset($match['falls_h'])) ? $self->falls_h = $match['falls_h'] : $self->falls_h = '';
            (isset($match['falls_g'])) ? $self->falls_g = $match['falls_g'] : $self->falls_g = '';
            (isset($match['ud_v_stv_h'])) ? $self->ud_v_stv_h = $match['ud_v_stv_h'] : $self->ud_v_stv_h = '';
            (isset($match['ud_v_stv_g'])) ? $self->ud_v_stv_g = $match['ud_v_stv_g'] : $self->ud_v_stv_g = '';
            (isset($match['corner_h'])) ? $self->corner_h = $match['corner_h'] : $self->corner_h = '';
            (isset($match['corner_g'])) ? $self->corner_g = $match['corner_g'] : $self->corner_g = '';
            (isset($match['saves_h'])) ? $self->saves_h = $match['saves_h'] : $self->saves_h = '';
            (isset($match['saves_g'])) ? $self->saves_g = $match['saves_g'] : $self->saves_g = '';
            (isset($match['yelkar_h'])) ? $self->yelkar_h = $match['yelkar_h'] : $self->yelkar_h = '';
            (isset($match['yelkar_g'])) ? $self->yelkar_g = $match['yelkar_g'] : $self->yelkar_g = '';
            (isset($match['ballpos_h'])) ? $self->ballpos_h = $match['ballpos_h'] : $self->ballpos_h = '';
            (isset($match['ballpos_g'])) ? $self->ballpos_g = $match['ballpos_g'] : $self->ballpos_g = '';
            (isset($match['shtraf_h'])) ? $self->shtraf_h = $match['shtraf_h'] : $self->shtraf_h = '';
            (isset($match['shtraf_g'])) ? $self->shtraf_g = $match['shtraf_g'] : $self->shtraf_g = '';
            (isset($match['outs_h'])) ? $self->outs_h = $match['outs_h'] : $self->outs_h = '';
            (isset($match['outs_g'])) ? $self->outs_g = $match['outs_g'] : $self->outs_g = '';
            (isset($bets[0])) ? $self->bet_h = $bets[0] : $self->bet_h = '';
            (isset($bets[1])) ? $self->bet_n = $bets[1]  : $self->bet_n = '';
            (isset($bets[2])) ? $self->bet_g = $bets[2]  : $self->bet_g = '';

           // var_dump($match['goul_h']); exit;


            return $self;
        }

        return null;

    }

    ##############################################
    public static function getDb()
    {
        return \Yii::$app->db_rockncontroll;
    }
    public static function tableName()
    {
        return '{{%foo_matches}}';
    }

    public function rules(): array
    {
        return [
            [['date', 'host', 'tournament', 'guest', 'time', 'gett', 'lett'], 'required'],
            [['date', 'host', 'tournament', 'guest', 'time', 'gett', 'lett'],
                'unique',
                'targetAttribute' => ['date', 'host', 'tournament', 'guest', 'time', 'gett', 'lett']]
        ];
    }

    public function goalH_str()
    {
        if ($this->goul_h != '') {

            $exp_goalh = explode(",", $this->goul_h, -1);

            foreach ($exp_goalh as $value) {
                /* if($this->stra_h) {
                     $country = $this->getCountryCode($this->stra_h, $value);
                   //  var_dump($country);
                 }
                 else $country = "";
                */
                echo $this->insertSpaceBeforeBracket($value). " <br />";
                //echo  $value. " <br />";
            }
        }
        else return false;


    }

    public function goalG_str()
    {
        if ($this->goul_g != '') {

            $exp_goalh = explode(",", $this->goul_g, -1);

            foreach ($exp_goalh as $value) {
                /*
                if($this->stra_g) {
                    $country = $this->getCountryCode($this->stra_g, $value);
                    //  var_dump($country);
                }
                else $country = "";
                */
                echo  $this->insertSpaceBeforeBracket($value). " <br />";
                //echo  $value. " <br />";
            }
        }
        else return false;


    }

    public function redCardH_str()
    {
        if ($this->red_kart_h != '') {


            $exp_rcard = explode(",", $this->red_kart_h, -1);

            foreach ($exp_rcard as $value) {
                echo "<img src='".Url::to('/image/redcard.png')."'/> " . $value . "<br />";
            }
        }

        else return false;


    }

    public function redCardG_str()
    {
        if ($this->red_kart_g != '') {

            $exp_rcard = explode(",", $this->red_kart_g, -1);

            foreach ($exp_rcard as $value) {
                echo "<img src='".Url::to('/image/redcard.png')."'/> " . $value . "<br />";
            }
        }

        else return false;


    }

    public function yellCardCount_h() {
        if ($this->yel_kart_h != '') {

            $exp_ycard = explode(",", $this->yel_kart_h, -1);

            foreach ($exp_ycard as $value) {
                echo " <img src='".Url::to('/image/yellcard.png')."'/> ";
            }
        }

        else return false;

    }

    public function yellCardCount_g() {

        if ($this->yel_kart_g != '') {

            $exp_ycard = explode(",", $this->yel_kart_g, -1);

            foreach ($exp_ycard as $value) {
                echo " <img src='".Url::to('/image/yellcard.png')."'/> ";
            }
        }

        else return false;

    }

    /**
     * Замены хозяев
     * @return bool
     */
    public function substitH_str()
    {
        if ($this->substit_h != '') {


            $exp_substit = explode(",", $this->substit_h, -1);

            foreach ( $exp_substit as $value) {
                $value = str_replace('.', '. ', $value);
                echo  $value . "<br>";
            }
        }

        else return false;


    }

    /**
     * Замены гостей
     * @return bool
     */
    public function substitG_str()
    {
        if ($this->substit_g != '') {

            $exp_substit = explode(",", $this->substit_g, -1);

            foreach ($exp_substit as $value) {
                $value = str_replace('.', '. ', $value);
                echo  $value . "<br>";
            }
        }

        else return false;


    }

    /**
     * Нереализованный пенальти хозяев
     * @return bool
     */
    public function penMissH_str()
    {
        if ($this->pen_miss_h != '') {


            $exp_penmiss = explode(",", $this->pen_miss_h, -1);

            foreach ( $exp_penmiss as $value) {
                echo $value . "<br>";
            }
        }

        else return false;


    }

    /**
     * Нереализованные пенальти гостей
     * @return bool
     */
    public function penMissG_str()
    {
        if ($this->pen_miss_g != '') {

            $exp_penmiss = explode(",", $this->pen_miss_g, -1);

            foreach ($exp_penmiss as $value) {
                echo $value . "<br />";
            }
        }

        else return false;


    }

    /**
     * Фильтруем и выводим состав хозяев
     */
    public function getSost_h(){
        if($this->stra_h != ''){
            $sost = '';
            $i = 0;
            $sost_arr = explode(',', $this->stra_h);

            while($i < 11){
                if(isset($sost_arr[$i])) $sost .= $sost_arr[$i];
                $i++;
            }

            echo self::clearString($sost);

        }
        else echo '?';

    }

    /**
     * Фильтруем и выводим состав гостей
     */
    public function getSost_g(){
        if($this->stra_g != ''){
            $sost = '';
            $i = 0;
            $sost_arr = explode(',', $this->stra_g);

            while($i < 11){
                if(isset($sost_arr[$i])) $sost .= $sost_arr[$i];
                $i++;
            }

            echo self::clearString($sost);

        }
        else echo '?';

    }

    /**
     * Информация о матче
     */
    public function getInfo(){
        if($this->info != ''){
            $res = $this->info;
            $res = str_replace('Стадион:', '<br>Стадион:', $res);
            $res = str_replace('Судья:', '<br>Судья:', $res);
            $res = str_replace('Посещаемость:', '<br>Посещаемость:', $res);
            echo $res;
        }
        else echo '';
    }

    /**
     * Оставляем буквы и кое-какие знаки в строке
     * @param $string
     * @return mixed
     */
    public static function clearString($string){

        $string = preg_replace("/-/", "- ", $string);

        return  preg_replace("/[^-а-яёА-ЯЁa-zA-Z.() ]+/iu", "", $string);
    }

    public static function insertSpaceBeforeBracket($string)
    {
        $new_string = preg_replace("/[)]+/", ") ", $string);

        return  $new_string ? $new_string : $string;
    }


    /**
     * Все события матча у хоэяев
     */
    public function allMatchEventsH(){
        $this->events_h = explode(',',$this->goalH_str().$this->penMissH_str().$this->redCardH_str().$this->substitH_str());
        $this->events_h = sort($this->events_h);
        var_dump($this->events_h);
        /*foreach ($this->events_h as $value) {
            echo $value . "<br />";
        }
        */

    }

    /**
     * Все события матча у гостей
     */
    public function allMatchEventsG(){

        $this->events_g = explode(',',$this->goalG_str().$this->penMissG_str().$this->redCardG_str().$this->substitG_str());
        $this->events_g = sort($this->events_g, SORT_REGULAR);
        echo $this->events_g;
        /*
        foreach ($this->events_g as $value) {
            echo $value . "<br />";
        }
        */

    }

    /**
     * Тренер хозяев
     * "TODO +couch[2]
     */
    public function getCoachH(){

        if ($this->stra_h != '') {
            $players = explode(',', $this->stra_h);
            $couch = explode('-', $players[count($players) - 2]);

            if(isset($couch[1])){
                if (preg_match('/\d/', $couch[1]) ||
                    preg_match('/\(([^\)]*)\)/', $couch[1]) ||
                    $this->isTournamentTooOld()) {
                    return false;
                }
                else  return trim($couch[1]);
            }
            else return false;
        }
        else return false;
    }

    /**
     * Тренер гостей
     */
    public function getCoachG(){

        if ($this->stra_g != '') {
            $players = explode(',', $this->stra_g);
            $couch = explode('-', $players[count($players) - 2]);

            if(isset($couch[1])){
                if (preg_match('/\d/', $couch[1]) ||
                    preg_match('/\(([^\)]*)\)/', $couch[1]) ||
                    $this->isTournamentTooOld()) {
                    return false;
                }
                else  return trim($couch[1]);
            }
            else return false;
        }
        else return false;
    }

    /**
     * Вратарь хозяев
     */
    public function getKeeperH(){
        if ($this->stra_h != '') {

            $players = explode(',', $this->stra_h);
            foreach ($players as $player) {
                if(preg_match('/\(В\)/',$player)) {
                    $keeper = preg_replace ("/[0-9\-]/","",$player);
                    $keeper = preg_replace ("/\(В\)/","", $keeper);
                    return $keeper;
                }

            }
        }

    }

    /**
     * Вратарь гостей
     */
    public function getKeeperG(){
        if ($this->stra_g != '') {

            $players = explode(',', $this->stra_g);
            foreach ($players as $player) {
                if(preg_match('/\(В\)/',$player)) {
                    $keeper = preg_replace ("/[0-9\-]/","",$player);
                    $keeper = preg_replace ("/\(В\)/","", $keeper);
                    return $keeper;
                }

            }
        }

    }

    /**
     * Возвращает iso код страны игрока
     * @param string $players
     * @return mixed
     */
    public function getCountryCode($players, $player){
        $players = explode(',', $players);
        //$player = preg_replace ("/[0-9\']/","",$player);
        /*$player = explode(" ", $player);
        //trim($player);
        $new_player = "";
        foreach($player as $part) {
            if(!preg_match("/[\(\)]/",$part)) $new_player .= $part;
        }
        //trim($new_player);
        var_dump($new_player);*/
        foreach($players as $one) {

            if (strstr($one,substr($player,3)) || strstr($one,substr($player,4))) {
                $country_code = explode('-', $one);

                $country = Country::find()
                    ->where(['soccer_code' => (int)$country_code[0]])
                    ->one();

                return $country->iso_code;
            }

        }
        return null;
    }

    public static function cutAfterBracket($str){

        if(strstr($str,'Спортинг (Исп)'))  $str = str_replace('Спортинг (Исп)', 'Спортинг Хихон', $str);
        if(strstr($str,'Спортинг (Пор)'))  $str = str_replace('Спортинг (Пор)', 'Спортинг Лиссабон', $str);

        $pos = strpos($str, '(');

        if($pos)
            return substr($str, 0, $pos);
        else
            return $str;


    }

    private function isTournamentTooOld(): bool
    {
        $old_years = [2009,2008,2007,2006,2005,2006,2005,2004,2003,2002,2001,2000,1999];
        foreach ($old_years as $year){
            $tournament = Tournament::findOne($this->tournament_id);
            if($tournament){
                if($t = strpos(Tournament::findOne($this->tournament_id)->name, (string)$year)){
                    return true;
                }
                return false;
            }
        }
        return false;
    }


}