<?php
namespace core\entities\Soccer;

use core\entities\Country;
use core\entities\EventTrait;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Tournament
 * @property integer $id
 * @property string $name
 * @property string $hash
 * @property integer $country_id
 * @property string $season
 * @property string $description
 * @property integer $done
 * @property array $hash_image
 *
 * @property Country $country
 *
 * @package core\entities\Soccer
 */
class Tournament extends ActiveRecord
{
    use EventTrait;

    public $hash_image = [
        'W6BOzpK2' => 'bundesliga.jpg',
        'nXxWpLmT' => 'sweden_high.jpg'
    ];

    public static function create($countryId, $name, $hash, $season, $description): self
    {
        $tournament = new static();
        $tournament->country_id = $countryId;
        $tournament->name = $name;
        $tournament->hash = $hash;
        $tournament->season = $season;
        $tournament->description = $description;
        return $tournament;
    }

    public function edit($countryId, $name, $hash, $season, $description): void
    {
        $this->country_id = $countryId;
        $this->name = $name;
        $this->description = $description;
        $this->hash = $hash;
        $this->season = $season;
    }

    public function getCountry(): ActiveQuery
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    public function markParsed()
    {
        $this->done = 1;
        if($this->save()) return 'Parsed';
        return 'Error';
    }

    ##############################################

    public static function tableName()
    {
        return '{{%foo_tournament}}';
    }

    public static function getDb()
    {
        return \Yii::$app->db_rockncontroll;
    }

    public function rules(): array
    {
        return [
            [['name', 'hash', 'country_id', 'season'], 'required'],
            [['name', 'hash'],
                'unique',
                'targetAttribute' => ['name', 'hash']]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['country']
            ],
        ];
    }

}