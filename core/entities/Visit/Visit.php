<?php

namespace core\entities\Visit;

use Yii;

/**
 * This is the model class for table "visit".
 *
 * @property int $id
 * @property string $time
 * @property string $ip
 * @property string $refer
 * @property string $browser
 */
class Visit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visit';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time'], 'safe'],
            [['refer', 'browser'], 'required'],
            [['ip', 'refer', 'browser'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'ip' => 'Ip',
            'refer' => 'Refer',
            'browser' => 'Browser',
        ];
    }
}
