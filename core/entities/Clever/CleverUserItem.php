<?php

namespace core\entities\Clever;


use core\entities\User\User;
use yii\db\ActiveRecord;

class CleverUserItem extends ActiveRecord
{
    public $sum;
    public $cnt;

    public static function tableName()
    {
        return '{{%radio_item_user_assignments}}';
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}