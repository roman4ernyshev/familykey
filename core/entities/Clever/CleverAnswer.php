<?php
namespace core\entities\Clever;

use core\entities\Rockncontroll\RadioItem;
use yii\db\ActiveRecord;
use Yii;

/**
* This is the model class for table "clever_answers".
*
* @property int $id
* @property int $item_id
* @property string $ans
* @property int $right
*
* @property RadioItem $item
*/
class CleverAnswer extends ActiveRecord
{

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public static function tableName()
    {
        return '{{%clever_answers}}';
    }

    public function getItem()
    {
        return $this->hasOne(RadioItem::class, ['id' => 'item_id']);
    }

}