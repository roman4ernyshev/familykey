<?php

namespace core\entities\Clever;


use yii\db\ActiveRecord;

/**
 * Class CleverCat
 * @package core\entities\Clever
 *
 * @property string $title
 * @property integer $id
 */
class CleverCat extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%clever_cats}}';
    }

}