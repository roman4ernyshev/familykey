<?php

namespace core\entities\Clever;

use yii\db\ActiveRecord;

/**
 * Class CleverItemShown
 * @package core\entities\Clever
 *
 * @property int $item_id
 * @property int $shown
 * @property int $cat_id
 */
class CleverItemShown extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%radio_item_id_shown}}';
    }

    public function getCat()
    {
        return $this->hasOne(CleverCat::class, ['id' => 'cat_id']);
    }


}