<?php

namespace core\entities\Books;

/**
 * Class Book
 *
 * @property integer $release_year
 * @property string $title
 * @property integer $author_id
 * @property integer $edition
 *
 * @package core\entities\Books
 *
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @param array $params
     * @return Book
     *
     */
    public static function create(array $params): self
    {
        $book = new static();

        $book->title = $params['title'];
        $book->release_year = $params['release_year'];
        $book->edition = $params['edition'];
        $book->author_id = $params['author_id'];

        return $book;
    }

    public function editAuthor($authorId): void
    {
        $this->author_id = $authorId;
    }

    public static function tableName()
    {
        return '{{%books}}';
    }

    public function getAuthor()
    {
        return $this->hasOne(Author::class, ['id' => 'author_id']);
    }

}