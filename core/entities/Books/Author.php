<?php
namespace core\entities\Books;

/**
 * Class Author
 *
 * @property string $name
 * @property string $last_name
 * @property integer $birth_year
 * @property string $country
 * @property integer $id
 *
 * @package core\entities\Books
 */
class Author extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%book_authors}}';
    }


    public function getBooks()
    {
        return $this->hasMany(Book::class, ['author_id' => 'id']);
    }

}