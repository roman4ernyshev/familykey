<?php
namespace core\entities\Word;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "words".
 *
 * @property int $id
 * @property string $title
 * @property int $lang
 *
 */
class Word extends ActiveRecord
{

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public static function tableName(): string
    {
        return '{{%word}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['product_id', 'act_id', 'shop_id', 'user_id'], 'required'],
            //[['product_id', 'act_id', 'shop_id', 'user_id'], 'integer'],
            ['title', 'unique']
            // [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarkUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            // [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            // [['act_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiaryActs::className(), 'targetAttribute' => ['act_id' => 'id']],
            // [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'act_id' => 'Act ID',
            'shop_id' => 'Shop ID',
            'user_id' => 'User ID',
            'spent' => 'Spent',
            'item_price' => 'Item Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefinitions()
    {
        return $this->hasMany(Definition::className(), ['word_id' => 'id']);
    }


}
