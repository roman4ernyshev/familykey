<?php

namespace core\entities\Word;

use Yii;
use yii\data\ActiveDataProvider;

class ExampleSearch extends Definition
{

    public function search($params, $id): ActiveDataProvider
    {
        $query = Example::find()
            ->where(['definition_id' => $id])
            ->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $this->load($params);

        return $dataProvider;
    }

}