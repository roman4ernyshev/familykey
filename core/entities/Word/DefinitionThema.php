<?php

namespace core\entities\Word;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "definition_crossword_thema".
 *
 * @property int $id
 * @property int $crossword_thema_id
 * @property int $definition_id
 * @property string $title
 */

class DefinitionThema extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'definition_crossword_thema';
    }

    public function rules()
    {
        return [
            [['crossword_thema_id', 'definition_id', 'title'], 'required'],
            [['crossword_thema_id', 'definition_id'], 'integer'],
            [['title'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['crossword_thema_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Thema::class, 'targetAttribute' => ['crossword_thema_id' => 'id']],
            [['definition_id'],
                'exist',
                'skipOnError' => true, 'targetClass' => Definition::class,
                'targetAttribute' => ['definition_id' => 'id']],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDefinition()
    {
        return $this->hasOne(Definition::class, ['id' => 'definition_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getThema()
    {
        return $this->hasOne(Thema::class, ['id' => 'crossword_thema_id']);
    }

}