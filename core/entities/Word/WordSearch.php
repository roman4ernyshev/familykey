<?php
namespace core\entities\Word;

use Yii;
use yii\data\ActiveDataProvider;

class WordSearch extends Word
{
    public function rules()
    {
        return [
            [['id', 'title', 'lang'], 'save'],
          ];
    }

    public function search($params, $additional=''){
        $query = Word::find()->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['=', 'id', $this->id]);

        return $dataProvider;
    }

    
}