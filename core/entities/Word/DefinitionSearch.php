<?php

namespace core\entities\Word;

use Yii;
use yii\data\ActiveDataProvider;

class DefinitionSearch extends Definition
{

    public function search($params, $id): ActiveDataProvider
    {
        $query = Definition::find()
            ->where(['word_id' => $id])
            ->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $this->load($params);

        return $dataProvider;
    }

}