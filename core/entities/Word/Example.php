<?php

namespace core\entities\Word;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "definitions".
 *
 * @property int $id
 * @property int $definition_id
 * @property string $text
 * @property string $image
 * @property string $audio
 */

class Example extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public static function tableName(): string
    {
        return '{{%examples}}';
    }

    /**
     * @return ActiveQuery
     */
    public function getDefinition()
    {
        return $this->hasOne(Word::className(), ['id' => 'definition_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'act_id', 'shop_id', 'user_id'], 'required'],
            [['product_id', 'act_id', 'shop_id', 'user_id'], 'integer'],
            [['spent', 'item_price'], 'number'],
            // [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarkUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            // [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            // [['act_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiaryActs::className(), 'targetAttribute' => ['act_id' => 'id']],
            // [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'act_id' => 'Act ID',
            'shop_id' => 'Shop ID',
            'user_id' => 'User ID',
            'spent' => 'Spent',
            'item_price' => 'Item Price',
        ];
    }


}