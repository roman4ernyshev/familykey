<?php

namespace core\entities\Word;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "crossword_thema".
 *
 * @property int $id
 * @property string $title
 */

class Thema extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public static function tableName(): string
    {
        return '{{%crossword_thema}}';
    }

    public function rules()
    {
        return [
            [['title'], 'string'],
        ];
    }

    public function getThemeItems()
    {
        return $this->hasMany(DefinitionThema::className(), ['crossword_thema_id' => 'id']);
    }

    public function getDefinitions(){
        return $this->hasMany(Definition::class, ['id' => 'definition_id'])->
        viaTable('{{%definition_crossword_thema}}', ['crossword_thema_id' => 'id']);
    }


}