<?php

namespace core\entities\Word;

use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "definitions".
 *
 * @property int $id
 * @property int $word_id
 * @property string $value
 * @property string $synonyms
 * @property string $antonyms
 * @property string $tags
 * @property string $etymology
 * @property string $forms
 * @TODO in db add
 * @property string $type
 * @property string $adult_status
 */

class Definition extends ActiveRecord
{
    //public $type;
    public $examples;

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public static function tableName(): string
    {
        return '{{%definitions}}';
    }

    /**
     * @return ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Word::className(), ['id' => 'word_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['word_id'], 'required'],
            [['id, word_id'], 'integer'],
            [['id, word_id'], 'safe'],
           // [['type', 'item_price'], 'number'],
           // [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarkUser::className(), 'targetAttribute' => ['user_id' => 'id']],
           // [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
           // [['act_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiaryActs::className(), 'targetAttribute' => ['act_id' => 'id']],
           // [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    public function getExamples()
    {
        return $this->hasMany(Example::className(), ['definition_id' => 'id']);
    }

    /**
     * @throws InvalidConfigException
     */
    public function getThemes(){
        return $this->hasMany(Thema::class, ['id' => 'crossword_thema_id'])->
               viaTable('{{%definition_crossword_thema}}', ['definition_id' => 'id']);
    }

}