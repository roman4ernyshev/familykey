<?php

namespace core\entities\Word;

use Yii;
use yii\data\ActiveDataProvider;
class ThemaSearch extends Thema
{
    public function rules()
    {
        return [
            [['title'], 'save'],
        ];
    }

    public function search($params, $additional=''){
        $query = Thema::find()->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

}