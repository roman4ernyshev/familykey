<?php

namespace core\entities\Product;

use yii\db\ActiveRecord;

/**
 * Class CategoryAssignment
 * @property integer $notion_id
 * @property integer $category_id
 *
 * @package core\entities\Notion\Notion
 */
class CategoryAssignment extends ActiveRecord
{
    public static function create($categoryId): self
    {
        $assignment = new static();
        $assignment->category_id = $categoryId;
        return $assignment;
    }

    public function isForCategory($id): bool
    {
        return $this->category_id == $id;
    }

    public static function tableName(): string
    {
        return '{{%product_category_assignments}}';
    }


}