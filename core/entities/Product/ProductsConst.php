<?php

namespace core\entities\Product;

use core\entities\behaviors\MetaBehavior;
use core\entities\EventTrait;
use Yii;
use core\entities\Meta;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * This is the model class for table "products_const".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property integer $category_id
 * @property int $carbohydrates on 100 g
 * @property int $fats on 100 g
 * @property int $squirrels on 100 g
 * @property int $kkal on 100 g
 * @property int $ferrum microgram on 100 g
 * @property int $magnesium microgram on 100 g
 * @property int $cuprum microgram on 100 g
 * @property int $iodum microgram on 100 g
 * @property int $fluorum microgram on 100 g
 * @property int $zincum microgram on 100 g
 * @property int $cobaltum microgram on 100 g
 *
 * @property string $slug
 * @property string $meta_json
 * @property string $image
 * @property string $link
 *
 * @property Category $category
 * @property CategoryAssignment[] $categoryAssignments
 *
 *  @property Photo[] $photos
 */
class ProductsConst extends \yii\db\ActiveRecord
{
    use EventTrait;

    public $meta;

    public static function create(
        $categoryId,
        $name,
        $description,
        $carbohydrates,
        $fats,
        $squirrels,
        $kkal,
        $ferrum,
        $magnesium,
        $cuprum,
        $iodum,
        $fluorum,
        $zincum,
        $cobaltum,
        Meta $meta

    ): self
    {

        $product = new static();
        $product->category_id = $categoryId;
        $product->name = $name;
        $product->description = $description;
        $product->carbohydrates = $carbohydrates;
        $product->fats = $fats;
        $product->squirrels = $squirrels;
        $product->kkal = $kkal;
        $product->ferrum = $ferrum;
        $product->magnesium = $magnesium;
        $product->cuprum = $cuprum;
        $product->iodum = $iodum;
        $product->fluorum = $fluorum;
        $product->zincum = $zincum;
        $product->cobaltum = $cobaltum;
        $product->slug = Inflector::slug($name);
        $product->meta = $meta;
        return $product;
    }

    public function edit(
        $name,
        $description,
        $carbohydrates,
        $fats,
        $squirrels,
        $kkal,
        $ferrum,
        $magnesium,
        $cuprum,
        $iodum,
        $fluorum,
        $zincum,
        $cobaltum,
         Meta $meta): void
    {
        $this->name = $name;
        $this->description = $description;
        $this->carbohydrates = $carbohydrates;
        $this->fats = $fats;
        $this->squirrels = $squirrels;
        $this->kkal = $kkal;
        $this->ferrum = $ferrum;
        $this->magnesium = $magnesium;
        $this->cuprum = $cuprum;
        $this->iodum = $iodum;
        $this->fluorum = $fluorum;
        $this->zincum = $zincum;
        $this->cobaltum = $cobaltum;
        $this->meta = $meta;
    }

    ####################Categories#######################

    public function assignCategory($id): void
    {
        $assignments = $this->categoryAssignments;
        foreach ($assignments as $assignment){
            if ($assignment->isForCategory($id)){
                return;
            }
        }
        $assignments[] = CategoryAssignment::create($id);
        $this->categoryAssignments = $assignments;
    }

    public function changeMainCategory($categoryId): void
    {
        $this->category_id = $categoryId;
    }


    public function revokeCategory($id): void
    {
        $assignments = $this->categoryAssignments;
        foreach ($assignments as $i => $assignment){
            if ($assignment->isForCategory($id)){
                unset($assignments[$i]);
                $this->categoryAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException('Assignment is not found.');

    }

    public function revokeCategories(): void
    {
        $this->categoryAssignments = [];
    }


    public static function isCategoryExists($slug): bool
    {
        return (bool)Category::find()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }



    ####################Photos#######################

    public function addPhoto(UploadedFile $file): void
    {
        $photos = $this->photos;
        $photos[] = Photo::create($file);
        $this->updatePhotos($photos);
    }

    public function removePhoto($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)){
                unset($photos[$i]);
                $this->updatePhotos($photos);
                return;
            }
        }
        throw new \DomainException('Photo is not found.');

    }

    public function removePhotos(): void
    {
        $this->updatePhotos([]);
    }

    public function movePhotoUp($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i - 1] ?? null) {
                    $photos[$i] = $prev;
                    $photos[$i - 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }

    public function movePhotoDown($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i + 1] ?? null) {
                    $photos[$i] = $prev;
                    $photos[$i + 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }


    private function updatePhotos(array $photos): void
    {

        foreach ($photos as $i => $photo){

            $photo->setSort($i);
        }

        $this->photos = $photos;
        $this->populateRelation('image', reset($photos));
    }




    #################################

    public function getPhotos(): ActiveQuery
    {
        return $this->hasMany(Photo::class, ['product_id' => 'id'])->orderBy('sort');
    }


    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function getCategoryAssignments(): ActiveQuery
    {
        return $this->hasMany(CategoryAssignment::class, ['product_id' => 'id']);
    }

    public function getCategories(): ActiveQuery
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])->via('categoryAssignments');
    }

    public function getMainPhoto(): ActiveQuery
    {
        return $this->hasOne(Photo::class, ['id' => 'image_link']);
    }


    ################################


    public function behaviors()
    {
        return [
            MetaBehavior::class,
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['photos']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%products_const}}';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'carbohydrates' => 'Carbohydrates',
            'fats' => 'Fats',
            'squirrels' => 'Squirrels',
            'kkal' => 'Kkal',
            'ferrum' => 'Ferrum',
            'magnesium' => 'Magnesium',
            'cuprum' => 'Cuprum',
            'iodum' => 'Iodum',
            'fluorum' => 'Fluorum',
            'zincum' => 'Zincum',
            'cobaltum' => 'Cobaltum',
        ];
    }
}
