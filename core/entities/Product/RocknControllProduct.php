<?php

namespace core\entities\Product;

use Yii;

/**
 * Class RocknControllProduct
 * @package core\entities\Product
 *
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property integer $category_id
 * @property int $carbohydrates on 100 g
 * @property int $fats on 100 g
 * @property int $squirrels on 100 g
 * @property int $kkal on 100 g
 * @property int $ferrum microgram on 100 g
 * @property int $magnesium microgram on 100 g
 * @property int $cuprum microgram on 100 g
 * @property int $iodum microgram on 100 g
 * @property int $fluorum microgram on 100 g
 * @property int $zincum microgram on 100 g
 * @property int $cobaltum microgram on 100 g
 */
class RocknControllProduct extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%products}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'описание',
            'carbohydrates' => 'угл',
            'fats' => 'жир',
            'squirrels' => 'бел',
            'kkal' => 'Kkal',
            'ferrum' => 'Fe',
            'magnesium' => 'Mg',
            'cuprum' => 'Cu',
            'iodum' => 'Im',
            'fluorum' => 'F',
            'zincum' => 'Zn',
            'cobaltum' => 'Cb',
        ];
    }

}