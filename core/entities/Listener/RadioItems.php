<?php

namespace core\entities\Listener;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property int $id
 * @property int $cat_id
 * @property string $text
 * @property string $tags
 * @property string $audio
 * @property string $img
 * @property int $cens
 * @property int $published
 * @property string $title
 * @property string $alias
 * @property int $source_id
 * @property string $anons
 * @property string $d_created
 * @property string $d_updated
 * @property int $count
 * @property int $likes
 * @property int $next_item
 * @property int $next_song
 *
 * @property RadioCategory $cat
 * @property RadioSource $source
 * @property ThemeItems[] $themeItems
 */
class RadioItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'text', 'tags', 'audio', 'img', 'cens'], 'required'],
            [['cat_id', 'cens', 'published', 'source_id', 'count', 'likes', 'next_item', 'next_song'], 'default', 'value' => null],
            [['cat_id', 'cens', 'published', 'source_id', 'count', 'likes', 'next_item', 'next_song'], 'integer'],
            [['text', 'anons'], 'string'],
            [['d_created', 'd_updated'], 'safe'],
            [['tags', 'audio', 'img', 'title', 'alias'], 'string', 'max' => 255],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => RadioCategory::className(), 'targetAttribute' => ['cat_id' => 'id']],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => RadioSource::className(), 'targetAttribute' => ['source_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Cat ID',
            'text' => 'Text',
            'tags' => 'Tags',
            'audio' => 'Audio',
            'img' => 'Img',
            'cens' => 'Cens',
            'published' => 'Published',
            'title' => 'Title',
            'alias' => 'Alias',
            'source_id' => 'Source ID',
            'anons' => 'Anons',
            'd_created' => 'D Created',
            'd_updated' => 'D Updated',
            'count' => 'Count',
            'likes' => 'Likes',
            'next_item' => 'Next Item',
            'next_song' => 'Next Song',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(RadioCategory::className(), ['id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(RadioSource::className(), ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThemeItems()
    {
        return $this->hasMany(ThemeItems::className(), ['item_id' => 'id']);
    }
}
