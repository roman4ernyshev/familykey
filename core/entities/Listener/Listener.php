<?php

namespace core\entities\Listener;

use Yii;

/**
 * This is the model class for table "listener".
 *
 * @property int $id
 * @property int $time
 * @property string $ip
 * @property string $user_agent
 * @property int $seconds
 * @property string $current_track
 */
class Listener extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'listener';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time', 'ip', 'user_agent'], 'required'],
            [['time', 'seconds'], 'integer'],
            [['ip', 'user_agent'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'ip' => 'Ip',
            'user_agent' => 'User Agent',
            'seconds' => 'Seconds',
        ];
    }
}
