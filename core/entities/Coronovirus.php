<?php

namespace core\entities;


use yii\db\ActiveRecord;

/**
 * Class Coronovirus
 *
 * @property integer $time
 * @property string $country
 * @property integer $total_cases
 * @property integer $new_cases
 * @property integer $total_deaths
 * @property integer $new_deaths
 * @property integer $total_recovered
 * @property integer $active_cases
 * @property integer $serious_critical
 * @property integer $tot_pop
 *
 * @package core\entities
 */
class Coronovirus extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%coronavirus}}';
    }

    public static function getDb()
    {
        return \Yii::$app->get('db_rockncontroll');
    }

}