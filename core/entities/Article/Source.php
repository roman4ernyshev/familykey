<?php

namespace core\entities\Article;

use yii\db\ActiveRecord;
use Yii;

/**
 * Source
 * @property int $id
 * @property int $author_id
 * @property string $title
 *
 * @property ArticleContent[] $articles
 * @property Author $author
 */
class Source extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%source}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public function getArticles()
    {
        return $this->hasMany(ArticleContent::class, ['source_id' => 'id']);
    }

    public function getAuthor()
    {
        return $this->hasOne(Author::class, ['id' => 'author_id']);
    }

}