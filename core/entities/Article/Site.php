<?php

namespace core\entities\Article;


use yii\db\ActiveRecord;
use Yii;

/**
 * Site
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string $theme
 *
 * @property Article[] $articles
 */
class Site extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%sites}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public function getArticles()
    {
        return $this->hasMany(Article::class, ['site_id' => 'id']);
    }

}