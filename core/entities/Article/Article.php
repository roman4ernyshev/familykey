<?php

namespace core\entities\Article;


use yii\db\ActiveRecord;
use Yii;

/**
 * Article
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property int $d_created
 * @property string $img
 * @property string $anons
 * @property string $text
 * @property string $audio
 * @property string $video
 * @property string $tags
 * @property string $status
 * @property int $site_id
 *
 * @property ArticleContent[] $content
 * @property Site $site
 */
class Article extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%articles}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public function getSite()
    {
        return $this->hasOne(Site::class, ['id' => 'site_id']);
    }

    public function getContent()
    {
        return $this->hasMany(ArticleContent::class, ['articles_id' => 'id']);
    }

}