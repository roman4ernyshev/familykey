<?php

namespace core\entities\Article;


use yii\db\ActiveRecord;
use Yii;

/**
 * Author
 * @property int $id
 * @property string $name
 *
 * @property Source[] $sources
 */
class Author extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%author}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public function getSources()
    {
        return $this->hasMany(Source::className(), ['author_id' => 'id']);
    }

}