<?php

namespace core\entities\Article;


use yii\db\ActiveRecord;
use Yii;

/**
 * ArticleContent
 * @property int $id
 * @property int $articles_id
 * @property int $source_id
 * @property string $body
 * @property string $minititle
 * @property int $d_shown
 * @property string $img
 * @property int $page
 * @property int $count
 * @property int $likes
 * @property string $tags
 * @property string $status
 *
 * @property Source $source
 * @property Article $article
 */
class ArticleContent extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%articles_content}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_postgres');
    }

    public function getSource()
    {
        return $this->hasOne(Source::class, ['id' => 'source_id']);
    }

    public function getArticle()
    {
        return $this->hasOne(Article::class, ['id' => 'articles_id']);
    }

}