<?php
namespace core\entities\RockncontrollNew;

use yii\db\ActiveRecord;
use Yii;

class NsbNewss extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%nsb_news}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll_new');
    }

}