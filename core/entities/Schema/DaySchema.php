<?php
namespace core\entities\Schema;


use core\entities\Schema\storage\StorageInterface;
use yii\db\ActiveRecord;

class DaySchema extends ActiveRecord
{
    public $keys = [
        'remain_for_rul',
        'remain_to_play',
        'hour_7',
        'hour_8',
        'hour_9',
        'hour_10',
        'hour_11',
        'hour_12',
        'hour_13',
        'hour_14',
        'hour_15',
        'hour_16',
        'hour_17',
        'hour_18',
        'hour_19',
        'hour_20',
        'hour_21',
        'hour_7_sel',
        'hour_8_sel',
        'hour_9_sel',
        'hour_10_sel',
        'hour_11_sel',
        'hour_12_sel',
        'hour_13_sel',
        'hour_14_sel',
        'hour_15_sel',
        'hour_16_sel',
        'hour_17_sel',
        'hour_18_sel',
        'hour_19_sel',
        'hour_20_sel',
        'hour_21_sel',
    ];

    private $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
        parent::__construct();
    }

    public function saveParams($params)
    {
        $this->storage->save($params);
    }

    public function getParams(): array
    {
        return $this->storage->load($this->keys);
    }

}