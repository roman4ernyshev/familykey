<?php

namespace core\entities\Schema\storage;

use core\entities\tablo\TabloItem;

interface StorageInterface
{
    /**
     * @return TabloItem[]
     * @param array
     */
    public function load(array $filter = []): array;

    /**
     * @param TabloItem[] $items
     */
    public function save(array $items): void;

}