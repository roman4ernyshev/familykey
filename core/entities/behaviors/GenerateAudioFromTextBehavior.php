<?php

namespace core\entities\behaviors;

use yii\base\Event;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use yii\helpers\Url;

class GenerateAudioFromTextBehavior extends Behavior
{
    public $to = 'generated_audio_link';
    public $from = 'body';
    public $audioPath = '@staticRoot/generated/';
    public $staticPath = '@static/generated/';

    public function events(): array
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'onBeforeSave',
           // ActiveRecord::EVENT_BEFORE_UPDATE => 'onBeforeSave'
        ];
    }

    /**
     * Using utilite espeak
     * http://37.192.187.83/plis/item/show/3160
     * @param Event $event
     */
    public function onBeforeSave(Event $event): void
    {
        //var_dump($event->sender->voice); exit;
        $model = $event->sender;
        $slug = md5(time().Inflector::slug($model->{$this->from}));

        $file = Url::to($this->audioPath.$slug.".wav");
        //$cmd = "espeak -v mb-de4 '".$model->{$this->from}."' -s 100 -w ".$file;
        $cmd = $event->sender->voice." '".$model->{$this->from}."' -s 120 -w ".$file;

        $model->setAttribute($this->to, $this->staticPath.$slug.".wav");

        shell_exec($cmd);

    }

}