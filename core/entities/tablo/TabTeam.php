<?php

namespace core\entities\tablo;

use Yii;

/**
 * This is the model class for table "tab_team".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @property TabMatch[] $tabMatches
 * @property TabMatch[] $tabMatches0
 */
class TabTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabMatches()
    {
        return $this->hasMany(TabMatch::className(), ['team1_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabMatches0()
    {
        return $this->hasMany(TabMatch::className(), ['team2_id' => 'id']);
    }
}
