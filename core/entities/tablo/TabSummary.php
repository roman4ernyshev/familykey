<?php

namespace core\entities\tablo;

/**
 * Class TabSummary
 * @package core\entities\tablo
 *
 * @property integer $team_id
 * @property integer $games
 * @property integer $victories
 * @property integer $ties
 * @property integer $defeats
 * @property integer $gets
 * @property integer $lets
 * @property integer $balls
 */
class TabSummary
{
    public $games=0;
    public $victories=0;
    public $ties=0;
    public $defeats=0;
    public $gets=0;
    public $lets=0;
    public $balls=0;

    public $team_id;
    private $matches;

    public function __construct($matches, $team_id)
    {
        $this->matches = $matches;
        $this->team_id = $team_id;
    }

    public function sumTeam(): self
    {
        /**
         * @var TabMatch $match
         */
        foreach ($this->matches as $match){
            if($match->team1_id == $this->team_id){
                $this->games++;
                $this->gets += $match->goals_first;
                $this->lets += $match->goals_second;

                if($match->goals_first > $match->goals_second){
                    $this->victories++;
                    $this->balls += 3;
                }
                elseif($match->goals_first == $match->goals_second){
                    $this->ties++;
                    $this->balls++;
                }
                else $this->defeats++;
            }
            elseif($match->team2_id == $this->team_id){
                $this->games++;
                $this->lets += $match->goals_first;
                $this->gets += $match->goals_second;

                if($match->goals_first < $match->goals_second){
                    $this->victories++;
                    $this->balls += 3;
                }
                elseif($match->goals_first == $match->goals_second){
                    $this->ties++;
                    $this->balls++;
                }
                else $this->defeats++;
            }
        }

        return $this;
    }

    public function getTeamName(): string
    {
       return TabTeam::findOne($this->team_id)->name;
    }


}