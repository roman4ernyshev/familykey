<?php
namespace core\entities\tablo;

class TabloItem
{
    private $id;
    private $tournament;
    private $team1;
    private $team2;


    public function __construct($id, $tournament, $team1, $team2)
    {
        $this->id = $id;
        $this->tournament = $tournament;
        $this->team1 = $team1;
        $this->team2 = $team2;
    }

    public function getId()
    {
        return $this->id;
    }

}