<?php

namespace core\entities\tablo\storage;

use core\entities\tablo\TabMatch;
use core\entities\tablo\TabSummary;
use core\entities\tablo\TabTeam;
use core\entities\tablo\TabTournament;
use yii\db\Connection;
use yii\db\Exception;
use yii\db\Query;

class DbStorage implements StorageInterface
{
    private $db;

    public function __construct()
    {
        $db = new Connection();
        $this->db = $db;
    }

    public function load(array $filter = []): array
    {
        return [];
    }

    public function save(array $items): void
    {

        if(!$tournament = TabTournament::find()->where(['like', 'name', $items['tournament']])->one()){
           $tournament = new TabTournament();
           $tournament->name = $items['tournament'];
           $tournament->save();
        }
        if(!$host = TabTeam::find()->where(['like', 'name', $items['host']])->one()){
           $host = new TabTeam();
           $host->name = $items['host'];
           $host->save();
        }
        if(!$guest = TabTeam::find()->where(['like', 'name', $items['guest']])->one()){
            $guest = new TabTeam();
            $guest->name = $items['guest'];
            $guest->save();
        }

        try {
            TabMatch::getDb()->createCommand()->insert(TabMatch::tableName(), [
                'tournament_id' => $tournament->id,
                'team1_id' => $host->id,
                'team2_id' => $guest->id,
                'goals_first' => $items['sc_host'],
                'goals_second' => $items['sc_guest'],
                'create_time' => time()
            ])->execute();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    public function teams(): ?array
    {
        return TabTeam::find()->all();
    }

    public function tournaments(): ?array
    {
        return TabTournament::find()->all();
    }

    public function tournamentData($tournament_name): ?array
    {
        $summary = [];
        /**
         * @var $tournament TabTournament
         */
        $tournament = TabTournament::find()->where(['like', 'name', $tournament_name])->one();
        $matches = TabMatch::find()->where(['tournament_id' => $tournament->id])->all();
        $teams = TabTeam::find()->all();

        /**
         * @var TabTeam $team
         * @var TabSummary $sumTeam
         */
        foreach ($teams as $team){
            $team_summary = new TabSummary($matches, $team->id);
            $sumTeam = $team_summary->sumTeam();
            if($sumTeam->games) $summary[] = $sumTeam;
        }


        usort($summary, function($a, $b)
        {
            return $this->sortByBalls($a, $b);
        });

        return [
            'matches' => $matches,
            'summary' => $summary
        ];
    }

    /**
     * @param $f1 TabSummary
     * @param $f2 TabSummary
     * @return int
     */
    function sortByBalls($f1,$f2)
    {
        if($f1->balls > $f2->balls) return -1;
        elseif($f1->balls < $f2->balls) return 1;
        else return 0;
    }


}