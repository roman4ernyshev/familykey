<?php

namespace core\entities\tablo\storage;

use core\entities\tablo\TabloItem;

interface StorageInterface
{
    /**
     * @return TabloItem[]
     * @param array
     */
    public function load(array $filter = []): array;

    /**
     * @param TabloItem[] $items
     */
    public function save(array $items): void;

    public function teams(): ?array;

    public function tournaments(): ?array;

    public function tournamentData($tournament_id): ?array;
}