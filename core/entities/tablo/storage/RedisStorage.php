<?php

namespace core\entities\tablo\storage;

use yii\redis\Connection;

class RedisStorage implements StorageInterface
{
    private $redis;

    public function __construct()
    {
        $redis = new Connection();
        $this->redis = $redis;
    }

    public function load(array $keys = []): array
    {
        $result = [];
        foreach ($keys as $key){
            $result[$key] = $this->redis->get($key);
        }

        return $result;
    }

    public function save(array $params): void
    {
        foreach ($params as $key => $param) {
            $res[$key] = $param;
            $this->redis->set($key, $param);
        }

    }

    public function teams(): array
    {
        // TODO: Implement teams() method.
    }

    public function tournaments(): array
    {
        // TODO: Implement tournaments() method.
    }

    public function tournamentData($tournament_id): array
    {
        // TODO: Implement tournamentData() method.
    }
}