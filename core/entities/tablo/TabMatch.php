<?php

namespace core\entities\tablo;

use Yii;

/**
 * This is the model class for table "tab_match".
 *
 * @property int $id
 * @property int $tournament_id
 * @property int $team1_id
 * @property int $team2_id
 * @property string $description
 * @property int $goals_first
 * @property int $goals_second
 *
 * @property TabTeam $team1
 * @property TabTeam $team2
 * @property TabTournament $tournament
 */
class TabMatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tournament_id', 'team1_id', 'team2_id'], 'integer'],
            ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tournament_id' => 'Tournament ID',
            'team1_id' => 'Team1 ID',
            'team2_id' => 'Team2 ID',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam1()
    {
        return $this->hasOne(TabTeam::class, ['id' => 'team1_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam2()
    {
        return $this->hasOne(TabTeam::class, ['id' => 'team2_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTournament()
    {
        return $this->hasOne(TabTournament::class, ['id' => 'tournament_id']);
    }
}
