<?php
namespace core\entities\tablo;

use core\entities\tablo\storage\StorageInterface;
use yii\db\ActiveRecord;

class Tablo extends ActiveRecord
{
    private $storage;
    private $keys = [
        'host',
        'guest' ,
        'sc_host' ,
        'sc_guest' ,
        'tournament',
        'minute' ,
        'second',
        'timeUp',
        'parted'
    ];

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
        parent::__construct();
    }

    public function saveParams($params)
    {
        $this->storage->save($params);
    }

    public function getParams(): array
    {
        return $this->storage->load($this->keys);
    }

    public function getTeams(): ?array
    {
        return $this->storage->teams();
    }

    public function getTournaments(): ?array
    {
        return $this->storage->tournaments();
    }

    public function getTournament($tournament_name)
    {
        return $this->storage->tournamentData($tournament_name);
    }

}