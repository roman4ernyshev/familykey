<?php

namespace core\entities\tablo;

use Yii;

/**
 * This is the model class for table "tab_tournament".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @property TabMatch[] $tabMatches
 */
class TabTournament extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_tournament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabMatches()
    {
        return $this->hasMany(TabMatch::class, ['tournament_id' => 'id']);
    }

    public function getTabMatchesByName($name)
    {
        /**
         * @var $tournament $this
         */
        return $tournament = $this::find()->where(['like', 'name', $name])->one();
        //return $this->hasMany(TabMatch::class, ['tournament_id' => $tournament->id]);
    }
}
