<?php

namespace core\entities\Project;

use core\entities\behaviors\MetaBehavior;
use core\entities\EventTrait;
use core\entities\Meta;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $meta_json
 * @property string $image
 * @property string $link
 *
 *  @property Photo[] $photos
 */
class Project extends \yii\db\ActiveRecord
{

    use EventTrait;

    public $meta;

    public static function create($title, $description, $link, Meta $meta): self
    {
        $project = new static();
        $project->title = $title;
        $project->description = $description;
        $project->link = $link;
        $project->slug = Inflector::slug($title);
        $project->meta = $meta;
        return $project;
    }

    public function edit($title, $description, $link, Meta $meta): void
    {
        $this->title = $title;
        $this->description = $description;
        $this->link = $link;
        $this->meta = $meta;
    }

    ####################Photos#######################

    public function addPhoto(UploadedFile $file): void
    {
        $photos = $this->photos;
        $photos[] = Photo::create($file);
        $this->updatePhotos($photos);
    }

    public function removePhoto($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)){
                unset($photos[$i]);
                $this->updatePhotos($photos);
                return;
            }
        }
        throw new \DomainException('Photo is not found.');

    }

    public function removePhotos(): void
    {
        $this->updatePhotos([]);
    }

    public function movePhotoUp($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i - 1] ?? null) {
                    $photos[$i] = $prev;
                    $photos[$i - 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }

    public function movePhotoDown($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i + 1] ?? null) {
                    $photos[$i] = $prev;
                    $photos[$i + 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }


    private function updatePhotos(array $photos): void
    {

        foreach ($photos as $i => $photo){

            $photo->setSort($i);
        }

        $this->photos = $photos;
        $this->populateRelation('image', reset($photos));
    }


    public function getPhotos(): ActiveQuery
    {
        return $this->hasMany(Photo::class, ['project_id' => 'id'])->orderBy('sort');
    }

    #################################

    public function behaviors()
    {
        return [
            MetaBehavior::class,
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['photos']
            ],
        ];
    }

    public static function tableName()
    {
        return 'projects';
    }

    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['title', 'description', 'link'], 'string', 'max' => 128],
            [['slug', 'meta_json'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'description' => 'Description',
            'meta_json' => 'Meta Json',
            'image' => 'Image',
            'link' => 'Link',
        ];
    }
}
