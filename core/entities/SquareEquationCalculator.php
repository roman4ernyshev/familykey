<?php

namespace core\entities;


class SquareEquationCalculator
{
    private $squareMember;
    private $lineMember;
    private $freeMember;

    public $discriminant;

    private $roots = [];

    public function getRoots($square, $line, $free): array
    {
       $this->squareMember = (int)$square;
       $this->lineMember = (int)$line;
       $this->freeMember = (int)$free;

       if($this->squareMember == 0) return $this->roots;

       if($this->Discriminant() > 0) {
           array_push($this->roots,
               (- $this->lineMember + sqrt($this->Discriminant()))/(2*$this->squareMember));
           array_push($this->roots,
               (- $this->lineMember - sqrt($this->Discriminant()))/(2*$this->squareMember));
       }
       elseif($this->Discriminant() === 0) {
           array_push($this->roots,
               (- $this->lineMember )/(2*$this->squareMember));
       }
       return $this->roots;

    }

    public function Discriminant()
    {
        $this->discriminant = $this->lineMember*$this->lineMember - (4*$this->squareMember*$this->freeMember);
        return $this->discriminant;
    }

}