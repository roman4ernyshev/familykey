<?php

namespace core\entities\Notion;

use core\entities\AggregateRoot;
use core\entities\behaviors\GenerateAudioFromTextBehavior;
use core\entities\behaviors\MetaBehavior;
use core\entities\EventTrait;
use core\entities\Meta;
use core\entities\Notion\queries\NotionQuery;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * Class Notion
 * @property integer $id
 * @property string $body
 * @property string $question
 * @property string $answer
 * @property string $description
 * @property integer $category_id
 * @property integer $slug
 * @property integer $image_link
 * @property integer $audio_link
 * @property string $generated_audio_link
 * @property string $voice;
 *
 * @property Meta $meta
 * @property Category $category
 * @property CategoryAssignment[] $categoryAssignments
 * @property ExampleAssignment[] $example
 * @property Photo[] $photos
 * @property Audio[] $audios
 *
 * @package core\entities\Notion
 */
class Notion extends ActiveRecord implements AggregateRoot
{
    use EventTrait;

    public $meta;
    public $voice;

    public static function create($categoryId, $body, $question, $answer, $description, Meta $meta): self
    {
        $notion = new static();
        if($categoryId == 3) $notion->voice = 'espeak -v mb-de4';
        if($categoryId == 4) $notion->voice = 'espeak -v mb-en1';

        $notion->category_id = $categoryId;
        $notion->body = $body;
        $notion->question = $question;
        $notion->answer = $answer;
        $notion->description = $description;
        $notion->slug = Inflector::slug($body);
        $notion->meta = $meta;
        //var_dump($notion); exit;
        return $notion;
    }

    public function edit($body, $question, $answer, $description, Meta $meta): void
    {
        $this->body = $body;
        $this->question = $question;
        $this->description = $description;
        $this->answer = $answer;
        $this->meta = $meta;
        //var_dump($this); exit;
    }

    public function assignCategory($id): void
    {
        $assignments = $this->categoryAssignments;
        foreach ($assignments as $assignment){
            if ($assignment->isForCategory($id)){
                return;
            }
        }
        $assignments[] = CategoryAssignment::create($id);
        $this->categoryAssignments = $assignments;
    }

    public function changeMainCategory($categoryId): void
    {
        $this->category_id = $categoryId;
    }


    public function revokeCategory($id): void
    {
        $assignments = $this->categoryAssignments;
        foreach ($assignments as $i => $assignment){
            if ($assignment->isForCategory($id)){
                unset($assignments[$i]);
                $this->categoryAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException('Assignment is not found.');

    }

    public function revokeCategories(): void
    {
        $this->categoryAssignments = [];
    }


    public static function isCategoryExists($slug): bool
    {
        return (bool)Category::find()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }



    ####################Audios#######################


    public function addAudio(UploadedFile $file): void
    {
        $audios = $this->audios;
        $audios[] = Audio::create($file);
        $this->updateAudio($audios);
    }

    private function updateAudio(array $audios): void
    {

        foreach ($audios as $i => $audio){
            $audio->setSort($i);
        }

        $this->audios = $audios;
        $this->populateRelation('mainAudio', reset($audios));
    }


    ####################Photos#######################

    public function addPhoto(UploadedFile $file): void
    {
        $photos = $this->photos;
        $photos[] = Photo::create($file);
        $this->updatePhotos($photos);
    }

    public function removePhoto($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)){
                unset($photos[$i]);
                $this->updatePhotos($photos);
                return;
            }
        }
        throw new \DomainException('Photo is not found.');

    }

    public function removePhotos(): void
    {
        $this->updatePhotos([]);
    }

    public function movePhotoUp($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i - 1] ?? null) {
                    $photos[$i] = $prev;
                    $photos[$i - 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }

    public function movePhotoDown($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i + 1] ?? null) {
                    $photos[$i] = $prev;
                    $photos[$i + 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }


    private function updatePhotos(array $photos): void
    {

        foreach ($photos as $i => $photo){

            $photo->setSort($i);
        }

        $this->photos = $photos;
        $this->populateRelation('mainPhoto', reset($photos));
    }


    ############################################

    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function getCategoryAssignments(): ActiveQuery
    {
        return $this->hasMany(CategoryAssignment::class, ['product_id' => 'id']);
    }

    public function getCategories(): ActiveQuery
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])->via('categoryAssignments');
    }

    public function getPhotos(): ActiveQuery
    {
        return $this->hasMany(Photo::class, ['notion_id' => 'id'])->orderBy('sort');
    }

    public function getAudios(): ActiveQuery
    {
        return $this->hasMany(Audio::class, ['notion_id' => 'id'])->orderBy('sort');
    }

    public function getMainPhoto(): ActiveQuery
    {
        return $this->hasOne(Photo::class, ['id' => 'image_link']);
    }

    public function getMainAudio(): ActiveQuery
    {
        return $this->hasOne(Audio::class, ['id' => 'audio_link']);
    }

    ##############################################


    public function getExamples(): ActiveQuery
    {
        return $this->hasMany(Example::class, ['id' => 'notion_example_id'])->via('exampleAssignments');
    }

    public function getExample()
    {
        return $this->hasOne(Example::class, ['id' => 'notion_example_id'])->via('exampleAssignments');
    }

    public function getExampleAssignments(): ActiveQuery
    {
        return $this->hasMany(ExampleAssignment::class, ['notion_id' => 'id']);
    }


    ##############################################

    public static function tableName()
    {
        return '{{%notions}}';
    }


    public function behaviors()
    {
        return [
            MetaBehavior::class,
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['categoryAssignments', 'exampleAssignments', 'photos', 'audios']
            ],
            /*
            [
                'class' => GenerateAudioFromTextBehavior::class
            ]
            */

        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function afterSave($insert, $changedAttributes): void
    {
        $related = $this->getRelatedRecords();
        parent::afterSave($insert, $changedAttributes);
        if (array_key_exists('mainPhoto', $related)) {
            $this->updateAttributes(['image_link' => $related['mainPhoto'] ? $related['mainPhoto']->id : null]);
        }
        if (array_key_exists('mainAudio', $related)) {
            $this->updateAttributes(['audio_link' => $related['mainAudio'] ? $related['mainAudio']->id : null]);
        }

    }

    public static function find(): NotionQuery
    {
        return new NotionQuery(static::class);
    }





}