<?php

namespace core\entities\Notion;

use yii\db\ActiveRecord;

/**
 * Class ExampleAssignment
 * @property integer $notion_id
 * @property integer $notion_example_id
 *
 * @package core\entities\Notion\Notion
 */
class ExampleAssignment extends ActiveRecord
{
    public static function create($notion_id, $exampleId): self
    {
        $assignment = new static();
        $assignment->notion_id = $notion_id;
        $assignment->notion_example_id = $exampleId;
        return $assignment;
    }

    public function isForExample($id): bool
    {
        return $this->notion_example_id == $id;
    }

    public static function tableName(): string
    {
        return '{{%notion_example_assignments}}';
    }


}