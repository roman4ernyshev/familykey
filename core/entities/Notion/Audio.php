<?php

namespace core\entities\Notion;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yiidreamteam\upload\FileUploadBehavior;

/**
* Class Audio
 * @property integer $id
* @property string $file
* @property integer $sort
*
 * @package core\entities\Notion
*
 * @mixin FileUploadBehavior
*/
class Audio extends ActiveRecord
{
    public static function create(UploadedFile $file): self
    {

        $audio = new static();
        $audio->file = $file;
        return $audio;
    }

    public function setSort($sort): void
    {
        $this->sort = $sort;
    }

    public function isIdEqualTo($id): bool
    {
        return $this->id == $id;
    }

    public static function tableName(): string
    {
        return '{{%notion_audios}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => FileUploadBehavior::class,
                'attribute' => 'file',
                'filePath' => '@staticRoot/origin/notions/audio/[[attribute_notion_id]]/[[id]].[[extension]]',
                'fileUrl' => '@static/origin/notions/audio/[[attribute_notion_id]]/[[id]].[[extension]]',
            ],
        ];

    }

}