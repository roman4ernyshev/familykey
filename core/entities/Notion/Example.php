<?php

namespace core\entities\Notion;


use core\entities\behaviors\GenerateAudioFromTextBehavior;
use core\entities\EventTrait;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * Class Example
 *
 * @property integer $id
 * @property string $body
 * @property string $description
 *
 * @property integer $image_link
 * @property integer $audio_link
 * @property string $generated_audio_link
 *
 * @property Photo[] $photos
 * @property Audio[] $audios
 *
 * @package core\entities\Notion
 */
class Example extends ActiveRecord
{

    use EventTrait;

    public static function create($body, $description): self
    {
        $example = new static();
        $example->body = $body;
        $example->description = $description;
        return $example;
    }

    public function edit($body, $description): void
    {
        $this->body = $body;
        $this->description = $description;
    }

    ####################Audios#######################


    public function addAudio(UploadedFile $file): void
    {
        $audios = $this->audios;
        $audios[] = Audio::create($file);
        $this->updateAudio($audios);
    }

    private function updateAudio(array $audios): void
    {

        foreach ($audios as $i => $audio){
            $audio->setSort($i);
        }

        $this->audios = $audios;
        $this->populateRelation('mainAudio', reset($audios));
    }


    ####################Photos#######################

    public function addPhoto(UploadedFile $file): void
    {
        $photos = $this->photos;
        $photos[] = Photo::create($file);
        $this->updatePhotos($photos);
    }

    public function removePhoto($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)){
                unset($photos[$i]);
                $this->updatePhotos($photos);
                return;
            }
        }
        throw new \DomainException('Photo is not found.');

    }

    public function removePhotos(): void
    {
        $this->updatePhotos([]);
    }

    public function movePhotoUp($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i - 1] ?? null) {
                    $photos[$i] = $prev;
                    $photos[$i - 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }

    public function movePhotoDown($id): void
    {
        $photos = $this->photos;
        foreach ($photos as $i => $photo){
            if($photo->isIdEqualTo($id)) {
                if ($prev = $photos[$i + 1] ?? null) {
                    $photos[$i] = $prev;
                    $photos[$i + 1] = $photo;
                    $this->updatePhotos($photos);
                }
                return;
            }
        }
        throw new \DomainException('Photo is not found.');
    }


    private function updatePhotos(array $photos): void
    {

        foreach ($photos as $i => $photo){

            $photo->setSort($i);
        }

        $this->photos = $photos;
        $this->populateRelation('mainPhoto', reset($photos));
    }

    ##################################################

    public function getPhotos(): ActiveQuery
    {
        return $this->hasMany(Photo::class, ['notion_id' => 'id'])->orderBy('sort');
    }

    public function getAudios(): ActiveQuery
    {
        return $this->hasMany(Audio::class, ['notion_id' => 'id'])->orderBy('sort');
    }

    public function getMainPhoto(): ActiveQuery
    {
        return $this->hasOne(Photo::class, ['id' => 'image_link']);
    }

    public function getMainAudio(): ActiveQuery
    {
        return $this->hasOne(Audio::class, ['id' => 'audio_link']);
    }

    ##################################################


    public static function tableName()
    {
        return '{{%notion_examples}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['photos', 'audios']
            ],
            [
                'class' => GenerateAudioFromTextBehavior::class,
                'from' => 'description',
                'audioPath' => '@staticRoot/generated/phraze/',
                'staticPath' => '@static/generated/phraze/'
            ]

        ];
    }

    /**
     * Wrap counting of new category tree in transaction
     * @return array
     */
    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL
        ];
    }

    public function afterSave($insert, $changedAttributes): void
    {
        $related = $this->getRelatedRecords();
        parent::afterSave($insert, $changedAttributes);
        if (array_key_exists('mainPhoto', $related)) {
            $this->updateAttributes(['image_link' => $related['mainPhoto'] ? $related['mainPhoto']->id : null]);
        }
        if (array_key_exists('mainAudio', $related)) {
            $this->updateAttributes(['audio_link' => $related['mainAudio'] ? $related['mainAudio']->id : null]);
        }

    }

}