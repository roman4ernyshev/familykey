<?php

namespace core\entities\Notion;

use core\entities\User\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class NotionUserMarks
 * @property integer $user_id
 * @property integer $notion_id
 * @property integer $time
 * @property integer $mark
 *
 * @package core\entities\Notion\Notion
 */
class NotionUserMarks extends ActiveRecord
{
    public $cnt;
    public $sum;

    public static function create($userId, $notionId, $mark): self
    {
        $assignment = new static();
        $assignment->user_id = $userId;
        $assignment->notion_id = $notionId;
        $assignment->mark = $mark;
        return $assignment;
    }


    public static function tableName(): string
    {
        return '{{%notion_user_marks}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'time',
                'updatedAtAttribute' => false,
                //'value' => new Expression('NOW()'),
                'value' => function() {
                    return date('U'); // unix timestamp
                },
            ],
        ];
    }

    public function getNotion(): ActiveQuery
    {
        return $this->hasOne(Notion::class, ['id' => 'notion_id']);
    }

    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }


}