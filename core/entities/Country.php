<?php
namespace core\entities;

use yii\db\ActiveRecord;

/**
 * Class Country
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property string $iso_code
 * @property string $soccer_code
 *
 * @package core\entities
 */
class Country extends ActiveRecord
{

    ##############################################

    public static function tableName()
    {
        return '{{%country}}';
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'],
                'unique',
                'targetAttribute' => ['name']]
        ];
    }

}