<?php
namespace core\entities\Rockncontroll;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class SourceSearch extends Source
{
    public function rules()
    {
        return [

            [['title'], 'save'],
        ];
    }


    public function search($params)
    {
        $query = Source::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    public function searchArtistSources($params, $id)
    {

        $query = Source::find()->where("author_id = $id");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    public function searchIt($params)
    {

        $query = Source::find()->where("cat_id = 53 or cat_id = 116 or cat_id = 243");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        return $dataProvider;
    }

    public function searchArtistSourcesBySourceId($params, $id)
    {

        $source = Source::findOne($id);

        $query = Source::find()->where("author_id = $source->author_id");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
