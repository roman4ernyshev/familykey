<?php

namespace core\entities\Rockncontroll;


use yii\db\ActiveRecord;
use Yii;


/**
 * This is the model class for table "shop".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Bought[] $boughts
 */
class Shop extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%shop}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoughts()
    {
        return $this->hasMany(Bought::className(), ['shop_id' => 'id']);
    }
}
