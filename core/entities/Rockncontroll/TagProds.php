<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;
/**
 * This is the model class for table "tag_prods".
 *
 * @property integer $id
 * @property string $prod
 */
class TagProds extends \yii\db\ActiveRecord
{


    public static function tableName()
    {
        return '{{%tag_prods}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod'], 'required'],
            [['prod'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod' => 'Prod',
        ];
    }
}
