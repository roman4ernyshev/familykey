<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "deals".
 *
 * @property integer $id
 * @property string $name
 * @property integer $mark
 * @property integer $status
 *
 * @property DoneDeal[] $doneDeals
 */
class DiaryDeals extends ActiveRecord
{
    public $sum;
    public $cnt;
    

    public static function tableName()
    {
        return '{{%deals%}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['mark', 'status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'mark' => 'Mark',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoneDeals()
    {
        return $this->hasMany(DiaryDoneDeal::className(), ['deal_id' => 'id']);
    }
}
