<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "sources".
 *
 *
 */

class Source extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%sources}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

            'name' => 'Name',

        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }
}