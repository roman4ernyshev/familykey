<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "spravka".
 *
 * @property integer $id
 * @property integer $item_id
 *
 * @property Source $source
 */
class Spravka extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%spravka}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    public function getItem()
    {
        return $this->hasOne(Items::class, ['id' => 'item_id']);
    }


}