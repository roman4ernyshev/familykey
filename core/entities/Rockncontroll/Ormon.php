<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

class Ormon extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%ormon}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }



}