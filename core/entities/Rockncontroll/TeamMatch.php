<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "team_match_assignments".
 *
 * @property int $team_id
 * @property int $match_id
 *
 * @property Matches $match
 * @property TeamSum $team
 */
class TeamMatch extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%team_match_assignments}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'match_id'], 'required'],
            [['team_id', 'match_id'], 'integer'],
            [['match_id'], 'exist', 'skipOnError' => true, 'targetClass' => Matches::className(), 'targetAttribute' => ['match_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => TeamSum::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'match_id' => 'Match ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatch()
    {
        return $this->hasOne(Matches::className(), ['id' => 'match_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(TeamSum::className(), ['id' => 'team_id']);
    }
}
