<?php
namespace core\entities\Rockncontroll;

use Yii;
use yii\data\ActiveDataProvider;

class ItemsSearch extends Items 
{
    public function rules()
    {
        return [

            [['text', 'tags', 'title'], 'save'],
          ];
    }


    public function search($params, $additional=''){
        $query = Items::find()->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'tags', $this->tags])
          //  ->orFilterWhere(['like', 'fio', $this->search])
          //  ->orFilterWhere(['like', 'address', $this->search])
          //  ->orFilterWhere(['like', 'comment', $this->search])
        ;
        if($additional=='songs') {
            $query->andWhere("(audio_link = '' or audio_link is null) and source_id = 528");
            //$query->andWhere(['audio_link'=>'']);
        }
        if($additional=='kvns') {
            $query->andWhere("(audio_link = '' or audio_link is null) and source_id = 21");
            //$query->andWhere(['audio_link'=>'']);
        }

        return $dataProvider;
    }

    public function searchrow($params){
        $query = Items::find()->orderBy('id ASC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['>', 'id', $this->id])
            //  ->orFilterWhere(['like', 'fio', $this->search])
            //  ->orFilterWhere(['like', 'address', $this->search])
            //  ->orFilterWhere(['like', 'comment', $this->search])
        ;
        $query->andWhere('published=0 and cens<>0');

        return $dataProvider;
    }

    public function searchimg($params){
        $query = Items::find()
            ->where('cat_id = 259');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'tags', $this->tags])
            //  ->orFilterWhere(['like', 'fio', $this->search])
            //  ->orFilterWhere(['like', 'address', $this->search])
            //  ->orFilterWhere(['like', 'comment', $this->search])
        ;

        return $dataProvider;
    }
    
}