<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "day_params".
 *
 * @property integer $id
 * @property string $name
 *
 * @property RecDayParam[] $recDayParams
 */
class DiaryDayParams extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%day_params%}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecDayParams()
    {
        return $this->hasMany(DiaryRecDayParams::className(), ['day_param_id' => 'id']);
    }
}
