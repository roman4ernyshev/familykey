<?php

namespace core\entities\Rockncontroll\newgood;

use yii\db\ActiveRecord;
use Yii;

/**
 * Class Act
 * @package core\entities\Rockncontroll\newgood
 *
 * @property integer $id
 * @property integer $time
 * @property integer $model_id
 * @property integer $user_id
 * @property integer $mark
 * @property integer $mark_status
 */
class Act extends ActiveRecord
{

    public $cnt;
    public $yesterday=false;

    public static function create($actCategoryId, $actUserId, $mark, $markStatus=0): self
    {
        $act = new static();

        $act->model_id = $actCategoryId;
        $act->user_id = $actUserId;
        $act->mark = $mark;
        $act->mark_status = $markStatus;

        return $act;
    }

    ###################################################################

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActCategory()
    {
        return $this->hasOne(ActCategory::className(), ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInnerUser()
    {
        return $this->hasOne(InnerUser::className(), ['id' => 'model_id']);
    }

    #####################################################################


    public static function tableName()
    {
        return '{{%controll_acts}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    #####################################################################

    public function beforeSave($options = [])
    {
        if (parent::beforeSave(1)) {
            if ($this->isNewRecord && $this->yesterday === false) {
                $this->time = time()+7*60*60;
                return true;
            } else
                return true;
        } else
            return false;
    }
}