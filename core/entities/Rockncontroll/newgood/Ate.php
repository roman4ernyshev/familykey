<?php

namespace core\entities\Rockncontroll\newgood;

use Yii;

class Ate extends Act
{
    public static function create($actCategoryId, $actUserId, $mark, $markStatus=0): self
    {
        parent::create($actCategoryId, $actUserId, $mark, $markStatus=0);

    }

    public static function tableName()
    {
        return '{{%ate%}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

}