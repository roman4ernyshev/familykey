<?php

namespace core\entities\Rockncontroll\newgood;

use yii\db\ActiveRecord;
use Yii;

/**
 * Class ActCategory
 * @package core\entities\Rockncontroll\newgood
 *
 * @property integer $id
 * @property string $name
 */
class ActCategory extends ActiveRecord
{

    public static function create($name): self
    {
        $actCategory = new static();

        $actCategory->name = $name;

        return $actCategory;
    }


    public static function tableName()
    {
        return '{{%act_model}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

}