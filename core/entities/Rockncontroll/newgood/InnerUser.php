<?php

namespace core\entities\Rockncontroll\newgood;

use yii\db\ActiveRecord;
use Yii;

/**
 * Class InnerUser
 * @package core\entities\Rockncontroll\newgood
 *
 * @property integer $id
 * @property string $name
 * @property string $pseudo
 * @property float $money
 */
class InnerUser extends ActiveRecord
{
    public static function create($name, $pseudo, $money): self
    {
        $innerUser = new static();

        $innerUser->name = $name;
        $innerUser->pseudo = $pseudo;
        $innerUser->money = (float)$money;

        return $innerUser;
    }


    public static function tableName()
    {
        return '{{%mark_user%}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }


}