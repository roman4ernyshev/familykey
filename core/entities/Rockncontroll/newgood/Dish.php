<?php
namespace core\entities\Rockncontroll\newgood;

use yii\db\ActiveRecord;
use Yii;

/**
 * Class Dish
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $kkal
 * @property float $carbohydrates
 * @property float $fats
 * @property float $squirrels
 * @property float $ferrum
 * @property float $magnesium
 * @property float $cuprum
 * @property float $iodum
 * @property float $fluorum
 * @property float $zincum
 * @property float $cobaltum
 *
 * @package core\entities\Rockncontroll\newgood
 */
class Dish extends ActiveRecord
{

    public static function create($name, $description, $kkal, $carbohydrates, $fats,
                                  $squirrels, $ferrum, $magnesium, $cuprum, $iodum, $fluorum, $zincum, $cobaltum): self
    {
        $dish = new static();

        $dish->name = $name;
        $dish->kkal = $kkal;
        $dish->carbohydrates = $carbohydrates;
        $dish->fats = $fats;
        $dish->description = $description;
        $dish->squirrels = $squirrels;
        $dish->ferrum = $ferrum;
        $dish->magnesium = $magnesium;
        $dish->cuprum = $cuprum;
        $dish->iodum = $iodum;
        $dish->fluorum = $fluorum;
        $dish->zincum = $zincum;
        $dish->cobaltum = $cobaltum;

        return $dish;
    }

    public function edit($name, $description, $kkal, $carbohydrates, $fats,
                         $squirrels, $ferrum, $magnesium, $cuprum, $iodum, $fluorum, $zincum, $cobaltum): void
    {

        $this->name = $name;
        $this->kkal = $kkal;
        $this->carbohydrates = $carbohydrates;
        $this->fats = $fats;
        $this->description = $description;
        $this->squirrels = $squirrels;
        $this->ferrum = $ferrum;
        $this->magnesium = $magnesium;
        $this->cuprum = $cuprum;
        $this->iodum = $iodum;
        $this->fluorum = $fluorum;
        $this->zincum = $zincum;
        $this->cobaltum = $cobaltum;
    }

    public static function tableName()
    {
        return '{{%dish}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

}