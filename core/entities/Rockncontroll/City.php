<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property string $name
 * @property string $weather_link
 * @property integer $country_id
 */

class City extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
    return '{{%cities}}';
    }

    public static function getDb()
    {
    return Yii::$app->get('db_rockncontroll');
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'weather_link' => 'Link',
            'country_id' => 'Id Country',
        ];
    }

    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }
}