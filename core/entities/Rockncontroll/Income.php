<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;
/**
 * This is the model class for table "income".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property Incomes[] $incomes
 */
class Income extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%income%}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomes()
    {
        return $this->hasMany(Incomes::className(), ['income_id' => 'id']);
    }
}
