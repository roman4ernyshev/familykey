<?php
namespace core\entities\Rockncontroll;

use paulzi\nestedsets\NestedSetsBehavior;

class CategoriesQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsBehavior::className(),
        ];
    }
}
