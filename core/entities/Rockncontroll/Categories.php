<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

use paulzi\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_parent
 */
class Categories extends ActiveRecord
{
    public $rootCat;


    public static function tableName()
    {
        return '{{%qpcategory}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['name'], 'required'],
            [['title'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'title' => 'Титул',
            'rootCat' => 'Корневая категория',
            'cssclass' => 'Класс css',

        ];
    }

    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'level',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new CategoriesQuery(get_called_class());
    }

    public function getProducts() {
        return $this->hasMany(Products::className(), ['cat_id' => 'id']);
    }
}