<?php

namespace core\entities\Rockncontroll;


use yii\data\ActiveDataProvider;

class EventSearch extends Event
{
    public function rules()
    {
        return [

            [['text'], 'save'],
        ];
    }


    public function search($params){
        $query = Event::find()->orderBy('id DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'text', $this->text])
            //->andFilterWhere(['like', 'tags', $this->tags])
            //  ->orFilterWhere(['like', 'fio', $this->search])
            //  ->orFilterWhere(['like', 'address', $this->search])
            //  ->orFilterWhere(['like', 'comment', $this->search])
        ;

        return $dataProvider;
    }

}