<?php
namespace core\entities\Rockncontroll;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use Yii;

class DeutschItemSearch extends DeutschItem
{
    public function rules()
    {
        return [

            [['d_word'], 'save'],
        ];
    }


    public function search($params)
    {
        $query = DeutschItem::find()->orderBy('id DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'd_word', $this->d_word]);

        return $dataProvider;
    }

    public function searchTicket($params)
    {
        $query = DeutschItem::find()
            ->where(['shown' => 2])
            ->orderBy('id DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'd_word', $this->d_word]);

        return $dataProvider;
    }

    public function searchRusDoubles($params)
    {

        $double_words = DeutschItem::find()
            ->select('d_word_translation, COUNT(id) cnt')
            ->groupBy('d_word_translation')->having("cnt > 1")->asArray()->all();

        $query = DeutschItem::find()
            ->where(['in', 'd_word_translation', $double_words])
            ->orderBy('d_word_translation DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'd_word_translation', $this->d_word_translation]);

        return $dataProvider;

    }

    public function searchDoubles($params)
    {

        $double_words = DeutschItem::find()
            ->select('d_word, COUNT(id) cnt')
            ->groupBy('d_word')->having("cnt > 1")->asArray()->all();

        //return var_dump($double_words);

        $query = DeutschItem::find()
            ->where(['in', 'd_word', $double_words])
            ->orderBy('d_word DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'd_word', $this->d_word]);

        return $dataProvider;

    }

    public function searchWithBracket($params)
    {

        $query = DeutschItem::find()
            ->where(['like', 'd_word', [')', '(']])
            ->orderBy('d_word DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'd_word', $this->d_word]);

        return $dataProvider;

    }
}