<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "song_texts".
 *
 * @property integer $id
 * @property integer $source_id
 * @property string $title
 * @property string $text
 * @property string $link
 */
class SongText extends ActiveRecord
{
    public $source_title;


    public static function tableName()
    {
        return '{{%song_texts}}';
    }

    public static function getDb()
    {
        //return Yii::$app->get('db_rockncontroll');
        if(\Yii::getAlias('@new_site') === 'on'){
            //echo 2;
            return Yii::$app->db_rockncontroll_new;
            //return Yii::$app->get('db_rockncontroll_new');
        }

        else {
            //echo 1;
            return Yii::$app->db_rockncontroll;
            //return Yii::$app->get('db_rockncontroll');
        }
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id', 'title', 'text'], 'required'],
            [['source_id'], 'integer'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => Source::className(), 'targetAttribute' => ['source_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'title' => 'Title',
            'text' => 'Text',
        ];
    }

    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }
}
