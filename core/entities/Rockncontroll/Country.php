<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $name
 * @property string $weather_link
 * @property integer $country_id
 */

class Country extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%country}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
