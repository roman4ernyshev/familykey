<?php

namespace core\entities\Rockncontroll;

use Yii;

/**
 * This is the model class for table "foo_team_year".
 *
 * @property int $team_id
 * @property int $season
 * @property int $cout
 * @property int $vic
 * @property int $nob
 * @property int $def
 * @property int $get
 * @property int $let
 * @property int $balls
 * @property int $id
 *
 * @property TeamSum $team
 * @property TeamMatchYear[] $teamMatchYearAssignments
 * @property Matches[] $matches
 */

class TeamYear extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%foo_team_year}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'id'], 'required'],
            [['team_id', 'season', 'cout', 'vic', 'nob', 'def', 'get', 'let', 'balls', 'id'], 'integer'],
            [['id'], 'unique'],
            [['team_id', 'season'], 'unique', 'targetAttribute' => ['team_id', 'season']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => TeamSum::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'season' => 'Season',
            'cout' => 'Cout',
            'vic' => 'Vic',
            'nob' => 'Nob',
            'def' => 'Def',
            'get' => 'Get',
            'let' => 'Let',
            'balls' => 'Balls',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(TeamSum::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamMatchYearAssignments()
    {
        return $this->hasMany(TeamMatchYear::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws
     */
    public function getMatches()
    {
        return $this->hasMany(Matches::className(), ['id' => 'match_id'])->viaTable('team_match_year_assignments', ['team_id' => 'id']);
    }
}
