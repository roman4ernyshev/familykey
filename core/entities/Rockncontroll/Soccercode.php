<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;

class Soccercode extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%soccerstand_match%}}';
    }

    public static function getDb()
    {
        return Yii::$app->get('db_rockncontroll');
    }

}