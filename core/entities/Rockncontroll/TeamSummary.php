<?php

namespace core\entities\Rockncontroll;

use yii\db\ActiveRecord;
use Yii;


/**
 * Class TeamSummary
 * @package core\entities\Rockncontroll
 *
 * @property string $team
 * @property integer $games
 * @property integer $victories
 * @property integer $ties
 * @property integer $defeats
 * @property integer $gets
 * @property integer $lets
 * @property integer $balls
 *
 * @property float $vic_bets
 * @property float $n_bets
 * @property float $def_bets
 *
 * @property string $row_team
 */
class TeamSummary
{
    public $games=0;
    public $victories=0;
    public $ties=0;
    public $defeats=0;
    public $gets=0;
    public $lets=0;
    public $balls=0;

    public $vic_bets=0.00;
    public $n_bets=0.00;
    public $def_bets=0.00;

    public $team;
    public $row_team;
    private $matches;

    public function __construct($matches, $team)
    {
        $this->matches = $matches;
        //$this->team = mb_strcut($team,2);
        $this->team = $team;
        $this->row_team = $team;
    }

    public function sumTeam(): string
    {
        /**
         * @var Matches $match
         */
        foreach ($this->matches as $match){

            //$this->team = preg_replace('/\s*\([^)]*\)/', '', mb_strcut($team,2));
            //sudo apt-get install php-mbstring
            $host = mb_strcut($match->host,2);
            //var_dump($match); exit;
            $guest = mb_substr($match->guest, 0, -1);
            if($host == $this->team ||
                preg_replace('/\s*\([^)]*\)/', '', $host) === $this->team){
                $this->games++;
                $this->gets += $match->gett;
                $this->lets += $match->lett;

                if($match->gett > $match->lett){
                    $this->victories++;
                    $this->balls += 3;
                }
                elseif($match->gett == $match->lett){
                    $this->ties++;
                    $this->balls++;
                }
                else $this->defeats++;
            }
            elseif($guest == $this->team || preg_replace('/\s*\([^)]*\)/', '', $guest) === $this->team){
                $this->games++;
                $this->lets += $match->gett;
                $this->gets += $match->lett;

                if($match->gett < $match->lett){
                    $this->victories++;
                    $this->balls += 3;
                }
                elseif($match->gett == $match->lett){
                    $this->ties++;
                    $this->balls++;
                }
                else $this->defeats++;


            }

        }

        return $this->games.' +'.
            $this->victories.' ='.
            $this->ties.' -'.
            $this->defeats.' '.
            $this->gets.':'.
            $this->lets. ' '.
            $this->balls;
    }

    public function sumTeamBets(): array
    {
        /**
         * @var Matches $match
         */
        foreach ($this->matches as $match){
            //var_dump($match->bet_h); exit;
            if($match->bet_h != 0 && $match->bet_n != 0 && $match->bet_g != 0) {
                //$this->team = preg_replace('/\s*\([^)]*\)/', '', mb_strcut($team,2));
                //sudo apt-get install php-mbstring
                $host = mb_strcut($match->host,2);
                //var_dump($this->team); exit;
                $guest = mb_substr($match->guest, 0, -1);
                if($host == $this->team ||
                    preg_replace('/\s*\([^)]*\)/', '', $host) === $this->team){
                    //var_dump($host); exit;
                    $this->games++;

                    if($match->gett > $match->lett){
                        $this->vic_bets += $match->bet_h;
                        $this->n_bets -= 1;
                        $this->def_bets -= 1;
                    }
                    elseif($match->gett == $match->lett){
                        $this->vic_bets -= 1;
                        $this->n_bets += $match->bet_n;
                        $this->def_bets -= 1;
                    }
                    else {
                        $this->vic_bets -= 1;
                        $this->n_bets -= 1;
                        $this->def_bets += $match->bet_g;
                    }
                }
                elseif($guest == $this->team || preg_replace('/\s*\([^)]*\)/', '', $guest) === $this->team){
                    //var_dump($guest); exit;
                    $this->games++;
                    if($match->gett < $match->lett){
                        $this->vic_bets += $match->bet_g;
                        $this->n_bets -= 1;
                        $this->def_bets -= 1;
                    }
                    elseif($match->gett == $match->lett){
                        $this->vic_bets -= 1;
                        $this->n_bets += $match->bet_n;
                        $this->def_bets -= 1;
                    }
                    else {
                        $this->vic_bets -= 1;
                        $this->n_bets -= 1;
                        $this->def_bets += $match->bet_n;
                    }
                }
            }
            else continue;

        }

        return [$this->games, round($this->vic_bets,2), round($this->n_bets,2), round($this->def_bets,2) , $this->team];

    }

}