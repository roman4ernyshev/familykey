<?php

namespace core\services;

use core\readModels\Rockncontroll\DataForGraphicRepository;

class GraphicManager
{
    public $data;

    public function __construct(DataForGraphicRepository $data)
    {
        $this->data = $data;
    }

    public function weightToMonthFromCurrentAndPreviousYearAsJson(){

     return json_encode(
            [
                $this->fillArrayWithDataFromYear(date("Y"), 'weight'),
                $this->fillArrayWithDataFromYear(date("Y")-1, 'weight')
            ]
        );
    }

    public function coldWaterToMonthFromCurrentAndPreviousYearAsJson(){

        return json_encode(
            [
                $this->fillArrayWithDataFromYear(date("Y"), 'coldWatter'),
                $this->fillArrayWithDataFromYear(date("Y")-1, 'coldWatter')
            ]
        );
    }

    public function warmWaterToMonthFromCurrentAndPreviousYearAsJson(){

        return json_encode(
            [
                $this->fillArrayWithDataFromYear(date("Y"), 'warmWatter'),
                $this->fillArrayWithDataFromYear(date("Y")-1, 'warmWatter')
            ]
        );
    }

    public function weightMichael(){
        return json_encode(
            [
                $this->fillArrayWithDataFromYear(date("Y"), 'MichaelWeight'),
                $this->fillArrayWithDataFromYear(date("Y")-1, 'MichaelWeight')
            ]
        );
    }

    public function boughtEur(){
        return json_encode(
            [
                $this->fillArrayWithDataFromYear(date("Y"), 'BoughtEur'),
                $this->fillArrayWithDataFromYear(date("Y")-1, 'BoughtEur')
            ]
        );
    }

    public function boughtShop($shop_id){
        $params['shop_id'] = $shop_id;
        return json_encode(
            [
                $this->fillArrayWithDataFromYearParams(date("Y"), $params),
                $this->fillArrayWithDataFromYearParams(date("Y")-1, $params)
            ]
        );
    }

    public function boughtWare($ware_id){
        $params['ware_id'] = $ware_id;
        return json_encode(
            [
                $this->fillArrayWithDataFromYearParams(date("Y"), $params),
                $this->fillArrayWithDataFromYearParams(date("Y")-1, $params)
            ]
        );
    }

    public function boughtCat($cat_id){
        $params['cat_id'] = $cat_id;
        return json_encode(
            [
                $this->fillArrayWithDataFromYearParams(date("Y"), $params),
                $this->fillArrayWithDataFromYearParams(date("Y")-1, $params)
            ]
        );
    }

    public function heizung(){
        return json_encode(
            [
                $this->fillArrayWithDataFromYear(date("Y"), 'heizung'),
                $this->fillArrayWithDataFromYear(date("Y")-1, 'heizung')
            ]
        );
    }

    private function fillArrayWithDataFromYear($year, $strategy): array
    {
        $res=[];
        for($i=1; $i<=12; $i++) {
            if($i<10) $month = '0'.$i;
            else $month = $i;

            $res['name'] = $year;
            $res['data'][] = $this->data->avgScalarByYearAndMonth($strategy, $year, $month);
        }

        return $res;
    }

    private function fillArrayWithDataFromYearParams($year, $params=[]): array
    {
        $res=[];
        for($i=1; $i<=12; $i++) {
            if($i<10) $month = '0'.$i;
            else $month = $i;

            $res['name'] = $year;
            $res['data'][] = $this->data->avgScalarByYearAndMonthParams($params, $year, $month);
        }

        return $res;
    }

}