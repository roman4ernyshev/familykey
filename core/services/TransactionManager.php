<?php

namespace core\services;

use core\dispatchers\DeferredEventDispatcher;
use http\Exception\RuntimeException;
use yii\base\ErrorException;
use yii\db\Exception;

class TransactionManager
{
    private $dispatcher;

    public function __construct(DeferredEventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function wrap(callable $function): void
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $this->dispatcher->defer();
            $function();
            $transaction->commit();
            $this->dispatcher->release();
        } catch (\Exception $e) {
            try {
                $transaction->rollBack();
            } catch (ErrorException $e) {
                throw new RuntimeException('Transaction Error');
            }
            $this->dispatcher->clean();
            throw $e;
        }
    }
}