<?php

namespace core\readModels;


use core\entities\Listener\RadioItems;
use yii\sphinx\Query;

class RadioReadRepository
{
    public function getAllItemsIndexes()
    {
        $txt = ['папа', 'мама'];
        $query  = new Query();
        $query_items_ids = [];

        foreach ($txt as $item) {
            $query_items_ids += $query->from('items')
                ->match($item)
                ->limit(5)
                ->all();
        }
        return $query_items_ids;
    }

    public function getAllItems()
    {
        $res = [];
        $items = $this->getAllItemsIndexes();
        foreach ($items as $item){
            $res[] = RadioItems::findOne($item);
        }
        return $res;
    }

}