<?php

namespace core\readModels\Word;

use core\entities\Word\DefinitionThema;
use core\entities\Word\Word;
use yii\db\Expression;

class WordCrosswordRepository
{
    public function getVertRandWordWithThema($thema_id, $lang=0): array
    {
        if($thema_id){
            return $this->getWordByThemaAndLang($thema_id, 10, 13);
        }
        $word = Word::find()
            ->select('id, title')
            ->where('CHAR_LENGTH(title) < 13')
            ->andWhere('CHAR_LENGTH(title) > 10')
            ->orderBy(new Expression('random()'))
            ->asArray()->one();
        return [
            'id' => $word['id'],
            'string' => $word['title']
        ];
    }

    public function getWordByThemaAndLang($thema_id, $lang_from=0, $lang_to=12):  array
    {
        $word = DefinitionThema::find()
            ->select('definitions.*, word.*, definition_crossword_thema.*, word.title as word')
            ->leftJoin('definitions', 'definitions.id = definition_crossword_thema.definition_id')
            ->leftJoin('word', 'word.id = definitions.word_id')
            ->where("CHAR_LENGTH(word.title) < $lang_to")
            ->andWhere("CHAR_LENGTH(word.title) > $lang_from")
            ->andWhere("crossword_thema_id = $thema_id")
            ->orderBy(new Expression('random()'))
            ->asArray()
            ->one();
        //var_dump($thema_id); exit;
        return [
            'id' => $word['word_id'],
            'string' => $word['word']
        ];

    }


    public function getAllWordsByThema($thema_id): array
    {
        //var_dump($thema_id); exit;
        if($thema_id) {
            $words = DefinitionThema::find()
                ->select('definitions.*, word.*, definition_crossword_thema.*, word.title as word')
                ->leftJoin('definitions', 'definitions.id = definition_crossword_thema.definition_id')
                ->leftJoin('word', 'word.id = definitions.word_id')
                ->where("crossword_thema_id = $thema_id")
                ->andWhere('CHAR_LENGTH(word.title) < 10')
                ->asArray()
                ->all();

            $res = [];
            shuffle($words);

            foreach ($words as $key=>$word){
                $res[$key]['id'] = $word['word_id'];
                $res[$key]['title'] = $word['word'];
            }
            //var_dump($res); exit;
            return $res;
        }

        $words = Word::find()->asArray()->all();
        shuffle($words);
        return $words;

    }

    public function getLangWordsByThema($thema_id): array
    {
        if($thema_id) {
            return $this->getWordByThemaAndLang($thema_id, 11, 20);
        }
        $word = Word::find()
            ->select('id, title')
            ->where('CHAR_LENGTH(title) > 10')
            ->orderBy(new Expression('random()'))
            ->asArray()->one();
        return [
            'id' => $word['id'],
            'string' => $word['title']
        ];

    }

}