<?php

namespace core\readModels\Soccer;

use core\entities\Soccer\Match;
use core\entities\Soccer\TeamSummary;
use yii\data\Sort;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class MatchReadRepository
{
    public function getMatchesByTournamentId($id): DataProviderInterface
    {
        $query = Match::find()->where(['tournament_id' => $id]);
        return $this->getMatchesProvider($query);
    }

    public function getMatchesByTour($tour, $id): DataProviderInterface
    {
        /*
         * use yii\data\Sort;

$sort = new Sort([
    'attributes' => [
        'age',
        'name' => [
            'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
            'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
            'default' => SORT_DESC,
            'label' => 'Name',
        ],
    ],
]);

$articles = Article::find()
    ->where(['status' => 1])
    ->orderBy($sort->orders)
    ->all();
         */
        $query = Match::find()
            ->select([
                new Expression("STR_TO_DATE(date, '%d.%m.%Y')"),
            ])
            ->where(['tournament_id' => $id])
            ->andWhere(['like', 'tournament', addslashes($tour), false])
            ->andWhere('tournament NOT LIKE "%Понижение%"')
            ->orderBy('date');

        return $this->getMatchesProvider($query);
    }

    public function allTournamentMatchesToTour($tour, $id)
    {
        $max_id = Match::find()
            ->where(['tournament_id' => $id])
            ->andWhere(['like', 'tournament', addslashes($tour), false])
            ->limit(1)
            ->scalar();

        return Match::find()
            ->where(['tournament_id' => $id])
            ->andWhere('tournament NOT LIKE "%Понижение%"')
            ->andWhere('id >= '.$max_id)
            ->all();
    }

    public function allTournamentMatches($id)
    {
        return Match::find()
            ->where(['tournament_id' => $id])
            ->andWhere('tournament NOT LIKE "%Понижение%"')
            ->orderBy('id DESC')
            ->all();
    }

    public function getAllTournamentTeams($id)
    {
        $teams = Match::find()
            ->select([
                new Expression("SUBSTRING_INDEX(host, ' (', 1) as host"),
            ])
            ->where(['tournament_id' => $id])
            ->andWhere('tournament NOT LIKE "%Понижение%"')
            //->andWhere('host NOT LIKE "%(%"')
            ->distinct()
            ->all();
        return ArrayHelper::getColumn($teams, 'host');
    }

    public function getAllTeams()
    {
        $teams = Match::find()
            ->select('host')
            ->andWhere('tournament NOT LIKE "%Понижение%"')
            ->andWhere('host NOT LIKE "%(%"')
            ->distinct()
            ->all();
        return ArrayHelper::getColumn($teams, 'host');
    }

    public function getMatchesByTeamName($name, $tournament_id): DataProviderInterface
    {
        $query = Match::find()
            ->select([
                new Expression("*, STR_TO_DATE(date, '%d.%m.%Y') as sort_date"),
            ])
            ->filterWhere(['like', 'host', addslashes($name), false])
            ->orWhere(['like', 'host', addslashes($name).' (%', false])
            ->orWhere(['like', 'guest', substr(addslashes($name).'_',2), false])
            ->orWhere(['like', 'guest', substr(addslashes($name).'_(%',2), false])
            ->orWhere(['like', 'guest', addslashes($name), false])
            ->andWhere(['tournament_id' => $tournament_id])
            ->orderBy('sort_date ASC');

        return $this->getMatchesProvider($query);
    }

    public function getMatchesOnlyTeamVictory($name, $tournament_id): DataProviderInterface
    {
        $query = Match::find()
            ->select([
                new Expression("*, STR_TO_DATE(date, '%d.%m.%Y') as sort_date"),
            ])
            ->where(
                ['OR',
                    ['AND',
                        ['OR',
                            ['like', 'host', addslashes($name), false],
                            ['like', 'host', addslashes($name).' (%', false]
                        ],
                        new Expression('`gett` > `lett`'),
                        ['tournament_id' => $tournament_id]
                    ],
                    ['AND',
                        ['OR',
                            ['like', 'guest', substr(addslashes($name).'_',2), false],
                            ['like', 'guest', substr(addslashes($name).'_(%',2), false]
                        ],
                        new Expression('`gett` < `lett`'),
                        ['tournament_id' => $tournament_id]
                    ],
                ])
            ->andWhere('tournament NOT LIKE "%Понижение%"')
            ->orderBy('sort_date ASC');

        return $this->getMatchesProvider($query);
    }

    public function getMatchesOnlyTeamTie($name, $tournament_id): DataProviderInterface
    {
        $query = Match::find()
            ->select([
                new Expression("*, STR_TO_DATE(date, '%d.%m.%Y') as sort_date"),
            ])
            ->where(
                ['OR',
                    ['AND',
                        ['OR',
                            ['like', 'host', addslashes($name), false],
                            ['like', 'host', addslashes($name).' (%', false]
                        ],
                        new Expression('`gett` = `lett`'),
                        ['tournament_id' => $tournament_id]
                    ],
                    ['AND',
                        ['OR',
                            ['like', 'guest', substr(addslashes($name).'_',2), false],
                            ['like', 'guest', substr(addslashes($name).'_(%',2), false]
                        ],
                        new Expression('`gett` = `lett`'),
                        ['tournament_id' => $tournament_id]
                    ],
                ])
            ->andWhere('tournament NOT LIKE "%Понижение%"')
            ->orderBy('sort_date ASC');

        return $this->getMatchesProvider($query);
    }

    public function getMatchesOnlyTeamDefeat($name, $tournament_id): DataProviderInterface
    {
        $query = Match::find()
            ->select([
                new Expression("*, STR_TO_DATE(date, '%d.%m.%Y') as sort_date"),
            ])
            ->where(
                ['OR',
                    ['AND',
                        ['OR',
                            ['like', 'host', addslashes($name), false],
                            ['like', 'host', addslashes($name).' (%', false]
                        ],
                        new Expression('`gett` < `lett`'),
                        ['tournament_id' => $tournament_id]
                    ],
                    ['AND',
                        ['OR',
                            ['like', 'guest', substr(addslashes($name).'_',2), false],
                            ['like', 'guest', substr(addslashes($name).'_(%',2), false]
                        ],
                        new Expression('`gett` > `lett`'),
                        ['tournament_id' => $tournament_id]
                    ],
                ])
            ->andWhere('tournament NOT LIKE "%Понижение%"')
            ->orderBy('sort_date ASC');

        return $this->getMatchesProvider($query);
    }

    public function getMatchesByData($data): DataProviderInterface
    {
        $query = Match::find()
            ->select([
                new Expression("*, STR_TO_DATE(date, '%d.%m.%Y') as sort_date"),
            ])
            ->where(['like', 'date', addslashes($data)])
            ->orderBy('sort_date ASC');
        //echo $data.' '.$query->createCommand()->sql; exit;
        //echo $query->createCommand()->getRawSql(); exit;
        //var_dump($query); exit;

        return $this->getMatchesProvider($query);
    }

    public function getMatchesTenRandomByData($data): DataProviderInterface
    {
        $query = Match::find()
            ->where(['like', 'date', addslashes($data)])
            ->orderBy(new Expression('rand()'))
            ->limit(10);

        return $this->getMatchesLimitProvider($query);
    }

    public function getMatchesOfTwoTeams($team1, $team2): DataProviderInterface
    {

        $query = Match::find()
            ->select([
                new Expression("*, STR_TO_DATE(date, '%d.%m.%Y') as sort_date"),
            ])
            ->where(['OR',
                ['AND',
                    ['OR',
                        ['like', 'host', addslashes($team1), false],
                        ['like', 'host', addslashes($team1).' (%', false]
                    ],
                    ['OR',
                        ['like', 'guest', substr(addslashes($team2).'_',2), false],
                        ['like', 'guest', substr(addslashes($team2).'_(%',2), false]
                    ]
                ],
                ['AND',
                    ['OR',
                        ['like', 'host', addslashes($team2), false],
                        ['like', 'host', addslashes($team2).' (%', false]
                    ],
                    ['OR',
                        ['like', 'guest', substr(addslashes($team1).'_',2), false],
                        ['like', 'guest', substr(addslashes($team1).'_(%',2), false]
                    ]
                ],
            ])->orderBy('sort_date ASC');

        return $this->getMatchesProvider($query);

    }

    public function allMatchesOfTwoTeams($team1, $team2)
    {
        return Match::find()
            ->where(['OR',
                ['AND',
                    ['OR',
                        ['like', 'host', addslashes($team1), false],
                        ['like', 'host', addslashes($team1).' (%', false]
                    ],
                    ['OR',
                        ['like', 'guest', substr(addslashes($team2).'_',2), false],
                        ['like', 'guest', substr(addslashes($team2).'_(%',2), false]
                    ]
                ],
                ['AND',
                    ['OR',
                        ['like', 'host', addslashes($team2), false],
                        ['like', 'host', addslashes($team2).' (%', false]
                    ],
                    ['OR',
                        ['like', 'guest', substr(addslashes($team1).'_',2), false],
                        ['like', 'guest', substr(addslashes($team1).'_(%',2), false]
                    ]
                ],
            ])
            ->all();
    }


    public function getSortedTeamsSummary($team_names, $tournament_matches): array
    {
        $summary = [];
        /**
         * @var string $team
         * @var TeamSummary $sumTeam
         */
        foreach ($team_names as $team){
            //if(preg_match('/\s*\([^)]*\)/', $team)) continue;
            $team_summary = new TeamSummary($tournament_matches, $team);
            $sumTeam = $team_summary->sumTeam();
            if($sumTeam->games) $summary[] = $sumTeam;
        }

        usort($summary, function($a, $b)
        {
            return $this->sortByBalls($a, $b);
        });

        return $summary;
    }

    private function getMatchesProvider(ActiveQuery $query): DataProviderInterface
    {
         $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [45, 50],
            ]
         ]);

         return $dataProvider;
    }

    private function getMatchesLimitProvider(ActiveQuery $query): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }


    /**
     * @param $f1 TeamSummary
     * @param $f2 TeamSummary
     * @return int
     */
    private function sortByBalls($f1,$f2)
    {
        if($f1->balls > $f2->balls) return -1;
        elseif($f1->balls < $f2->balls) return 1;
        else return 0;
    }

}