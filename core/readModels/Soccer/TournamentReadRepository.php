<?php
namespace core\readModels\Soccer;

use core\entities\Soccer\Tournament;
use yii\helpers\ArrayHelper;

class TournamentReadRepository
{
    public function getDistinctCountryIdsOfTournaments()
    {
        $tournaments = Tournament::find()->select('country_id')->distinct()->all();
        return ArrayHelper::getColumn($tournaments, 'country_id');
    }

    public function getTournamentsByCountryId($id): array
    {
       return Tournament::find()->where(['country_id' => $id])->all();
    }

    public function getAllTournamentsNamesByCountryId($id): array
    {
        return ArrayHelper::getColumn(
            Tournament::find()
                ->select('name')
                ->where(['country_id' => $id])
                ->all(),
            'name'
        );
    }

    public function getIdByTournamentName($name): ?int
    {
        $tournament = Tournament::find()->filterWhere(['like', 'name', $name])->one();
        return ($tournament instanceof Tournament) ? $tournament->id : null;
        //return ArrayHelper::map($tournaments, 'id', 'name');
    }

    public function getNameById($id)
    {
        $tournament = Tournament::findOne($id);
        return ($tournament instanceof Tournament) ? $tournament->name : null;
    }

}