<?php

namespace core\readModels;

use core\entities\Product\RocknControllProduct;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class RocknControllProductReadRepository
{

    public function getAll(): DataProviderInterface
    {
        $query = RocknControllProduct::find();
        return $this->getProvider($query);
    }


    private function getProvider(ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,

            'pagination' => [
                'pageSizeLimit' => [100, 100],
            ]
        ]);
    }


}