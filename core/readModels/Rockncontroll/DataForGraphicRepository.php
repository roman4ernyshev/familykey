<?php

namespace core\readModels\Rockncontroll;

use core\entities\Rockncontroll\BoughtEur;
use core\entities\Rockncontroll\DiaryActs;
use core\entities\Rockncontroll\DiaryRecDayParams;
use core\entities\Rockncontroll\Products;
use core\entities\Rockncontroll\Snapshot;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class DataForGraphicRepository
{

    public function avgScalarByYearAndMonth($strategy, $year, $month){
        if($strategy==='weight') return $this->weightByYearAndMonth($year, $month);
        if($strategy==='coldWatter') return $this->waterRCSAueByYearAndMonth($year, $month);
        if($strategy==='warmWatter') return $this->waterRCSAueByYearAndMonth($year, $month, 0);
        if($strategy==='MichaelWeight') return $this->weightFromRecParamsByYearAndMonth($year, $month);
        if($strategy==='BoughtEur') return $this->boughtEuroByYearAndMonth($year, $month);
        if($strategy==='heizung') return $this->heizungByYearAndMonth($year, $month);
        return 0;
    }

    public function avgScalarByYearAndMonthParams($params, $year, $month){
        if(array_key_exists('shop_id', $params)) return $this->boughtShopEuroByYearAndMonth($year, $month, $params['shop_id']);
        if(array_key_exists('ware_id', $params)) return $this->boughtWareEuroByYearAndMonth($year, $month, $params['ware_id']);
        if(array_key_exists('cat_id', $params)) return $this->boughtCatEuroByYearAndMonth($year, $month, $params['cat_id']);
    }

    public function weightByYearAndMonth($year, $month): float
    {
        return (
            (float)Snapshot::find()
                ->select(['AVG(weight)'])
                ->where('date like "%'.$year.'-'.$month.'-%"')
                ->scalar());
    }

    public function weightFromRecParamsByYearAndMonth($year, $month): float
    {
        $first_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['start'];
        $last_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['end'];

        return (float)DiaryRecDayParams::find()
            ->select(['AVG(value)'])
            ->where(['day_param_id' => 24])
            ->andWhere(['>', 'act_id' , $first_from_interval])
            ->andWhere(['<', 'act_id' , $last_from_interval])
            ->scalar();
    }

    public function heizungByYearAndMonth($year, $month): float
    {
        $first_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['start'];
        $last_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['end'];

        return (float)DiaryRecDayParams::find()
            ->select(['SUM(value)'])
            ->where(['day_param_id' => 65])
            ->andWhere(['>', 'act_id' , $first_from_interval])
            ->andWhere(['<', 'act_id' , $last_from_interval])
            ->scalar();

    }

    public function boughtEuroByYearAndMonth($year, $month): float
    {
        $first_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['start'];
        $last_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['end'];

        return (float)BoughtEur::find()
            ->select(['SUM(spent)'])
            ->andWhere(['>', 'act_id' , $first_from_interval])
            ->andWhere(['<', 'act_id' , $last_from_interval])
            ->scalar();

    }

    public function boughtShopEuroByYearAndMonth($year, $month, $shop_id): float
    {
        $first_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['start'];
        $last_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['end'];

        return (float)BoughtEur::find()
            ->select(['SUM(spent)'])
            ->andWhere(['>', 'act_id' , $first_from_interval])
            ->andWhere(['<', 'act_id' , $last_from_interval])
            ->andWhere(['shop_id' => $shop_id])
            ->scalar();

    }

    public function boughtWareEuroByYearAndMonth($year, $month, $product_id): float
    {
        $first_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['start'];
        $last_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['end'];

        return (float)BoughtEur::find()
            ->select(['SUM(spent)'])
            ->andWhere(['>', 'act_id' , $first_from_interval])
            ->andWhere(['<', 'act_id' , $last_from_interval])
            ->andWhere(['product_id' => $product_id])
            ->scalar();

    }

    public function boughtCatEuroByYearAndMonth($year, $month, $cat_id): float
    {
        $first_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['start'];
        $last_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['end'];

        $ids = Products::find()
            ->where(['cat_id' => $cat_id])
            ->select('id')
            ->column();

        $cat_products_ids = implode(',', $ids);

        return (float)BoughtEur::find()
            ->select(['SUM(spent)'])
            ->andWhere(['>', 'act_id' , $first_from_interval])
            ->andWhere(['<', 'act_id' , $last_from_interval])
            ->andWhere('product_id IN (' . $cat_products_ids . ')')
            ->andWhere('id > 28')
            ->scalar();

    }

    private function getStartAndEndPointActForMonatYear($year, $month): array
    {
        $start_time = mktime(0, 0, 0, $month, 1, $year);
        $last_time = mktime(0, 0, 0, $month+1, 1, $year);

        return [
            'start' => $this->getRecordedParamsActIdByTime($start_time),
            'end' => $this->getRecordedParamsActIdByTime($last_time)
        ];
    }

    public function waterRCSAueByYearAndMonth($year, $month, $cold=1): float
    {

        $first_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['start'];
        $last_from_interval = $this->getStartAndEndPointActForMonatYear($year, $month)['end'];

        if($cold){
            $first = DiaryRecDayParams::find()
                ->where(['day_param_id' => 63])
                ->andWhere(['>', 'act_id' , $first_from_interval])
                ->one();

            $last = DiaryRecDayParams::find()
                ->where(['day_param_id' => 63])
                ->andWhere(['>', 'act_id' , $last_from_interval])
                ->one();
        }
        else {
            $first = DiaryRecDayParams::find()
                ->where(['day_param_id' => 64])
                ->andWhere(['>', 'act_id' , $first_from_interval])
                ->one();

            $last = DiaryRecDayParams::find()
                ->where(['day_param_id' => 64])
                ->andWhere(['>', 'act_id' , $last_from_interval])
                ->one();
        }


        $first_val = @$first->attributes['value'];
        $last_val = @$last->attributes['value'];

        if($first_val && $last_val) return ($last_val-$first_val);
        else return 0;

    }

    private function getRecordedParamsActsByStartAndEnd($start_timestamp, $end_timestamp): array
    {
        return
            ArrayHelper::map(
                DiaryActs::find()
                    ->where("time > $start_timestamp and time < $end_timestamp and user_id = 8 and model_id = 4")
                    ->all(), 'id', 'id');
    }

    private function getRecordedParamsActIdByTime($time){
       $id = @DiaryActs::find()
           ->where(['>', 'time' , $time])
           ->one()->id;

       return $id;

    }


}