<?php
namespace core\readModels\Rockncontroll;

use core\entities\RockncontrollNew\ManyNewss;
use core\entities\RockncontrollNew\NsbNewss;

class NsbNewsReadRepository
{
    public function getLast()
    {
        $news = [];

        /*
        $news_nb = NsbNewss::find()
            ->orderBy('id DESC')
            ->limit(30)
            ->all();

        foreach ($news_nb as $n){
            $news[] = $n->pud_date.' :: '.$n->title.' :: '.$n->description;
        }
        */

        $news_s = ManyNewss::find()
            ->orderBy('id DESC')
            ->limit(70)
            ->all();
        foreach ($news_s as $n){
            $news[] = $n->pud_date.' :: '.$n->title.' :: '.$n->description;
        }

        return $news[rand(0, count($news)-1)];
    }

}