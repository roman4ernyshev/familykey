<?php

namespace core\readModels;

use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\Source;
use core\forms\article\SearchForm;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class ItemReadRepository
{
    private $sources = [];

    function __construct()
    {
        $this->getAllSourcesIds();
    }

    public function getAll(): DataProviderInterface
    {
        $query = Items::find();
        return $this->getArticleSourceProvider($query);
    }

    public function getAllSourcesIds(): void
    {
        $sources = Items::find()->select('source_id')->distinct()->all();

        /**
         * @var Items $sources
         */
        foreach ($sources as $source){
            array_push($this->sources, $source->source_id);
        }

    }

    public function getAllSourcesTitle(): array
    {
        return Source::find()
            ->select('title')
            ->where(['in', 'id', $this->sources])
            ->all();
    }

    public function findSource(SearchForm $form): ?Source
    {
        $source = Source::find()
            ->where(['in', 'id', $this->sources])
            ->andWhere(['title' => $form->title])
            ->one();

        if($source instanceof Source) return $source;
        else return null;
    }

    public function getSourceById($id): ?Source
    {
        return Source::findOne($id);
    }

    public function getProvidedContentByArticleId($source_id): DataProviderInterface
    {
        $query = Items::find()->where(['source_id' => $source_id])->orderBy('id ASC');
        return $this->getArticleSourceProvider($query);
    }

    private function getArticleSourceProvider(ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [5, 10],
                'route' => 'item/source',
            ]

        ]);
    }

}