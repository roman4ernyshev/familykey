<?php

namespace core\readModels;

use core\entities\Notion\Category;
use core\entities\Notion\Notion;
use core\entities\Notion\NotionUserMarks;
use core\repositories\NotFoundException;
use SebastianBergmann\Timer\RuntimeException;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;

class NotionReadRepository
{
    public function getAll(): DataProviderInterface
    {
        $query = Notion::find();
        return $this->getProvider($query);
    }

    public function getAllDescriptions(): array
    {
        return Notion::find()->select('description')->all();

    }

    public function getAllAnswers($category_id): array
    {
        return Notion::find()
            ->select('answer')
            ->where(['category_id' => $category_id])
            ->all();
    }

    public function getRandomItem(): Notion
    {
        //@TODO rand count
        $word = Notion::find()
            //->where(['shown' => 0])
            ->orderBy(['rand()' => SORT_DESC])
            ->one();
        //var_dump($word); exit;
        return $word;
    }

    public function getNextNotion($user_id, $category_id)
    {

        if(!$notion_id = $this->getLastUserNotionId($user_id)) {
            $notion_id = $this->getFirstNotion($category_id)->id;
            return $this->getById($notion_id);
        }

        if($next_notion_id = $this->getNextNotionId($notion_id, $category_id))
            return $this->getById($next_notion_id);

    }

    public function isTranslationRight($translation, $word_id): bool
    {
        if(!($word = $this->getById($word_id)) || !($trans_word = $this->getByAnswer($translation))) return false;
        if($word->answer == $trans_word->answer) {
            return true;
        }
        return false;
    }

    public function isUserMarked($user_id, $word_id, $mark): bool
    {
        $user_mark = NotionUserMarks::create((int)$user_id, $word_id, $mark);
        if(!$user_mark->save()) return null;
        else return true;
    }

    public function isCategoryExists($slug): bool
    {
        return (bool)Category::find()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }

    public function getCategoryIdByName($name): ?int
    {
        return Category::find()->andWhere(['name' => $name])->andWhere(['>', 'depth', 0])->one()->id;
    }

    public function getBadOrGoodLearnedNotionLimit($user_id, $bad=1, $limit=20, $category_id)
    {
        return NotionUserMarks::find()
            ->select(['notion_id, user_id, COUNT(time) as cnt, SUM(mark) as sum'])
            ->where(['user_id' => $user_id])
            ->innerJoin('notions', 'notions.category_id='.$category_id.' AND notions.id=notion_id')
            ->groupBy('notion_id')
            ->orderBy($bad ? 'sum ASC' : 'sum DESC')
            ->limit($limit)
            ->all();
    }

    public function getBySlug($slug, $category_id): ?Notion
    {
        return Notion::find()->where("slug LIKE '".$slug."' AND category_id = $category_id ")->one();
    }


    private function getProvider(\yii\db\ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,

            'pagination' => [
                'pageSizeLimit' => [15, 100],
            ]
        ]);
    }

    private function getById($id): Notion
    {
        return Notion::findOne($id);
    }


    private function getByDescription($description): ?Notion
    {
        if($item = Notion::find()->where('description like "'.trim($description).'"')->one()) {
            return $item;
        }
        else return null;
    }

    private function getByAnswer($answer): ?Notion
    {
        if($item = Notion::find()->where('answer like "'.trim($answer).'"')->one()) {
            return $item;
        }
        else return null;
    }

    private function getLastUserNotionId($user_id)
    {
        $max_user_notion = NotionUserMarks::find()
            ->where(['user_id' => $user_id])
            ->orderBy('time DESC')
            ->one();
        return $max_user_notion ? $max_user_notion->notion_id : null;
    }

    private function getFirstNotion($category_id): ?Notion
    {
        $min_id = Notion::find()->where(['category_id' => $category_id])->min('id');
        return Notion::findOne($min_id);
    }

    private function getNextNotionId($id, $category_id): int
    {
        if($id > $this->getMaxNotionId()) return $this->getFirstNotion($category_id)->id;
        return Notion::findOne(['id' => $id+1, 'category_id' => $category_id]) ?
            Notion::findOne(['id' => $id+1, 'category_id' => $category_id])->id :
            $this->getNextNotionId($id+1, $category_id);
    }

    private function getMaxNotionId()
    {
        return Notion::find()->max('id');
    }


}