<?php

namespace core\readModels;

use core\entities\Project\Project;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;

class ProjectReadRepository
{
    public function getAll()
    {
        $projects = Project::find()->all();
        return $projects;
    }

    private function getProvider(\yii\db\ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,

            'pagination' => [
                'pageSizeLimit' => [15, 100],
            ]
        ]);
    }

}