<?php

namespace core\readModels;

use core\entities\Schema\DaySchema;
use yii\redis\Connection;


class SchemaReadRepository
{

    private $redis;

    public function __construct()
    {
        $redis = new Connection();
        $this->redis = $redis;
    }

    public function load(): array
    {
        $result = [];

        $keys = DaySchema::$keys;
        foreach ($keys as $key){
            $result[$key] = $this->redis->get($key);
        }

        return $result;
    }

    public function save(array $params): void
    {
        foreach ($params as $key => $param) {
            $res[$key] = $param;
            $this->redis->set($key, $param);
        }

    }


}