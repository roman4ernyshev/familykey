<?php

namespace core\readModels;


use core\entities\Article\Article;
use core\entities\Article\ArticleContent;
use core\entities\Article\Source;
use core\forms\article\SearchForm;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class ArticleReadRepository
{
    private $sources = [];

    function __construct()
    {
        $this->getAllSourcesIds();
    }

    public function getAll(): DataProviderInterface
    {
        $query = Article::find();
        return $this->getArticleSourceProvider($query);
    }

    public function getAllContent(): DataProviderInterface
    {
        $query = ArticleContent::find();
        return $this->getArticleSourceProvider($query);
    }

    public function getAllSourcesIds(): void
    {
        $sources = ArticleContent::find()->select('source_id')->distinct()->all();

        /**
         * @var ArticleContent $sources
         */
        foreach ($sources as $source){
            array_push($this->sources, $source->source_id);
        }

    }

    public function getAllSourcesTitle(): array
    {
        return Source::find()
            ->select('title')
            ->where(['in', 'id', $this->sources])
            ->all();
    }

    public function findSource(SearchForm $form): ?Source
    {
        $source = Source::find()
            ->where(['in', 'id', $this->sources])
            ->andWhere(['like', 'title', $form->title])
            ->one();

        if($source instanceof Source) return $source;
        else return null;
    }

    public function getProvidedContentByArticleId($source_id): DataProviderInterface
    {
        $query = ArticleContent::find()->where(['source_id' => $source_id])->orderBy('id ASC');
        return $this->getArticleSourceProvider($query);
    }

    public function getSourceById($id): ?Source
    {
        return Source::findOne($id);
    }

    private function getArticleSourceProvider(ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [5, 10],
                'route' => 'article/source',
            ]

        ]);
    }

}