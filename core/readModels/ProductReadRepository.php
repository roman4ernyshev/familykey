<?php

namespace core\readModels;


use core\entities\Product\Category;
use core\entities\Product\ProductsConst;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class ProductReadRepository
{
    public function getAll(): DataProviderInterface
    {
        $query = ProductsConst::find();
        return $this->getProvider($query);
    }

    public function isCategoryExists($slug): bool
    {
        return (bool)Category::find()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }

    private function getProvider(ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,

            'pagination' => [
                'pageSizeLimit' => [15, 100],
            ]
        ]);
    }

}