<?php

namespace core\readModels;

use core\entities\Country;
use yii\helpers\ArrayHelper;

class CountryReadRepository
{

    public function getCountriesByIds(array $ids=[]): array
    {
        $res = [];
        foreach ($ids as $id){
           $res[] = Country::findOne($id);
        }
        return $res;
    }

    public function getAllCountriesNames(): array
    {
        $countries = Country::find()->select('name')->all();
        return ArrayHelper::getColumn($countries, 'name');
    }

    public function getIdByCountryName($name): ?int
    {
        $country = Country::find()->filterWhere(['like', 'name', $name])->one();
        return ($country instanceof Country) ? $country->id : null;
        //return ArrayHelper::map($tournaments, 'id', 'name');
    }




}