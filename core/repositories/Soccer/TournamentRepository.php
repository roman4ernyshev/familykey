<?php
namespace core\repositories\Soccer;

use core\dispatchers\EventDispatcher;
use core\entities\Soccer\Tournament;
use core\repositories\events\EntityPersisted;
use core\repositories\events\EntityRemoved;
use core\repositories\NotFoundException;

class TournamentRepository
{
    private $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function get($id): Tournament
    {
        if (!$tournament = Tournament::findOne($id)) {
            throw new NotFoundException('Notion is not found.');
        }
        return $tournament;
    }


    public function save(Tournament $tournament): void
    {

        if (!$tournament->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->dispatcher->dispatchAll($tournament->releaseEvents());
        $this->dispatcher->dispatch(new EntityPersisted($tournament));
    }

    public function markParsed($id)
    {
        if (!$tournament = Tournament::findOne($id)) {
            throw new NotFoundException('Notion is not found.');
        }
        $tournament->done = 1;
        return $tournament->save();
    }

    public function remove(Tournament $tournament): void
    {
        if (!$tournament->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $this->dispatcher->dispatchAll($tournament->releaseEvents());
        $this->dispatcher->dispatch(new EntityRemoved($tournament));
    }


}