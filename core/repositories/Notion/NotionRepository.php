<?php

namespace core\repositories\Notion;

use core\dispatchers\EventDispatcher;
use core\entities\Notion\Notion;
use core\repositories\events\EntityPersisted;
use core\repositories\events\EntityRemoved;
use core\repositories\NotFoundException;

class NotionRepository
{
    private $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function get($id): Notion
    {
        if (!$notion = Notion::findOne($id)) {
            throw new NotFoundException('Notion is not found.');
        }
        return $notion;
    }


    public function save(Notion $notion): void
    {

        if (!$notion->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->dispatcher->dispatchAll($notion->releaseEvents());
        $this->dispatcher->dispatch(new EntityPersisted($notion));
    }

    public function remove(Notion $notion): void
    {
        if (!$notion->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $this->dispatcher->dispatchAll($notion->releaseEvents());
        $this->dispatcher->dispatch(new EntityRemoved($notion));
    }

}