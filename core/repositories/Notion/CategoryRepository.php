<?php

namespace core\repositories\Notion;

use core\entities\Notion\Category;
use core\repositories\NotFoundException;

class CategoryRepository
{
    public function get($id): Category
    {
        if(!$category = Category::findOne($id)){
            throw new NotFoundException('Category is not found');
        }
        return $category;
    }

    public function save(Category $category): void
    {
        if(!$category->save()){
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Category $category): void
    {
        if(!$category->delete()){
            throw new \RuntimeException('Removing error');
        }
    }

    public function getCategoryIdByName($category): int
    {
        return Category::find()->andWhere(['LIKE', 'name', $category])->one() ?
            Category::find()->andWhere(['LIKE', 'name', $category])->one()->id : 0;
    }

}