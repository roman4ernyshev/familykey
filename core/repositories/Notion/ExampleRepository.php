<?php

namespace core\repositories\Notion;


use core\dispatchers\EventDispatcher;
use core\entities\Notion\Example;
use core\entities\Notion\Notion;
use core\repositories\events\EntityPersisted;
use core\repositories\events\EntityRemoved;
use core\repositories\NotFoundException;

class ExampleRepository
{
    private $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function get($id): Example
    {
        if (!$notion = Example::findOne($id)) {
            throw new NotFoundException('Notion is not found.');
        }
        return $notion;
    }


    public function save(Example $example): void
    {

        if (!$example->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->dispatcher->dispatchAll($example->releaseEvents());
        $this->dispatcher->dispatch(new EntityPersisted($example));
    }

    public function remove(Notion $notion): void
    {
        if (!$notion->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $this->dispatcher->dispatchAll($notion->releaseEvents());
        $this->dispatcher->dispatch(new EntityRemoved($notion));
    }


}