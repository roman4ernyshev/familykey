<?php

namespace core\repositories;


use core\dispatchers\EventDispatcher;
use core\entities\Project\Project;
use core\repositories\events\EntityPersisted;
use core\repositories\events\EntityRemoved;

class ProjectRepository
{
    private $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function get($id): Project
    {
        if (!$notion = Project::findOne($id)) {
            throw new NotFoundException('Project is not found.');
        }
        return $notion;
    }


    public function save(Project $project): void
    {

        if (!$project->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->dispatcher->dispatchAll($project->releaseEvents());
        $this->dispatcher->dispatch(new EntityPersisted($project));
    }

    public function remove(Project $notion): void
    {
        if (!$notion->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $this->dispatcher->dispatchAll($notion->releaseEvents());
        $this->dispatcher->dispatch(new EntityRemoved($notion));
    }

}