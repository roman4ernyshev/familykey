<?php

namespace core\repositories;

use core\entities\Books\Book;
use core\entities\Rockncontroll\Items;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class BookRepository
{
    public function get($id): Book
    {
        if (!$book = Items::findOne($id)) {
            throw new NotFoundException('Notion is not found.');
        }
        return $book;
    }

    public function save(Book $book): void
    {
        if (!$book->save()) {
            throw new \RuntimeException('Saving error.');
        }

    }

    public function update(Book $book): void
    {
        if (!$book->update()) {
            throw new \RuntimeException('Saving error.');
        }

    }

    public function remove(Book $book): void
    {
        if (!$book->delete()) {
            throw new \RuntimeException('Removing error.');
        }

    }

    public function findById($id): ?Items
    {
        return Items::findOne($id);
    }

    public function getAllAuthorBooks($author_id): DataProviderInterface
    {
        $query = Book::find()->where(['author_id' => $author_id]);
        return $this->getProvider($query);
    }

    private function getProvider(ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,

            /*
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [
                    'id' => [
                        'asc' => ['p.id' => SORT_ASC],
                        'desc' => ['p.id' => SORT_DESC],
                    ],
                    'name' => [
                        'asc' => ['p.name' => SORT_ASC],
                        'desc' => ['p.name' => SORT_DESC],
                    ],
                    'price' => [
                        'asc' => ['p.price_new' => SORT_ASC],
                        'desc' => ['p.price_new' => SORT_DESC],
                    ],
                    'rating' => [
                        'asc' => ['p.rating' => SORT_ASC],
                        'desc' => ['p.rating' => SORT_DESC],
                    ],
                ],
            ],
            */

            'pagination' => [
                'pageSizeLimit' => [15, 100],
            ]
        ]);
    }

}