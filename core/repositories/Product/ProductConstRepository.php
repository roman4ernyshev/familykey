<?php

namespace core\repositories\Product;

use core\dispatchers\EventDispatcher;
use core\entities\Product\ProductsConst;
use core\repositories\events\EntityPersisted;
use core\repositories\events\EntityRemoved;

class ProductConstRepository
{
    private $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function get($id): ProductsConst
    {
        if (!$product = ProductsConst::findOne($id)) {
            throw new NotFoundException('Project is not found.');
        }
        return $product;
    }


    public function save(ProductsConst $product): void
    {

        if (!$product->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->dispatcher->dispatchAll($product->releaseEvents());
        $this->dispatcher->dispatch(new EntityPersisted($product));
    }

    public function remove(ProductsConst $product): void
    {
        if (!$product->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $this->dispatcher->dispatchAll($product->releaseEvents());
        $this->dispatcher->dispatch(new EntityRemoved($product));
    }

}