<?php

namespace core\repositories;

use core\entities\Books\Author;

class BookAuthorRepository
{
    public function get($id): Author
    {
        if (!$author = Author::findOne($id)) {
            throw new NotFoundException('Notion is not found.');
        }
        return $author;
    }

    public function save(Author $author): void
    {
        if (!$author->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Author $author): void
    {
        if (!$author->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    public function findById($id): ?Author
    {
        return Author::findOne($id);
    }


}