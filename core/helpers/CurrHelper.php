<?php

namespace core\helpers;


use core\entities\Rockncontroll\Currencies;
use core\entities\Rockncontroll\CurrHistory;
use Yii;

class CurrHelper
{
    private static $_htmlId = 0;

    public static function getHtmlId()
    {
        return md5('repleh' . Yii::$app->request->getUrl()) . '_' . self::$_htmlId++;
    }

    public static function echoImg($file = '', $w = 300, $h = 300, $type = 'w', $htmlOptions = array())
    {
        $id = self::getHtmlId();
        $file = trim($file, '/');
        $src = 'http://'.$_SERVER['SERVER_NAME'].'/resize/'.$h.'/'.$w.'/'.$type.'/'.$file;

        $_htmlOptions = '';
        if (!empty($htmlOptions))
        {
            foreach ($htmlOptions as $attribute => $value)
                $_htmlOptions .= ' '.$attribute.'="'.$value.'"';
        }

        echo '<span id="'.$id.'"></span>';
        echo strtr('
			<script type="text/javascript">
				(function(d){
					$(\'#'.$id.'\').html(\'<i\'+\'mg s\'+\'rc=":src":htmlOptions />\');
				})(document);
			</script>
		', array(
            ':src' => $src,
            ':htmlOptions' => $_htmlOptions,
        ));

        return true;
    }

    public static function cropStr($string, $length = 50, $end = '...')
    {
        $result = mb_substr($string, 0, $length, 'utf-8');

        $stringLength = mb_strlen($string, 'utf-8');

        if ($stringLength > $length)
            $result .= $end;

        return $result;
    }

    public static function getDateIntervalYesterdayInDashOrSlashFormat(\DateTime $day, $interval, $delimiter){
        $day->sub(new \DateInterval('P'.(int)$interval.'D'));

        if($delimiter == 'slash') {
            return $day->format('d/m/Y');
        }
        else {
            return $day->format('Y-m-d');
        }

    }

    /**
     * Обрезает строку после скобки
     * @param $str
     * @return string
     */
    public static function cutAfterBracket($str){

        if(strstr($str,'Спортинг (Исп)'))  $str = str_replace('Спортинг (Исп)', 'Спортинг Хихон', $str);
        if(strstr($str,'Спортинг (Пор)'))  $str = str_replace('Спортинг (Пор)', 'Спортинг Лиссабон', $str);

        $pos = strpos($str, '(');

        if($pos)
            return substr($str, 0, $pos);
        else
            return $str;


    }

    /**
     * Каркулятор актуальных значений валют
     * @param $value
     * @param int $currency_id
     * @return mixed|string
     */
    public static function currencyAdapter($value, $currency_id = 11){

        $curr = CurrHistory::find()
            ->where(['currency_id' => $currency_id])
            ->andWhere(['date' => date('Y-m-d', time())])
            ->one();
        if($curr) {
            return $curr->nominal*$curr->value*(float)$value;
        }
        else return 0;

    }

    public static function actionGetCurrency($cur_id)
    {

        $data_slash = CurrHelper::getDateIntervalYesterdayInDashOrSlashFormat(new \DateTime(), 0, 'slash');

        //$url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . $data_slash;
        $url = 'https://www.cbr-xml-daily.ru/daily_utf8.xml?date_req=' . $data_slash;

        $context = stream_context_create(
            array(
                'http' => array(
                    'max_redirects' => 5
                )
            )
        );
        try {
            $xml_contents = file_get_contents($url, false, $context);
        } catch (\Exception $e) {
            //echo $e->getMessage();
            return 'no data';
        }
        //var_dump($http_response_header); exit;


        if ($xml_contents === false)
            throw new \ErrorException('Error loading ' . $url);

        $xml = new \SimpleXMLElement($xml_contents);

        $currency = Currencies::findOne($cur_id);

        foreach ($xml as $node) {
            //$current_curr = new CurrHistory();
            //var_dump($node->attributes()->ID); var_dump($currency->valute_id);
            if($currency->valute_id == $node->attributes()->ID) {
                return str_replace(',', '.', $node->Value);
                //var_dump($currency->valute_id);
            }

        }

        return null;


    }

}