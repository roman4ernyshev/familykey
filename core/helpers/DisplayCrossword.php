<?php

namespace core\helpers;

use core\entities\Word\DefinitionThema;
use core\entities\Word\Word;
use core\readModels\Word\WordCrosswordRepository;
use yii\db\Expression;

class DisplayCrossword
{
    private WordCrosswordRepository $rep_words;
    private static array $used_words_ids = [];
    private array $words = [];
    private static int $in_cross_counter = 0;

    const CROSSWORD_WIDTH = 13;
    const CROSSWORD_GROSS = 169;

    public function __construct(WordCrosswordRepository $words)
    {
        $this->rep_words = $words;

        //self::with($words);
       //$words = Word::find()->asArray()->all();
       //shuffle($words);
        //$this::$couter_words = count($words);
       // $this::$words = $words;
       // var_dump($this::$couter_words); exit;
        /*
        switch ($words) {
            case 'all':
                $words = Word::find()->asArray()->all();
                shuffle($words);
                //$this::$couter_words = count($words);
                self::$words = $words;

                break;
            case 'webentwickler':
                $definitions = DefinitionThema::find()
                    ->select('definitions.*, word.*, definition_crossword_thema.*')
                    ->where('crossword_thema_id = 1')
                    ->leftJoin('definitions', 'definitions.id = definition_crossword_thema.definition_id')
                    ->leftJoin('word', 'word.id = definitions.word_id')
                    ->asArray()
                    ->all();
                $words = Word::find()->asArray()->all();
            default:
        */

    }


    private function getWorter(): array
    {

/*
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>['Content-Type', 'application/json']
            )
        );

       $context = stream_context_create($opts);
*/

      // $words = Word::find()->asArray()->all();


       //$words1 = json_decode(file_get_contents('https://api.roomab.art/wordobs', false, $context), 1);
    //  var_dump($words); exit;


      // $json = file_get_contents('https://api.roomab.art/wordobs', false, $context);
        //json_decode(file_get_contents("file.json"), 1);
        //var_dump(json_decode(file_get_contents(__DIR__ . '/../../../src/Data/Crossword/crossword.json')));
        //json_last_error();
        //exit;
        //return json_decode(file_get_contents(__DIR__ . '/../../../src/Data/Crossword/crossword.json'), 1);
       //return json_decode(file_get_contents('https://api.roomab.art/wordobs', false, $context), 1);
       return $this->words;
    }

    private function getVertRandWord($thema_id): array
    {
        return $this->rep_words->getVertRandWordWithThema($thema_id);
    }

    private static function getRandomWord(): array
    {

       // $words = self::getWorter();
        //$model = Test::find()->asArray()->all(); // Fetching data as array
        //
        //        $model = array_rand($model,30); // get 30 rendom items


        $word = Word::find()
            ->select('id, title')
            ->orderBy(new Expression('random()'))->asArray()->one();
        //$word = array_rand($words);
        //var_dump($word); exit;
        /*
        $words = self::getWorter();
        $random_id = rand(0,count($words)-1);
        $word2 = $words[$random_id];
        */
        //var_dump($word['titel']); exit;
        //var_dump($word2, $word1); exit;


        return [
            'id' => $word['id'],
            'string' => $word['title']
        ];
    }

    private static function isVerticalPlaceForWord($start, $word, &$cross_array): bool
    {
        $word_lang = mb_strlen($word);
        $word_array = mb_str_split($word);
        if($start+$word_lang*self::CROSSWORD_WIDTH > self::CROSSWORD_GROSS) {
            // echo ($start+$word_lang*self::CROSSWORD_WIDTH).'<br>';
            return false;
        }

        for ($i=$start-self::CROSSWORD_WIDTH,$word_liter=0; $i < self::CROSSWORD_GROSS, $word_liter<=$word_lang+1;
             $i=$i+self::CROSSWORD_WIDTH,$word_liter++){
            if(isset($cross_array[$i])){
                if(self::isWortBeginOrEnd($cross_array[$i])) {
                    //var_dump($word).' word<br>';
                    return false;
                }
                if(isset($word_array[$word_liter])){
                    if($cross_array[$i]['liter'] !== '' && ($cross_array[$i]['liter'] !== $word_array[$word_liter])) {
                        //var_dump($cross_array[$i]['liter'] .'!==' . $word_array[$word_liter]).'<br>';
                        return false;
                    }
                }
                if($cross_array[$i]['blank'] === false) {
                    //echo 'blank false<br>';
                    return false;
                }
                else continue;
            }
            else continue;
        }
        return true;
    }

    private static function isWortBeginOrEnd($word_structure): bool
    {
        if($word_structure['blank'] === true && $word_structure['begin'] === true) return true;
        if($word_structure['blank'] === true && $word_structure['end'] === true) return true;
        return false;
    }

    private static function rand_color(): string {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    private static function haveWordsEqualLitters($word1, $word2): array
    {
       // var_dump($word2); exit;
        $arr_word_1 = mb_str_split($word1);
        $arr_word_2 = mb_str_split($word2);
        $intersect = array_intersect($arr_word_1, $arr_word_2);
        //print_r($intersect);
        if(count($intersect)>0) {
            $intersect['ob'] = $arr_word_2;
            //print_r($intersect); exit;
        }

        return $intersect;
    }

    private static function isHorizontalPlaceForWord($start, $word, &$cross_array): bool
    {
        $word_lang = mb_strlen($word);
        $word_array = mb_str_split($word);

        $stop_ten = (ceil($start*1/self::CROSSWORD_WIDTH))*self::CROSSWORD_WIDTH;
        // echo $start.'-'.$stop_ten.'<br>';
        if($start+$word_lang > $stop_ten) {
            // echo $start.'+'.$word_lang.'>'. $stop_ten.'<br>'; exit;
            return false;
        }
        //return false;
        for ($i=$start,$word_liter=0; $i<=$stop_ten; $i++,$word_liter++){

            if(self::isWortBeginOrEnd($cross_array[$i])) {
                // var_dump($cross_array[$i]); exit;
                return false;
            }
            //in_array($i, [5,15,25,35,45,55,65,75,85,95]
            /*if(in_array($i, [5,15,25,35,45,55,65,75,85,95]) &&
                ($cross_array[$i]['liter'] !== $word_array[$word_liter])) {
                return false;
            }*/

            if(isset($cross_array[$i]) && isset($word_array[$word_liter])){
                if(
                    $cross_array[$i]['blank'] === false &&
                    //$cross_array[$i]['liter'] !== '' &&
                    ($cross_array[$i]['liter'] !== $word_array[$word_liter])
                    //&&
                   // !in_array($i, [5,15,25,35,45,55,65,75,85,95])
                ) {
                    //echo $i.' < '.$start;
                    //var_dump($cross_array[$i]['blank']);
                    return false;
                }
            }
        }
        return true;
    }

    private static function setVerticalWort($start, $word, &$cross_array, $j): void
    {
        //var_dump($word); exit;
        $word_lang = mb_strlen($word['string']);
        $word_array = mb_str_split($word['string']);
        $rand_color = self::rand_color();
        //self::$in_cross_counter++;

        try {
            for ($i=$start,$word_liter=0; $i < self::CROSSWORD_WIDTH, $word_liter<$word_lang;
                 $i=$i+self::CROSSWORD_WIDTH,$word_liter++) {
                if (isset($cross_array[$start-self::CROSSWORD_WIDTH])) {
                    $cross_array[$start-self::CROSSWORD_WIDTH] = [
                        'blank' => true,
                        'liter' => '',
                        'word' => $word['string'],
                        'i' => $j,
                        'id' => $word['id'],
                        'begin' => true,
                        'end' => false,
                        'word_color' => 'red',
                        'cross_word_number' => ''
                    ];
                }
                $cross_array[$i] = [
                    'blank' => false,
                    'liter' => $word_array[$word_liter],
                    'word' => $word['string'],
                    'i' => $j,
                    'id' => $word['id'],
                    'begin' => false,
                    'end' => false,
                    'color' => 'red'
                ];
                if (isset($cross_array[$start+$word_lang*self::CROSSWORD_WIDTH])) {
                    $cross_array[$start+$word_lang*self::CROSSWORD_WIDTH] = [
                        'blank' => true,
                        'liter' => '',
                        'word' => $word['string'],
                        'i' => $j,
                        'id' => $word['id'],
                        'begin' => false,
                        'end' => true
                    ];
                }
            }
        } catch (\Exception $e) {
            $cross_array[170] = ['blank' => false, 'liter' => '', 'error' => $e->getMessage()];
        }
    }

    private static function setHorizontalWord($start, $word, &$cross_array, $j): void
    {
        $word_lang = mb_strlen($word['string']);
        $word_array = mb_str_split($word['string']);
        $stop_ten = (ceil($start*(1/self::CROSSWORD_WIDTH)))*self::CROSSWORD_WIDTH;
       // var_dump($stop_ten); exit;
        $rand_color = self::rand_color();
        //self::$in_cross_counter++;

        try {
            for ($i=$start,$word_liter=0; $i < $stop_ten, $word_liter<$word_lang; $i++,$word_liter++) {
                //echo $start; exit;
                if (isset($cross_array[$start-1])) {
                    $cross_array[$start-1] = [
                        'blank' => true,
                        'liter' => '',
                        'word' => $word['string'],
                        'i' => $j,
                        'id' => $word['id'],
                        'begin' => true,
                        'end' => false,
                        'word_color' => $rand_color,
                        'cross_word_number' => ''
                    ];
                }
                $cross_array[$i] = [
                    'blank' => false,
                    'liter' => $word_array[$word_liter],
                    'word' => $word['string'],
                    'i' => $j,
                    'id' => $word['id'],
                    'begin' => false,
                    'end' => false,
                    'color' => in_array($i, [6,19,32,45,58,71,84,97,110,123,136,149,161]) ? 'red' : $rand_color
                ];

                if (isset($cross_array[$start+$word_lang])) {
                    $cross_array[$start+$word_lang] = [
                        'blank' => true,
                        'liter' => '',
                        'word' => $word['string'],
                        'i' => $j,
                        'id' => $word['id'],
                        'begin' => false,
                        'end' => true
                    ];
                }
            }
            //var_dump($cross_array); exit;
        } catch (\Exception $e) {
            $cross_array[122] = ['blank' => false, 'liter' => '', 'error' => $e->getMessage()];
        }

    }

    private static function setVerticalCrossWord($start, $word, $word_n, &$cross_array, $j): void
    {

        if (count($interception = self::haveWordsEqualLitters($word['string'], $word_n['string'])) !== 0) {
            //print_r($interception);
            // Array ( [0] => o [2] => e ) Array ( [0] => o [2] => e [ob] => Array ( [0] => p [1] => a [2] => c [3] => e [4] => )
            foreach ($interception as $key => $exist_litter) {
                //echo $key . '=>' . $vert;
                foreach ($interception['ob'] as $key_ob => $new_litter){
                    if($exist_litter !== '' && $exist_litter===$new_litter){
                        /* echo 'exist_litter '. $exist_litter . ' new_litter '. $new_litter . ' start '.
                             $start .' von gor word ' . $word['string']. ' start '.
                             (($start+$key)-$key_ob*self::CROSSWORD_WIDTH). ' von new ver word '. $word_n['string'] . '<br>';*/

                        if (self::isVerticalPlaceForWord(($start+$key)-$key_ob*self::CROSSWORD_WIDTH, $word_n['string'], $cross_array)) {
                            //var_dump($start-(self::CROSSWORD_WIDTH*$key_ob)); exit;
                            self::setVerticalWort(($start+$key)-$key_ob*self::CROSSWORD_WIDTH, $word_n, $res, $exist_litter.'--+--'.$new_litter);

                        }
                    }
                }
            }
        }
    }

    private static function setHorizontalCrossWord($start, $word, $word_n, &$cross_array, $j): void
    {
        if (count($interception = self::haveWordsEqualLitters($word['string'], $word_n['string'])) !== 0) {
            foreach ($interception as $key=>$exist_litter) {
                // echo $key . '=>' . $vert;
                // Array ( [0] => o [2] => e ) Array ( [0] => o [2] => e [ob] => Array ( [0] => p [1] => a [2] => c [3] => e [4] => )
                foreach ($interception['ob'] as $key_ob => $new_litter){
                    if($exist_litter !== '' && $exist_litter===$new_litter){
                        /* echo 'exist_litter '. $exist_litter . ' new_litter '. $new_litter . ' start '.
                             $start .' von ver word ' . $word['string']. ' start '.
                             ($start+($key*self::CROSSWORD_WIDTH)-$key_ob). ' von new gor word '. $word_n['string'] . '<br>';*/
                        if (self::isHorizontalPlaceForWord($start+($key*self::CROSSWORD_WIDTH)-$key_ob, $word_n['string'], $cross_array)) {
                            // var_dump($start+($key*self::CROSSWORD_WIDTH)-$key_ob); exit;
                            if(!self::isWordInCrossword($word_n['id'])){
                                self::$used_words_ids[] = $word_n['id'];
                                self::setHorizontalWord(($start+($key*self::CROSSWORD_WIDTH)-$key_ob), $word_n, $cross_array, $j);
                            }
                            // var_dump($cross_array); exit;
                        }
                    }
                }
            }
        }

    }

    private static function makeOddRandom($min, $max)
    {
        $min += 1 - $min & 1;
        $max -= 1 - $max & 1;
        $n = ($max - $min) / 2;
        return $min + 2 * rand(0, $n);
    }

    private static function isWordInCrossword($word_id): bool
    {
        if(in_array($word_id, self::$used_words_ids)) {
            return true;
        }
        else {
            return false;
        }
    }

    /*
     * Hier befindet sich der Einstiegspunkt zum Generator.
     */
    public function getAxleCrossword($theme_id=0): array
    {
        $this->words = $this->rep_words->getAllWordsByThema($theme_id);
        //self::with($theme);
        /*
         * Es wird ein leeres Array gesetzt
         */
        for ($i = 0; $i < self::CROSSWORD_GROSS; $i++) {
            $res[$i] = [
                'blank' => true,
                'liter' => '',
                'word' => '',
                'i' => $i,
                'end' => false,
                'begin' => false,
                'color' => ''
            ];
        }

        /*
         * An dieser Stelle wird eine Methode aufgerufen,
         * die versucht, ein Wort in das Kreuzworträtsel einzufügen.
         * Dieser Generator funktioniert nur im Kontext der Klasse,
         * d.h. er wird nicht als Objekt verwendet und läuft nur offline,
         * um ein Array mit Daten für den Client im JSON-Format
         * (Die erforderlichen Kopfzeilen werden in der Phase der Antwortverarbeitung hinzugefügt)
         * zu erstellen.
         */
        $this->trySetWord($res, $theme_id);
        $this->wordNumerator($res);

        /*
         * Nach dem Füllen wird das fertige Array zurückgegeben.
         */
        return mb_convert_encoding($res, 'UTF-8', 'UTF-8');
    }

    private function wordNumerator(&$cross_array){
        for ($i = 0; $i < self::CROSSWORD_GROSS; $i++) {
            if ($cross_array[$i]['begin'] === true) {
                $cross_array[$i]['cross_word_number'] =
                    ++self::$in_cross_counter;
            }

        }
    }

    private function trySetWord(&$cross_array, $thema_id): void
    {
        $word = $this->getVertRandWord($thema_id);

        if(!self::isWordInCrossword($word['id'])){
            self::$used_words_ids[] = $word['id'];
        }

        else self::trySetWord($cross_array,  $thema_id);

        $start = self::CROSSWORD_WIDTH+6;

        if (self::isVerticalPlaceForWord($start, $word['string'], $cross_array)) {

            self::setVerticalWort($start, $word, $cross_array, 0);

            $words = $this->getWorter();

            foreach ($words as $key => $word_n) {
                $new_word = ['id' => $word_n['id'], 'string' => $word_n['title']];

                self::setHorizontalCrossWord($start, $word, $new_word, $cross_array, 1);
            }
        }

        else self::trySetWord($cross_array,  $thema_id);
        //var_dump(self::$used_words_ids);

    }

    public function getCrossword(): array
    {
        $res = [];
        try {
            for($j=0;$j<40;$j++) {
                $word = self::getRandomWord();

                if ($j === 0) {
                    for ($i = 0; $i < self::CROSSWORD_GROSS; $i++) {
                        $res[$i] = [
                            'blank' => true,
                            'liter' => '',
                            'word' => '',
                            'i' => $i,
                            'end' => false,
                            'begin' => false,
                            'color' => ''
                        ];
                    }
                }

                $shift = rand(0, 99 - mb_strlen($word['string']));

                for ($i = 0; $i < self::CROSSWORD_GROSS; $i++) {
                    if ($i === $shift) {
                        if (self::isHorizontalPlaceForWord($shift, $word['string'], $res)) {
                            self::setHorizontalWord($shift, $word, $res, $j);
                            $words = $this->getWorter();
                            foreach ($words as $key=>$word_n){
                                //$word_n = self::getRandomWord();
                                $new_word = ['id'=>$word_n['id'],'string'=>$word_n['title']];
                                //var_dump(self::haveWordsEqualLitters($word_n['string'], $word['string']));
                                self::setVerticalCrossWord($shift, $word, $new_word, $res, $j);
                            }
                        }
                    }
                }

                $word = self::getRandomWord();
                $upright_shift = rand(0, 99 - mb_strlen($word['string']));

                for ($i = 0; $i < self::CROSSWORD_GROSS; $i++) {
                    if ($i === $upright_shift) {
                        if (self::isVerticalPlaceForWord($upright_shift, $word['string'], $res)) {
                            self::setVerticalWort($upright_shift, $word, $res, $j);
                            $words = $this->getWorter();
                            foreach ($words as $key=>$word_n){
                                //$word_n = self::getRandomWord();
                                $new_word = ['id'=>$word_n['id'],'string'=>$word_n['title']];
                                //var_dump(self::haveWordsEqualLitters($word_n['string'], $word['string']));
                                self::setHorizontalCrossWord($shift, $word, $new_word, $res, $j);
                            }
                        }
                    }

                }

            }
        } catch (\Exception $e) {
            $res[121] = ['blank' => false, 'liter' => '', 'error' => $e->getMessage()];
        }
        return mb_convert_encoding($res, 'UTF-8', 'UTF-8');
    }


}