<?php


namespace core\helpers;


class PdfHelper
{
    /**
     * Table in pdf returns in array rows
     * @param $file
     * @param $cut_head
     * @param $offset_head
     * @param $pattern
     * @return array
     * @throws \Exception
     */
    public static function splitPdfTableInRowsByDelimiterPattern($file, $cut_head, $offset_head, $pattern): array
    {
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($file);

        $text = $pdf->getText();

        $tex = substr($text, strpos($text, $cut_head)+$offset_head);


        $ret = [];

        $t = preg_split($pattern, $tex, -1,PREG_SPLIT_DELIM_CAPTURE);

        //$i=0;
        foreach ($t as $k=>$r){
            $m = preg_split("/\n/", $r);
            foreach ($m as $u)
                if($u){
                    if(count($m)==1)$ret[$k-1][5] = $u;
                    else {
                        $ret[$k][] = $u;
                        //$i++;
                    }
                }

        }

        return array_reverse($ret);
    }

    public static function generatePdfFromItems()
    {
        // Require composer autoload
        //require_once __DIR__ . '/vendor/autoload.php';
        // Create an instance of the class:
        $mpdf = new \Mpdf\Mpdf();

        // Write some HTML code:
        $mpdf->WriteHTML('Hello World');

         // Output a PDF file directly to the browser
        //$mpdf->Output();
        $mpdf->Output('/home/romanych/public_html/myframework/console/files/test.pdf', 'F');

    }
}