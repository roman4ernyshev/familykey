<?php


namespace core\helpers;


class CmdHelper
{
    /**
     * @param $phrase
     * @param $file_name
     */
    public static function espeakDeutschWav($phrase, $file_name)
    {
        $cmd = "espeak -v mb-de4 '".$phrase."' -s 100 -w /home/romanych/Музыка/Thoughts_and_klassik/deutsch/".$file_name.".wav";
        shell_exec($cmd);

    }

}