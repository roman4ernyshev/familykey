<?php

namespace core\helpers;

use core\entities\Notion\Notion;
use Yii;

class NotionHelper
{
    public static function makeJsonFromNotionObject(Notion $word)
    {
        $datas = [
            'id' => $word->id,
            'body' => $word->body,
            'description' => $word->description,
            'audio_link' => Yii::getAlias($word->generated_audio_link),
            'slug' => $word->slug,
            'answer' => $word->answer,
            'example' => $word->example ? $word->example->description : null,
            'example_audio' => $word->example ?  ($word->example->generated_audio_link ?
                                 Yii::getAlias($word->example->generated_audio_link) : null) : null
        ];

        return json_encode($datas);
    }

}