<?php

namespace core\helpers;


class SoccerHelper
{
    public static function getTourFromMatchTournament($tournament)
    {
        $tournament_array = explode('-', $tournament, 2);
        return isset($tournament_array[1]) ? $tournament_array[1] : $tournament;
    }

}