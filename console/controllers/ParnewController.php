<?php

namespace console\controllers;

use core\entities\RockncontrollNew\FootballNews;
use core\entities\RockncontrollNew\ManyNewss;
use core\entities\RockncontrollNew\NsbNewss;
use yii\console\Controller;
use yii\db\IntegrityException;
use yii\helpers\Url;

class ParnewController extends Controller
{

    /**
     * Получение новостей Новосибирска
     */
    function actionGetNsbNews(){

        $items = new \SimpleXMLElement(@file_get_contents($url = Url::to("http://news.ngs.ru/rss/")));

        //return var_dump($items->channel[0]->item[2]);

        for($i=0; $i<20; $i++) {

            $rec = new NsbNewss();

            $rec->title = $items->channel[0]->item[$i]->title;
            $rec->description = $items->channel[0]->item[$i]->description;
            $rec->guid = (int)$items->channel[0]->item[$i]->guid;
            $rec->link = $items->channel[0]->item[$i]->link;
            $rec->pdalink = $items->channel[0]->item[$i]->pdalink;
            $rec->author = $items->channel[0]->item[$i]->author;
            $rec->category = $items->channel[0]->item[$i]->category;
            $rec->enclosure = $items->channel[0]->item[$i]->enclosure['url'].'$$'.$items->channel[0]->item[$i]->enclosure['type'];
            $rec->pud_date = $items->channel[0]->item[$i]->pubDate;

            try {
                if ($rec->save(false)) echo $rec->id . " success".PHP_EOL;
                else echo $rec->id . " fail".PHP_EOL;
            } catch (IntegrityException $e) {
                echo $rec->id . " dubl ".$e->getMessage().PHP_EOL;
                continue;
            }


        }

    }

    function actionGetManyNews() {

        $urls = [
            'http://feeds.bbci.co.uk/news/rss.xml',
            'http://rss.cnn.com/rss/edition.rss',
            'http://feeds.foxnews.com/foxnews/latest',
            //'http://www.iol.co.za/cmlink/1.640',
            //'https://www.vesti.ru/vesti.rss',
            'https://feeds.soccerstats.com/rss/liveblog.xml',
            'http://feeds.bbci.co.uk/sport/football/rss.xml',
            'https://www.sport-express.ru/services/materials/news/se/',
            'https://habrahabr.ru/rss/interesting/',
            'https://rss.dw.com/xml/rss-de-news',
            'https://rss.dw.com/xml/rss-de-all',
            'https://rss.dw.com/xml/rss-de-eco',
            'https://rss.dw.com/xml/rss-de-sport',
            'https://rss.dw.com/xml/rss-ru-all'
        ];

        foreach ($urls as $url){

            $items = new \SimpleXMLElement(@file_get_contents(Url::to($url)));

            //return $items->channel[0]->item[2]->pubDate;
            //echo $items->rss;

            for($i=0; $i<20; $i++){

                $rec = new ManyNewss();

                if(!isset($items->channel[0]->item[$i])) break;

                try {
                    $rec->title = $items->channel[0]->item[$i]->title;
                    $rec->description = $items->channel[0]->item[$i]->description;
                    if($items->channel[0]->item[$i]->guid)
                        $rec->guid = $items->channel[0]->item[$i]->guid;
                    else $rec->guid = $items->channel[0]->item[$i]->link;
                    $rec->link = $items->channel[0]->item[$i]->link;
                    //$rec->pdalink = $items->channel[0]->item[$i]->pdalink;
                    //$rec->author = $items->channel[0]->item[$i]->author;
                    //$rec->category = $items->channel[0]->item[$i]->category;
                    //$rec->enclosure = $items->channel[0]->item[$i]->enclosure['url'].'$$'.$items->channel[0]->item[$i]->enclosure['type'];
                    $rec->pud_date = $items->channel[0]->item[$i]->pubDate;
                    if ($rec->save(false)) echo $rec->id . " success".PHP_EOL;
                    else echo $rec->id . " fail".PHP_EOL;
                } catch (IntegrityException $e) {
                    echo $rec->id . " " . $rec->guid ." dubl ".$e->getMessage().PHP_EOL;
                    continue;
                }
            }
        }
    }

    function actionFootballNews(){

        $items = new \SimpleXMLElement(@file_get_contents($url = Url::to("https://www.championat.com/rss/news/football/")));

        //return var_dump($items->channel[0]->item[2]);

        for($i=0; $i<20; $i++) {

            $rec = new FootballNews();

            $rec->title = $items->channel[0]->item[$i]->title;
            $rec->description = $items->channel[0]->item[$i]->description;
            $rec->guid = md5($items->channel[0]->item[$i]->guid);
            $rec->link = $items->channel[0]->item[$i]->link;
            $rec->pdalink = $items->channel[0]->item[$i]->pdalink;
            //$rec->author = $items->channel[0]->item[$i]->author;
            //$rec->category = $items->channel[0]->item[$i]->category[0].'$$'.$items->channel[0]->item[$i]->category[1];
            //$rec->enclosure = $items->channel[0]->item[$i]->enclosure['url'].'$$'.$items->channel[0]->item[$i]->enclosure['type'];
            $rec->pud_date = $items->channel[0]->item[$i]->pud_date;

            try {
                if ($rec->save(false))
                    echo $rec->id . " success".PHP_EOL;
                else
                    echo $rec->id . " fail".PHP_EOL;
            } catch (IntegrityException $e) {
                echo $rec->id . " dubl ".$e->getMessage().PHP_EOL;
                continue;
            }


        }

    }

    function actionBBCFootballNews()
    {

        //$arr = [];
        $items_bbc = new \SimpleXMLElement(@file_get_contents($url = Url::to("http://feeds.bbci.co.uk/sport/football/rss.xml")));
        //$items_fifa = new \SimpleXMLElement(@file_get_contents($url = Url::to("http://www.fifa.com/rss-feeds/index.html")));
        //return var_dump($items_bbc->channel[0]->item[0]);

        for($i=0; $i<20; $i++) {
            $content = @file_get_contents(Url::to($items_bbc->channel[0]->item[$i]->link));
            /*
            $allowedTags = '<a><br><b><h1><h2><h3><h4><i>' .
                '<img><li><ol><p><strong><table><pre>' .
                '<tr><td><th><u><ul>';
            $content = strip_tags($content, $allowedTags);
            */


            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            //$head = file_get_contents(Url::to("@app/commands/header.html"));
            $new = $content; //добавляем хэдер

            $dom->loadHTML($new);


            $div = $dom->getElementsByTagName("div");
            foreach ($div as $node) {

                if ($node->getAttribute('id') === 'story-body') {

                    $rec = new FootballNews();

                    $rec->title = substr(strip_tags($node->nodeValue, '<br><br />'), 0, 30);
                    $rec->description = strip_tags($node->nodeValue, '<br><br />');
                    $rec->guid = (string)$items_bbc->channel[0]->item[$i]->guid;
                    $rec->link = (string)$items_bbc->channel[0]->item[$i]->link;
                    //$rec->pdalink = $items->channel[0]->item[$i]->pdalink;
                    $rec->author = 'BBC';
                    //$rec->category = $items->channel[0]->item[$i]->category[0].'$$'.$items->channel[0]->item[$i]->category[1];
                    //$rec->enclosure = $items->channel[0]->item[$i]->enclosure['url'].'$$'.$items->channel[0]->item[$i]->enclosure['type'];
                    $rec->pud_date = $items_bbc->channel[0]->item[$i]->pudDate;

                    try {
                        if ($rec->save(false))
                            echo $rec->id . " success" . PHP_EOL;
                        else
                            echo $rec->id . " fail" . PHP_EOL;
                    } catch (IntegrityException $e) {
                        echo $rec->id . " dubl " . $e->getMessage() . PHP_EOL;
                        continue;
                    }

                }


            }
        }


    }

}
