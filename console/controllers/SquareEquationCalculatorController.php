<?php

namespace console\controllers;


use core\entities\SquareEquationCalculator;
use yii\console\Controller;

class SquareEquationCalculatorController extends Controller
{
    function actionRoots()
    {
        $square = readline('Введите коэффициет при x квадрат: ');
        $line = readline('Введите коэффициет при x: ');
        $free = readline('Введите свободный коэффициет: ');

        $calculator = new SquareEquationCalculator();
        $roots = $calculator->getRoots($square, $line, $free);
        //return var_dump($roots);

        if($roots) echo implode(PHP_EOL, $roots).PHP_EOL;
        //if($roots) var_dump($roots);
        else echo 'Корней нет'.PHP_EOL;
    }

}