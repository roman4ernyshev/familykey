<?php
namespace console\controllers;


use core\entities\Rockncontroll\Snapshot;
use yii\console\Controller;
use yii\helpers\Url;

class CalculatorController extends Controller
{
    function actionStadiumSum()
    {

        $snapshots = Snapshot::find()
            ->where('id > 1266')
            ->all();

        $res = 0;

        foreach ($snapshots as $snapshot){
            if($snapshot->mish_oz>10) $res += $snapshot->mish_oz*(6+(($snapshot->mish_oz-10)/10));
            else $res += $snapshot->mish_oz;
        }

        echo ($res+1000-20000-2000-27500-2200).PHP_EOL;

    }

    function actionFindDivisions($n)
    {
        $i = 2;
        /* Пока не поделили на все множители */
        while ($n > 1) {
            /* Если нашли очередной множитель - выводим его,
             * если он последний - после него перевод строки, иначе '*',
             * делим N на этот множитель и не увеличиваем i (может быть
             * несколько равных множителей) */
            if (($n % $i) == 0) {
                echo $i.PHP_EOL;
                //printf("%li%c", $i, $n > $i ? '*' : '\n');
                $n /= $i;
                continue;
            }
            /* i - не множитель - проверяем следующий */
            $i++;
        }

    }

    function actionMaxReplacedNum($n){
        $max = $n;
        $next_is_null = null;
        for($i=0;$i<4;$i++){
            if($n%10) $next_is_null = true;
            else $next_is_null = null;

            if($next_is_null)$curr_num = (int)(($n%10 ? $n%10 : 0) . $n/10);

            if($max > $curr_num) {
                $n = $curr_num;

            }
            else {
                $n = $curr_num;
                $max = $curr_num;
            }
            echo $n.PHP_EOL;
        }
        echo '-------------'.PHP_EOL.$max.PHP_EOL;
    }

    function actionKnoledgeGhraf()
    {

        //require '.api_key';
        $service_url = 'https://kgsearch.googleapis.com/v1/entities:search';
        $params = array(
            'query' => 'Новосибирск',
            'limit' => 10,
            'indent' => TRUE,
            'key' => 'AIzaSyAA-rvN03tDPodIVxQ075oArrmbRtgMnaE');
        $url = $service_url . '?' . http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch), true);
        curl_close($ch);
        foreach ($response['itemListElement'] as $element) {
            //echo $element['result']['name'] . PHP_EOL;
            if(isset($element['result']['detailedDescription']['articleBody']))
                echo $element['result']['detailedDescription']['articleBody'] . PHP_EOL;
        }
    }

    function actionMysqlToJson()
    {


        $servername = "localhost";
        $username   = "root";
        $password   = "vbifcdtnf";
        $dbname     = "plis";

        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $conn = new \mysqli($servername, $username, $password, $dbname);
        $conn->set_charset('utf8');

        $sql = "select * from qparticles_content where id > 16920";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $rows = $result->fetch_all(MYSQLI_ASSOC);

            $str = json_encode($rows);
        } else {

            echo "no results found";
            $str = "";
        }
        if (json_decode($str) != null)
        {
            $file = fopen((Url::to("/home/romanych/Загрузки/data.json")),'w');
            fwrite($file, $str);
            fclose($file);
        }
        else
        {
            echo "invalid JSON, handle the error";
        }


    }

}