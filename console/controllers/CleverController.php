<?php

namespace console\controllers;


use core\entities\Clever\CleverItemShown;
use core\entities\Rockncontroll\RadioItem;
use yii\console\Controller;
use yii\db\IntegrityException;
use yii\helpers\Url;

class CleverController extends Controller
{
    public function actionFillCleverItemShown()
    {
        $max_id = CleverItemShown::find()->one() ? (int)CleverItemShown::find()
            ->select('MAX(id)')
            ->scalar() : 0;

        if($max_id) $max_item_id = CleverItemShown::findOne($max_id)->item_id;

        else $max_item_id = 1;

        $items = RadioItem::find()
            ->where('cat_id IN (23,25,26,27,28,29,30,31,32,33,34,35,36,37)')
            ->andWhere('id > '.$max_item_id)
            ->all();
        if(!$items) {
            echo  'no items'.PHP_EOL;;
            return;
        }

        foreach ($items as $item){
            try {
                $show_item = new CleverItemShown();
                $show_item->item_id = $item->id;
                $show_item->shown = 0;
                $show_item->cat_id = $item->cat_id;
                $show_item->save(false);
                echo $show_item->item_id . ' ok '.PHP_EOL;
            } catch (IntegrityException $e) {
                echo $e->getMessage();
            }
        }


    }

    public function actionGeneratePdfFromItems()
    {
        $url = Url::to("@console/files/test.pdf");
        // Require composer autoload
        //require_once __DIR__ . '/vendor/autoload.php';
        // Create an instance of the class:
        $mpdf = new \Mpdf\Mpdf();

        // Write some HTML code:
        $mpdf->WriteHTML('Hello World');

        // Output a PDF file directly to the browser
        //$mpdf->Output();
        $mpdf->Output('test.pdf', \Mpdf\Output\Destination::FILE);

    }

}