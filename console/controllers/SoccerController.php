<?php

namespace console\controllers;

use core\entities\Rockncontroll\Matches;
use core\entities\Rockncontroll\Soccercode;
use core\entities\Soccer\Tournament;
use core\helpers\Helper;
use core\helpers\TranslateHelper;
use http\Exception\RuntimeException;
use Yii;
use \console\parser\SoccerStandData;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\Url;

class SoccerController extends Controller
{
    const TOURNAMENT_IDS = [1];

    private $soccerCodRec = null;

    public function actionSaveSoccerMatches($day_to_today=0)
    {
        $soccer = new SoccerStandData();
        try {
            $soccer->saveMatches($soccer->getSomeonePastPlayDayFromToday($day_to_today));
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @ToDo add tournament, this method run for one next tournament
     */
    public function actionSaveNextTournament()
    {
        for ($i=0;$i<10;$i++){
            $soccer = new SoccerStandData();
            $next_tournament_id = Tournament::findOne(['done' => null])->id; //query for next not saved
            try {
                $soccer->saveMatches($soccer->getMatchesOfTournamentByIdAndPart($next_tournament_id, $i), $next_tournament_id);
            } catch (RuntimeException $e) {
                echo $e->getMessage();
            }
        }

    }

    public function actionUpdateTournament($id)
    {
        for ($i=0;$i<10;$i++){
            $soccer = new SoccerStandData();

            try {
                $soccer->saveMatches($soccer->getMatchesOfTournamentByIdAndPart($id, $i), $id);
            } catch (RuntimeException $e) {
                echo $e->getMessage();
            }
        }

    }

    public function actionSoccerstandTournament(){
        //Кубок лиги Англия
        //https://d.soccerstand.com/ru/x/feed/t_1_198_OMT80ou8_7_ru_1
        $day_data = $this->_soccerStandCurl("https://d.soccerstand.com/ru/x/feed/t_1_198_OMT80ou8_7_ru_1");
        //$day_data = $this->_soccerStandCurl("https://d.soccerstand.com/ru/x/feed/tr_1_81_W6BOzpK2_168_1_-8_ru_1");
        var_dump($day_data);
    }

    /**
     * Парсинг основной таблицы матчей soccerstand.com
     * @params -dayb - дней назад
     */
    public function actionSoccerstand($dayb=0){

        $arr_date = [];
        $tournament = [];

        //$day_data = $this->_soccerStandCurl("https://d.soccerstand.com/ru/x/feed/tr_1_81_W6BOzpK2_168_1_-8_ru_1");

        $day_data = $this->_soccerStandCurl("http://d.soccerstand.com/ru/x/feed/f_1_".$dayb."_0_ru_1");

        $countries = explode(':',preg_replace("/[^-A-Za-z0-9а-ярьтцхчшуюэыйёА-ЯЁ.,!?:()№\/ ]+/", "", $day_data));

        //print_r($countries); exit;

        $i=0;

        $tournament = [];

        $matchs = [];

        foreach ($countries as $country) {

            //if($i>10) {print_r($country); exit;}

            //$matchs = explode('AB÷3¬CR÷3¬AC', $country);

            $matchs = preg_split('/¬AA/', $country, -1, PREG_SPLIT_DELIM_CAPTURE);


            //}

            //  exit;

            $n=0;
            foreach ($matchs as $match) {



                $recs = explode('¬', $match);
               // if($i>10) {print_r($recs); exit;}
                echo $i.PHP_EOL;


                if($n==0)$tournament[$i][$n]['tour'] = $recs[0];

                foreach ($recs as $rec){


                    $arr_loc[$i][$n] = explode('÷', $rec);
                    //print_r($rec); exit;
                    if(is_array($arr_loc[$i])) {

                        if(isset($arr_loc[$i][$n][1]))
                            $tournament[$i][$n][$arr_loc[$i][$n][0]] = $arr_loc[$i][$n][1];
                    }
                    else{
                        $n++; continue;
                    }

                }
                $n++;

            }


            $i++;

        }
        //var_dump($tournament);
       // exit;


        foreach ($tournament as $t){
            //var_dump($tournament); exit;
            $zee = '';
            $zb = 0;
            $zy = '';
            $zc = '';
            $zd = '';
            $ze = 0;
            $zf = 0;
            $zh = '';
            $zj = 0;
            $zl = '';
            $zx = '';
            $zcc = 0;
            $aa = '';
            $ad = 0;


            if(isset($t[0]['ZEE']))
                $zee = $t[0]['ZEE'];
            if(isset($t[0]['ZB']))
                $zb = $t[0]['ZB'];
            if(isset($t[0]['ZY']))
                $zy = $t[0]['ZY'];
            if(isset($t[0]['ZC']))
                $zc = $t[0]['ZC'];
            if(isset($t[0]['ZD']))
                $zd = $t[0]['ZD'];
            if(isset($t[0]['ZE']))
                $ze = $t[0]['ZE'];
            if(isset($t[0]['ZF']))
                $zf = $t[0]['ZF'];
            if(isset($t[0]['ZH']))
                $zh = $t[0]['ZH'];
            if(isset($t[0]['ZJ']))
                $zj = $t[0]['ZJ'];
            if(isset($t[0]['ZL']))
                $zl = $t[0]['ZL'];
            if(isset($t[0]['tour']))
                $zx = trim($t[0]['tour']);
            if(isset($t[0]['ZCC']))
                $zcc = $t[0]['ZCC'];


            for($i=1; $i<count($t); $i++){

                $match_soc = new Soccer();

                $match_soc->zee = $zee;
                $match_soc->zb = $zb;
                $match_soc->zy = $zy;
                $match_soc->zc = $zc;
                $match_soc->zd = $zd;
                $match_soc->ze = $ze;
                $match_soc->zf = $zf;

                $match_soc->zh = $zh;
                $match_soc->zj = $zj;
                $match_soc->zl = $zl;
                $match_soc->zx = $zx;
                $match_soc->zcc = $zcc;
                $match_soc->aa = $aa;
                $match_soc->ad = $ad;

                if(isset($t[$i]['']))
                    $match_soc->aa = $t[$i][''];
                if(isset($t[$i]['AD']))
                    $match_soc->ad = $t[$i]['AD'];

                if((time()-$match_soc->ad) < 3*60*60) continue;

                if(isset($t[$i]['CX']))
                    $match_soc->cx = $t[$i]['CX'];
                if(isset($t[$i]['AX']))
                    $match_soc->ax = $t[$i]['AX'];
                if(isset($t[$i]['AV']))
                    $match_soc->av = $t[$i]['AV'];
                if(isset($t[$i]['BX']))
                    $match_soc->bx = $t[$i]['BX'];
                if(isset($t[$i]['WN']))
                    $match_soc->wn = $t[$i]['WN'];
                if(isset($t[$i]['AF']))
                    $match_soc->af = $t[$i]['AF'];
                if(isset($t[$i]['WV']))
                    $match_soc->wv = $t[$i]['WV'];

                if(isset($t[$i]['AS']))
                    $match_soc->as = $t[$i]['AS'];
                if(isset($t[$i]['AZ']))
                    $match_soc->az = $t[$i]['AZ'];
                if(isset($t[$i]['AH']))
                    $match_soc->ah = $t[$i]['AH'];
                if(isset($t[$i]['BB']))
                    $match_soc->bb = $t[$i]['BB'];
                if(isset($t[$i]['BD']))
                    $match_soc->bd = $t[$i]['BD'];
                if(isset($t[$i]['WM']))
                    $match_soc->wm = $t[$i]['WM'];
                if(isset($t[$i]['AE']))
                    $match_soc->ae = $t[$i]['AE'];

                if(isset($t[$i]['ZA']))
                    $match_soc->za = $t[$i]['ZA'];
                if(isset($t[$i]['AG']))
                    $match_soc->ag = $t[$i]['AG'];
                if(isset($t[$i]['BA']))
                    $match_soc->ba = $t[$i]['BA'];
                if(isset($t[$i]['BC']))
                    $match_soc->bc = $t[$i]['BC'];
                if(isset($t[$i]['AN']))
                    $match_soc->an = $t[$i]['AN'];


               /* try {
                    if ($match_soc->save(false)) echo $match_soc->id . " success";
                    else echo $match_soc->id . " fail";
                } catch (IntegrityException $e) {
                    echo $e->getMessage();
                    continue;
                }
               */
                //exit;


            }
        }

    }

    public function actionSoccerstandTour($dayb=0){

        $arr_date = [];
        $tournament = [];

        // 168_0 - здесь зашифрована стадия турнира и итерация выборки (догрузки) 0,1,2
        $day_data = $this->_soccerStandCurl("https://d.soccerstand.com/ru/x/feed/tr_1_81_W6BOzpK2_168_0_-8_ru_1");
        //$day_data += $this->_soccerStandCurl("https://d.soccerstand.com/ru/x/feed/tr_1_81_W6BOzpK2_168_1_-8_ru_1");

        //здесь урл для парсинга календаря матчей, которые состоятся - напрмер для Швецкого чемпионата
        //$day_data = $this->_soccerStandCurl("https://d.soccerstand.com/ru/x/feed/tf_1_181_nXxWpLmT_155_1_-8_ru_1");
        //https://d.soccerstand.com/ru/x/feed/tr_1_81_W6BOzpK2_167_1_-8_ru_1 бундеслига 16-17

        //print_r($day_data); exit;

        //$day_data = $this->_soccerStandCurl("http://d.soccerstand.com/ru/x/feed/f_1_".$dayb."_0_ru_1");

        $countries = explode(':',preg_replace("/[^-A-Za-z0-9а-ярьтцхчшуюэыйёА-ЯЁ.,!?:()№\/ ]+/", "", $day_data));

        //print_r($countries); exit;

        $i=0;

        $tournament = [];

        $matchs = [];

        foreach ($countries as $country) {

            //if($i>10) {print_r($country); exit;}

            //$matchs = explode('AB÷3¬CR÷3¬AC', $country);

            $matchs = preg_split('/¬AA/', $country, -1, PREG_SPLIT_DELIM_CAPTURE);

            //}

            //  exit;

            $n=0;
            foreach ($matchs as $match) {

                $recs = explode('¬', $match);
                // if($i>10) {print_r($recs); exit;}

                if($n==0)$tournament[$i][$n]['tour'] = $recs[0];

                foreach ($recs as $rec){


                    $arr_loc[$i][$n] = explode('÷', $rec);
                    //print_r($rec); exit;
                    if(is_array($arr_loc[$i])) {

                        if(isset($arr_loc[$i][$n][1]))
                            $tournament[$i][$n][$arr_loc[$i][$n][0]] = $arr_loc[$i][$n][1];
                    }
                    else{
                        $n++; continue;
                    }

                }
                $n++;
                // echo $n.PHP_EOL;
            }

            $i++;

        }

        var_dump($tournament);

        // exit;

        // var_dump($matchs); exit;




        foreach ($tournament as $t){
            $zee = '';
            $zb = 0;
            $zy = '';
            $zc = '';
            $zd = '';
            $ze = 0;
            $zf = 0;
            $zh = '';
            $zj = 0;
            $zl = '';
            $zx = '';
            $zcc = 0;
            $aa = '';
            $ad = 0;


            if(isset($t[0]['ZEE']))
                $zee = $t[0]['ZEE'];
            if(isset($t[0]['ZB']))
                $zb = $t[0]['ZB'];
            if(isset($t[0]['ZY']))
                $zy = $t[0]['ZY'];
            if(isset($t[0]['ZC']))
                $zc = $t[0]['ZC'];
            if(isset($t[0]['ZD']))
                $zd = $t[0]['ZD'];
            if(isset($t[0]['ZE']))
                $ze = $t[0]['ZE'];
            if(isset($t[0]['ZF']))
                $zf = $t[0]['ZF'];
            if(isset($t[0]['ZH']))
                $zh = $t[0]['ZH'];
            if(isset($t[0]['ZJ']))
                $zj = $t[0]['ZJ'];
            if(isset($t[0]['ZL']))
                $zl = $t[0]['ZL'];
            if(isset($t[0]['tour']))
                $zx = trim($t[0]['tour']);
            if(isset($t[0]['ZCC']))
                $zcc = $t[0]['ZCC'];


            for($i=1; $i<count($t); $i++){

                //var_dump($t[$i]['']);

                $match_soc = new Soccercode();

                $match_soc->zee = $zee;
                $match_soc->zb = $zb;
                $match_soc->zy = $zy;
                $match_soc->zс = $zc;
                $match_soc->zd = $zd;
                $match_soc->ze = $ze;
                $match_soc->zf = $zf;

                $match_soc->zh = $zh;
                $match_soc->zj = $zj;
                $match_soc->zl = $zl;
                $match_soc->zx = $zx;
                $match_soc->zcc = $zcc;
                $match_soc->aa = $aa;
                $match_soc->ad = $ad;

                if(isset($t[$i]['']))
                    $match_soc->aa = $t[$i][''];
                if(isset($t[$i]['AD']))
                    $match_soc->ad = $t[$i]['AD'];

                if((time()-$match_soc->ad) < 3*60*60) continue;

                if(isset($t[$i]['CX']))
                    $match_soc->cx = $t[$i]['CX'];
                if(isset($t[$i]['AX']))
                    $match_soc->ax = $t[$i]['AX'];
                if(isset($t[$i]['AV']))
                    $match_soc->av = $t[$i]['AV'];
                if(isset($t[$i]['BX']))
                    $match_soc->bx = $t[$i]['BX'];
                if(isset($t[$i]['WN']))
                    $match_soc->wn = $t[$i]['WN'];
                if(isset($t[$i]['AF']))
                    $match_soc->af = $t[$i]['AF'];
                if(isset($t[$i]['WV']))
                    $match_soc->wv = $t[$i]['WV'];

                if(isset($t[$i]['AS']))
                    $match_soc->as = $t[$i]['AS'];
                if(isset($t[$i]['AZ']))
                    $match_soc->az = $t[$i]['AZ'];
                if(isset($t[$i]['AH']))
                    $match_soc->ah = $t[$i]['AH'];
                if(isset($t[$i]['BB']))
                    $match_soc->bb = $t[$i]['BB'];
                if(isset($t[$i]['BD']))
                    $match_soc->bd = $t[$i]['BD'];
                if(isset($t[$i]['WM']))
                    $match_soc->wm = $t[$i]['WM'];
                if(isset($t[$i]['AE']))
                    $match_soc->ae = $t[$i]['AE'];

                if(isset($t[$i]['ZA']))
                    $match_soc->za = $t[$i]['ZA'];
                if(isset($t[$i]['AG']))
                    $match_soc->ag = $t[$i]['AG'];
                if(isset($t[$i]['BA']))
                    $match_soc->ba = $t[$i]['BA'];
                if(isset($t[$i]['BC']))
                    $match_soc->bc = $t[$i]['BC'];
                if(isset($t[$i]['AN']))
                    $match_soc->an = $t[$i]['AN'];


                try {
                    if ($match_soc->save(false)) echo $match_soc->id . " success";
                    else echo $match_soc->id . " fail";
                } catch (IntegrityException $e) {
                    echo $e->getMessage();
                    continue;
                }
                //exit;


            }
        }

    }

    /**
     * Автопарсер soccerstand
     * @throws \Exception
     */
    function actionStatSoccerstand()
    {

        $last_id = (int)file_get_contents(Url::to("@app/web/uploads/soccertest.html"));

        $max_id = Soccercode::find()->select('MAX(id)')->scalar();

        //echo $max_id; exit;

        for ($i = $last_id+1; $i<=$max_id; $i++) {
            echo $i.PHP_EOL;

            $rec = Soccercode::findOne($i);
            if(!$rec) continue;
            $date = date('d.m.Y', $rec->ad+7*60*60);
            $time = date('H:i', $rec->ad+7*60*60);

            $content = $this->_soccerStandDetailCurl("https://www.soccerstand.com/ru/match/" . $rec->aa . "/#match-summary");

            $prepared = explode('÷', explode('¬', $this->_soccerStandCurl("http://d.soccerstand.com/ru/x/feed/df_dos_1_" . $rec->aa . "_"))[0]);
            if (isset($prepared[1]) && explode('|', $prepared[1])[1] && explode('|', $prepared[1])[2]) $bets = explode('|', $prepared[1]);
            else $bets = [];


            $urls = [

                "http://d.soccerstand.com/ru/x/feed/d_su_" . $rec->aa . "_ru_1",
                "http://d.soccerstand.com/ru/x/feed/d_st_" . $rec->aa . "_ru_1",
                "http://d.soccerstand.com/ru/x/feed/d_li_" . $rec->aa . "_ru_1",

            ];


            foreach ($urls as $url) {

                $content .= $this->_soccerStandCurl($url);

            }


            $content = str_replace(chr(9), '', $content);
            $content = str_replace(chr(11), '', $content);  // заменяем табуляцию на пробел
            $content = str_replace(chr(13), '', $content);
            $content = str_replace(chr(10), '', $content);


            $tournament = $rec->zy.': '.$rec->zx; // турнир
            $host = ' '.$rec->ae; //номинальный хозяин
            $guest = $rec->af; //номинальный гость
            $score = ''; //счёт
            $gett = $rec->ag; //голы, забитые хозяевами
            $lett = $rec->ah; //голы, забитые гостями
            $stay_h = ''; //расстановка хозяев
            $stay_g = ''; //расстановка гостей
            //$date = ''; // дата
            //$time = ''; // время начала матча
            $stra_h = '';
            $yel_kart_h = ''; //жёлтые карточки хозяев
            $yel_kart_g = ''; //жёлтые карточки гостей
            $red_kart_h = ''; //красные карточки хозяев
            $red_kart_g = ''; //красные карточки гостей
            $stra_h = ''; //расстановка
            $stra_g = '';
            $ud_h = 0; //удары
            $ud_g = 0;
            $ud_mim_h = 0; //удары мимо
            $ud_mim_g = 0;
            $offside_h = 0; //оффсайды
            $offside_g = 0;
            $falls_h = 0; //фолы
            $falls_g = 0;
            $ud_v_stv_h = 0; //Удары в створ
            $ud_v_stv_g = 0;
            $corner_h = 0; //Угловые
            $corner_g = 0;
            $saves_h = 0; //Сейвы
            $saves_g = 0;
            $yelkar_h = 0; //жёлтые карточки
            $yelkar_g = 0;
            $ballpos_h = 0; //владение мячом
            $ballpos_g = 0;
            $shtraf_h = 0; //штрафные
            $shtraf_g = 0;
            $outs_h = 0; //штрафные
            $outs_g = 0;
            $bet_g = $bets[2];  //ставки
            $bet_h = $bets[0];
            $bet_n = $bets[1];
            $substit_h = ''; //строка замен хозяев
            $substit_g = ''; //строка замен гостен
            $goul_h = ''; //голы хозяев
            $goul_g = ''; //голы гостей
            $pen_miss_h = ''; //нереализованные пенальти хозяев
            $pen_miss_g = ''; //нереализованные пенальти гостей
            $prim = ''; //примечание
            $onehalf_h = 0; // голы хозяев в первой половине матча
            $onehalf_g = 0; // голы гостей в первой половине матча
            $info = '';


            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $head = file_get_contents(Url::to("@app/commands/header.html"));
            $match = $head . $content; //добавляем хэдер

            $dom->loadHTML($match);


            $div = $dom->getElementsByTagName("div");
            foreach ($div as $node) {


                if ($node->getAttribute('class') === 'fleft') {
                    $tournament = $node->nodeValue;

                }


                if ($node->getAttribute('id') === 'tab-statistics-0-statistic') {
                    $dt = $node->nodeValue;
                    $dom_in = new \DomDocument();
                    $html = $node->ownerDocument->saveHTML($node);
                    libxml_use_internal_errors(true);
                    $newhtml = $head . $html;
                    $dom_in->loadHTML($newhtml);


                    $tr = $dom_in->getElementsByTagName("tr");


                    foreach ($tr as $node) {

                        if ($node->getAttribute('class') === 'odd') {
                            $odd = $node->nodeValue;


                            if (preg_match('/\vУдары\v/', $odd))  //в регулярке - вертикальный пробельный символ
                            {
                                $statistic = preg_split('/Удары/', $odd);

                                $ud_h = (int)$statistic[0];

                                $ud_g = (int)$statistic[1];


                            }

                            if (preg_match('/Удары мимо/', $odd)) {
                                $statistic = preg_split('/Удары мимо/', $odd);

                                $ud_mim_h = (int)$statistic[0];

                                $ud_mim_g = (int)$statistic[1];


                            }

                            if (preg_match('/Офсайды/', $odd)) {
                                $statistic = preg_split('/Офсайды/', $odd);

                                $offside_h = (int)$statistic[0];

                                $offside_g = (int)$statistic[1];


                            }

                            if (preg_match('/Фолы/', $odd)) {
                                $statistic = preg_split('/Фолы/', $odd);

                                $falls_h = (int)$statistic[0];

                                $falls_g = (int)$statistic[1];


                            }

                            if (preg_match('/Удары в створ/', $odd)) {
                                $statistic = preg_split('/Удары в створ/', $odd);

                                $ud_v_stv_h = (int)$statistic[0];

                                $ud_v_stv_g = (int)$statistic[1];


                            }

                            if (preg_match('/Угловые/', $odd)) {
                                $statistic = preg_split('/Угловые/', $odd);

                                $corner_h = (int)$statistic[0];

                                $corner_g = (int)$statistic[1];


                            }

                            if (preg_match('/Сэйвы/', $odd)) {
                                $statistic = preg_split('/Сэйвы/', $odd);

                                $saves_h = (int)$statistic[0];

                                $saves_g = (int)$statistic[1];


                            }

                            if (preg_match('/Желтые карточки/', $odd)) {
                                $statistic = preg_split('/Желтые карточки/', $odd);

                                $yelkar_h = (int)$statistic[0];

                                $yelkar_g = (int)$statistic[1];


                            }

                            if (preg_match('/Владение мячом/', $odd)) {
                                $statistic = preg_split('/Владение мячом/', $odd);

                                $ballpos_h = (int)$statistic[0];

                                $ballpos_g = (int)$statistic[1];

                            }

                            if (preg_match('/Штрафные/', $odd)) {
                                $statistic = preg_split('/Штрафные/', $odd);

                                $shtraf_h = (int)$statistic[0];

                                $shtraf_g = (int)$statistic[1];

                            }

                            if (preg_match('/Вбрасывания/', $odd)) {
                                $statistic = preg_split('/Вбрасывания/', $odd);

                                $outs_h = (int)$statistic[0];

                                $outs_g = (int)$statistic[1];

                            }

                        }
                    }

                    foreach ($tr as $node) {

                        if ($node->getAttribute('class') === 'even') {
                            $even = $node->nodeValue;


                            if (preg_match('/\vУдары\v/', $even)) {
                                $statistic = preg_split('/Удары/', $even);

                                $ud_h = (int)$statistic[0];

                                $ud_g = (int)$statistic[1];


                            }

                            if (preg_match('/Удары мимо/', $even)) {
                                $statistic = preg_split('/Удары мимо/', $even);

                                $ud_mim_h = (int)$statistic[0];

                                $ud_mim_g = (int)$statistic[1];


                            }

                            if (preg_match('/Офсайды/', $even)) {
                                $statistic = preg_split('/Офсайды/', $even);

                                $offside_h = (int)$statistic[0];

                                $offside_g = (int)$statistic[1];


                            }

                            if (preg_match('/Фолы/', $even)) {
                                $statistic = preg_split('/Фолы/', $even);

                                $falls_h = (int)$statistic[0];

                                $falls_g = (int)$statistic[1];


                            }

                            if (preg_match('/Удары в створ/', $even)) {
                                $statistic = preg_split('/Удары в створ/', $even);

                                $ud_v_stv_h = (int)$statistic[0];

                                $ud_v_stv_g = (int)$statistic[1];


                            }

                            if (preg_match('/Угловые/', $even)) {
                                $statistic = preg_split('/Угловые/', $even);

                                $corner_h = (int)$statistic[0];

                                $corner_g = (int)$statistic[1];


                            }

                            if (preg_match('/Сэйвы/', $even)) {
                                $statistic = preg_split('/Сэйвы/', $even);

                                $saves_h = (int)$statistic[0];

                                $saves_g = (int)$statistic[1];


                            }

                            if (preg_match('/Желтые карточки/', $even)) {
                                $statistic = preg_split('/Желтые карточки/', $even);

                                $yelkar_h = (int)$statistic[0];

                                $yelkar_g = (int)$statistic[1];


                            }
                            if (preg_match('/Владение мячом/', $even)) {
                                $statistic = preg_split('/Владение мячом/', $even);

                                $ballpos_h = (int)$statistic[0];

                                $ballpos_g = (int)$statistic[1];


                            }

                            if (preg_match('/Штрафные/', $even)) {
                                $statistic = preg_split('/Штрафные/', $even);

                                $shtraf_h = (int)$statistic[0];

                                $shtraf_g = (int)$statistic[1];

                            }

                            if (preg_match('/Вбрасывания/', $even)) {
                                $statistic = preg_split('/Вбрасывания/', $even);

                                $outs_h = (int)$statistic[0];

                                $outs_g = (int)$statistic[1];

                            }

                        }

                    }
                }


            }

            $table = $dom->getElementsByTagName("table");

            foreach ($table as $node) {

                if ($node->getAttribute('id') === 'parts') {
                    if (preg_match("/[0-9]{1} \- [0-9]{1} \- [0-9]{1} \- /", $node->nodeValue)) {

                        $stay = $node->nodeValue;
                        $sty = explode("Расстановка", $stay);
                        if (is_array($stay)) {
                            $stay_h = $sty[0];
                            $stay_g = $sty[1];
                        }


                    }
                }

                if ($node->getAttribute('class') === 'parts match-information') {
                    $info = substr($node->nodeValue, 34);

                }

            }

            $td = $dom->getElementsByTagName("td");

            foreach ($td as $node) {

                if ($node->getAttribute('class') === 'summary-vertical fl') {
                    $dt = $node->nodeValue;
                    $dom_in = new \DomDocument();
                    $html = $node->ownerDocument->saveHTML($node);
                    $newhtml = $head . $html;
                    $dom_in->loadHTML($newhtml);


                    $dv = $dom_in->getElementsByTagName("div");


                    foreach ($dv as $node) {

                        if ($node->getAttribute('class') === 'icon-box y-card') {


                            $yel_kart_h = $yel_kart_h . $dt . ", ";
                        }
                        if ($node->getAttribute('class') === 'icon-box r-card')
                            $red_kart_h = $red_kart_h . $dt . ", ";
                        if ($node->getAttribute('class') === 'icon-box yr-card')
                            $red_kart_h = $red_kart_h . $dt . "(вторая жёлтая), ";
                        if ($node->getAttribute('class') === 'icon-box substitution-in')
                            $substit_h = $substit_h . $dt . ", ";
                        if ($node->getAttribute('class') === 'icon-box soccer-ball')
                            $goul_h = $goul_h . $dt . ", ";
                        if ($node->getAttribute('class') === 'icon-box soccer-ball-own')
                            $goul_h = $goul_h . $dt . ", ";
                        if ($node->getAttribute('class') === 'icon-box penalty-missed')
                            $pen_miss_h = $pen_miss_h . $dt . ", ";


                    }


                    $sp = $dom_in->getElementsByTagName("span");

                    foreach ($sp as $node) {

                        for ($n = 0; $n < 250; $n++) {
                            if ($node->getAttribute('class') === "flag fl_$n") {
                                $stra_h = $stra_h . $n . "-" . $dt . ", ";
                            }
                        }


                    }


                }

                if ($node->getAttribute('class') === 'summary-vertical fr') {
                    $dt = $node->nodeValue;
                    $dom_in = new \DomDocument();
                    $html = $node->ownerDocument->saveHTML($node);
                    $dom_in->loadHTML($html);
                    $dv = $dom_in->getElementsByTagName("div");


                    foreach ($dv as $node) {

                        if ($node->getAttribute('class') === 'icon-box y-card') {
                            $yel_kart_g = $yel_kart_g . $dt . ", ";
                        }
                        if ($node->getAttribute('class') === 'icon-box r-card')
                            $red_kart_g = $red_kart_g . $dt . ", ";
                        if ($node->getAttribute('class') === 'icon-box yr-card')
                            $red_kart_g = $red_kart_g . $dt . "(вторая жёлтая), ";
                        if ($node->getAttribute('class') === 'icon-box substitution-in')
                            $substit_g = $substit_g . $dt . ", ";
                        if ($node->getAttribute('class') === 'icon-box soccer-ball')
                            $goul_g = $goul_g . $dt . ", ";
                        if ($node->getAttribute('class') === 'icon-box soccer-ball-own')
                            $goul_g = $goul_g . $dt . ", ";
                        if ($node->getAttribute('class') === 'icon-box penalty-missed')
                            $pen_miss_g = $pen_miss_g . $dt . ", ";

                    }

                    $sp = $dom_in->getElementsByTagName("span");

                    foreach ($sp as $node) {

                        for ($n = 0; $n < 250; $n++) {
                            if ($node->getAttribute('class') === "flag fl_$n") {
                                $stra_g = $stra_g . $n . "-" . $dt . ", ";
                            }
                        }


                    }
                }


                if ($node->getAttribute('class') === 'tname-home logo-enable')
                    $host = $node->nodeValue;
                if ($node->getAttribute('class') === 'tname-away logo-enable')
                    $guest = $node->nodeValue;
                if ($node->getAttribute('class') === 'current-result') {
                    $score = $node->nodeValue;
                    $sc = explode("-", $score);

                    $gett = (int)$sc[0];
                    $lett = (int)$sc[1];
                }
                if ($node->getAttribute('class') === 'mstat') {
                    if ($node->nodeValue !== 'Завершен') $prim = $prim . " " . $node->nodeValue;
                }

                /*if ($node->getAttribute('class') === 'mstat-date') {
                    $date_time = $node->nodeValue;
                    $dati = explode(" ", $date_time);

                    $date = $dati[0];
                    $time = $dati[1];
                }




                if ($node->getAttribute('class') === 'kx o_1')
                    $bet_h = (float)$node->nodeValue;
                if ($node->getAttribute('class') === 'kx o_0')
                    $bet_n = (float)$node->nodeValue;
                if ($node->getAttribute('class') === 'kx o_2')
                    $bet_g = (float)$node->nodeValue;
                if ($node->getAttribute('class') === 'kx o_1 winner')
                    $bet_h = (float)$node->nodeValue;
                if ($node->getAttribute('class') === 'kx o_0 winner')
                    $bet_n = (float)$node->nodeValue;
                if ($node->getAttribute('class') === 'kx o_2 winner')
                    $bet_g = (float)$node->nodeValue;
                */


            }
            $span = $dom->getElementsByTagName("span");

            foreach ($span as $node) {
                if ($node->getAttribute('class') === 'info-bubble')
                    $prim = $prim = $prim . " " . $node->nodeValue;
                if ($node->getAttribute('class') === 'p1_home')
                    $onehalf_h = (int)$node->nodeValue;
                if ($node->getAttribute('class') === 'p1_away')
                    $onehalf_g = (int)$node->nodeValue;
            }

            if(strstr($prim, 'еренесён') || strstr($prim, 'postponed') || strstr($prim, 'Перенесен')) continue;


            $match = new Matches();

            $match->date = $date;
            $match->time = $time;
            $match->tournament = $tournament;
            $match->host = $host;
            $match->guest = $guest;
            $match->gett = $gett;
            $match->lett = $lett;
            $match->stay_h = $stay_h;
            $match->stay_g = $stay_g;
            $match->yel_kart_h = $yel_kart_h;
            $match->yel_kart_g = $yel_kart_g;
            $match->red_kart_h = $red_kart_h;
            $match->red_kart_g = $red_kart_g;
            $match->substit_h = $substit_h;
            $match->substit_g = $substit_g;
            $match->goul_h = $goul_h;
            $match->goul_g = $goul_g;
            $match->pen_miss_h = $pen_miss_h;
            $match->pen_miss_g = $pen_miss_g;
            $match->onehalf_h = $onehalf_h;
            $match->onehalf_g = $onehalf_g;
            $match->prim = $prim;
            $match->info = $info;
            $match->stra_h = $stra_h;
            $match->stra_g = $stra_g;
            $match->ud_h = $ud_h;
            $match->ud_g = $ud_g;
            $match->ud_mim_h = $ud_mim_h;
            $match->ud_mim_g = $ud_mim_g;
            $match->offside_h = $offside_h;
            $match->offside_g = $offside_g;
            $match->falls_h = $falls_h;
            $match->falls_g = $falls_g;
            $match->ud_v_stv_h = $ud_v_stv_h;
            $match->ud_v_stv_g = $ud_v_stv_g;
            $match->corner_h = $corner_h;
            $match->corner_g = $corner_g;
            $match->saves_h = $saves_h;
            $match->saves_g = $saves_g;
            $match->yelkar_h = $yelkar_h;
            $match->yelkar_g = $yelkar_g;
            $match->ballpos_h = $ballpos_h;
            $match->ballpos_g = $ballpos_g;
            $match->shtraf_h = $shtraf_h;
            $match->shtraf_g = $shtraf_g;
            $match->outs_h = $outs_h;
            $match->outs_g = $outs_g;
            $match->bet_h = $bet_h;
            $match->bet_n = $bet_n;
            $match->bet_g = $bet_g;

            //var_dump($match);

            if($match->save(false)){
                $rec->match_id = $match->id;
                $rec->update(false);
                $last_id = $rec->id;

            }

        }

        $handle = fopen(Url::to("@app/web/uploads/soccertest.html"), "w");
        fwrite($handle,  $last_id);
        fclose($handle);

    }

    /**
     * Обращение к API d.soccerstand.com
     * @param $url
     * @return mixed
     */
    private function _soccerStandCurl($url)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                //'Accept-Encoding:gzip, deflate, sdch',
                //'Accept-Language:*',
                'Cache-Control:no-cache',
                'Connection:keep-alive',
                'Cookie:_dc_gtm_UA-28208502-12=1; _ga=GA1.2.1191596796.1477908016',
                'Host:d.soccerstand.com', 'Pragma:no-cache',
                'Referer:http://d.soccerstand.com/ru/x/feed/proxy-local',
                'User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
                'X-Fsign:SW9D1eZo',
                'X-GeoIP:1',
                'X-Requested-With:XMLHttpRequest',
                'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7'
            ]);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    /**
     *
     * @param $url
     * @return mixed
     */
    private
    function _soccerStandDetailCurl($url)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                //'Accept-Encoding:gzip, deflate, sdch',
                //'Accept-Language:*',
                'Cache-Control:no-cache',
                'Connection:keep-alive',
                'Cookie:my_project=455; _dc_gtm_UA-28208502-12=1; _gat_UA-28208502-12=1; _ga=GA1.2.1191596796.1477908016',
                'Host:www.soccerstand.com', 'Pragma:no-cache',
                //'Referer:http://d.soccerstand.com/ru/x/feed/proxy-local',
                'User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
                'Upgrade-Insecure-Requests:1',
                'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                //'X-Requested-With:XMLHttpRequest',
                //'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7'
            ]);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }


    public function actionGetSoccerstandMatchesByTournament()
    {
        $day_data = $this->_soccerStandDetailCurl("https://d.soccerstand.com/ru/x/feed/t_1_198_OMT80ou8_7_ru_1");

        $countries = explode(':',preg_replace("/[^-A-Za-z0-9а-ярьтцхчшуюэыйёА-ЯЁ.,!?:()№\/ ]+/", "", $day_data));

        //print_r($countries); exit;

        $i=0;

        $tournament = [];

        $matchs = [];

        foreach ($countries as $country) {

            //if($i>10) {print_r($country); exit;}

            //$matchs = explode('AB÷3¬CR÷3¬AC', $country);

            $matchs = preg_split('/¬AA/', $country, -1, PREG_SPLIT_DELIM_CAPTURE);

            //}

            //  exit;

            $n=0;
            foreach ($matchs as $match) {

                $recs = explode('¬', $match);
                // if($i>10) {print_r($recs); exit;}

                if($n==0)$tournament[$i][$n]['tour'] = $recs[0];

                foreach ($recs as $rec){


                    $arr_loc[$i][$n] = explode('÷', $rec);
                    //print_r($rec); exit;
                    if(is_array($arr_loc[$i])) {

                        if(isset($arr_loc[$i][$n][1]))
                            $tournament[$i][$n][$arr_loc[$i][$n][0]] = $arr_loc[$i][$n][1];
                    }
                    else{
                        $n++; continue;
                    }

                }
                $n++;
                // echo $n.PHP_EOL;
            }

            $i++;

        }

        // var_dump($tournament);

        // exit;

        // var_dump($matchs); exit;




        foreach ($tournament as $t){
            $zee = '';
            $zb = 0;
            $zy = '';
            $zc = '';
            $zd = '';
            $ze = 0;
            $zf = 0;
            $zh = '';
            $zj = 0;
            $zl = '';
            $zx = '';
            $zcc = 0;
            $aa = '';
            $ad = 0;


            if(isset($t[0]['ZEE']))
                $zee = $t[0]['ZEE'];
            if(isset($t[0]['ZB']))
                $zb = $t[0]['ZB'];
            if(isset($t[0]['ZY']))
                $zy = $t[0]['ZY'];
            if(isset($t[0]['ZC']))
                $zc = $t[0]['ZC'];
            if(isset($t[0]['ZD']))
                $zd = $t[0]['ZD'];
            if(isset($t[0]['ZE']))
                $ze = $t[0]['ZE'];
            if(isset($t[0]['ZF']))
                $zf = $t[0]['ZF'];
            if(isset($t[0]['ZH']))
                $zh = $t[0]['ZH'];
            if(isset($t[0]['ZJ']))
                $zj = $t[0]['ZJ'];
            if(isset($t[0]['ZL']))
                $zl = $t[0]['ZL'];
            if(isset($t[0]['tour']))
                $zx = trim($t[0]['tour']);
            if(isset($t[0]['ZCC']))
                $zcc = $t[0]['ZCC'];


            for($i=1; $i<count($t); $i++){

                //var_dump($t[$i]['']);

                $match_soc = new Soccercode();

                $match_soc->zee = $zee;
                $match_soc->zb = $zb;
                $match_soc->zy = $zy;
                $match_soc->zс = $zc;
                $match_soc->zd = $zd;
                $match_soc->ze = $ze;
                $match_soc->zf = $zf;

                $match_soc->zh = $zh;
                $match_soc->zj = $zj;
                $match_soc->zl = $zl;
                $match_soc->zx = $zx;
                $match_soc->zcc = $zcc;
                $match_soc->aa = $aa;
                $match_soc->ad = $ad;

                if(isset($t[$i]['']))
                    $match_soc->aa = $t[$i][''];
                if(isset($t[$i]['AD']))
                    $match_soc->ad = $t[$i]['AD'];

                if((time()-$match_soc->ad) < 3*60*60) continue;

                if(isset($t[$i]['CX']))
                    $match_soc->cx = $t[$i]['CX'];
                if(isset($t[$i]['AX']))
                    $match_soc->ax = $t[$i]['AX'];
                if(isset($t[$i]['AV']))
                    $match_soc->av = $t[$i]['AV'];
                if(isset($t[$i]['BX']))
                    $match_soc->bx = $t[$i]['BX'];
                if(isset($t[$i]['WN']))
                    $match_soc->wn = $t[$i]['WN'];
                if(isset($t[$i]['AF']))
                    $match_soc->af = $t[$i]['AF'];
                if(isset($t[$i]['WV']))
                    $match_soc->wv = $t[$i]['WV'];

                if(isset($t[$i]['AS']))
                    $match_soc->as = $t[$i]['AS'];
                if(isset($t[$i]['AZ']))
                    $match_soc->az = $t[$i]['AZ'];
                if(isset($t[$i]['AH']))
                    $match_soc->ah = $t[$i]['AH'];
                if(isset($t[$i]['BB']))
                    $match_soc->bb = $t[$i]['BB'];
                if(isset($t[$i]['BD']))
                    $match_soc->bd = $t[$i]['BD'];
                if(isset($t[$i]['WM']))
                    $match_soc->wm = $t[$i]['WM'];
                if(isset($t[$i]['AE']))
                    $match_soc->ae = $t[$i]['AE'];

                if(isset($t[$i]['ZA']))
                    $match_soc->za = $t[$i]['ZA'];
                if(isset($t[$i]['AG']))
                    $match_soc->ag = $t[$i]['AG'];
                if(isset($t[$i]['BA']))
                    $match_soc->ba = $t[$i]['BA'];
                if(isset($t[$i]['BC']))
                    $match_soc->bc = $t[$i]['BC'];
                if(isset($t[$i]['AN']))
                    $match_soc->an = $t[$i]['AN'];


                try {
                    if ($match_soc->save(false)) echo $match_soc->id . " success";
                    else echo $match_soc->id . " fail";
                } catch (IntegrityException $e) {
                    echo $e->getMessage();
                    continue;
                }
                //exit;


            }
        }

    }

    //
    function actionNewSoccercodeByHand()
    {
        $socc = new Soccercode();

        $socc->aa = 'CbW3drY9';
        $socc->ae = 'Манчестер Юнайтед';
        $socc->af = 'Манчестер Сити';
        $socc->ag = 1;
        $socc->ah = 3;

        if ($socc->save(false)) echo $socc->id . " success";
        else echo $socc->id . " fail";

    }


    function actionAddToMatchesFromToId($from_id=715113, $to_id=715301)
    {
        for ($i = $from_id; $i<=$to_id; $i++) {
            $this->soccerCodRec = Soccercode::findOne($i);
            if(!$this->soccerCodRec) continue;
            echo $i . PHP_EOL;
            $this->actionAddCouchesToMatch();
        }

    }

    function actionUpdateMatchesFromToId($from_id=962997, $to_id=964019)
    {
        for ($i = $from_id; $i<=$to_id; $i++) {
            $this->soccerCodRec = Soccercode::findOne($i);
            //var_dump($this->soccerCodRec); exit;
            if(!$this->soccerCodRec) continue;
            echo $i . PHP_EOL;
            $this->actionMatchUpdate($i);
        }

    }

    function actionAddCouchesToMatch()
    {

        $su = $this->getPlayersSoccerParts();
        $match = Matches::findOne($this->soccerCodRec->match_id);
        //var_dump($su); exit;
        if(isset($su['h_treneryi'])) {
            $match->coach_h = $su['h_treneryi'];
            if(isset($su['g_treneryi'])) {
                $match->coach_g = $su['g_treneryi'];
                if ($match->update()) echo $match->id . ' Coaches successfully added'. PHP_EOL;
                else echo $match->id . ' Coaches not added'. PHP_EOL;
            }
        }
        else echo $match->id . ' Coaches is absent';
    }

    function actionMatchUpdate($match_id=905056)
    {
        $this->soccerCodRec = Soccercode::findOne($match_id);

        $sost = $this->getPlayersSoccerParts();
        $bets = $this->getMatchBets();
        $time = $this->getMatchTime();
        $su = $this->getSu();
        $stats = $this->getStats();
        $total = $this->getTournament();
        $match_arr = array_merge($sost, $time, $bets, $su, $stats, $total);

        //var_dump($this->soccerCodRec); exit;

        $match = Matches::findOne($this->soccerCodRec->match_id);

        if($match){
            $match->date = isset($match_arr['date']) ? $match_arr['date'] : 0;
            $match->time = isset($match_arr['time']) ? $match_arr['time'] : 0;
            $match->tournament =
                isset($match_arr['tournament']) ?
                    $match_arr['tournament'] : ($this->soccerCodRec->zy.': '.$this->soccerCodRec->zx);
            $match->host = $this->soccerCodRec->ae ? $this->soccerCodRec->ae : $this->soccerCodRec->cx;
            $match->gett = $this->soccerCodRec->ag;
            $match->lett = $this->soccerCodRec->ah;
            $match->stay_h = isset($match_arr['h_rasstanovka']) ? $match_arr['h_rasstanovka'] : '';
            $match->stay_g = isset($match_arr['g_rasstanovka']) ? $match_arr['g_rasstanovka'] : '';
            $match->yel_kart_h = isset($match_arr['h_ycard']) ? $match_arr['h_ycard'] : '';
            $match->yel_kart_g = isset($match_arr['g_ycard']) ? $match_arr['g_ycard'] : '';
            $match->red_kart_h = isset($match_arr['h_rcard']) ? $match_arr['h_rcard'] : '';
            $match->red_kart_g = isset($match_arr['g_rcard']) ? $match_arr['g_rcard'] : '';
            $match->substit_h = isset($match_arr['h_sub']) ? $match_arr['h_sub'] : '';
            $match->substit_g = isset($match_arr['g_sub']) ? $match_arr['g_sub'] : '';
            $match->goul_h = isset($match_arr['h_gol']) ? $match_arr['h_gol'] : '';
            $match->goul_g = isset($match_arr['g_gol']) ? $match_arr['g_gol'] : '';
            $match->pen_miss_h = isset($match_arr['h_penalty_missed']) ? $match_arr['h_penalty_missed'] : '';
            $match->pen_miss_g = isset($match_arr['g_penalty_missed']) ? $match_arr['g_penalty_missed'] : '';
            $match->onehalf_h = isset($match_arr['p1_home']) ? (int)$match_arr['p1_home'] : 0;
            $match->onehalf_g = isset($match_arr['p1_away']) ? (int)$match_arr['p1_away'] : 0;
            $match->prim = isset($match_arr['prim']) ? $match_arr['prim'] : '';
            $match->info = isset($match_arr['info']) ? $match_arr['info'] : '';;
            $match->stra_h = isset($match_arr['h_startovyie-sostavyi']) ? $match_arr['h_startovyie-sostavyi'] : '';
            $match->stra_g = isset($match_arr['g_startovyie-sostavyi']) ? $match_arr['g_startovyie-sostavyi'] : '';
            $match->ud_h = isset($match_arr['h_udaryi']) ? (int)$match_arr['h_udaryi'] : 0;
            $match->ud_g = isset($match_arr['g_udaryi']) ? (int)$match_arr['g_udaryi'] : 0;
            $match->ud_mim_h = isset($match_arr['h_udaryi-mimo']) ? (int)$match_arr['h_udaryi-mimo'] : 0;
            $match->ud_mim_g = isset($match_arr['g_udaryi-mimo']) ? (int)$match_arr['g_udaryi-mimo'] : 0;
            $match->offside_h = isset($match_arr['h_ofsaydyi']) ? (int)$match_arr['h_ofsaydyi'] : 0;
            $match->offside_g = isset($match_arr['g_ofsaydyi']) ? (int)$match_arr['g_ofsaydyi'] : 0;
            $match->falls_h = isset($match_arr['h_folyi']) ? (int)$match_arr['h_folyi'] : 0;
            $match->falls_g = isset($match_arr['g_folyi']) ? (int)$match_arr['g_folyi'] : 0;
            $match->ud_v_stv_h = isset($match_arr['h_udaryi-v-stvor']) ? (int)$match_arr['h_udaryi-v-stvor'] : 0;
            $match->ud_v_stv_g = isset($match_arr['g_udaryi-v-stvor']) ? (int)$match_arr['g_udaryi-v-stvor'] : 0;
            $match->corner_h = isset($match_arr['h_uglovyie']) ? (int)$match_arr['h_uglovyie'] : 0;
            $match->corner_g = isset($match_arr['g_uglovyie']) ? (int)$match_arr['g_uglovyie'] : 0;
            $match->saves_h = isset($match_arr['h_seyvyi']) ? (int)$match_arr['h_seyvyi'] : 0;
            $match->saves_g = isset($match_arr['g_seyvyi']) ? (int)$match_arr['g_seyvyi'] : 0;
            $match->yelkar_h = isset($match_arr['h_jeltyie-kartochki']) ? (int)$match_arr['h_jeltyie-kartochki'] : 0;
            $match->yelkar_g = isset($match_arr['g_jeltyie-kartochki']) ? (int)$match_arr['g_jeltyie-kartochki'] : 0;
            $match->ballpos_h = isset($match_arr['h_vladenie-myachom']) ? $match_arr['h_vladenie-myachom'] : 0;
            $match->ballpos_g = isset($match_arr['g_vladenie-myachom']) ? $match_arr['g_vladenie-myachom'] : 0;
            $match->shtraf_h = isset($match_arr['h_shtrafnyie']) ? (int)$match_arr['h_shtrafnyie'] : 0;
            $match->shtraf_g = isset($match_arr['g_shtrafnyie']) ? (int)$match_arr['g_shtrafnyie'] : 0;
            $match->outs_h = 0;
            $match->outs_g = 0;
            $match->coach_g = isset($match_arr['g_treneryi']) ? $match_arr['g_treneryi'] : '';
            $match->coach_h = isset($match_arr['h_treneryi']) ? $match_arr['h_treneryi'] : '';
            $match->bet_h = isset($match_arr['bet_h']) ? (float)$match_arr['bet_h'] : 0;
            $match->bet_n = isset($match_arr['bet_n']) ? (float)$match_arr['bet_n'] : 0;
            $match->bet_g = isset($match_arr['bet_g']) ? (float)$match_arr['bet_g'] : 0;

            //var_dump($match); exit;

            if($match->update(false)){
                echo 'Match '.$match->id. ' success updated'.PHP_EOL;
            }
            else echo 'Match '.$match->id. ' upps'.PHP_EOL;
        }


        else echo 'Match is not'.PHP_EOL;

    }


    function actionNewStatSoccerstand()
    {
        $last_id = (int)file_get_contents(dirname(__DIR__)."/files/soccertest.html");

        if(!$last_id) {
            echo 'No id'.PHP_EOL;
            return;
        }

        $max_id = Soccercode::find()->select('MAX(id)')->scalar();

        for ($i = $last_id+1; $i<=$max_id; $i++) {
            echo $i . PHP_EOL;

            $rec = Soccercode::findOne($i);
            if (!$rec) continue;
            $this->soccerstandMatch($i);
        }

        $handle = fopen(dirname(__DIR__)."/files/soccertest.html", "w");
        fwrite($handle,  $this->soccerCodRec ? $this->soccerCodRec->id : $last_id);
        fclose($handle);
    }


    public function soccerstandMatch(
        $id
        //$id=710503
    )
    {
        // $host = ' '.$rec->ae; //номинальный хозяин
        //            $guest = $rec->af; //номинальный гость
        //            $score = ''; //счёт
        //            $gett = $rec->ag; //голы, забитые хозяевами
        //            $lett = $rec->ah; //голы, забитые гостями
        $this->soccerCodRec = Soccercode::findOne($id);

        $sost = $this->getPlayersSoccerParts(); //http://d.soccerstand.com/ru/x/feed/d_li_" . $this->soccerCodRec->aa . "_ru_1
        $bets = $this->getMatchBets(); //http://d.soccerstand.com/ru/x/feed/df_dos_1_" . $this->soccerCodRec->aa . "_"
        $time = $this->getMatchTime(); //http://d.soccerstand.com/ru/x/feed/df_dos_1_" . $this->soccerCodRec->aa . "_"
        $su = $this->getSu(); //https://d.soccerstand.com/ru/x/feed/d_su_ptj4063G_ru_1
        $stats = $this->getStats();
        $total = $this->getTournament();

        $match_arr = array_merge($sost, $time, $bets, $su, $stats, $total);

        //var_dump($this->soccerCodRec); exit;

        $match = new Matches();

        $match->date = isset($match_arr['date']) ? $match_arr['date'] : 0;
        $match->time = isset($match_arr['time']) ? $match_arr['time'] : 0;
        //$match->tournament = isset($match_arr['tournament']) ? $match_arr['tournament'] : 0;
        $match->tournament =
            isset($match_arr['tournament']) ?
                $match_arr['tournament'] : ($this->soccerCodRec->zy.': '.$this->soccerCodRec->zx);
        $match->host = $this->soccerCodRec->ae;
        $match->guest = $this->soccerCodRec->af;
        $match->gett = $this->soccerCodRec->ag;
        $match->lett = $this->soccerCodRec->ah;
        $match->stay_h = isset($match_arr['h_rasstanovka']) ? $match_arr['h_rasstanovka'] : '';
        $match->stay_g = isset($match_arr['g_rasstanovka']) ? $match_arr['g_rasstanovka'] : '';
        $match->yel_kart_h = isset($match_arr['h_ycard']) ? $match_arr['h_ycard'] : '';
        $match->yel_kart_g = isset($match_arr['g_ycard']) ? $match_arr['g_ycard'] : '';
        $match->red_kart_h = isset($match_arr['h_rcard']) ? $match_arr['h_rcard'] : '';
        $match->red_kart_g = isset($match_arr['g_rcard']) ? $match_arr['g_rcard'] : '';
        $match->substit_h = isset($match_arr['h_sub']) ? $match_arr['h_sub'] : '';
        $match->substit_g = isset($match_arr['g_sub']) ? $match_arr['g_sub'] : '';
        $match->goul_h = isset($match_arr['h_gol']) ? $match_arr['h_gol'] : '';
        $match->goul_g = isset($match_arr['g_gol']) ? $match_arr['g_gol'] : '';
        $match->pen_miss_h = isset($match_arr['h_penalty_missed']) ? $match_arr['h_penalty_missed'] : '';
        $match->pen_miss_g = isset($match_arr['g_penalty_missed']) ? $match_arr['g_penalty_missed'] : '';
        $match->onehalf_h = isset($match_arr['p1_home']) ? (int)$match_arr['p1_home'] : 0;
        $match->onehalf_g = isset($match_arr['p1_away']) ? (int)$match_arr['p1_away'] : 0;
        $match->prim = isset($match_arr['prim']) ? $match_arr['prim'] : '';
        $match->info = isset($match_arr['info']) ? $match_arr['info'] : '';;
        $match->stra_h = isset($match_arr['h_startovyie-sostavyi']) ? $match_arr['h_startovyie-sostavyi'] : '';
        $match->stra_g = isset($match_arr['g_startovyie-sostavyi']) ? $match_arr['g_startovyie-sostavyi'] : '';
        $match->ud_h = isset($match_arr['h_udaryi']) ? (int)$match_arr['h_udaryi'] : 0;
        $match->ud_g = isset($match_arr['g_udaryi']) ? (int)$match_arr['g_udaryi'] : 0;
        $match->ud_mim_h = isset($match_arr['h_udaryi-mimo']) ? (int)$match_arr['h_udaryi-mimo'] : 0;
        $match->ud_mim_g = isset($match_arr['g_udaryi-mimo']) ? (int)$match_arr['g_udaryi-mimo'] : 0;
        $match->offside_h = isset($match_arr['h_ofsaydyi']) ? (int)$match_arr['h_ofsaydyi'] : 0;
        $match->offside_g = isset($match_arr['g_ofsaydyi']) ? (int)$match_arr['g_ofsaydyi'] : 0;
        $match->falls_h = isset($match_arr['h_folyi']) ? (int)$match_arr['h_folyi'] : 0;
        $match->falls_g = isset($match_arr['g_folyi']) ? (int)$match_arr['g_folyi'] : 0;
        $match->ud_v_stv_h = isset($match_arr['h_udaryi-v-stvor']) ? (int)$match_arr['h_udaryi-v-stvor'] : 0;
        $match->ud_v_stv_g = isset($match_arr['g_udaryi-v-stvor']) ? (int)$match_arr['g_udaryi-v-stvor'] : 0;
        $match->corner_h = isset($match_arr['h_uglovyie']) ? (int)$match_arr['h_uglovyie'] : 0;
        $match->corner_g = isset($match_arr['g_uglovyie']) ? (int)$match_arr['g_uglovyie'] : 0;
        $match->saves_h = isset($match_arr['h_seyvyi']) ? (int)$match_arr['h_seyvyi'] : 0;
        $match->saves_g = isset($match_arr['g_seyvyi']) ? (int)$match_arr['g_seyvyi'] : 0;
        $match->yelkar_h = isset($match_arr['h_jeltyie-kartochki']) ? (int)$match_arr['h_jeltyie-kartochki'] : 0;
        $match->yelkar_g = isset($match_arr['g_jeltyie-kartochki']) ? (int)$match_arr['g_jeltyie-kartochki'] : 0;
        $match->ballpos_h = isset($match_arr['h_vladenie-myachom']) ? $match_arr['h_vladenie-myachom'] : 0;
        $match->ballpos_g = isset($match_arr['g_vladenie-myachom']) ? $match_arr['g_vladenie-myachom'] : 0;
        $match->shtraf_h = isset($match_arr['h_shtrafnyie']) ? (int)$match_arr['h_shtrafnyie'] : 0;
        $match->shtraf_g = isset($match_arr['g_shtrafnyie']) ? (int)$match_arr['g_shtrafnyie'] : 0;
        $match->outs_h = 0;
        $match->outs_g = 0;
        $match->coach_g = isset($match_arr['g_treneryi']) ? $match_arr['g_treneryi'] : '';
        $match->coach_h = isset($match_arr['h_treneryi']) ? $match_arr['h_treneryi'] : '';
        $match->bet_h = isset($match_arr['bet_h']) ? (float)$match_arr['bet_h'] : 0;
        $match->bet_n = isset($match_arr['bet_n']) ? (float)$match_arr['bet_n'] : 0;
        $match->bet_g = isset($match_arr['bet_g']) ? (float)$match_arr['bet_g'] : 0;

        //var_dump($match); exit;

        if($match->save(false)){
            $this->soccerCodRec->match_id = $match->id;
            $this->soccerCodRec->update(false);
            echo 'Match '.$match->id. ' success'.PHP_EOL;
        }
        else echo 'Match '.$match->id. ' upps'.PHP_EOL;

    }

    function getTournament()
    {
        $res = [];
        //https://www.soccerstand.com/ru/match/ptj4063G/#match-summary

        //$content = $this->_soccerStandCurl("https://www.soccerstand.com/ru/match/" . $this->soccerCodRec->aa . "/#match-summary");

        $content = $this->_soccerStandDetailCurl(
            "https://www.soccerstand.com/ru/match/" . $this->soccerCodRec->aa . "/#match-summary");

        //var_dump($content); exit;
        $dom = new \DOMDocument;
        $match = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $content;
        $dom->loadHTML($match, 100);

        $divs = $dom->getElementsByTagName("div");

        foreach ($divs as $span) {

            if ($span->getAttribute('class') === 'description') {
                $res['tournament'] = trim($this->DOMinnerHTML($span));
            }

        }

        $spans = $dom->getElementsByTagName("span");

        foreach ($spans as $span) {

            if ($span->getAttribute('class') === 'text') {
                $res['prim'] = trim($this->DOMinnerHTML($span));
            }

        }

        return $res;

    }

    function recursively_find_text_nodes($dom_element, $depth=1) {

        $return = array();

        foreach ($dom_element->childNodes as $dom_child) {

            switch ($dom_child->nodeType) {

                case XML_TEXT_NODE:
                    if (trim($dom_child->nodeValue) !== '') {
                        $return[] = array (
                            'depth' => $depth,
                            'value' => $dom_child->nodeValue
                        );
                    }
                    break;

                case XML_ELEMENT_NODE:
                    $return[] = array (
                        'depth' => $depth,
                        'value' => $dom_child
                    );

                    $return = array_merge($return, $this->recursively_find_text_nodes($dom_child, $depth+1));
                    break;
            }
        }

        return $return;
    }

    function getStats()
    {
        $res = [];
        $content = $this->_soccerStandCurl("http://d.soccerstand.com/ru/x/feed/d_st_" . $this->soccerCodRec->aa . "_ru_1");

        $dom = new \DOMDocument;
        $match = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $content;
        $dom->loadHTML($match);

        $divs = $dom->getElementsByTagName("div");

        foreach ($divs as $div) {
            if ($div->getAttribute('class') === 'statText statText--titleValue') {
                if($div->parentNode->parentNode->parentNode->getAttribute('id') === 'tab-statistics-0-statistic'){
                    $head = trim($this->DOMinnerHTML($div));
                    $res['h_'.TranslateHelper::translit($head)] = $this->DOMinnerHTML($div->previousSibling);
                    $res['g_'.TranslateHelper::translit($head)] = $this->DOMinnerHTML($div->nextSibling);
                }

            }

        }

        return $res;


    }

    /**
     *
     * @return array
     */
    function getSu()
    {
        $res = [];
        $str_inf = '';
        $str_h_gol = '';
        $str_h_ycard = '';
        $str_h_rcard = '';
        $str_h_sub = '';
        $str_g_gol = '';
        $str_g_ycard = '';
        $str_g_rcard = '';
        $str_g_sub = '';
        $str_g_penalty_missed = '';
        $str_h_penalty_missed = '';
        $str_half_g = '';
        $str_half_h = '';

        $content = $this->_soccerStandCurl("http://d.soccerstand.com/ru/x/feed/d_su_" . $this->soccerCodRec->aa . "_ru_1");

        $dom = new \DOMDocument;
        $match = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $content;
        $dom->loadHTML($match);

        $spans = $dom->getElementsByTagName("span");

        foreach ($spans as $span){

            if($span->getAttribute('class') === 'p1_home'){
                $str_half_h .= trim($this->DOMinnerHTML($span));
            }
            $res['p1_home'] = $str_half_h;
            if($span->getAttribute('class') === 'p1_away'){
                $str_half_g .= trim($this->DOMinnerHTML($span));
            }
            $res['p1_away'] = $str_half_g;
        }

        $divs = $dom->getElementsByTagName("div");

        foreach ($divs as $div){
            if($div->getAttribute('class') === 'match-information-data'){
                $str_inf .= trim($this->DOMinnerHTML($div)).',';
            }
            $res['info'] = $str_inf;




            if ($div->getAttribute('class') === 'detailMS__incidentRow incidentRow--home odd' ||
                $div->getAttribute('class') ==='detailMS__incidentRow incidentRow--home even') {


                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        //var_dump($node); exit;
                        if($node->nodeType != XML_TEXT_NODE){
                            if($node->getAttribute('class') ==='icon-box soccer-ball' ||
                                $node->getAttribute('class') ==='icon-box soccer-ball-own')
                                $str_h_gol .= trim($this->DOMinnerHTML($div)).',';
                        }

                    }
                    $res['h_gol'] = $str_h_gol;
                    //$str_h = '';
                }

                //$str_h .= trim($this->DOMinnerHTML($div)).',';
                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box y-card')
                                $str_h_ycard .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['h_ycard'] = $str_h_ycard;
                    //$str_h = '';
                }

                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box r-card' ||
                                $node->getAttribute('class') === 'icon-box yr-card')
                                $str_h_rcard .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['h_rcard'] = $str_h_rcard;
                    //$str_h = '';
                }

                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box penalty-missed')
                                $str_h_penalty_missed .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['h_penalty_missed'] = $str_h_penalty_missed;
                    //$str_h = '';
                }

                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box substitution-in')
                                $str_h_sub .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['h_sub'] = $str_h_sub;
                    //$str_h = '';
                }

                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box soccer-ball-own')
                                $str_h_sub .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['h_own'] = $str_h_sub;
                    //$str_h = '';
                }

            }

            if ($div->getAttribute('class') === 'detailMS__incidentRow incidentRow--away odd' ||
                $div->getAttribute('class') ==='detailMS__incidentRow incidentRow--away even') {

                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box soccer-ball' ||
                                $node->getAttribute('class') === 'icon-box soccer-ball-own')
                                $str_g_gol .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['g_gol'] = $str_g_gol;
                    //$str_h = '';
                }

                //$str_h .= trim($this->DOMinnerHTML($div)).',';
                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box y-card')
                                $str_g_ycard .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['g_ycard'] = $str_g_ycard;
                    //$str_h = '';
                }

                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box r-card' ||
                                $node->getAttribute('class') === 'icon-box yr-card')
                                $str_g_rcard .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['g_rcard'] = $str_g_rcard;
                    //$str_h = '';
                }

                //if ($node->getAttribute('class') === 'icon-box penalty-missed')
                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box penalty-missed')
                                $str_g_penalty_missed .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['g_penalty_missed'] = $str_g_penalty_missed;
                    //$str_h = '';
                }

                if($div->childNodes){
                    foreach ($div->childNodes as $node){
                        if($node->nodeType != XML_TEXT_NODE) {
                            if ($node->getAttribute('class') === 'icon-box substitution-in')
                                $str_g_sub .= trim($this->DOMinnerHTML($div)) . ',';
                        }
                    }
                    $res['g_sub'] = $str_g_sub;
                    //$str_h = '';
                }

            }
        }
        return $res;

    }

    function getMatchTime()
    {
        $res = [];

        $res['date'] = date('d.m.Y', $this->soccerCodRec->ad+7*60*60);
        $res['time'] = date('H:i', $this->soccerCodRec->ad+7*60*60);

        //description__match

        return $res;
    }

    function getMatchBets(): array
    {
        $res = [];
        $prepared = [];
        $bets['bet_g'] = 0;
        $bets['bet_n'] = 0;
        $bets['bet_h'] = 0;

        $data = explode('¬',
            $this->_soccerStandCurl("http://d.soccerstand.com/ru/x/feed/df_dos_1_" . $this->soccerCodRec->aa . "_"));

        if(isset($data[1])){
            $prepared = explode('÷', $data[1]);
        }

        if (isset($prepared[1])) {

            $t = (array)json_decode($prepared[1]);

            foreach ($t as $key => $bet){
                $bets['bet_g'] = (float)$bet[2];  //ставки
                $bets['bet_h'] = (float)$bet[0];
                $bets['bet_n'] = (float)$bet[1];
            }

        }

        return $bets;
    }

    /**
     * @return array
     */
    function getPlayersSoccerParts(): array
    {
        $res = [];
        $str = '';
        $str_h = '';
        $str_g = '';
        $head = '';
        $g_h = '';

        $content = $this->_soccerStandCurl("http://d.soccerstand.com/ru/x/feed/d_li_" . $this->soccerCodRec->aa . "_ru_1");

        $content = str_replace(chr(9), '', $content);
        $content = str_replace(chr(11), '', $content);  // заменяем табуляцию на пробел
        $content = str_replace(chr(13), '', $content);
        $content = str_replace(chr(10), '', $content);
        //$content = substr($content, 100);

        //$dom = $this->getDOM($content);
        $dom = new \DOMDocument;
        $match = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $content;
        $dom->loadHTML($match);

        $tds = $dom->getElementsByTagName("td");

        foreach ($tds as $td) {
            if ($td->getAttribute('class') === 'h-part') {
                if($str_h) {
                    $res['h_'.TranslateHelper::translit($head)] = $str_h;
                    $str_h = '';
                }
                if($str_g) {
                    $res['g_'.TranslateHelper::translit($head)] = $str_g;
                    $str_g = '';
                }
                $head = trim($this->DOMinnerHTML($td));
                if ($head === 'Расстановка') {
                    $res['h_'.TranslateHelper::translit($head)] = $this->DOMinnerHTML($td->previousSibling);
                    $res['g_'.TranslateHelper::translit($head)] = $this->DOMinnerHTML($td->nextSibling);
                }
            }

            if ($td->getAttribute('class') === 'summary-vertical fl') {
                $g_h = 'h_';
                $str_h .= trim($this->DOMinnerHTML($td)).',';

            }

            if ($td->getAttribute('class') === 'summary-vertical fr') {
                $g_h = 'g_';
                $str_g .= trim($this->DOMinnerHTML($td)).',';

            }

            if($str_h) $res['h_'.TranslateHelper::translit($head)] = $str_h;
            if($str_g) $res['g_'.TranslateHelper::translit($head)] = $str_g;

        }

        return $res;

    }

    function nextElementSibling($node)
    {
        while ($node && ($node = $node->nextSibling)) {
            if ($node instanceof \DOMElement) {
                break;
            }
        }
        return $node;
    }


    function getDOM($content)
    {
        /*$dom = new \DOMDocument;
        $match = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $content;
        $dom->loadHTML($match);
        return $dom;
        */
    }

    function DOMinnerHTML(\DOMNode $element)
    {
        $innerHTML = "";
        $children  = $element->childNodes;

        foreach ($children as $child)
        {

            $innerHTML .= $child->nodeValue;
        }

        return $innerHTML;
    }

    function actionSoccApi()
    {


        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://elenasport-io1.p.rapidapi.com/v2/countries/222",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "x-rapidapi-host: elenasport-io1.p.rapidapi.com",
                "x-rapidapi-key: 53d7f7d1dbmsh6a8caa1eeff6586p140e03jsnca4de7720572"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

}