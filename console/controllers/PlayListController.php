<?php

namespace console\controllers;

use core\entities\Rockncontroll\Author;
use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\RadioSongs;
use core\entities\Rockncontroll\RadioSource;
use core\entities\Rockncontroll\SongText;
use core\entities\Rockncontroll\Source;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class PlayListController extends Controller
{
    public function actionAll(){
        $f = fopen("playlist/all.txt", 'w');
        //$f = fopen("/home/romanych/radio/dio/playlist2.txt", 'w');

        $arr = [];
        $arr_other = [];
        $songs = [];
        $sources = [];
        $dibilizmy = Items::find()
            ->where(["source_id" => 17])
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->andWhere("parent_item_id = 0")
            ->all();
        shuffle($dibilizmy);
        $limerik = Items::find()
            ->where(["source_id" => 27])
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->andWhere("parent_item_id = 0")
            ->all();
        shuffle($limerik);
        $kvn = Items::find()
            ->where(["source_id" => 21])
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->andWhere("parent_item_id = 0")
            ->all();
        shuffle($kvn);
        $cavers = Items::find()
            ->where(["source_id" => 38])
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->andWhere("parent_item_id = 0")
            ->all();
        shuffle($cavers);
        $frazy = Items::find()
            ->where("source_id = 181 or source_id = 37 or 
                         source_id = 30 or source_id = 29 or 
                         source_id = 25 or source_id = 528 or
                         source_id = 10 or source_id = 484 or 
                         source_id = 486 or source_id = 1468 or
                         source_id = 463 or source_id = 15 or
                         source_id = 415 or source_id = 460
                         ")
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->andWhere("parent_item_id = 0")
            ->all();
        shuffle($frazy);
        $peredelki = Items::find()
            ->where(["source_id" => 19])
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->andWhere("parent_item_id = 0")
            ->all();
        shuffle($peredelki);
        $pesni = Items::find()
            ->where("source_id = 6 or source_id = 40")
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->andWhere("parent_item_id = 0")
            ->all();
        shuffle($pesni);

        $film = Items::find()
            ->where(["source_id" => 20])
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->all();
        shuffle($film);
        /*
        $film = array_slice($film, 0, 20);


        $eng = Items::find()
            ->where(["cat_id" => 155])
            ->andWhere("in_work_prim = ''")
            ->andWhere("audio_link <> ''")
            ->all();
        shuffle($eng);
        $eng = array_slice($eng, 0, 20);
        */

        $max_song_id = (int)SongText::find()
            ->select('MAX(id)')
            ->scalar();


        $authors_ids = ArrayHelper::map(Author::find()->all(), 'id', 'id');


        //var_dump($authors_ids); exit;




        foreach ($authors_ids as $id){
            //echo $id.PHP_EOL;
            //Трахтенберг нах в его терминологии, Красная плесень туда же
            if(
                $id != 218
                //&&
                //$id != 209
                //&&
                //$id != 649
            ) {
                //echo $id.PHP_EOL; exit;
                // echo rand(0, count(Source::find()->where(["author_id" => $id])->all())-1); exit;
                if(isset(Source::find()->where(["author_id" => $id])->all()[rand(0, count(Source::find()->where(["author_id" => $id])->all())-1)]->id))
                    $sources[] = Source::find()->where(
                        ["author_id" => $id])->all()[rand(0, count(Source::find()->where(["author_id" => $id])->all())-1)]->id;
                /*@ToDO popular autors ids*/
                if($id === 86 or $id === 184) {
                    $sources[] = Source::find()
                        ->where(["author_id" => $id])
                        ->all()[rand(0, count(Source::find()->where(["author_id" => $id])->all())-1)]->id;
                }

            }
            //exit;
            // $sources[] = Source::find()->where(["author_id" => $id])->all()[rand(0, count(Source::find()->where(["author_id" => $id])->all())-1)]->id;
            //else echo $id;
        }

        //exit;

        shuffle($sources);




        foreach ($sources as $id){
            $source_songs = SongText::find()->where(["source_id" => $id])->all();
            if(isset($source_songs) && count($source_songs)>0) {
                $songs[] = $source_songs[rand(0, count($source_songs)-1)];
            }
        }

        for ($i = 0; $i < 2000; $i++) {
            if (isset($kvn[$i]))
                array_push($arr, $kvn[$i]);
            //if (isset($songs[$i]))
            //    array_push($arr, $songs[$i]);
            if (isset($dibilizmy[$i]))
                array_push($arr, $dibilizmy[$i]);
            if (isset($limerik[$i]))
                array_push($arr, $limerik[$i]);
            if (isset($cavers[$i]))
                array_push($arr, $cavers[$i]);
            if (isset($frazy[$i]))
                array_push($arr, $frazy[$i]);
            if (isset($peredelki[$i]))
                array_push($arr, $peredelki[$i]);
            if (isset($film[$i]))
                array_push($arr, $film[$i]);
            if (isset($pesni[$i]))
                array_push($arr, $pesni[$i]);
            /*
            if (isset($eng[$i]))
                array_push($arr, $eng[$i]);
            */
        }

        shuffle($arr);
        //print_r($songs); exit;

        for ($i = 0; $i < 2000; $i++) {
            if (isset($arr[$i]))
                array_push($arr_other, $arr[$i]);
            // else echo $i.' my not set'.PHP_EOL;
            if (isset($songs[$i]))
                array_push($arr_other, $songs[$i]);
            if(count($songs)<$i) break;
            // else echo $i.' song not set'.PHP_EOL;
        }

        //var_dump($arr);
        //exit;

        $n=0;
        foreach ($arr_other as $item) {
            $n++;
            if($item instanceof Items) {
                //if (!strstr($item->audio_link, '.mp3'))
                // echo $item->audio_link.PHP_EOL;
                if (strstr($item->audio_link, '/music')) {
                    $one = str_replace('/music', 'music', $item->audio_link);
                    fwrite($f, "/home/romanych/" . $one . PHP_EOL);
                    if ($item->bind_item_id && $next_item = Items::findOne($item->bind_item_id)) {
                        if ($next_item->audio_link != '' && $next_item->in_work_prim == '') {
                            if (strstr($next_item->audio_link, '/music')) {
                                fwrite($f, "/home/romanych/" . str_replace('/music', 'music', $next_item->audio_link) . PHP_EOL);
                            }
                            elseif (strstr($next_item->audio_link, 'uploads')) {
                                //$one = str_replace('/music', 'music', $item->audio_link);
                                fwrite($f, "/home/romanych/public_html/plis/basic/web" . $next_item->audio_link . PHP_EOL);
                            }
                            elseif ($next_item->audio_link) {
                                fwrite($f, "/home/romanych/Музыка/Thoughts_and_klassik/new_ideas/" . $next_item->audio_link . PHP_EOL);
                            }
                        }
                    }
                }
                elseif (strstr($item->audio_link, 'uploads')) {
                    //$one = str_replace('/music', 'music', $item->audio_link);
                    fwrite($f, "/home/romanych/public_html/plis/basic/web" . $item->audio_link . PHP_EOL);
                    if ($item->bind_item_id && $next_item = Items::findOne($item->bind_item_id)) {
                        if ($next_item->audio_link != '' && $next_item->in_work_prim == '') {
                            if (strstr($next_item->audio_link, '/music')) {
                                fwrite($f, "/home/romanych/" . str_replace('/music', 'music', $next_item->audio_link) . PHP_EOL);
                            }
                            elseif (strstr($next_item->audio_link, 'uploads')) {
                                //$one = str_replace('/music', 'music', $item->audio_link);
                                fwrite($f, "/home/romanych/public_html/plis/basic/web" . $next_item->audio_link . PHP_EOL);
                            }
                            elseif ($next_item->audio_link) {
                                fwrite($f, "/home/romanych/Музыка/Thoughts_and_klassik/new_ideas/" . $next_item->audio_link . PHP_EOL);
                            }
                        }
                    }

                }
                elseif ($item->audio_link)  {
                    fwrite($f, "/home/romanych/Музыка/Thoughts_and_klassik/new_ideas/" . $item->audio_link . PHP_EOL);
                    if ($item->bind_item_id && $next_item = Items::findOne($item->bind_item_id)) {
                        if ($next_item->audio_link != '' && $next_item->in_work_prim == '') {
                            if (strstr($next_item->audio_link, '/music')) {
                                fwrite($f, "/home/romanych/" . str_replace('/music', 'music', $next_item->audio_link) . PHP_EOL);
                            }
                            elseif (strstr($next_item->audio_link, 'uploads')) {
                                //$one = str_replace('/music', 'music', $item->audio_link);
                                fwrite($f, "/home/romanych/public_html/plis/basic/web" . $next_item->audio_link . PHP_EOL);
                            }
                            elseif ($next_item->audio_link) {
                                fwrite($f, "/home/romanych/Музыка/Thoughts_and_klassik/new_ideas/" . $next_item->audio_link . PHP_EOL);
                            }
                        }
                    }
                }

                //else echo $item->audio_link.' Items '.PHP_EOL;

            }
            elseif ($item instanceof SongText){
                //if (!strstr($item->link, '.mp3')) echo $item->link.PHP_EOL;
                if(strstr($item->link, '/musicplis')) fwrite($f, $item->link . PHP_EOL);
                elseif(strstr($item->link, 'best_alboms')) fwrite($f, "/home/romanych/Музыка/Thoughts_and_klassik".$item->link . PHP_EOL);
                elseif(strstr($item->link, '/music/Music/')) fwrite($f, "/home/romanych".$item->link . PHP_EOL);
                elseif(strstr($item->link, '/stodesatmus/music/')) fwrite($f, "/home/romanych".$item->link . PHP_EOL);
                if ($n % 15 == 0) fwrite($f, "/home/romanych/Музыка/Thoughts_and_klassik/new_ideas/mp3/ohohoho.mp3" . PHP_EOL);
                if ($n % 10 == 0) fwrite($f, "/home/romanych/Музыка/Thoughts_and_klassik/new_ideas/mp3/komnata_s_mehom.mp3" . PHP_EOL);
                //else echo $item->id.' Song ' .PHP_EOL;
            }
            //else echo var_dump($item);
        }

        fclose($f);


    }

    function actionAddAlbomToAuthor($artist){
        $dir_list = [
            //'/home/romanych/Музыка/Thoughts_and_klassik/best_alboms/'. $artist,
            '/var/www/familykey/static/music/'. $artist,
            //'/home/romanych/music/Music/'. $artist,
            //'/home/romanych/stodesatmus/music/'. $artist,
        ];
        //$dir = '/home/romanych/Музыка/Thoughts_and_klassik/best_alboms/' . $new_artist;

        //return var_dump($dir);

        $i=0;

        foreach ($dir_list as $dir) {
            $i++;

            try {
                $alboms = scandir($dir);
            } catch (\ErrorException $e) {
                echo $e->getMessage() . PHP_EOL;
                continue;
            }

            if (is_array($alboms)) {
                $alboms = array_diff($alboms, array('.', '..'));

                if ($alboms) {
                    foreach ($alboms as $albom) {
                        //echo $albom; exit;
                        //$r = Source::find()->where('title LIKE "%'.$albom.'%"')->one();
                        // var_dump($r);
                        if(!Source::find()->where('title LIKE "'.addslashes($albom).'"')->one()) {
                            $source = new Source();
                            //echo '4'; exit;

                            //$radio_source = new RadioSource();

                            $source->title = $albom;

                            // echo '4'; exit;
                            // var_dump(Author::find()->where('name like "%' . addslashes($artist) . '%"')->one()); exit;
                            // echo $artist;

                            if (Author::find()->where('name like "%'. trim(addslashes($artist)) .'%"')->one()){
                                $source->author_id = Author::find()->where("name like '%" . trim(addslashes($artist)) . "%'")->one()->id;
                            }

                            else {
                                echo('author error');
                                exit;
                            }


                            $source->status = 1;
                            $source->cat_id = 34;
                            if (!$source->save(false)) return 'source error';


                            $radio_source = new RadioSource();
                            $radio_source->id = $source->id;
                            $radio_source->title = $albom;
                            $radio_source->author_id = $source->author_id;

                            if (!$radio_source->save(false)) return 'radio source error';

                            else echo $radio_source->title . ' made' . PHP_EOL;

                            if($dir == '/home/romanych/music/Music/'. $artist)
                                $path = '/home/romanych/music/Music/'. $artist . '/' . $albom;
                            else
                                $path = $dir . '/' . $albom;


                            if (is_dir($path)) {
                                //return var_dump($path);
                                $songs = scandir($path);
                                $songs = array_diff($songs, array('.', '..'));

                                //return var_dump($songs);
                                foreach ($songs as $song) {

                                    $song_obj = new SongText();
                                    try {
                                        $song_obj->source_id = $source->id;
                                    } catch (\ErrorException $e) {
                                        echo $e->getMessage();
                                        continue;
                                    }

                                    $song_path = $path . '/' . $song;
                                    //return var_dump($song_path);
                                    if (is_dir($song_path)) {
                                        $sub_songs = scandir($song_path);
                                        $sub_songs = array_diff($sub_songs, array('.', '..'));
                                        foreach ($sub_songs as $sub_song) {
                                            if (preg_match('/(.+).mp3$/', $sub_song, $match))
                                                $song_obj->title = mb_convert_encoding($sub_song, "UTF-8");
                                            if($i==1)$song_obj->link = substr($path . '/' . $song . '/' . $sub_song, 48);
                                            else $song_obj->link = $path . '/' . $song . '/' . $sub_song;
                                            //echo mb_detect_encoding($song_obj->link);
                                        }
                                    } else
                                        if (preg_match('/(.+).mp3$/', $song, $match)) {
                                            $song_obj->title = mb_convert_encoding($song, "UTF-8");;
                                            if($i==1)$song_obj->link = substr($path . '/' . $song, 48);
                                            else $song_obj->link = $path . '/' . $song;
                                            //return var_dump($song_obj);
                                            //echo mb_detect_encoding($song_obj->link);
                                        }

                                    $song_obj->save(false);

                                    $max_id = (int)RadioSongs::find()
                                        ->select('MAX(id)')
                                        ->scalar();

                                    $song_postg = new RadioSongs();
                                    $song_postg->id = $max_id+1;
                                    $song_postg->source_id = $song_obj->source_id;
                                    if($song_obj->title) $song_postg->title = $song_obj->title;
                                    else $song_postg->title = '';
                                    if($song_obj->link)$song_postg->link = $song_obj->link;
                                    else $song_postg->link = '';
                                    $song_postg->text = '';
                                    $song_postg->save(false);
                                }
                            } else {
                                echo $path . '-----no---dir--------------';
                            }
                        }

                        else echo $albom.' is real '.PHP_EOL;

                    }
                }
            }
        }
    }

}