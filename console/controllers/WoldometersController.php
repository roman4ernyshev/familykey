<?php

namespace console\controllers;

use core\entities\Coronovirus;
use yii\console\Controller;
use yii\helpers\Url;

class WoldometersController extends Controller
{

    function get_page($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                //'Accept-Encoding: gzip, deflate, sdch',
                'Accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
                'Cache-Control:max-age=0',
                'Connection:keep-alive',
                'Cookie: p=F5AAAGL7ngAA; 
                        mrcu=6E435BE3CBDE79FF7C7153BBC025; 
                        searchuid=1756633951436114371; 
                        i=AQDhK/ZbCAATAAhkIZUAAsAAAscAAQkBAR8BAWEBAXoBAc8BARoCARsCAeoCAYUEAY0EAVwGAtwHAecHAlsIAXQIAXgIATUJAToJAU8JAWcJAWwJAXEJAVoKAV8KAWMKAW4KAZgKAUILAVEQAV4QAZMCCH8qbAABbQABbwABcgABdAABdgABrAABygABzwABFQEBMAEBMgEBOwEBRQEBYwEBaQEBbwEBAgIBBQIBCQIBDwIBEQIBFgIBagIBmwIBtAIBuAIB4QIBUQQBUwQBSAUBSgUBaAUBcwUBdgUBoAUBowUBpQUBqAUBHgYBegYBzmMB3AQIBAEBAAHhBAkBAeIECgQBAW0HOgUIBAF3BwG9BwgEAa4VAYYLBQIBAA==; 
                        c=QoH2WwEAEHsTAAAUAgAACQCgHyi7/YDXDQAA; 
                        date=; 
                        VID=3YVxr521QAHl00000B0M54Hl::439623451:0-0-11198bb; 
                        b=wUUCAIA3DD4AAQAAxPLjtPCiThyGt+ngMJCDqjbBDFS1CaZLG2QA; 
                        tmr_detect=0%7C1542941445300',
                'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                //'X-Requested-With:XMLHttpRequest',
                'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7'
            ]);
        //curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        //curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)");
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return ($httpCode == 200) ? $data : false;
    }

    public function actionParsCoronvirus()
    {
        $url = 'https://www.worldometers.info/coronavirus/';
        $content = $this->get_page($url);

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $head = file_get_contents(Url::to("@static/header.html"));
        $content = $head . $content; //добавляем хэдер

        try {
            $dom->loadHTML($content);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        $trs = $dom->getElementById("main_table_countries_today")->getElementsByTagName("tr");

        $arr = [];
        foreach($trs as $key=>$tr){

            foreach ($tr->childNodes as $child){
                if($child->nodeName === 'td'){
                    $arr[$key][] = trim($child->textContent);
                }
            }
            /**
             * $string = str_replace(' ', '-', $string);
             * $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
             */
            if($arr) {
                $coutry_string = new Coronovirus();
                $coutry_string->time = time();
                $coutry_string->country = $arr[$key][0];
                $coutry_string->total_cases = (int)preg_replace('/[^0-9]/', '', $arr[$key][1]);
                $coutry_string->new_cases = (int)preg_replace('/[^0-9]/', '', $arr[$key][2]);
                $coutry_string->total_deaths = (int)preg_replace('/[^0-9]/', '', $arr[$key][3]);
                $coutry_string->new_deaths = (int)preg_replace('/[^0-9]/', '', $arr[$key][4]);
                $coutry_string->total_recovered = (int)preg_replace('/[^0-9]/', '', $arr[$key][5]);
                $coutry_string->active_cases = (int)preg_replace('/[^0-9]/', '', $arr[$key][6]);
                $coutry_string->serious_critical = (int)preg_replace('/[^0-9]/', '', $arr[$key][7]);
                $coutry_string->tot_pop = (int)preg_replace('/[^0-9]/', '', $arr[$key][8]);
                //var_dump($coutry_string); exit;

                $coutry_string->save(false);
            }
        }
    }

}