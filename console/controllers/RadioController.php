<?php
namespace console\controllers;

use core\entities\Listener\Listener;
use core\entities\Listener\RadioItems;
use core\entities\Rockncontroll\Author;
use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\SongText;
use core\entities\Rockncontroll\Source;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class RadioController extends Controller
{
    function getRemoteCurrentTrackText()
    {
        $file = html_entity_decode(strip_tags(file("http://radiorooma.ru:8000/status.xsl?mount=/test")[68]));
        $item = RadioItems::find()->where(['like', 'audio', trim($file)])->one();
        if($item) return $item->title;
        return var_dump($file);

    }

    function actionRadioEnters()
    {

        $page = $this->_conn("http://radiorooma.ru:8000/admin/listclients.xsl?mount=/test");
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($page);

        echo $this->getRemoteCurrentTrackText().'-----------------------'.PHP_EOL;

        for($i=8;$i<$dom->getElementsByTagName("td")->length;$i=$i+4) {
            $listener = new Listener();
            for($j=0;$j<4;$j++){

                if($dom->getElementsByTagName("td")[$i+$j]) {
                    if ($j == 0) {
                        $listener->current_track = $this->getRemoteCurrentTrackText();
                        $listener->ip = $dom->getElementsByTagName("td")[$i+$j]->nodeValue;
                        echo $listener->ip.PHP_EOL;
                    }
                    if ($j == 1) {
                        $listener->seconds = $dom->getElementsByTagName("td")[$i+$j]->nodeValue;
                        echo $listener->seconds.PHP_EOL;
                    }
                    if ($j == 2) {
                        $listener->user_agent = $dom->getElementsByTagName("td")[$i+$j]->nodeValue;
                        echo $listener->user_agent.PHP_EOL;
                    }
                    if ($j == 3) {
                        $listener->time = time();
                        $listener->save(false);
                        break;
                    }
                    echo $i.' '. $dom->getElementsByTagName("td")[$i]->nodeValue . PHP_EOL;
                }
                else {
                    echo PHP_EOL;
                }
            }

        }

    }

    private function _conn($url)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        // указываем имя и пароль
        //curl_setopt($ch, CURLOPT_USERPWD, "admin:vbifcdtnf");
        curl_setopt($ch, CURLOPT_USERPWD, "admin:rhfnjxdbkb10");
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                //'Accept-Encoding:gzip, deflate, sdch',
                //'Accept-Language:*',
                'Cache-Control:no-cache',
                'Connection:keep-alive',
                'Cookie:my_project=455; _dc_gtm_UA-28208502-12=1; _gat_UA-28208502-12=1; _ga=GA1.2.1191596796.1477908016',
                'Host:www.soccerstand.com', 'Pragma:no-cache',
                //'Referer:http://d.soccerstand.com/ru/x/feed/proxy-local',
                'User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
                'Upgrade-Insecure-Requests:1',
                'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                //'X-Requested-With:XMLHttpRequest',
                //'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7'
            ]);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    public function actionPlayListTestDump(){

        $f = fopen(\Yii::getAlias('@statdir') . "/radio.txt", 'w');
        if (!$f) {
            echo 'can not open';
        }
        $shot = RadioItems::find()
            ->where('cat_id IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,20,21)')
            ->andWhere(['published' => 1])
            ->all();
        shuffle($shot);
        for ($i = 0; $i < count($shot); $i++) {
            fwrite($f, $shot[$i]->audio . PHP_EOL);
            if ($i % 3 == 0) fwrite($f, "mp3/ohohoho.mp3" . PHP_EOL);
            if ($i % 5 == 0) fwrite($f, "mp3/komnata_s_mehom.mp3" . PHP_EOL);
        }

        fclose($f);
    }

    /**
     * Пишет плейлист для радио
     */
    public function actionRoomPlayList()
    {
        //echo \Yii::getAlias('@webroot').PHP_EOL; exit;

        $f = fopen(\Yii::getAlias('@statdir') . "/radio.txt", 'w');
        if (!$f) {
            echo 'can not open';
        }

        $shot = RadioItems::find()
            ->where('cat_id IN (1,3,6,9,14,15,16,12)')
            ->andWhere(['published' => 1])
            ->all();

        $long = RadioItems::find()
            ->where('cat_id IN (2,4,5,7,8,10,11)')
            ->andWhere(['published' => 1])
            ->all();

        $new_long = RadioItems::find()
            ->where('cat_id IN (1,2,3,4,5,6,7,8,9,10,11,12,14,15,16)')
            ->andWhere(['published' => 1])
            ->all();

        $guests = RadioItems::find()
            ->where('cat_id IN (13,17)')
            ->andWhere(['published' => 1])
            ->all();

        $unsorted_stories = RadioItems::find()
            ->where('cat_id IN (21)')
            ->andWhere(['published' => 1])
            ->orderBy('id')
            ->all();

        $elec = RadioItems::find()
            ->where('cat_id IN (20)')
            ->andWhere(['published' => 1])
            ->all();

        shuffle($shot);
        shuffle($new_long);
        shuffle($guests);
        echo 'shot ' . count($shot) . PHP_EOL;
        echo 'long ' . count($long) . PHP_EOL;
        echo 'long ' . count($new_long) . PHP_EOL;
        echo 'guests ' . count($guests) . PHP_EOL;

        $little_length_arr = ($guests >= $new_long) ? $guests : $new_long;

        $got_ids = [];
        $stories_counter = 0;

        $el_one_song = $elec[rand(0, count($elec) - 1)];

        fwrite($f, $el_one_song->audio . PHP_EOL);

        for ($i = 0; $i < count($little_length_arr); $i++) {

            if ($i % 200 == rand(2, 198)) {
                if (isset($unsorted_stories[$stories_counter])) {
                    $story = $unsorted_stories[$stories_counter];
                    fwrite($f, $story->audio . PHP_EOL);
                }
                $stories_counter++;
            }
            /*
            if($shot[$i]->next_item) {
                //var_dump($got_ids);
                if(!in_array($shot[$i]->id, $got_ids)){
                    $content .= $this->getInSiteMapItemXml($shot[$i]->alias, $shot[$i]->d_created);
                    fwrite($f, $shot[$i]->audio . PHP_EOL);
                }
                $next = RadioItem::findOne($shot[$i]->next_item);
                if(!in_array($next->id, $got_ids)){
                    $content .= $this->getInSiteMapItemXml($next->alias, $next->d_created);
                    fwrite($f, $next->audio . PHP_EOL);
                }
                array_push($got_ids, $next->id);
            }
            else
                if(!in_array($shot[$i]->id, $got_ids)) {
                    $content .= $this->getInSiteMapItemXml($shot[$i]->alias, $shot[$i]->d_created);
                    fwrite($f, $shot[$i]->audio . PHP_EOL);
                }

            if($long[$i]->next_item) {
                if(!in_array($long[$i]->id, $got_ids)){
                    $content .= $this->getInSiteMapItemXml($long[$i]->alias, $long[$i]->d_created);
                    fwrite($f, $long[$i]->audio . PHP_EOL);
                }
                /**
                 * @var $next RadioItem

                $next = RadioItem::findOne($long[$i]->next_item);
                if(!in_array($next->id, $got_ids)){
                    $content .= $this->getInSiteMapItemXml($next->alias, $next->d_created);
                    fwrite($f, $next->audio . PHP_EOL);
                }
                array_push($got_ids, $next->id);
            }
            else
                if(!in_array($long[$i]->id, $got_ids)){
                    $content .= $this->getInSiteMapItemXml($long[$i]->alias, $long[$i]->d_created);
                    fwrite($f, $long[$i]->audio . PHP_EOL);
                }
            */

            if (isset($guests[$i])) {
                fwrite($f, $guests[$i]->audio . PHP_EOL);
                if ($i % 5 == 1) fwrite($f, "mp3/ohohoho.mp3" . PHP_EOL);
            }
            if (isset($new_long[$i])) {
                fwrite($f, $new_long[$i]->audio . PHP_EOL);
            }


            // radionoravbory.mp3
            //if ($i % 10 == 0) fwrite($f, "mp3/radionoravbory.mp3" . PHP_EOL);
            if ($i % 10 == 0) fwrite($f, "mp3/komnata_s_mehom.mp3" . PHP_EOL);

        }

        fclose($f);

    }

    public function actionAllPlayListT(){

        $dir = \Yii::getAlias('@statdir')."/music/";
        //$dir = '/musicplis/remote/';
        $groups = scandir($dir);
        $lang = [];
        $albom_songs = [];


        foreach($groups as $group){
            $songs = [];

            if($group != '.' && $group != '..' && !is_file($dir.$group)){

                $albums = scandir($dir.$group);

                foreach ($albums as $album){

                    if($album != '.' && $album != '..'){
                        $files = scandir($dir.$group.'/'.$album);
                        if($group == 'Deutscher'){

                            foreach ($files as $file){
                                if($file != '.' && $file != '..') {
                                    $lang[] = $dir.$group.'/'.$album.'/'.$file;
                                }
                            }

                        }
                        else{

                            foreach ($files as $file){
                                if($file != '.' && $file != '..') {
                                    $songs[] = $dir.$group.'/'.$album.'/'.$file;
                                }
                            }

                        }

                    }

                }

                $len = count($songs);
                if($len > 0) {
                    $songs_rand_index = rand(0,$len-1);
                    array_push($albom_songs, $songs[$songs_rand_index]);
                }

            }

        }

        $items = ArrayHelper::map(RadioItems::find()
            //->where('audio != ""')
            ->where(['!=', 'audio', ''])
            ->andWhere(['published' => 1])
            ->all(), 'audio', 'audio');
        shuffle($items);
        shuffle($albom_songs);
        shuffle($lang);


        $f = fopen(\Yii::getAlias('@statdir') . "/all.txt", 'w');
        for($i=0; $i < count($albom_songs); $i++){

            if(isset($items[$i])) fwrite($f,$items[$i].PHP_EOL);
            if(isset($songs[$i])) fwrite($f,$albom_songs[$i].PHP_EOL);
            if(isset($lang[$i]) && $i%4 == 0) fwrite($f,$lang[$i].PHP_EOL);
        }

        fclose($f);


    }

    function actionGetAuthorsFromDirs(){
        //rsync -v -a --delete --files-from=/home/romanych/radio/dio/playlist3.txt . root@88.85.67.159:/var/www/familykey/static/music
    }

    function actionAllPlayList(){
        $dir = \Yii::getAlias('@statdir') . "/music/Armstrong, Louis ";
        //$file = fopen(\Yii::getAlias('@statdir') . "/music/all.txt", 'w');
        $ffs = scandir($dir);

        foreach($ffs as $ff){
            if($ff != '.' && $ff != '..'){

                echo  $dir.'/'.$ff;

                echo PHP_EOL;
            }
        }

    }



}
