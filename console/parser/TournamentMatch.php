<?php
namespace console\parser;


/**
 *
 * @property integer $id
 * @property string $zee
 * @property integer $zb
 * @property string $zy
 * @property string $zc
 * @property string $zd
 * @property integer $ze
 * @property integer $zf
 * @property string $zh
 * @property integer $zj
 * @property string $zl
 * @property string $zx
 * @property integer $zcc
 * @property string $aa
 * @property integer $ad
 * @property string $cx
 * @property integer $ax
 * @property integer $av
 * @property integer $bx
 * @property string $wn
 * @property string $af
 * @property string $wv
 * @property integer $as
 * @property integer $az
 * @property integer $ah
 * @property integer $bb
 * @property integer $bd
 * @property string $wm
 * @property string $ae
 * @property integer $ag
 * @property integer $ba
 * @property integer $bc
 * @property string $an
 * @property string $za
 * @property integer $tournament_id
 */
class TournamentMatch
{
    public $zee;
    public $zy;
    public $zc;
    public $zd;
    public $ze;
    public $zf;
    public $zh;
    public $zj;
    public $zl;
    public $zx;
    public $zcc;
    public $aa;
    public $ad;
    public $az;
    public $af;
    public $ae;
    public $ag;
    public $ah;
    public $bd;
    public $wm;
    public $za;

    public $t;


    public function __construct($t)
    {
        $this->t = $t;

        $this->zee = '';
        $this->zb = 0;
        $this->zy = '';
        $this->zc = '';
        $this->zd = '';
        $this->ze = 0;
        $this->zf = 0;
        $this->zh = '';
        $this->zj = 0;
        $this->zl = '';
        $this->zx = '';
        $this->zcc = 0;
        $this->aa = '';
        $this->ad = 0;
        $this->az;


        if (isset($t[0]['ZEE']))
            $this->zee = $t[0]['ZEE'];
        if (isset($t[0]['ZB']))
            $this->zb = $t[0]['ZB'];
        if (isset($t[0]['ZY']))
            $this->zy = $t[0]['ZY'];
        if (isset($t[0]['ZC']))
            $this->zc = $t[0]['ZC'];
        if (isset($t[0]['ZD']))
            $this->zd = $t[0]['ZD'];
        if (isset($t[0]['ZE']))
            $this->ze = $t[0]['ZE'];
        if (isset($t[0]['ZF']))
            $this->zf = $t[0]['ZF'];
        if (isset($t[0]['ZH']))
            $this->zh = $t[0]['ZH'];
        if (isset($t[0]['ZJ']))
            $this->zj = $t[0]['ZJ'];
        if (isset($t[0]['ZL']))
            $this->zl = $t[0]['ZL'];
        if (isset($t[0]['tour']))
            $this->zx = trim($t[0]['tour']);
        if (isset($t[0]['ZCC']))
            $this->zcc = $t[0]['ZCC'];

    }

    /**
     * @param array $match
     *
     * @return $this|null
     */
    public function getTournamentMatch(array $match)
    {

        if (isset($match['']))
            $this->aa = $match[''];
        if (isset($match['AD']))
            $this->ad = $match['AD'];
       // if ((time() - $this->ad) < 3 * 60 * 60) return null;

        if (isset($match['CX']))
            $this->cx = $match['CX'];
        if (isset($match['AX']))
            $this->ax = $match['AX'];
        if (isset($match['AV']))
            $this->av = $match['AV'];
        if (isset($match['BX']))
            $this->bx = $match['BX'];
        if (isset($match['WN']))
            $this->wn = $match['WN'];
        if (isset($match['AF']))
            $this->af = $match['AF'];
        if (isset($match['WV']))
            $this->wv = $match['WV'];

        if (isset($match['AS']))
            $this->as = $match['AS'];
        if (isset($match['AZ']))
            $this->az = $match['AZ'];
        if (isset($match['AH']))
            $this->ah = $match['AH'];
        if (isset($match['BB']))
            $this->bb = $match['BB'];
        if (isset($match['BD']))
            $this->bd = $match['BD'];
        if (isset($match['WM']))
            $this->wm = $match['WM'];
        if (isset($match['AE']))
            $this->ae = $match['AE'];

        if (isset($match['ZA']))
            $this->za = $match['ZA'];
        if (isset($match['AG']))
            $this->ag = $match['AG'];
        if (isset($match['BA']))
            $this->ba = $match['BA'];
        if (isset($match['BC']))
            $this->bc = $match['BC'];
        if (isset($match['AN']))
            $this->an = $match['AN'];

        //var_dump($this);


        return $this;
    }
}