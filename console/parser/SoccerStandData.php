<?php
namespace console\parser;

use core\entities\Soccer\Match;
use core\entities\Soccer\Tournament;
use http\Exception\RuntimeException;
use yii\base\ErrorException;


class SoccerStandData
{

    const HEADER_PROTECTED = [
        //'Accept-Encoding:gzip, deflate, sdch',
        //'Accept-Language:*',
        'Cache-Control:no-cache',
        'Connection:keep-alive',
        'Cookie:my_project=455; _dc_gtm_UA-28208502-12=1; _gat_UA-28208502-12=1; _ga=GA1.2.1191596796.1477908016',
        'Host:www.soccerstand.com', 'Pragma:no-cache',
        //'Referer:http://d.soccerstand.com/ru/x/feed/proxy-local',
        'User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
        'Upgrade-Insecure-Requests:1',
        'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        //'X-Requested-With:XMLHttpRequest',
        //'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7'
    ];
    const HEADER_AS_BROWSER =  [
        //'Accept-Encoding:gzip, deflate, sdch',
        //'Accept-Language:*',
        'Cache-Control:no-cache',
        'Connection:keep-alive',
        'Cookie:_dc_gtm_UA-28208502-12=1; _ga=GA1.2.1191596796.1477908016',
        'Host:d.soccerstand.com', 'Pragma:no-cache',
        'Referer:http://d.soccerstand.com/ru/x/feed/proxy-local',
        'User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
        'X-Fsign:SW9D1eZo',
        'X-GeoIP:1',
        'X-Requested-With:XMLHttpRequest',
        'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7'
    ];

    const HEADER = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                      <html xmlns="http://www.w3.org/1999/xhtml">
                      <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>FBase</title>
                      </head>
                      <body class="soccer detailbody gecko gecko30 os-win32" id="top">';

    private $days_from_today;
    private $countries;
    private $countries_counter;
    private $matches_counter;
    private $matches;
    private $current_record;
    private $tournament;

    /**
     * @param int $days_from_today
     * @return mixed
     * @throws ErrorException
     */
    public function getSomeonePastPlayDayFromToday($days_from_today=0)
    {
        $this->days_from_today = $days_from_today;
        try {
            return $this->getCountries()
                        ->getMatchesFromCountriesArray()
                        ->getMatches();
        } catch (RuntimeException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    public function getMatchesOfTournamentByIdAndPart($tournament_id, $part)
    {
        $tournament = Tournament::findOne($tournament_id);
        try {
            return $this->getTournament($tournament->country->soccer_code, $tournament->hash, $tournament->season, $part)
                        ->getMatchesFromCountriesArray()
                        ->getMatches();
        } catch (RuntimeException $e) {
            throw new RuntimeException($e->getMessage());
        }
    }

    private function getTournament($tournament, $country_code, $stad, $i)
    {
        // 168_0 - здесь зашифрована стадия турнира и итерация выборки (догрузки) 0,1,2

        //$day_data += $this->_soccerStandCurl("https://d.soccerstand.com/ru/x/feed/tr_1_81_W6BOzpK2_168_1_-8_ru_1");
        // https://d.soccerstand.com/ru/x/feed/t_1_181_nXxWpLmT_-8_ru_1

        //здесь урл для парсинга календаря матчей, которые состоятся - напрмер для Швецкого чемпионата
        //$day_data = $this->_soccerStandCurl("https://d.soccerstand.com/ru/x/feed/tf_1_181_nXxWpLmT_155_1_-8_ru_1");
        //https://d.soccerstand.com/ru/x/feed/tr_1_181_nXxWpLmT_155_1_-8_ru_1

        //https://d.soccerstand.com/ru/x/feed/tr_1_81_W6BOzpK2_168_1_-8_ru_1 Бундеслига 17/18

        $this->countries_counter = 0;
        try {

            $this->countries = explode(
                ':',
                preg_replace("/[^-A-Za-z0-9а-ярьтцхчшуюэыйёА-ЯЁ.,!?:()№\/ ]+/", "",
                    //$this->getFromUrl("https://d.soccerstand.com/ru/x/feed/tr_1_181_nXxWpLmT_155_1_-8_ru_1",
                    $this->getFromUrl("https://d.soccerstand.com/ru/x/feed/tr_1_".$tournament."_".$country_code."_".$stad."_".$i."_-8_ru_1",
                        $this::HEADER_AS_BROWSER)));

            //var_dump($this->countries); exit;

        } catch (RuntimeException $e) {
            throw new RuntimeException('Can not get country');
        }

        if(is_array($this->countries)) {
            return $this;
        }

        throw new RuntimeException('Can not get country');
    }


    public function saveMatches($matches, $tournament_id=null)
    {

        foreach ($matches as $match){

            if($match){
                ######################

                $date = date('d.m.Y', $match->ad+7*60*60);
                $time = date('H:i', $match->ad+7*60*60);

                $prepared = explode('÷',
                    explode('¬',
                        $this->getFromUrl("http://d.soccerstand.com/ru/x/feed/df_dos_1_" . $match->aa . "_",
                            self::HEADER_AS_BROWSER))[1]);
                /*
                if (isset($prepared[1]) && explode('|', $prepared[1])[1] &&
                    explode('|', $prepared[1])[2]) $bets = explode('|', $prepared[1]);

                $prepared = explode('÷', explode('¬',
                $this->_soccerStandCurl("http://d.soccerstand.com/ru/x/feed/df_dos_1_" . $rec->aa . "_"))[1]);
            //var_dump($prepared[1][1]); exit;
            //if (isset($prepared[1]) && explode('|', $prepared[1])[1] && explode('|', $prepared[1])[2]) $bets = explode('|', $prepared[1]);

            $bet_g = 0;  //ставки
            $bet_h = 0;
            $bet_n = 0;
            if (isset($prepared[1])) {

                $t = (array)json_decode($prepared[1]);

                foreach ($t as $key => $bet){
                    $bet_g = (float)$bet[2];  //ставки
                    $bet_h = (float)$bet[0];
                    $bet_n = (float)$bet[1];
                }

            }
                */
                $bet_g = 0;  //ставки
                $bet_h = 0;
                $bet_n = 0;
                $bets = [];
                if (isset($prepared[1])) {

                    $t = (array)json_decode($prepared[1]);

                    foreach ($t as $key => $bet){
                        $bet_g = (float)$bet[2];  //ставки
                        $bet_h = (float)$bet[0];
                        $bet_n = (float)$bet[1];
                        $bets = [(float)$bet[2], (float)$bet[0], (float)$bet[1]];
                    }

                }
                else $bets = [];

                #####################

                $content = $this->getFromUrl("https://www.soccerstand.com/ru/match/" . $match->aa . "/#match-summary",
                    self::HEADER_PROTECTED);

                $urls = [
                    "http://d.soccerstand.com/ru/x/feed/d_su_" . $match->aa . "_ru_1",
                    "http://d.soccerstand.com/ru/x/feed/d_st_" . $match->aa . "_ru_1",
                    "http://d.soccerstand.com/ru/x/feed/d_li_" . $match->aa . "_ru_1",
                ];

                foreach ($urls as $url) {
                    $content .= $this->getFromUrl($url, self::HEADER_AS_BROWSER);
                }

                $content = $this->filterRowData($content);

                $model = Match::create($this->domParserForSoccerMatchBase($content), $date, $time, $bets, $tournament_id);

                if($model->validate(null,false)) {
                    echo 'validated! and ...';
                    $model->save();
                    echo 'saved! '.$model->tournament.' '.$model->host.PHP_EOL;
                }
                else {
                    var_dump($model->getErrors());
                    continue;
                }

            }

        }

    }

    /**
     * Heuristic method, which grouped countries datas
     * @return $this
     */
    private function getCountries()
    {
        $this->countries_counter = 0;
        try {
            $this->countries = explode(
                ':',
                preg_replace("/[^-A-Za-z0-9а-ярьтцхчшуюэыйёА-ЯЁ.,!?:()№\/ ]+/", "",
                    $this->getFromUrl("http://d.soccerstand.com/ru/x/feed/f_1_" . $this->days_from_today . "_0_ru_1",
                        self::HEADER_AS_BROWSER)));
        } catch (RuntimeException $e) {
            throw new RuntimeException('Can not get country');
        }

        if(is_array($this->countries)) {
            return $this;
        }

        throw new RuntimeException('Can not get country');
    }

    /**
     * euristic method, which grouped matches datas
     * @return $this
     */
    private function getMatchesFromCountriesArray()
    {
        if(is_array($this->countries)) {
            foreach ($this->countries as $country) {
                $this->matches = preg_split('/¬AA/', $country, -1, PREG_SPLIT_DELIM_CAPTURE);

                $this->matches_counter = 0;
                if(is_array($this->matches)){
                    foreach ($this->matches as $match) {
                        $this->groupMatchesByTournaments($match);
                    }
                }
                $this->countries_counter++;
            }

            return $this;
        }

        throw new RuntimeException('Can not get matches');

    }

    private function groupMatchesByTournaments($match)
    {

        $recs = explode('¬', $match);

        if ($this->matches_counter == 0) $this->tournament[$this->countries_counter][$this->matches_counter]['tour'] = $recs[0];

        foreach ($recs as $rec) {

            $arr_loc[$this->countries_counter][$this->matches_counter] = explode('÷', $rec);

            if (is_array($arr_loc[$this->countries_counter])) {

                if (isset($arr_loc[$this->countries_counter][$this->matches_counter][1]))
                    $this->tournament[$this->countries_counter][$this->matches_counter][$arr_loc[$this->countries_counter][$this->matches_counter][0]]
                        = $arr_loc[$this->countries_counter][$this->matches_counter][1];
            } else {
                $this->matches_counter++;
                continue;
            }

        }
        $this->matches_counter++;

    }

    private function getMatches()
    {
        if(is_array($this->tournament)) {

            foreach ($this->tournament as $t) {


                for($i=1; $i<count($t); $i++){
                    $matches = new TournamentMatch($t);
                    if(isset($t[$i]))
                       $this->current_record[] = $matches->getTournamentMatch($t[$i]);
                }
            }

            return $this->current_record;
        }
        throw new RuntimeException('Can not get matches');
    }



    /**
     * Обращение к API d.soccerstand.com
     * @param $url
     * @param array $headers
     * @return mixed
     */
    private function getFromUrl($url, array $headers=[])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    /**
     * @param $content
     * @return mixed
     */
    private function filterRowData($content)
    {
        $content = str_replace(chr(9), '', $content);
        $content = str_replace(chr(11), '', $content);  // заменяем табуляцию на пробел
        $content = str_replace(chr(13), '', $content);

        return str_replace(chr(10), '', $content);
    }

    /**
     * @param $content
     * @return array|null
     */
    private function domParserForSoccerMatchBase($content)
    {
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $result = [];
        $result['goul_g'] = '';
        $result['goul_h'] = '';

        $dom->loadHTML(self::HEADER.$content);

        $div = $dom->getElementsByTagName("div");
        foreach ($div as $node) {

            if ($node->getAttribute('class') === 'fleft') {
                $result['tournament'] = $node->nodeValue;
            }

            if ($node->getAttribute('id') === 'tab-statistics-0-statistic') {

                $dom_in = new \DomDocument();
                $html = $node->ownerDocument->saveHTML($node);
                libxml_use_internal_errors(true);
                $dom_in->loadHTML(self::HEADER.$html);

                $tr = $dom_in->getElementsByTagName("tr");
                foreach ($tr as $node) {

                    if ($node->getAttribute('class') === 'odd') {
                        $odd = $node->nodeValue;

                        if (preg_match('/\vУдары\v/', $odd))  //в регулярке - вертикальный пробельный символ
                        {
                            $statistic = preg_split('/Удары/', $odd);
                            $result['ud_h'] = (int)$statistic[0];
                            $result['ud_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Удары мимо/', $odd)) {
                            $statistic = preg_split('/Удары мимо/', $odd);
                            $result['ud_mim_h'] = (int)$statistic[0];
                            $result['ud_mim_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Офсайды/', $odd)) {
                            $statistic = preg_split('/Офсайды/', $odd);
                            $result['offside_h'] = (int)$statistic[0];
                            $result['offside_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Фолы/', $odd)) {
                            $statistic = preg_split('/Фолы/', $odd);
                            $result['falls_h'] = (int)$statistic[0];
                            $result['falls_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Удары в створ/', $odd)) {
                            $statistic = preg_split('/Удары в створ/', $odd);
                            $result['ud_v_stv_h'] = (int)$statistic[0];
                            $result['ud_v_stv_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Угловые/', $odd)) {
                            $statistic = preg_split('/Угловые/', $odd);
                            $result['corner_h'] = (int)$statistic[0];
                            $result['corner_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Сэйвы/', $odd)) {
                            $statistic = preg_split('/Сэйвы/', $odd);
                            $result['saves_h'] = (int)$statistic[0];
                            $result['saves_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Желтые карточки/', $odd)) {
                            $statistic = preg_split('/Желтые карточки/', $odd);
                            $result['yelkar_h'] = (int)$statistic[0];
                            $result['yelkar_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Владение мячом/', $odd)) {
                            $statistic = preg_split('/Владение мячом/', $odd);
                            $result['ballpos_h'] = (int)$statistic[0];
                            $result['ballpos_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Штрафные/', $odd)) {
                            $statistic = preg_split('/Штрафные/', $odd);
                            $result['shtraf_h'] = (int)$statistic[0];
                            $result['shtraf_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Вбрасывания/', $odd)) {
                            $statistic = preg_split('/Вбрасывания/', $odd);
                            $result['outs_h'] = (int)$statistic[0];
                            $result['outs_g'] = (int)$statistic[1];
                        }
                    }

                    if ($node->getAttribute('class') === 'even') {
                        $even = $node->nodeValue;

                        if (preg_match('/\vУдары\v/', $even)) {
                            $statistic = preg_split('/Удары/', $even);
                            $result['ud_h'] = (int)$statistic[0];
                            $result['ud_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Удары мимо/', $even)) {
                            $statistic = preg_split('/Удары мимо/', $even);
                            $result['ud_mim_h'] = (int)$statistic[0];
                            $result['ud_mim_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Офсайды/', $even)) {
                            $statistic = preg_split('/Офсайды/', $even);
                            $result['offside_h'] = (int)$statistic[0];
                            $result['offside_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Фолы/', $even)) {
                            $statistic = preg_split('/Фолы/', $even);
                            $result['falls_h'] = (int)$statistic[0];
                            $result['falls_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Удары в створ/', $even)) {
                            $statistic = preg_split('/Удары в створ/', $even);
                            $result['ud_v_stv_h'] = (int)$statistic[0];
                            $result['ud_v_stv_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Угловые/', $even)) {
                            $statistic = preg_split('/Угловые/', $even);
                            $result['corner_h'] = (int)$statistic[0];
                            $result['corner_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Сэйвы/', $even)) {
                            $statistic = preg_split('/Сэйвы/', $even);
                            $result['saves_h'] = (int)$statistic[0];
                            $result['saves_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Желтые карточки/', $even)) {
                            $statistic = preg_split('/Желтые карточки/', $even);
                            $result['yelkar_h'] = (int)$statistic[0];
                            $result['yelkar_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Владение мячом/', $even)) {
                            $statistic = preg_split('/Владение мячом/', $even);
                            $result['ballpos_h'] = (int)$statistic[0];
                            $result['ballpos_g'] = (int)$statistic[1];
                        }

                        if (preg_match('/Штрафные/', $even)) {
                            $statistic = preg_split('/Штрафные/', $even);
                            $result['shtraf_h'] = (int)$statistic[0];
                            $result['shtraf_g'] = (int)$statistic[1];

                        }

                        if (preg_match('/Вбрасывания/', $even)) {
                            $statistic = preg_split('/Вбрасывания/', $even);
                            $result['outs_h'] = (int)$statistic[0];
                            $result['outs_g'] = (int)$statistic[1];
                        }

                    }
                }
            }

        }

        $table = $dom->getElementsByTagName("table");

        foreach ($table as $node) {

            if ($node->getAttribute('id') === 'parts') {
                if (preg_match("/[0-9]{1} \- [0-9]{1} \- [0-9]{1} \- /", $node->nodeValue)) {

                    $stay = $node->nodeValue;
                    $sty = explode("Расстановка", $stay);
                    if (is_array($stay)) {
                        $result['stay_h'] = $sty[0];
                        $result['stay_g'] = $sty[1];
                    }
                }
            }

            if ($node->getAttribute('class') === 'parts match-information') {
                $result['info'] = substr($node->nodeValue, 34);

            }

        }

        $td = $dom->getElementsByTagName("td");

        foreach ($td as $node) {

            if ($node->getAttribute('class') === 'summary-vertical fl') {
                $dt = $node->nodeValue;
                $dom_in = new \DomDocument();
                $html = $node->ownerDocument->saveHTML($node);
                $dom_in->loadHTML(self::HEADER.$html);

                $dv = $dom_in->getElementsByTagName("div");

                foreach ($dv as $node) {

                    if ($node->getAttribute('class') === 'icon-box y-card')
                        $result['yel_kart_h'] = isset($result['yel_kart_h']) ? $result['yel_kart_h'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box r-card')
                        $result['red_kart_h'] = isset($result['red_kart_h']) ? $result['red_kart_h'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box yr-card')
                        $result['red_kart_h'] = isset($result['red_kart_h']) ? $result['red_kart_h'] . $dt . "(вторая жёлтая), " : '';
                    if ($node->getAttribute('class') === 'icon-box substitution-in')
                        $result['substit_h'] = isset($result['substit_h']) ? $result['substit_h'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box soccer-ball')
                        $result['goul_h'] = isset($result['goul_h']) ? $result['goul_h'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box soccer-ball-own')
                        $result['goul_h'] = isset($result['goul_h']) ? $result['goul_h'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box penalty-missed')
                        $result['pen_miss_h'] = isset($result['pen_miss_h']) ? $result['pen_miss_h'] . $dt . ", " : '';
                }


                $sp = $dom_in->getElementsByTagName("span");

                foreach ($sp as $node) {

                    for ($n = 0; $n < 250; $n++) {
                        if ($node->getAttribute('class') === "flag fl_$n") {
                            $result['stra_h'] = isset($result['stra_h']) ? $result['stra_h'] . $n . "-" . $dt . ", " : '';
                        }
                    }
                }
            }

            if ($node->getAttribute('class') === 'summary-vertical fr') {
                $dt = $node->nodeValue;
                $dom_in = new \DomDocument();
                $html = $node->ownerDocument->saveHTML($node);
                $dom_in->loadHTML($html);
                $dv = $dom_in->getElementsByTagName("div");


                foreach ($dv as $node) {

                    if ($node->getAttribute('class') === 'icon-box y-card') {
                        $result['yel_kart_g'] = isset($result['yel_kart_g']) ? $result['yel_kart_g'] . $dt . ", " : '';
                    }
                    if ($node->getAttribute('class') === 'icon-box r-card')
                        $result['red_kart_g'] = isset($result['red_kart_g']) ? $result['red_kart_g'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box yr-card')
                        $result['red_kart_g'] = isset($result['red_kart_g']) ?  $result['red_kart_g'] . $dt . "(вторая жёлтая), " : '';
                    if ($node->getAttribute('class') === 'icon-box substitution-in')
                        $result['substit_g'] = isset($result['substit_g']) ? $result['substit_g'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box soccer-ball')
                        $result['goul_g'] = isset($result['goul_g']) ? $result['goul_g'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box soccer-ball-own')
                        $result['goul_g'] = isset($result['goul_g']) ? $result['goul_g'] . $dt . ", " : '';
                    if ($node->getAttribute('class') === 'icon-box penalty-missed')
                        $result['pen_miss_g'] = isset($result['pen_miss_g']) ? $result['pen_miss_g'] . $dt . ", " : '';

                }

                $sp = $dom_in->getElementsByTagName("span");

                foreach ($sp as $node) {

                    for ($n = 0; $n < 250; $n++) {
                        if ($node->getAttribute('class') === "flag fl_$n") {
                            $result['stra_g'] = isset($result['stra_g']) ? $result['stra_g'] . $n . "-" . $dt . ", " : '';
                        }
                    }
                }
            }


            if ($node->getAttribute('class') === 'tname-home logo-enable')
                $result['host'] = $node->nodeValue;
            if ($node->getAttribute('class') === 'tname-away logo-enable')
                $result['guest'] = $node->nodeValue;
            if ($node->getAttribute('class') === 'current-result') {
                $result['score'] = $node->nodeValue;
                $sc = explode("-", $node->nodeValue);

                $result['gett'] = (int)$sc[0];
                $result['lett'] = (int)$sc[1];
            }
            if ($node->getAttribute('class') === 'mstat') {
                if ($node->nodeValue !== 'Завершен') $result['prim'] = isset($result['prim']) ? $result['prim'] . " " . $node->nodeValue : '';
            }

        }
        $span = $dom->getElementsByTagName("span");

        foreach ($span as $node) {
            if ($node->getAttribute('class') === 'info-bubble')
                $result['prim'] = $prim = isset($result['prim']) ? $result['prim'] . " " . $node->nodeValue : '';
            if ($node->getAttribute('class') === 'p1_home')
                $result['onehalf_h'] = (int)$node->nodeValue;
            if ($node->getAttribute('class') === 'p1_away')
                $result['onehalf_g'] = (int)$node->nodeValue;
        }



        if(isset($result['prim']) && (strstr($result['prim'], 'еренесён')
            || strstr($result['prim'], 'postponed')
            || strstr($result['prim'], 'Перенесен'))) return null;

        //var_dump($result); exit;

        return $result;

    }

}