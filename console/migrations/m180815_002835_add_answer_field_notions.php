<?php

use yii\db\Migration;

/**
 * Class m180815_002835_add_answer_field_notions
 */
class m180815_002835_add_answer_field_notions extends Migration
{
    public function up()
    {
        $this->addColumn('{{%notions}}', 'answer', $this->string(128));
        $this->alterColumn('{{%notions}}', 'description', $this->string(512));
    }

    public function down()
    {
        $this->dropColumn('{{%notions}}', 'answer');
    }
}
