<?php

use yii\db\Migration;

/**
 * Class m180826_044746_add_fielt_toutnament_id_to_match_and_tbl_country
 */
class m180826_044746_add_fielt_toutnament_id_to_match_and_tbl_country extends Migration
{

    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . '/../../zips/country.sql'));

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%foo_tournament}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'hash' => $this->string(128),
            'country_id' => $this->integer()->notNull(),
            'season' => $this->string()->notNull(),
            'description' => $this->string(128),
        ], $tableOptions);

        $this->createIndex('{{%idx-foo_tournament-country_id}}', '{{%foo_tournament}}', 'country_id');
        $this->addForeignKey('{{%fk-foo_tournament-country_id}}', '{{%foo_tournament}}',
            'country_id', '{{%country}}', 'id', 'CASCADE', 'RESTRICT');

        $this->addColumn('{{%foo_matches}}', 'tournament_id', $this->integer());

        $this->createIndex('{{%idx-foo_matches-tournament_id}}', '{{%foo_matches}}', 'tournament_id');
        $this->addForeignKey('{{%fk-foo_matches-tournament_id}}', '{{%foo_matches}}',
            'tournament_id', '{{%foo_tournament}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function safeDown()
    {
        echo "m180826_044746_add_fielt_toutnament_id_to_match_and_tbl_country cannot be reverted.\n";

        return false;
    }

}
