<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notion`.
 */
class m180725_072412_create_notion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%notions}}', [
            'id' => $this->primaryKey(),
            'body' => $this->string(128)->notNull(),
            'question' => $this->string(128),
            'category_id' => $this->integer()->notNull(),
            'slug' => $this->string()->notNull(),
            'description' => $this->string(128),
            'meta_json' => $this->string(),
            'image_link' => $this->string(128),
            'audio_link' => $this->string(128),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notions');
    }
}
