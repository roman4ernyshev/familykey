<?php

use yii\db\Migration;

/**
 * Class m180728_092606_add_generated_audio_field_to_notions
 */
class m180728_092606_add_generated_audio_field_to_notions extends Migration
{
    public function up()
    {

        $this->addColumn('{{%notions}}', 'generated_audio_link', $this->string());

    }

    public function down()
    {
        $this->dropColumn('{{%notions}}', 'generated_audio_link');
    }
}
