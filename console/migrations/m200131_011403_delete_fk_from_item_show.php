<?php

use yii\db\Migration;

/**
 * Class m200131_011403_delete_fk_from_item_show
 */
class m200131_011403_delete_fk_from_item_show extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->dropForeignKey('{{%fk-radio_item_id_shown-cat_id}}', '{{%radio_item_id_shown}}');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200131_011403_delete_fk_from_item_show cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200131_011403_delete_fk_from_item_show cannot be reverted.\n";

        return false;
    }
    */
}
