<?php

use yii\db\Migration;

/**
 * Class m180901_092943_add_done_fiels_to_tournament
 */
class m180901_092943_add_done_fiels_to_tournament extends Migration
{
    public function up()
    {
        $this->addColumn('{{%foo_tournament}}', 'done', $this->tinyInteger(1));
    }

    public function down()
    {
        $this->dropColumn('{{%foo_tournament}}', 'done');
    }
}
