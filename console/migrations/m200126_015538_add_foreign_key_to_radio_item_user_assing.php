<?php

use yii\db\Migration;

/**
 * Class m200126_015538_add_foreign_key_to_radio_item_user_assing
 */
class m200126_015538_add_foreign_key_to_radio_item_user_assing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('{{%fk-radio_item_user_assignments-user_id}}', '{{%radio_item_user_assignments}}', 'user_id', '{{%users}}', 'id', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200126_015538_add_foreign_key_to_radio_item_user_assing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_015538_add_foreign_key_to_radio_item_user_assing cannot be reverted.\n";

        return false;
    }
    */
}
