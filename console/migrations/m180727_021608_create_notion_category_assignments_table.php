<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notion_category_assignments`.
 */
class m180727_021608_create_notion_category_assignments_table extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%notion_category_assignments}}', [
            'notion_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-notion_category_assignments}}', '{{%notion_category_assignments}}', ['notion_id', 'category_id']);

        $this->createIndex('{{%idx-notion_category_assignments-notion_id}}', '{{%notion_category_assignments}}', 'notion_id');
        $this->createIndex('{{%idx-notion_category_assignments-category_id}}', '{{%notion_category_assignments}}', 'category_id');

        $this->addForeignKey('{{%fk-notion_category_assignments-notion_id}}', '{{%notion_category_assignments}}', 'notion_id', '{{%notions}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-notion_category_assignments-category_id}}', '{{%notion_category_assignments}}', 'category_id', '{{%notion_categories}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%notion_category_assignments}}');
    }
}
