<?php

use yii\db\Migration;

/**
 * Class m200126_012939_create_radio_items_user_assig_tbl
 */
class m200126_012939_create_radio_items_user_assig_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%radio_item_id_shown}}', [
            'item_id' => $this->integer()->notNull(),
            'shown' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-radio_item_id_shown-item_id}}', '{{%radio_item_id_shown}}', 'item_id');

        $this->createTable('{{%radio_item_user_assignments}}', [
            'item_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'ball' => $this->integer()->notNull(),
            'time' => $this->integer(12)->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-radio_item_user_assignments-item_id}}', '{{%radio_item_user_assignments}}', 'item_id');
        $this->createIndex('{{%idx-radio_item_user_assignments-user_id}}', '{{%radio_item_user_assignments}}', 'user_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200126_012939_create_radio_items_user_assig_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_012939_create_radio_items_user_assig_tbl cannot be reverted.\n";

        return false;
    }
    */
}
