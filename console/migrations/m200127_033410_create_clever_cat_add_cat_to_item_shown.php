<?php

use yii\db\Migration;

/**
 * Class m200127_033410_create_clever_cat_add_cat_to_item_shown
 */
class m200127_033410_create_clever_cat_add_cat_to_item_shown extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%clever_cats}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(128),
        ], $tableOptions);

        $this->addColumn('{{%radio_item_id_shown}}', 'cat_id', $this->integer());

        $this->createIndex('{{%idx-radio_item_id_shown-cat_id}}', '{{%radio_item_id_shown}}', 'cat_id');

        $this->addForeignKey('{{%fk-radio_item_id_shown-cat_id}}', '{{%radio_item_id_shown}}', 'cat_id', '{{%clever_cats}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200127_033410_create_clever_cat_add_cat_to_item_shown cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200127_033410_create_clever_cat_add_cat_to_item_shown cannot be reverted.\n";

        return false;
    }
    */
}
