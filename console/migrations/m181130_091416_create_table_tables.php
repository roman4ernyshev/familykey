<?php

use yii\db\Migration;

/**
 * Class m181130_091416_create_table_tables
 */
class m181130_091416_create_table_tables extends Migration
{

    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%tab_tournament}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'description' => $this->string(128),
        ], $tableOptions);

        $this->createTable('{{%tab_team}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'description' => $this->string(128),
        ], $tableOptions);

        $this->createTable('{{%tab_match}}', [
            'id' => $this->primaryKey(),
            'tournament_id' => $this->integer(),
            'team1_id' => $this->integer(),
            'team2_id' => $this->integer(),
            'description' => $this->string(128),
        ], $tableOptions);

        $this->createIndex('{{%idx-tab_match_tournament_id}}', '{{%tab_match}}', 'tournament_id');
        $this->createIndex('{{%idx-tab_match_team1_id}}', '{{%tab_match}}', 'team1_id');
        $this->createIndex('{{%idx-tab_match_team2_id}}', '{{%tab_match}}', 'team2_id');

        $this->addForeignKey('{{%fk-tab_match_tournament_id}}', '{{%tab_match}}', 'tournament_id', '{{%tab_tournament}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-tab_match_team1_id}}', '{{%tab_match}}', 'team1_id', '{{%tab_team}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-tab_match_team2_id}}', '{{%tab_match}}', 'team2_id', '{{%tab_team}}', 'id', 'CASCADE', 'RESTRICT');

    }


    public function safeDown()
    {
        echo "m181130_091416_create_table_tables cannot be reverted.\n";

        return false;
    }


}
