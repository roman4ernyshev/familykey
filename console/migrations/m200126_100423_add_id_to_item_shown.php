<?php

use yii\db\Migration;

/**
 * Class m200126_100423_add_id_to_item_shown
 */
class m200126_100423_add_id_to_item_shown extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%radio_item_id_shown}}', 'id', $this->primaryKey());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200126_100423_add_id_to_item_shown cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_100423_add_id_to_item_shown cannot be reverted.\n";

        return false;
    }
    */
}
