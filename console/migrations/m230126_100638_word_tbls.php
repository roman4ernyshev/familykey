<?php

use yii\db\Migration;

/**
 * Class m230126_100638_word_tbls
 */
class m230126_100638_word_tbls extends Migration
{
    public function init()
    {
        $this->db = 'db_postgres';
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('word', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'lang' => $this->string(),
        ]);

        $this->createTable('definitions', [
            'id' => $this->primaryKey(),
            'word_id' => $this->integer()->notNull(),
            'value' => $this->string(),
            'synonyms' => $this->string(),
            'antonyms' => $this->string(),
            'tags' => $this->string(),
            'etymology' => $this->string(),
            'forms' => $this->string()
        ]);

        $this->createIndex(
            'idx-definitions-word_id',
            'definitions',
            'word_id'
        );

        $this->addForeignKey(
            'fk-definitions-word_id',
            'definitions',
            'word_id',
            'word',
            'id',
            'CASCADE'
        );

        $this->createTable('examples', [
            'id' => $this->primaryKey(),
            'definition_id' => $this->integer()->notNull(),
            'text' => $this->text(),
            'image' => $this->string(),
            'audio' => $this->string(),
        ]);

        $this->createIndex(
            'idx-examples-definition_id',
            'examples',
            'definition_id'
        );

        $this->addForeignKey(
            'fk-examples-definition_id',
            'examples',
            'definition_id',
            'definitions',
            'id',
            'CASCADE'
        );

        $this->addColumn('{{%definitions}}', 'adult_status', $this->boolean());
        $this->addColumn('{{%definitions}}', 'type', $this->integer());

        $this->createTable('expressions', [
            'id' => $this->primaryKey(),
            'word_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'value' => $this->string(),
            'tags' => $this->string(),
        ]);

        $this->createIndex(
            'idx-expressions-word_id',
            'expressions',
            'word_id'
        );

        $this->addForeignKey(
            'fk-expressions-word_id',
            'expressions',
            'word_id',
            'word',
            'id',
            'CASCADE'
        );
    }


        /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230126_100638_word_tbls cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230126_100638_word_tbls cannot be reverted.\n";

        return false;
    }
    */
}
