<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_photos`.
 */
class m181005_064634_create_project_photos_table extends Migration
{
    public function safeUp()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%project_photos}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'file' => $this->string()->notNull(),
            'sort' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('{{%idx-notion_photos-notion_id}}', '{{%project_photos}}', 'project_id');


        $this->alterColumn('{{projects}}', 'image', $this->integer() );

        $this->addForeignKey('{{%fk-project-image}}', '{{%projects}}', 'image', '{{%project_photos}}', 'id', 'SET NULL', 'RESTRICT');

        $this->addForeignKey('{{%fk-project_photos-project_id}}', '{{%project_photos}}', 'project_id', '{{%projects}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function safeDown()
    {
        $this->dropTable('{{%project_photos}}');
    }
}
