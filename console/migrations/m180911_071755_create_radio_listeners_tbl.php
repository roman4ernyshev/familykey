<?php

use yii\db\Migration;

/**
 * Class m180911_071755_create_radio_listeners_tbl
 */
class m180911_071755_create_radio_listeners_tbl extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%listener}}', [
            'id' => $this->primaryKey(),
            'time' => $this->integer(12)->notNull(),
            'ip' => $this->string()->notNull(),
            'user_agent' => $this->string()->notNull(),
            'seconds' => $this->integer(14)->defaultValue(0),
        ], $tableOptions);

    }

    public function down()
    {
        echo "cannot be reverted.\n";

        return false;
    }
}
