<?php

use yii\db\Migration;

/**
 * Class m190803_011617_books_tables
 */
class m190803_011617_books_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%book_authors}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'last_name' => $this->string(),
            'birth_year' => $this->integer(),
            'country' => $this->string(),
        ], $tableOptions);

        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'edition' => $this->string(),
            'release_year' => $this->integer(),
            'author_id' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex(
            '{{%idx-books-author_id}}',
            '{{%books}}',
            'author_id');

        $this->addForeignKey(
            '{{%fk-books-author_id}}',
            '{{%books}}',
            'author_id', '{{%book_authors}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190803_011617_books_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190803_011617_books_tables cannot be reverted.\n";

        return false;
    }
    */
}
