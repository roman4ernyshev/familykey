<?php

use yii\db\Migration;

/**
 * Class m180725_074253_create_notion_categories_insert
 */
class m180725_074253_create_notion_categories_insert extends Migration
{
    public function up()
    {
        $this->createIndex('{{%idx-notion_categories-slug}}', '{{%notion_categories}}', 'slug', true);

        $this->insert('{{%notion_categories}}', [
            'id' => 1,
            'name' => '',
            'slug' => 'root',
            'title' => null,
            'description' => null,
            'meta_json' => '{}',
            'lft' => 1,
            'rgt' => 2,
            'depth' => 0,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180508_045825_create_shop_categories_insert cannot be reverted.\n";

        return false;
    }
}
