<?php

use yii\db\Migration;

/**
 * Class m190202_103708_add_fields_to_tab_match
 */
class m190202_103708_add_fields_to_tab_match extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tab_match}}', 'goals_first', $this->integer());
        $this->addColumn('{{%tab_match}}', 'goals_second', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190202_103708_add_fields_to_tab_match cannot be reverted.\n";

        return false;
    }


}
