<?php

use yii\db\Migration;

/**
 * Class m180912_072511_add_current_track_listener
 */
class m180912_072511_add_current_track_listener extends Migration
{
    public function up()
    {
        $this->addColumn('{{%listener}}', 'current_track', $this->string(128));
    }

    public function down()
    {
        $this->dropColumn('{{%listener}}', 'current_track');
    }
}
