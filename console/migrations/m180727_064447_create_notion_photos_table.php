<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notion_photos`.
 */
class m180727_064447_create_notion_photos_table extends Migration
{
    public function up()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%notion_photos}}', [
            'id' => $this->primaryKey(),
            'notion_id' => $this->integer()->notNull(),
            'file' => $this->string()->notNull(),
            'sort' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-notion_photos-notion_id}}', '{{%notion_photos}}', 'notion_id');


        $this->alterColumn('{{notions}}', 'image_link', $this->integer() );
        $this->alterColumn('{{notions}}', 'audio_link', $this->integer() );

        $this->addForeignKey('{{%fk-notions-image_link}}', '{{%notions}}', 'image_link', '{{%notion_photos}}', 'id', 'SET NULL', 'RESTRICT');

        $this->addForeignKey('{{%fk-notion_photos-notion_id}}', '{{%notion_photos}}', 'notion_id', '{{%notions}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%notion_photos}}');
    }
}
