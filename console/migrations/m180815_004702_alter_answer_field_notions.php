<?php

use yii\db\Migration;

/**
 * Class m180815_004702_alter_answer_field_notions
 */
class m180815_004702_alter_answer_field_notions extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%notions}}', 'answer', $this->string(255)->unique());
    }

    public function down()
    {
        $this->alterColumn('{{%notions}}', 'answer', $this->string(255));
    }
}
