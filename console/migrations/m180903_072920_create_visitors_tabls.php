<?php

use yii\db\Migration;

/**
 * Class m180903_072920_create_visitors_tabls
 */
class m180903_072920_create_visitors_tabls extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%visitor}}', [
            'id' => $this->primaryKey(),
            'time' => $this->integer(12)->notNull(),
            'visitor_id' => $this->integer(8),
            'site' => $this->string()->notNull(),
            'block' => $this->string()->notNull(),
            'user_agent' => $this->string()->notNull(),
            'language' => $this->string()->notNull(),
            'color_depth' => $this->integer(4)->defaultValue(0),
            'pixel_ratio' => $this->decimal(10,4)->defaultValue(0),
            'hardware_concurrency' => $this->integer(4)->defaultValue(0),
            'resolution_x' => $this->integer(4)->defaultValue(0),
            'resolution_y' => $this->integer(4)->defaultValue(0),
            'available_resolution_x' => $this->integer(4)->defaultValue(0),
            'available_resolution_y' => $this->integer(4)->defaultValue(0),
            'timezone_offset' => $this->integer(4)->defaultValue(0),
            'session_storage' => $this->integer(4)->defaultValue(0),
            'local_storage' => $this->integer(4)->defaultValue(0),
            'indexed_db' => $this->integer(4)->defaultValue(0),
            'open_database' => $this->integer(4)->defaultValue(0),
            'cpu_class' => $this->string()->notNull(),
            'navigator_platform' => $this->string()->notNull(),
            'do_not_track' => $this->string()->notNull(),
            'regular_plugins' => $this->text(),
            'canvas' => $this->text(),
            'webgl' => $this->text(),
            'adblock' => $this->integer(1)->defaultValue(0),
            'has_lied_languages' => $this->integer(1)->defaultValue(0),
            'has_lied_resolution' => $this->integer(1)->defaultValue(0),
            'has_lied_os' => $this->integer(1)->defaultValue(0),
            'has_lied_browser' => $this->integer(1)->defaultValue(0),
            'touch_support' => $this->string()->notNull(),
            'js_fonts' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%visitor_count}}', [
            'id' => $this->primaryKey(),
            'hash' => $this->string()->notNull(),
            'count' => $this->integer(12)->defaultValue(0),
        ], $tableOptions);


        $this->createIndex("ux_visitor_visitor_id", 'visitor', "visitor_id", false);
        $this->addForeignKey('{{%fk-visitor_visitor_id}}',
            '{{%visitor}}', 'visitor_id', '{{%visitor_count}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex("ux_visitor_count_hash", 'visitor_count', "hash", true);


        $this->createTable(
            '{{visit_error}}', [
            'id' => $this->primaryKey(),
            'time' => $this->integer(12)->defaultValue(0),
            'text' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable(
            '{{visit_block}}', [
            'id' => $this->primaryKey(),
            'time' => $this->integer(12)->defaultValue(0),
            'visitor_id' => $this->integer(8)->defaultValue(0),
            'site' => $this->string()->notNull(),
            'block' => $this->string()->notNull(),
        ], $tableOptions);


        $this->createIndex("ux_visit_block_visitor_id", 'visit_block', "visitor_id", false);
        $this->addForeignKey('{{%fk-visit_block_visitor_id}}',
            '{{%visit_block}}', 'visitor_id', '{{%visitor_count}}', 'id', 'CASCADE', 'RESTRICT');


        $this->addColumn('{{visit_block}}', 'ip', 'VARCHAR(225) NOT NULL');
        $this->addColumn('{{visit_block}}', 'hostname', 'VARCHAR(225) NOT NULL');
        $this->addColumn('{{visit_block}}', 'city', 'VARCHAR(225) NOT NULL');
        $this->addColumn('{{visit_block}}', 'region', 'VARCHAR(225) NOT NULL');
        $this->addColumn('{{visit_block}}', 'country', 'VARCHAR(225) NOT NULL');
        $this->addColumn('{{visit_block}}', 'loc', 'VARCHAR(225) NOT NULL');
        $this->addColumn('{{visit_block}}', 'org', 'VARCHAR(225) NOT NULL');
        $this->addColumn('{{visit_block}}', 'postal', 'VARCHAR(225) NOT NULL');

    }

    public function down()
    {
        echo "cannot be reverted.\n";

        return false;
    }
}
