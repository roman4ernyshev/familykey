<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notion_user_marks`.
 */
class m180725_093701_create_notion_user_marks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('notion_user_marks', [
            'user_id' => $this->integer(),
            'notion_id' => $this->integer(),
            'time' => $this->integer()->notNull(),
            'mark' =>  $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-notion_user_marks}}', '{{%notion_user_marks}}', 'time');

        $this->createIndex('{{%idx-notion_user_marks_user_id}}', '{{%notion_user_marks}}', 'user_id');
        $this->createIndex('{{%idx-notion_user_marks_notion_id}}', '{{%notion_user_marks}}', 'notion_id');

        $this->addForeignKey('{{%fk-notion_user_marks_user_id}}', '{{%notion_user_marks}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-notion_user_marks_notion_id}}', '{{%notion_user_marks}}', 'notion_id', '{{%notions}}', 'id', 'CASCADE', 'RESTRICT');

    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notion_user_marks');
    }
}
