<?php

use yii\db\Migration;

/**
 * Class m180717_044156_add_user_phone_field
 */
class m180717_044156_add_user_phone_field extends Migration
{
    public function up()
    {

        $this->addColumn('{{%users}}', 'phone', $this->string());

        $this->createIndex('{{%idx-users-phone}}', '{{%users}}', 'phone', true);

    }

    public function down()
    {
        $this->dropColumn('{{%users}}', 'phone');
    }
}
