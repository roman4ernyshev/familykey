<?php

use yii\db\Migration;

/**
 * Class m190107_012612_fix2_products_const_tbl
 */
class m190107_012612_fix2_products_const_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%products_const}}', 'carbohydrates', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'fats', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'squirrels', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'kkal', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'ferrum', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'magnesium', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'cuprum', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'iodum', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'fluorum', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'zincum', $this->decimal(10,2));
        $this->alterColumn('{{%products_const}}', 'cobaltum', $this->decimal(10,2));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190107_012612_fix2_products_const_tbl cannot be reverted.\n";

        return false;
    }


}
