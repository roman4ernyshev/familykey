<?php

use yii\db\Migration;

/**
 * Class m181005_034711_create_project_tbl
 */
class m181005_034711_create_project_tbl extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull(),
            'slug' => $this->string()->notNull(),
            'description' => $this->string(128),
            'meta_json' => $this->string(),
            'image' => $this->string(128),
            'link' => $this->string(128),
        ], $tableOptions);

    }

    public function down()
    {
        echo "cannot be reverted.\n";

        return false;
    }
}
