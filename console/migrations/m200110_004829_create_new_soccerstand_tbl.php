<?php

use yii\db\Migration;

/**
 * Class m200110_004829_create_new_soccerstand_tbl
 */
class m200110_004829_create_new_soccerstand_tbl extends Migration
{
    public function init()
    {
        $this->db = 'db_rockncontroll';
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /*
        $this->addColumn('{{%foo_matches}}', 'coach_h', $this->string(128));
        $this->addColumn('{{%foo_matches}}', 'coach_g', $this->string(128));
        */
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200110_004829_create_new_soccerstand_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200110_004829_create_new_soccerstand_tbl cannot be reverted.\n";

        return false;
    }
    */
}
