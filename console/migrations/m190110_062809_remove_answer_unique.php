<?php

use yii\db\Migration;

/**
 * Class m190110_062809_remove_answer_unique
 */
class m190110_062809_remove_answer_unique extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('answer', 'notions');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190110_062809_remove_answer_unique cannot be reverted.\n";

        return false;
    }

}
