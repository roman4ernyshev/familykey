<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notion_example`.
 */
class m180725_073523_create_notion_example_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%notion_examples}}', [
            'id' => $this->primaryKey(),
            'body' => $this->string(128)->notNull(),
            'description' => $this->text(),
            'image_link' => $this->string(128),
            'audio_link' => $this->string(128),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('notion_examples');
    }
}
