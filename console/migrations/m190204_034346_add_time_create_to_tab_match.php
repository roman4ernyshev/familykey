<?php

use yii\db\Migration;

/**
 * Class m190204_034346_add_time_create_to_tab_match
 */
class m190204_034346_add_time_create_to_tab_match extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tab_match}}', 'create_time', $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190204_034346_add_time_create_to_tab_match cannot be reverted.\n";

        return false;
    }


}
