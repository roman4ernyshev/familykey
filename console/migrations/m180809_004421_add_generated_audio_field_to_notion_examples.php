<?php

use yii\db\Migration;

/**
 * Class m180809_004421_add_generated_audio_field_to_notion_examples
 */
class m180809_004421_add_generated_audio_field_to_notion_examples extends Migration
{
    public function up()
    {

        $this->addColumn('{{%notion_examples}}', 'generated_audio_link', $this->string());

    }

    public function down()
    {
        $this->dropColumn('{{%notion_examples}}', 'generated_audio_link');
    }
}
