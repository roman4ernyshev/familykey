<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notion_example_assignments`.
 */
class m180725_074743_create_notion_example_assignments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%notion_example_assignments}}', [
            'notion_id' => $this->integer(),
            'notion_example_id' => $this->integer(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-notion_example_assignments}}', '{{%notion_example_assignments}}', ['notion_id', 'notion_example_id']);

        $this->createIndex('{{%idx-notion_example_assignments-notion_id}}', '{{%notion_example_assignments}}', 'notion_id');
        $this->createIndex('{{%idx-notion_example_assignments-notion_example_id}}', '{{%notion_example_assignments}}', 'notion_example_id');


        $this->addForeignKey('{{%fk-notion_example_assignments-notion_id}}', '{{%notion_example_assignments}}', 'notion_id', '{{%notions}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-notion_example_assignments-notion_example_id}}', '{{%notion_example_assignments}}', 'notion_example_id', '{{%notion_examples}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notion_example_assignments');
    }
}
