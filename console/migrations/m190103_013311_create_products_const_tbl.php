<?php

use yii\db\Migration;

/**
 * Class m190103_013311_create_products_const_tbl
 */
class m190103_013311_create_products_const_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%products_const}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'description' => $this->string(128),
            'carbohydrates' => $this->integer(),
            'fats' => $this->integer(),
            'squirrels' => $this->integer(),
            'kkal' => $this->integer(),
            'ferrum' => $this->integer(),
            'magnesium' => $this->integer(),
            'cuprum' => $this->integer(),
            'iodum' => $this->integer(),
            'fluorum' => $this->integer(),
            'zincum' => $this->integer(),
            'cobaltum' => $this->integer(),
        ], $tableOptions);

        $this->addCommentOnColumn('products_const','carbohydrates','on 100 g');
        $this->addCommentOnColumn('products_const','fats','on 100 g');
        $this->addCommentOnColumn('products_const','squirrels','on 100 g');
        $this->addCommentOnColumn('products_const','kkal','on 100 g');
        $this->addCommentOnColumn('products_const','ferrum','microgram on 100 g');
        $this->addCommentOnColumn('products_const','magnesium','microgram on 100 g');
        $this->addCommentOnColumn('products_const','cuprum','microgram on 100 g');
        $this->addCommentOnColumn('products_const','iodum','microgram on 100 g');
        $this->addCommentOnColumn('products_const','fluorum','microgram on 100 g');
        $this->addCommentOnColumn('products_const','zincum','microgram on 100 g');
        $this->addCommentOnColumn('products_const','cobaltum','microgram on 100 g');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190103_013311_create_products_const_tbl cannot be reverted.\n";

        return false;
    }

}
