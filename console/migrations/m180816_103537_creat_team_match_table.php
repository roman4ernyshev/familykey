<?php

use yii\db\Migration;

/**
 * Class m180816_103537_creat_team_match_table
 */
class m180816_103537_creat_team_match_table extends Migration
{
    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . '/../../zips/foo_matches.sql'));
        $this->execute(file_get_contents(__DIR__ . '/../../zips/foo_team.sql'));

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%team_match_assignments}}', [
            'team_id' => $this->integer()->notNull(),
            'match_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-team_match_assignments}}', '{{%team_match_assignments}}', ['team_id', 'match_id']);

        $this->createIndex('{{%idx-team_match_assignments-team_id}}', '{{%team_match_assignments}}', 'team_id');
        $this->createIndex('{{%idx-team_match_assignments-match_id}}', '{{%team_match_assignments}}', 'match_id');

        $this->addForeignKey('{{%fk-team_match_assignments-team_id}}', '{{%team_match_assignments}}', 'team_id', '{{%foo_team}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-team_match_assignments-match_id}}', '{{%team_match_assignments}}', 'match_id', '{{%foo_matches}}', 'id', 'CASCADE', 'RESTRICT');

    }
    public function down()
    {
        $this->dropTable('team_match');
    }
}
