<?php

use yii\db\Migration;

/**
 * Class m190106_062840_fix_products_const_tbl
 */
class m190106_062840_fix_products_const_tbl extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->alterColumn('{{%products_const}}', 'carbohydrates', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'fats', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'squirrels', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'kkal', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'ferrum', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'magnesium', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'cuprum', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'iodum', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'fluorum', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'zincum', $this->decimal(2));
        $this->alterColumn('{{%products_const}}', 'cobaltum', $this->decimal(2));


        $this->addColumn('{{%products_const}}', 'slug', $this->string()->notNull()->defaultValue(''));

        $this->addColumn('{{%products_const}}', 'meta_json', $this->string());
        $this->addColumn('{{%products_const}}', 'image', $this->integer());


        $this->createTable('{{%product_photos}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'file' => $this->string()->notNull(),
            'sort' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('{{%idx-product_photos-product_id}}', '{{%product_photos}}', 'product_id');

        $this->addForeignKey('{{%fk-products_const-image}}', '{{%products_const}}', 'image', '{{%product_photos}}', 'id', 'SET NULL', 'RESTRICT');

        $this->addForeignKey('{{%fk-product_photos-project_id}}', '{{%product_photos}}', 'product_id', '{{%products_const}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190106_062840_fix_products_const_tbl cannot be reverted.\n";

        return false;
    }

}
