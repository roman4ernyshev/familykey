<?php

use yii\db\Migration;

/**
 * Class m190108_093220_create_product_categories_tbls
 */
class m190108_093220_create_product_categories_tbls extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%product_categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'title' => $this->string(),
            'description' => $this->string(),
            'meta_json' => $this->string(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addColumn('{{%products_const}}', 'product_id', $this->integer()->notNull());

        $this->createIndex('{{%idx-product_categories-slug}}', '{{%product_categories}}', 'slug', true);

        $this->insert('{{%product_categories}}', [
            'id' => 1,
            'name' => '',
            'slug' => 'root',
            'title' => null,
            'description' => null,
            'meta_json' => '{}',
            'lft' => 1,
            'rgt' => 2,
            'depth' => 0,
        ]);

        $this->createTable('{{%product_category_assignments}}', [
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-product_category_assignments}}', '{{%product_category_assignments}}', ['product_id', 'category_id']);

        $this->createIndex('{{%idx-product_category_assignments-product_id}}', '{{%product_category_assignments}}', 'product_id');
        $this->createIndex('{{%idx-product_category_assignments-category_id}}', '{{%product_category_assignments}}', 'category_id');

        $this->addForeignKey('{{%fk-product_category_assignments-product_id}}', '{{%product_category_assignments}}', 'product_id', '{{%products_const}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-product_category_assignments-category_id}}', '{{%product_category_assignments}}', 'category_id', '{{%product_categories}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190108_093220_create_product_categories_tbls cannot be reverted.\n";

        return false;
    }


}
