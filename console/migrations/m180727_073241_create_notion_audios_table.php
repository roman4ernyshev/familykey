<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notion_audios`.
 */
class m180727_073241_create_notion_audios_table extends Migration
{
    public function up()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%notion_audios}}', [
            'id' => $this->primaryKey(),
            'notion_id' => $this->integer()->notNull(),
            'file' => $this->string()->notNull(),
            'sort' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-notion_audios-notion_id}}', '{{%notion_audios}}', 'notion_id');


        $this->addForeignKey('{{%fk-notions-audio_link}}', '{{%notions}}', 'audio_link', '{{%notion_audios}}', 'id', 'SET NULL', 'RESTRICT');

        $this->addForeignKey('{{%fk-notion_audios-notion_id}}', '{{%notion_audios}}', 'notion_id', '{{%notions}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%notion_audios}}');
    }
}
