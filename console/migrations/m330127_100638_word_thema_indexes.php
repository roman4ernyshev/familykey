<?php

use yii\db\Migration;

/**
* Class m320127_100638_word_thema_indexes
*/
class m330127_100638_word_thema_indexes extends Migration
{
    public function init()
    {
        $this->db = 'db_postgres';
        parent::init();
    }
    /**
    * {@inheritdoc}
    */
    public function safeUp()
    {
    //$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('crossword_thema', [
            'id' => $this->primaryKey(),
            'title' => $this->string()
        ]);

        $this->createTable('definition_crossword_thema', [
            'id' => $this->primaryKey(),
            'crossword_thema_id' => $this->integer()->notNull(),
            'definition_id' => $this->integer()->notNull(),
            'title' => $this->string()
        ]);

        $this->createIndex(
            'idx-definition_crossword_thema-crossword_thema_id',
            'definition_crossword_thema',
            'crossword_thema_id'
        );

        $this->createIndex(
            'idx-definition_crossword_thema-definition_id',
            'definition_crossword_thema',
            'definition_id'
        );

        $this->addForeignKey(
            'fk-crossword_thema-crossword_thema_id',
            'definition_crossword_thema',
            'crossword_thema_id',
            'crossword_thema',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-definitions-crossword_thema_id',
            'definition_crossword_thema',
            'definition_id',
            'definitions',
            'id',
            'CASCADE'
        );

    }


    /**
    * {@inheritdoc}
    */
    public function safeDown()
    {
        echo "m320127_100638 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
    echo "m230126_100638_word_tbls cannot be reverted.\n";

    return false;
    }
    */
}

