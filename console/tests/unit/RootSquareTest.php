<?php

use Codeception\Test\Unit;

class RootSquareTest extends Unit
{
    public function fixtures()
    {
        return [
            'members' => include(codecept_data_dir() . 'members.php')
        ];
    }

    public function testOne()
    {
        $equation = new \core\entities\SquareEquationCalculator();

        //var_dump($this->fixtures()); exit;

        foreach ($this->fixtures()['members'] as $row){
            //var_dump($row); exit;
            $this->assertNotNull($equation->getRoots($row[0], $row[1], $row[2]));
        }


    }


}