<?php

namespace backend\controllers;


use backend\forms\Project\ProjectSearch;
use core\entities\Project\Project;
use core\forms\manage\Project\PhotosForm;
use core\forms\manage\Project\ProjectCreateForm;
use core\repositories\NotFoundException;
use core\useCases\manage\ProjectManageService;

use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class ProjectController extends Controller
{

    private $service;

    public function __construct(string $id, $module, ProjectManageService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;

    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    //'activate' => ['POST'],
                    //'draft' => ['POST'],
                    //'delete-photo' => ['POST'],
                    //'move-photo-up' => ['POST'],
                    //'move-photo-down' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionView($id)
    {
        $project = $this->findModel($id);

        $photosForm = new PhotosForm();
        if ($photosForm->load(Yii::$app->request->post()) && $photosForm->validate()) {
            try {
                $this->service->addPhotos($project->id, $photosForm);
                return $this->redirect(['view', 'id' => $project->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('view', [
            'project' => $project,
            'photosForm' => $photosForm
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ProjectCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            try {
                $project = $this->service->create($form);
                return $this->redirect(['view', 'id' => $project->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }


    public function actionUpdate($id)
    {
        $project = $this->findModel($id);

        $form = new ProjectCreateForm($project);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($project->id, $form);
                return $this->redirect(['view', 'id' => $project->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'project' => $project
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }


    /**
     * @param integer $id
     * @return Project the loaded model
     * @throws
     */
    protected function findModel($id): Project
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundException('The requested page does not exist.');
    }


}