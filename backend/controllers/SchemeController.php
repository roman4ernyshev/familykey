<?php
namespace backend\controllers;


use core\entities\Schema\DaySchema;
use core\entities\Schema\storage\RedisStorage;
use core\readModels\SchemaReadRepository;
use yii\web\Controller;
use Yii;

class SchemeController extends Controller
{

    private $scema;

    function __construct($id, $module, $config = [])
    {
        $this->scema = new DaySchema(new RedisStorage());
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        return $this->render('index', ['par' => $this->scema->getParams()]);
    }

    public function actionSetParams()
    {
        if($params = Yii::$app->request->post()) {

            $this->scema->saveParams($params);

            return $this->render('index', ['par' => $this->scema->getParams()]);
        }

        return null;
    }

    public function actionGetParams()
    {
        //var_dump($this->scema->getParams());

        return $this->render('response', ['resp' => $this->scema->getParams()]);

    }

}