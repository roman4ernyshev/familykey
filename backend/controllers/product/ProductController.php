<?php

namespace backend\controllers\product;


use backend\forms\Product\ProductSearch;
use core\entities\Product\ProductsConst;
use core\forms\manage\Product\PhotosForm;
use core\forms\manage\Product\ProductConstCreateForm;
use core\useCases\manage\Product\ProductConstManageService;
use UsingRefs\Product;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class ProductController extends Controller
{
    private $service;

    public function __construct(string $id, $module, ProductConstManageService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    //'activate' => ['POST'],
                    //'draft' => ['POST'],
                    //'delete-photo' => ['POST'],
                    //'move-photo-up' => ['POST'],
                    //'move-photo-down' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ProductConstCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            try {
                $tournament = $this->service->create($form);
                return $this->redirect(['view', 'id' => $tournament->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $product = $this->findModel($id);

        $form = new ProductConstCreateForm($product);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($product->id, $form);
                return $this->redirect(['view', 'id' => $product->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'product' => $product
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionView($id)
    {
        $project = $this->findModel($id);

        $photosForm = new PhotosForm();
        if ($photosForm->load(Yii::$app->request->post()) && $photosForm->validate()) {
            try {
                $this->service->addPhotos($project->id, $photosForm);
                return $this->redirect(['view', 'id' => $project->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('view', [
            'product' => $project,
            'photosForm' => $photosForm
        ]);
    }


    /**
     * @param integer $id
     * @return ProductsConst the loaded model
     * @throws
     */
    protected function findModel($id): ProductsConst
    {
        if (($model = ProductsConst::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundException('The requested page does not exist.');
    }


}