<?php
namespace backend\controllers;

use backend\forms\Soccer\TournamentSearch;
use console\parser\SoccerStandData;
use core\entities\Soccer\Tournament;
use core\forms\manage\Soccer\TournamentCreateForm;
use core\repositories\NotFoundException;
use core\useCases\manage\SoccerManageService;
use http\Exception\RuntimeException;
use yii\web\Controller;
use Yii;

class SoccerController extends Controller
{
    private $service;

    public function __construct(string $id, $module, SoccerManageService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex()
    {
        $searchModel = new TournamentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSaveTournamentById($id, $part)
    {
        $soccer = new SoccerStandData();
        try {
            $soccer->saveMatches($soccer->getMatchesOfTournamentByIdAndPart($id, $part), $id);
        } catch (RuntimeException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new TournamentCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            try {
                $tournament = $this->service->create($form);
                return $this->redirect(['view', 'id' => $tournament->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $tournament = $this->findModel($id);

        $form = new TournamentCreateForm($tournament);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($tournament->id, $form);
                return $this->redirect(['view', 'id' => $tournament->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'tournament' => $tournament
        ]);
    }

    public function actionMarkParsed($id)
    {
        if($this->service->markParsed($id)) return $this->redirect(['index']);
        else {
            Yii::$app->session->setFlash('error');
        }

        $searchModel = new TournamentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'tournament' => $this->findModel($id),
        ]);
    }

    /**
     * @param integer $id
     * @return Tournament the loaded model
     * @throws
     */
    protected function findModel($id): Tournament
    {
        if (($model = Tournament::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundException('The requested page does not exist.');
    }


}