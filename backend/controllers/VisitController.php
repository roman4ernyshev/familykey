<?php
namespace backend\controllers;

use backend\forms\Visit\VisitorBlockSearch;
use backend\forms\Visit\VisitorRadioSearch;
use Yii;
use backend\forms\Visit\VisitorSearch;
use core\useCases\manage\VisitManageService;
use yii\web\Controller;

class VisitController extends Controller
{
    /*private $service;

    public function __construct(string $id, $module, VisitManageService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }
    */

    public function actionIndex()
    {
        $searchModel = new VisitorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBlock()
    {
        $searchModel = new VisitorBlockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('block', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRadio()
    {
        $searchModel = new VisitorRadioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('radio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}