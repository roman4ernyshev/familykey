<?php

namespace backend\controllers\notion;

use backend\forms\Notion\NotionSearch;
use core\entities\Notion\Example;
use core\entities\Notion\Notion;
use core\forms\manage\Notion\ExampleCreateForm;
use core\forms\manage\Notion\NotionCreateForm;
use core\forms\manage\Notion\PhotosForm;
use core\readModels\CategoryReadRepository;
use core\repositories\NotFoundException;
use core\useCases\manage\Notion\NotionManageService;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class NotionController extends Controller
{

    private $service;
    private $category;

    public function __construct(string $id, $module, NotionManageService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->category =
            $this->service->isCategoryExists(Yii::$app->request->get('category'))
                ? Yii::$app->request->get()['category']
                : null;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    //'activate' => ['POST'],
                    //'draft' => ['POST'],
                    //'delete-photo' => ['POST'],
                    //'move-photo-up' => ['POST'],
                    //'move-photo-down' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotionSearch();
        $dataProvider = $searchModel->search($this->category, \Yii::$app->request->queryParams);
        //var_dump($dataProvider); exit;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $this->category
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionView($id)
    {
        $notion = $this->findModel($id);

        $photosForm = new PhotosForm();
        if ($photosForm->load(Yii::$app->request->post()) && $photosForm->validate()) {
            try {
                $this->service->addPhotos($notion->id, $photosForm);
                return $this->redirect(['view', 'id' => $notion->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('view', [
            'notion' => $notion,
            'photosForm' => $photosForm
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new NotionCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            try {
                $product = $this->service->create($form, $this->category);
                return $this->redirect(['view', 'id' => $product->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }


    public function actionUpdate($id)
    {
        $notion = $this->findModel($id);

        $form = new NotionCreateForm($notion);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($notion->id, $form);
                return $this->redirect(['view', 'id' => $notion->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'notion' => $notion
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $notion = $this->findModel($id);

        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index', 'category' => $notion->category->name]);
    }

    public function actionAddExample($id)
    {
        $notion = $this->findModel($id);

        $form = new ExampleCreateForm($notion);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $example = $this->service->addExample($id, $form);
                return $this->redirect(['view', 'id' => $notion->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('add-example', [
            'model' => $form,
        ]);

    }


    /**
     * @param integer $id
     * @return Notion the loaded model
     * @throws
     */
    protected function findModel($id): Notion
    {
        if (($model = Notion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundException('The requested page does not exist.');
    }


}