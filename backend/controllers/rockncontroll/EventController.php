<?php
namespace backend\controllers\rockncontroll;

use core\entities\Rockncontroll\EventSearch;
use core\helpers\TranslateHelper;
use yii\web\Controller;
use core\entities\Rockncontroll\Event;
use core\forms\rockncontroll\UploadForm;

use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\UploadedFile;
use Yii;

class EventController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ['events' => $dataProvider,  'searchModel' => $searchModel]);
    }

    public function actionUpdateText($id){

        $model = $this->loadModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->text = Yii::$app->request->post('Event')['text'];
            $model->save(false);

            return $this->redirect(Url::toRoute('/rockncontroll/event/index'));

        } else {

            return $this->render('update_text', [
                'model' => $model,
            ]);
        }

    }


    /**
     * Добавление картинки
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionAddImg($id){

        $model = $this->loadModel($id);
        $uploadImg = new UploadForm();


        if (Yii::$app->request->isPost) {

            $uploadImg->img = UploadedFile::getInstance($uploadImg, 'img');

            if($uploadImg->img && $uploadImg->validate()) {

                $base_name = TranslateHelper::translit($uploadImg->img->baseName).md5(time());
                //$uploadImg->img->saveAs('uploads/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension);
                $uploadImg->img->saveAs((Yii::getAlias('@staticRoot')).'/uploads/' . $base_name . '.' .$uploadImg->img->extension);

                /*
                 * $base_name = TranslateHelper::translit($uploadImg->img->baseName).md5(time());
                $uploadImg->img->saveAs((Yii::getAlias('@staticRoot')).'/uploads/item_pics/' . $base_name . '.' .$uploadImg->img->extension);

                 */
            }
            else {print_r($uploadImg->getErrors()); } 

            if($uploadImg->errors) return var_dump($uploadImg->getErrors());
            if(isset($uploadImg->img)) {
                $model->img = Url::base().'uploads/' . $base_name . '.' .$uploadImg->img->extension;
                $model->update(false);

                return $this->redirect(Url::toRoute('rockncontroll/event/index'));
            }
            
        
        }


        return $this->render('_form', [
            'model' => $model,
            'uploadImg' => $uploadImg,
        ]);

    }

    /**
     * Загружает запись модели текущего контроллера по айдишнику
     * @param $id
     * @return null|static
     * @throws \yii\web\HttpException
     */
    public function loadModel($id)
    {

        $model = Event::findOne($id);

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
}