<?php
namespace backend\controllers\rockncontroll;


use core\entities\Article\ArticleContent;
use core\helpers\TranslateHelper;
use core\entities\Rockncontroll\Articles;
use core\entities\Rockncontroll\ArticlesContent;
use core\entities\Rockncontroll\RadioArticle;
use core\entities\Rockncontroll\RadioArticleContent;
use core\entities\Rockncontroll\DiaryActs;
use core\entities\Rockncontroll\ImageStorage;
use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\Source;
use core\forms\rockncontroll\UploadForm;

use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\Url;
use Yii;
use yii\data\ActiveDataProvider;
use yii\sphinx\Query; // обязательно используем sphinx вместо стандартной yii\db\Query

class ArticlesController extends Controller
{
    //public $layout = 'rockncontroll';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@static').'/new_upload/imperaviim2/',// Directory URL address, where files are stored.
                'path' => '@staticRoot/new_upload/imperaviim2/' // Or absolute path to directory where files are stored.
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/images/blog/', // Directory URL address, where files are stored.
                'path' => '@static/images/blog/', // Or absolute path to directory where files are stored.
                'type' => '0',
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/files/blog/', // Directory URL address, where files are stored.
                'path' => '@static/files/blog/', // Or absolute path to directory where files are stored.
                'type' => '1',//GetAction::TYPE_FILES,
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/files/blog/', // Directory URL address, where files are stored.
                'path' => '@static/files/blog/' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex()
    {
        $articles = Articles::find()->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $articles,
        ]);

        return $this->render('index', ['articles' => $dataProvider]);
    }

    /**
     * Создание контента
     * @return string
     */
    public function actionCreate()
    {

        $model = new Articles();

        $uploadFile = new UploadForm();
        $uploadImg = new UploadForm();


        if (Yii::$app->request->isPost) {
            $uploadFile->file = UploadedFile::getInstance($uploadFile, 'file');
            $uploadImg->img = UploadedFile::getInstance($uploadImg, 'img');

            if ($uploadFile->file && $uploadFile->validate()) {
                $uploadFile->file->saveAs('uploads/' . Yii::$app->translater->translit($uploadFile->file->baseName) . '.' .$uploadFile->file->extension);

            }
            elseif($uploadImg->img && $uploadImg->validate()) {
                $uploadImg->img->saveAs('uploads/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension);

            }
            else { print_r($uploadFile->getErrors()); }
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->text = Yii::$app->request->post('Articles')['text'];
            $model->title = Yii::$app->request->post('Articles')['title'];
            $model->alias = TranslateHelper::translit(Yii::$app->request->post('Articles')['title']);
            $model->site_id = Yii::$app->request->post('Articles')['site_id'];
            $model->cat_id = Yii::$app->request->post('Articles')['cat_id'];
            $model->tags = Yii::$app->request->post('Articles')['tags'];
            if(Yii::$app->request->post('Articles')['redactor']) $model->redactor = 1;
            else $model->redactor = 0;
            if(isset(Yii::$app->request->post('Articles')['source_id']))$model->source_id = Yii::$app->request->post('Articles')['source_id'];
             else $model->source_id = 2;
            if(isset($uploadFile->file))
                $model->audio = Url::base().'uploads/' . Yii::$app->translater->translit($uploadFile->file->baseName) . '.' .$uploadFile->file->extension;

            if(isset($uploadImg->img))
                $model->img = Url::base().'uploads/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension;
            if($model->site_id == 13) {
                $act = new DiaryActs();
                $act->model_id = 6;
                $act->user_id = 8;
                $act->save(false);
                $model->act_id = $act->id;
            }

            if($model->save(false))
            {
                $radio_article = new RadioArticle();

                $radio_article->id = $model->id;
                $radio_article->title = $model->title;
                $radio_article->alias = $model->alias;
                $radio_article->d_created = $model->d_created;
                $radio_article->img = $model->img;
                $radio_article->anons = $model->anons;
                $radio_article->text = $model->text;
                $radio_article->audio = $model->audio;
                $radio_article->video = $model->video;
                $radio_article->tags = $model->tags;
                $radio_article->status = $model->status;
                $radio_article->site_id = 16;
                $radio_article->save(false);
            }


                
            return $this->redirect(Url::toRoute('rockncontroll/articles/index'));

        } else {

            return $this->render('_form', [
                'model' => $model,
                'uploadFile' => $uploadFile,
                'uploadImg' => $uploadImg,

            ]);
        }

    }
   

    public function actionUpdate($id){

        $model = $this->loadModel($id);
        $uploadFile = new UploadForm();
        $uploadImg = new UploadForm();


        if (Yii::$app->request->isPost) {
            $uploadFile->file = UploadedFile::getInstance($uploadFile, 'file');
            $uploadImg->img = UploadedFile::getInstance($uploadImg, 'img');

            if ($uploadFile->file && $uploadFile->validate()) {
                $uploadFile->file->saveAs('uploads/' . Yii::$app->translater->translit($uploadFile->file->baseName) . '.' .$uploadFile->file->extension);

            }
            elseif($uploadImg->img && $uploadImg->validate()) {
                $uploadImg->img->saveAs('uploads/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension);

            }
            //else { print_r($uploadFile->getErrors()); }
        }

        //var_dump($uploadImg); exit;
        if ($model->load(Yii::$app->request->post())) {
            $model->text = Yii::$app->request->post('Articles')['text'];
            $model->title = Yii::$app->request->post('Articles')['title'];
            $model->alias = TranslateHelper::translit(Yii::$app->request->post('Articles')['title']);
            $model->site_id = Yii::$app->request->post('Articles')['site_id'];
            $model->cat_id = Yii::$app->request->post('Articles')['cat_id'];
            if(Yii::$app->request->post('Articles')['redactor']) $model->redactor = 1;
            else $model->redactor = 0;
            if(isset(Yii::$app->request->post('Articles')['source_id']))$model->source_id = Yii::$app->request->post('Articles')['source_id'];
                else $model->source_id = 2;
            if(isset($uploadFile->file))
                $model->audio = Url::base().'uploads/' . Yii::$app->translater->translit($uploadFile->file->baseName) . '.' .$uploadFile->file->extension;

            if(isset($uploadImg->img))
                $model->img = Url::base().'uploads/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension;
            if($model->save(false))
            {
                $radio_article = RadioArticle::findOne($id);
                //var_dump($id); exit;

                $radio_article->title = $model->title;
                $radio_article->alias = $model->alias;
                $radio_article->d_created = $model->d_created;
                $radio_article->img = $model->img;
                $radio_article->anons = $model->anons;
                $radio_article->text = $model->text;
                $radio_article->audio = $model->audio;
                $radio_article->video = $model->video;
                $radio_article->tags = $model->tags;
                $radio_article->status = $model->status;
                $radio_article->site_id = 16;

                $radio_article->save(false);
            }


            return $this->redirect(Url::toRoute('articles/index'));
        } else {

            return $this->render('_form', [
                'model' => $model,
                'uploadFile' => $uploadFile,
                'uploadImg' => $uploadImg,
            ]);
        }

    }

    /**
     * Удаляет контент
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {

        if ($model = $this->loadModel($id)->delete()) {
            $articles = Articles::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $articles,
            ]);

            return $this->render('index', ['articles' => $dataProvider]);
        } else {
            throw new \yii\web\HttpException(404, 'Cant delete record.');
        };

    }

    /**
     * Добавляем страницу контента
     *
     */
    //@TODO добавление страницы из списка страниц контента
    //@TODO при добавлении страницы выводить название контента
    public function actionAddpage($id){

        $artContent = new ArticlesContent;

        $redactor = $this->loadModel($id)->redactor;

        $upload = new UploadForm();

        if (Yii::$app->request->isPost) {
            $upload->file = UploadedFile::getInstance($upload, 'file');

            if ($upload->file && $upload->validate()) {
                $upload->file->saveAs('uploads/' . Yii::$app->translater->translit($upload->file->baseName) . '.' .$upload->file->extension);

            }
            //else { print_r($upload->getErrors()); }
        }

        if ($artContent->load(Yii::$app->request->post())) {
            $artContent->body = Yii::$app->request->post('ArticlesContent')['body'];
            $artContent->minititle = Yii::$app->request->post('ArticlesContent')['minititle'];

            //var_dump(Yii::$app->request->post('ArticlesContent')['source_title']); exit;

             if(Source::find()->where(['title' => Yii::$app->request->post('ArticlesContent')['source_title']])->one()){

                 $artContent->source_id = Source::find()->where(['title' => Yii::$app->request->post('ArticlesContent')['source_title']])->one()->id;
             }
            else return 'mistake';

            $artContent->articles_id = $id;
            if(isset($upload->file))$artContent->audio = Url::base().'uploads/' . Yii::$app->translater->translit($upload->file->baseName) . '.' .$upload->file->extension;
            else $artContent->audio = '';

            if($this->loadModel($id)->site_id == 13) {
                $act = new DiaryActs();
                $act->model_id = 6;
                $act->mark = 1;
                $act->user_id = 8;
                $act->save(false);
            }

            if($artContent->save())
            {
                $radio_article_content = new RadioArticleContent();

                $radio_article_content->id = $artContent->id;
                $radio_article_content->articles_id = $artContent->articles_id;
                $radio_article_content->body = $artContent->body;
                $radio_article_content->minititle = $artContent->minititle;
                $radio_article_content->img = $artContent->img;
                $radio_article_content->page = $artContent->page;
                $radio_article_content->count = $artContent->count;
                $radio_article_content->likes = $artContent->likes;
                $radio_article_content->d_shown = $artContent->d_shown;
                if(Source::findOne($artContent->source_id))
                    $radio_article_content->source_id = $artContent->source_id;
                else
                    $radio_article_content->source_id = 327;

                try {
                    $radio_article_content->save(false);
                    //echo $radio_article_content->id.' '.PHP_EOL;
                } catch (\Exception $e) {
                    echo $e->getMessage(); exit;
                }

            }


            $content = ArticlesContent::find()
                ->where(['articles_id' => $id]);

            $dataCont = new ActiveDataProvider([
                'query' => $content,

            ]);

            return $this->render('pages', [
                'content' => $dataCont,
                'model' => $artContent,

            ]);

        } else {

            return $this->render('page_form', [
                'model' => $artContent,
                'upload' => $upload,
                'redactor' => $redactor

            ]);
        }

    }

    /**
     * Обновляет страницу
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionUpdatepage($id) {

        $artContent = $this->loadModelcontent($id);
        $artId = $artContent->articles_id;
        $redactor = $this->loadModel($artId)->redactor;

        $artContent->source_title = Source::findOne($artContent->source_id)->title;
        $upload = new UploadForm();
        //var_dump($upload); exit;
        if (Yii::$app->request->isPost) {
            $upload->file = UploadedFile::getInstance($upload, 'file');

            if ($upload->file && $upload->validate()) {
                $upload->file->saveAs('uploads/' . Yii::$app->translater->translit($upload->file->baseName) . '.' .$upload->file->extension);

            }
            else { print_r($upload->getErrors()); }
        }


        if ($artContent->load(Yii::$app->request->post())) {
            $artContent->body = Yii::$app->request->post('ArticlesContent')['body'];
            $artContent->minititle = Yii::$app->request->post('ArticlesContent')['minititle'];
            //$artContent->source_id = Yii::$app->request->post('ArticlesContent')['source_id'];

            if(Source::find()->where(['title' => Yii::$app->request->post('ArticlesContent')['source_title']])->one()){

                $artContent->source_id = Source::find()->where(['title' => Yii::$app->request->post('ArticlesContent')['source_title']])->one()->id;
            }
            else return 'mistake';


            $artContent->articles_id = $artId;
            if(isset($upload->file))
            $artContent->audio = Url::base().'uploads/' . Yii::$app->translater->translit($upload->file->baseName) . '.' .$upload->file->extension;

            if($artContent->save())
            {

                $radio_article_content = RadioArticleContent::findOne($id);

                $radio_article_content->id = $artContent->id;
                $radio_article_content->articles_id = $artContent->articles_id;
                $radio_article_content->body = $artContent->body;
                $radio_article_content->minititle = $artContent->minititle;
                $radio_article_content->img = $artContent->img;
                $radio_article_content->page = $artContent->page;
                $radio_article_content->count = $artContent->count;
                $radio_article_content->likes = $artContent->likes;
                $radio_article_content->d_shown = $artContent->d_shown;
                if(Source::findOne($artContent->source_id))
                    $radio_article_content->source_id = $artContent->source_id;
                else
                    $radio_article_content->source_id = 327;

                try {
                    $radio_article_content->save(false);
                    echo $radio_article_content->id.' '.PHP_EOL;
                } catch (\Exception $e) {
                    echo $e->getMessage(); exit;
                }

            }

            $content = ArticlesContent::find()
                ->where(['articles_id' => $artId]);

            $dataCont = new ActiveDataProvider([
                'query' => $content,

            ]);

            return $this->render('pages', [
                'content' => $dataCont,
                'model' => $artContent,

            ]);

        } else {

            return $this->render('page_form', [
                'model' => $artContent,
                'upload' => $upload,
                'redactor' => $redactor
            ]);
        }
    }

    /**
     * Удаление страницы контента
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionDeletepage($id)
    {
        $model = $this->loadModelcontent($id);
        $artId = $model->articles_id;

        if ($this->loadModelcontent($id)->delete()) {


            $content = ArticlesContent::find()
                ->where(['articles_id' => $artId]);

            $dataCont = new ActiveDataProvider([
                'query' => $content,

            ]);

            return $this->render('pages', [
                'content' => $dataCont,

            ]);
        } else {
            throw new \yii\web\HttpException(404, 'Cant delete record.');
        };


    }

    public function actionSearchPage()
    {
        if(Yii::$app->request->post())
        {
            $content = ArticlesContent::find()
                //->where('like', 'minintitle', Yii::$app->request->post('ArticlesContent')['minititle'])
                ->where('minititle like "%'.Yii::$app->request->post('ArticlesContent')['minititle'].'%"');

            $dataCont = new ActiveDataProvider([
                'query' => $content,

            ]);

            return $this->render('pages', [
                'content' => $dataCont,

            ]);

        }
        $model = new ArticlesContent();
        return $this->render('page', ['model' => $model]);

    }


    /**
     * Показывает страницы контента
     * @param $id
     * @return string
     */
    public function actionPages($id) {
        $content = ArticlesContent::find()
            ->where(['articles_id' => $id]);

        $dataCont = new ActiveDataProvider([
            'query' => $content,
            'pagination' => [
                'pageSize' => 10,
            ],

        ]);

        return $this->render('pages', [
            'content' => $dataCont,

        ]);

    }

    /**
     * Загружает запись модели текущего контроллера по айдишнику
     * @param $id
     * @return null|static
     * @throws \yii\web\HttpException
     */
    public function loadModel($id)
    {

        $model = Articles::findOne($id);

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Возвращает модель страницы контента
     * @param $id
     * @return null|static
     * @throws \yii\web\HttpException
     */
    public function loadModelcontent($id)
    {

        $model = ArticlesContent::findOne($id);

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Загрузка картинок статьи 
     * @param $id
     * @return \yii\web\Response
     */
    public function actionLoadArticlePictures($id){
        $model = ArticlesContent::findOne($id);

        $dom = new \DOMDocument('1.0', 'UTF-8');

        // set error level
        $internalErrors = libxml_use_internal_errors(true);

        // load HTML
        $dom->loadHTML($model->body);

        // Restore error level
        libxml_use_internal_errors($internalErrors);

        $img = $dom->getElementsByTagName("img");
        foreach ($img as $node) {

                foreach ($node->attributes as $attr) {
                    if($attr->localName === 'src') {
                        $extension = '.png';
                        if(strstr($attr->localName, 'jpg')) $extension = '.jpg';
                        $imageFile = md5($attr->nodeValue).$extension;
                       // var_dump($attr->nodeValue); exit;
                        //if()
                        copy($attr->nodeValue, '/home/romanych/public_html/plis/basic/web/uploads/article_img/'.$imageFile);

                        $image = new ImageStorage();
                        $image->img = '/home/romanych/public_html/plis/basic/web/uploads/article_img/'.$imageFile;
                        $image->orig_tag = $attr->nodeValue;
                        $image->cont_art_id = $id;
                        $image->save();

                      
                    }
                }
        }

        return $this->redirect(Url::toRoute('articles/index'));
        
    }
    
    public function actionKlavir(){

/*

        $query  = new Query();
       // $search_result = $query_search->from('siteSearch')->match($q)->all();  // поиск осуществляется по средством метода match с переданной поисковой фразой.
        $r = $query->from('items')
             ->match('шкалика')
             ->all();

        var_dump(Items::findOne($r)); exit;
*/


        $article_id = Articles::find()
            ->select('MAX(id)')
            ->scalar();
        
        if($article_id) $article = Articles::findOne($article_id);
        else return $this->redirect(Url::toRoute('articles/index'));

        $artContent = new ArticlesContent;

        if ($artContent->load(Yii::$app->request->post())) {
            $artContent->body = Yii::$app->request->post('ArticlesContent')['body'];
            $artContent->minititle = Yii::$app->request->post('ArticlesContent')['minititle'];
            $artContent->source_id = 434;

            $artContent->articles_id = $article_id;


            if($this->loadModel($article_id)->site_id == 13) {
                $act = new DiaryActs();
                $act->model_id = 6;
                $act->mark = 1;
                $act->user_id = 8;
                $act->save(false);
            }

            $artContent->save();


            $content = ArticlesContent::find()
                ->where(['articles_id' => $article_id]);

            $dataCont = new ActiveDataProvider([
                'query' => $content,

            ]);

            return $this->render('pages', [
                'content' => $dataCont,
                'model' => $artContent,

            ]);

        } else {

            return $this->render('klavir', ['model' => $artContent]);
        }


    }

    /**
     * Отдаём запрошенную статью
     *
     */
    function actionShow($id){
        //return 45;
        if ($id) {
            $this->layout = 'rncont';
            $article = ArticlesContent::findOne((int)$id);
            $article->d_shown = time();
            $article->update(false);
            return $this->render('article_by_id', ['rec' => $article]);
        }
        else return 'ups';

    }


    /*
    public function actionDealCats(){
        $res = [];

        $m = Categories::find()->where(['site_id' => 13])->all();

        foreach ($m as $h){
            $res[] = $h->name;

        }

        return  json_encode($res);
    }
   


    function get_picture($url, $target){

        copy($url,$target);
        return;



        $ch = curl_init($url);

        //curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11');

        $content = curl_exec($ch);
        curl_close($ch);

        if (file_exists($target)) :
            unlink($target);
        endif;

        $fp = fopen($target , 'x');
        fwrite($fp, $content);
        fclose($fp);

       
    }
    */





}

