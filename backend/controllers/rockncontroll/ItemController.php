<?php
namespace backend\controllers\rockncontroll;

use core\entities\Rockncontroll\Categories;
use core\entities\Rockncontroll\Source;
use core\entities\Rockncontroll\Tag;

use core\entities\Rockncontroll\DiaryActs;
use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\ItemsSearch;
use core\entities\Rockncontroll\RadioItem;
use core\entities\Rockncontroll\ItemsQueSearch;
use core\entities\Rockncontroll\MarkUser;
use core\entities\Rockncontroll\Playlist;

use core\forms\rockncontroll\UploadForm;

use core\helpers\TranslateHelper;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\web\Controller;

class ItemController extends Controller
{

    const STATUS_CREATE = 0;
    const STATUS_UPDATE = 1;
    public static $curr_playlist;

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@static').'/new_upload/imperaviim2/',// Directory URL address, where files are stored.
                'path' => '@staticRoot/new_upload/imperaviim2/' // Or absolute path to directory where files are stored.
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/images/blog/', // Directory URL address, where files are stored.
                'path' => '@static/images/blog/', // Or absolute path to directory where files are stored.
                'type' => '0',
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/files/blog/', // Directory URL address, where files are stored.
                'path' => '@static/files/blog/', // Or absolute path to directory where files are stored.
                'type' => '1',//GetAction::TYPE_FILES,
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/files/blog/', // Directory URL address, where files are stored.
                'path' => '@static/files/blog/' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex()
    {
       // $items = Items::find()->orderBy('id DESC');
        $searchModel = new ItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /*
        $dataProvider = new ActiveDataProvider([
            'query' => $items,

        ]);
        */

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionIndexRow()
    {
        // $items = Items::find()->orderBy('id DESC');
        $searchModel = new ItemsSearch();
        $dataProvider = $searchModel->searchrow(Yii::$app->request->queryParams);

        /*
        $dataProvider = new ActiveDataProvider([
            'query' => $items,

        ]);
        */

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionImgs(){
        $searchModel = new ItemsSearch();
        $dataProvider = $searchModel->searchimg(Yii::$app->request->queryParams);
        return $this->render('img', ['items' => $dataProvider, 'searchModel' => $searchModel]);
    }


    public function actionCreate()
    {

        $model = new Items();


        $uploadImg = new UploadForm();

        $hash = time();


        if (Yii::$app->request->isPost) {

            $uploadImg->img = UploadedFile::getInstance($uploadImg, 'img');

            if($uploadImg->img && $uploadImg->validate()) {
                $base_name = TranslateHelper::translit($uploadImg->img->baseName).md5(time());
                $uploadImg->img->saveAs(
                    (Yii::getAlias('@staticRoot')).'/uploads/item_pics/' . $base_name . '.' .$uploadImg->img->extension);
            }
            else { print_r($uploadImg->getErrors()); }


        }

        if ($model->load(Yii::$app->request->post())) {
            $model->text = Yii::$app->request->post('Items')['text'];
            $model->tags = Yii::$app->request->post('Items')['tags'];
            $model->title = Yii::$app->request->post('Items')['title'];
            $model->cens = Yii::$app->request->post('Items')['cens'];
            $model->cens = Yii::$app->request->post('Items')['published'];
            //$model->cat_id = Yii::$app->request->post('Items')['cat_id'];

            if(Categories::find()->where(['title' => Yii::$app->request->post('Items')['cat_title']])->one()){

                $model->cat_id = Categories::find()->where(['title' => Yii::$app->request->post('Items')['cat_title']])->one()->id;
            }
            
            $model->audio_link = Yii::$app->request->post('Items')['audio_link'];
            $model->in_work_prim = Yii::$app->request->post('Items')['in_work_prim'];
            $model->play_status = 1;


            //if(isset(Yii::$app->request->post('Items')['source_id']))$model->source_id = Yii::$app->request->post('Items')['source_id'];
            //else $model->source_id = 2;

            if(Source::find()->where(['title' => Yii::$app->request->post('Items')['source_title']])->one()){

                $model->source_id = Source::find()->where(['title' => Yii::$app->request->post('Items')['source_title']])->one()->id;
            }
            else $model->source_id = 2;

            $act = new DiaryActs();
            $act->model_id = 7;
            $act->mark = 1;
            $act->user_id = 8;
            $act->save(false);

            $model->act_id = $act->id;

            /*

            if(isset($uploadFile->file))
                $model->audio = Url::base().'uploads/' . Yii::$app->translater->translit($uploadFile->file->baseName) . '_' . $hash. '.' .$uploadFile->file->extension;

            */
            if(isset($uploadImg->img))
                $model->img = Url::base().'/uploads/item_pics/' . $base_name . '.' .$uploadImg->img->extension;

            $model->save(false);
            if($model->cat_id == 259){

                $max_id = (int)RadioItem::find()
                    ->select('MAX(id)')
                    ->scalar();

                $radio_item = new RadioItem();
                $radio_item->cat_id = 22;
                $radio_item->id = $max_id+1;
                $radio_item->source_id = $model->source_id;
                $radio_item->title = $model->title;
                $radio_item->text = $model->text;
                $radio_item->anons = '';
                $radio_item->tags = $model->tags;
                $radio_item->audio = $model->audio_link;
                $radio_item->cens = $model->cens;
                $radio_item->img = $model->img;
                $radio_item->published = 1;
                $radio_item->save(false);
            }
            //var_dump($model->id); exit;
            Tag::addTags($model->tags, $model->id);

            return $this->redirect(Url::toRoute('rockncontroll/item/index'));

        } else {

            return $this->render('_form', [
                'model' => $model,
                //'uploadFile' => $uploadFile,
                'uploadImg' => $uploadImg,

            ]);
        }

    }

    public function actionAddTag($id){

        $model = $this->loadModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->tags .= ', '.Yii::$app->request->post('Items')['tag'];
            //return var_dump($model);
            Tag::addTags(Yii::$app->request->post('Items')['tag'], $id);

            $model->save(false);
            return $this->redirect(Url::toRoute('/rockncontroll/item/index'));
            // return $this->redirect(Url::toRoute('/rockncontroll/pogxxi/index'));

        } else {

            return $this->render('add_tag_form', [
                'model' => $model,
            ]);
        }

    }


    public function actionAddAudio($id){

        $model = $this->loadModel($id);

        $uploadFile = new UploadForm();

        if (Yii::$app->request->isPost) {

            $uploadFile = new UploadForm();

            $uploadFile->file = UploadedFile::getInstance($uploadFile, 'file');

            if($uploadFile->file && $uploadFile->validate()) {
                $base_name_file = TranslateHelper::translit($uploadFile->file->baseName).md5(time());
                $uploadFile->file->saveAs(
                    (Yii::getAlias('@staticRoot')).'/mp3/' . $base_name_file . '.' .$uploadFile->file->extension);
            }
            else { print_r($uploadFile->getErrors()); }

            if(isset($uploadFile->file))
                $model->audio_link = Url::base().'/mp3/' . $base_name_file . '.' .$uploadFile->file->extension;

            //var_dump($uploadFile); exit;

            $model->save(false);
            return $this->redirect(Url::toRoute('/rockncontroll/item/index'));
            // return $this->redirect(Url::toRoute('/rockncontroll/pogxxi/index'));

        } else {

            return $this->render('add_audio_form', [
                'model' => $model,
                'uploadFile' => $uploadFile,
            ]);
        }

    }

    public function actionUpdate($id){

        $model = $this->loadModel($id);
        $uploadFile = new UploadForm();
        $uploadImg = new UploadForm();

        $model->cat_title = Categories::findOne($model->cat_id)->title;
        $model->source_title = Source::findOne($model->source_id)->title;

       // var_dump($model->source_title); exit;

        if (Yii::$app->request->isPost) {
            $uploadFile->file = UploadedFile::getInstance($uploadFile, 'file');
            $uploadImg->img = UploadedFile::getInstance($uploadImg, 'img');



            if($uploadImg->img && $uploadImg->validate()) {
                $base_name = TranslateHelper::translit($uploadImg->img->baseName).md5(time());
                $uploadImg->img->saveAs((Yii::getAlias('@staticRoot')).'/uploads/item_pics/' . $base_name . '.' .$uploadImg->img->extension);

            }
            else { print_r($uploadImg->getErrors()); }

            $uploadFile->file = UploadedFile::getInstance($uploadFile, 'mp3');

            if($uploadFile->file && $uploadFile->validate()) {
                $base_name_file = TranslateHelper::translit($uploadFile->file->baseName).md5(time());
                $uploadFile->file->saveAs(
                    (Yii::getAlias('@staticRoot')).'/mp3/' . $base_name_file . '.' .$uploadFile->file->extension);
            }
            else { print_r($uploadFile->getErrors()); }

        }

        if ($model->load(Yii::$app->request->post())) {
            $model->text = Yii::$app->request->post('Items')['text'];
            $model->tags = Yii::$app->request->post('Items')['tags'];
            $model->title = Yii::$app->request->post('Items')['title'];
            $model->cens = Yii::$app->request->post('Items')['cens'];
            $model->published = Yii::$app->request->post('Items')['published'];
            //$model->cat_id = Yii::$app->request->post('Items')['cat_id'];
            
            if(Categories::find()->where(['title' => Yii::$app->request->post('Items')['cat_title']])->one()){

                $model->cat_id = Categories::find()->where(['title' => Yii::$app->request->post('Items')['cat_title']])->one()->id;
            }
            
            $model->audio_link = Yii::$app->request->post('Items')['audio_link'];
            $model->in_work_prim = Yii::$app->request->post('Items')['in_work_prim'];
            //$model->play_status = 1;

            //Tag::addTags($model->tags, $id);


            //if(isset(Yii::$app->request->post('Items')['source_id']))$model->source_id = Yii::$app->request->post('Items')['source_id'];
            //else $model->source_id = 2;

            if(Source::find()->where(['title' => Yii::$app->request->post('Items')['source_title']])->one()){

                $model->source_id = Source::find()->where(['title' => Yii::$app->request->post('Items')['source_title']])->one()->id;
            }
            else $model->source_id = 2;

            $act = new DiaryActs();
            $act->model_id = 7;
            $act->mark = 1;
            $act->user_id = 8;
            $act->save(false);


            $model->act_id = $act->id;

            if(isset($uploadImg->img))
                $model->img = Url::base().'/uploads/item_pics/' . $base_name . '.' .$uploadImg->img->extension;
            if(isset($uploadFile->file))
                $model->img = Url::base().'/mp3/' . $base_name_file . '.' .$uploadFile->file->extension;
            /*

            if(isset($uploadFile->file))
                $model->audio = Url::base().'uploads/' . Yii::$app->translater->translit($uploadFile->file->baseName) . '.' .$uploadFile->file->extension;

            if(isset($uploadImg->img))
                $model->img = Url::base().'uploads/item_pics/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension;
            */
            $model->save(false);
            return $this->redirect(Url::toRoute('rockncontroll/item/index'));
            //print_r($uploadFile->getErrors());

            //return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);

             //return $this->redirect(Url::toRoute('/rockncontroll/item/index'));
           // return $this->redirect(Url::toRoute('/rockncontroll/pogxxi/index'));

        } else {

            return $this->render('_form', [
                'model' => $model,
                'uploadFile' => $uploadFile,
                'uploadImg' => $uploadImg,

            ]);
        }

    }

    /**
     * Удаляет айтем
     * @param $id
     *
     * @throws
     * @return
     */
    public function actionDelete($id)
    {

        if ($model = $this->loadModel($id)->delete()) {
            $items = Items::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $items,
            ]);

            return $this->render('index', ['items' => $dataProvider]);
        } else {
            throw new \yii\web\HttpException(404, 'Cant delete record.');
        }

    }

    /**
     * Список без аудио
     * @return string
     */
    public function actionListNoAudio(){
        $items = Items::find()
            //->where(['audio_link' => ''])
            ->where('cat_id = 259');
            //->andWhere("source_id <> 2 and source_id <> 43 and cat_id <> 53")
            //->orderBy(['rand()' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $items,
        ]);

        return $this->render('list', ['items' => $dataProvider]);
    }

    /**
     * КВН без аудио
     * @return string
     */
    public function actionListKvnNoAudio(){

        $searchModel = new ItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'kvns');

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);

    }

    /**
     * Рабочий список
     * @return string
     */
    public function actionInWork(){
        $items = Items::find()->where("in_work_prim <> '' ");

        $dataProvider = new ActiveDataProvider([
            'query' => $items,
        ]);

        return $this->render('list', ['items' => $dataProvider]);
    }
    
    public function actionShowRepertoire(){
        $items = Items::find()->where("cat_id = 90 or cat_id = 89");

        $dataProvider = new ActiveDataProvider([
            'query' => $items,
        ]);

        return $this->render('list', ['items' => $dataProvider]);
    }

    /**
     * Добавление плейлиста
     * @return string|\yii\web\Response
     */
    public function actionAddPlaylist(){
        $playlist = new Playlist();

        if ($playlist->load(Yii::$app->request->post())) {
            $playlist->name = Yii::$app->request->post('Playlist')['name'];
            $playlist->save();

            return $this->redirect(Url::toRoute('item/index'));

        } else {

            return $this->render('new_playlist', ['playlist' => $playlist]);
        }

        
    }
    
    public function actionChoosePlaylist(){
        $playlists = Playlist::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $playlists,
        ]);


        return $this->render('playlists', ['playlists' => $dataProvider]);
    }

    /**
     * Формирование плейлиста
     * @param $id
     * @return string
     */
    public function actionFormPlaylist($id){
        
        if($id == 7) {
            $searchModel = new ItemsQueSearch();
            $dataProvider = $searchModel->searchRepertoire(Yii::$app->request->queryParams);

            $this_items = Items::find()->where(['play_status' => $id])->orderBy('radio_que');
            $dataProvider2 = new ActiveDataProvider([
                'query' => $this_items,
            ]);
        }
        else {
            $searchModel = new ItemsQueSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $this_items = Items::find()->where(['play_status' => $id])->orderBy('radio_que');
            $dataProvider2 = new ActiveDataProvider([
                'query' => $this_items,
            ]);
        }


        return $this->render('playlist', ['items' => $dataProvider, 'new_items' => $dataProvider2, 'pl' => $id, 'searchModel' => $searchModel]);
    }

    /**
     * Добавление в плейлист
     * @param $id
     * @param $pl
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionPlAdd($id, $pl){
        

        $model = $this->loadModel($id);
        $model->play_status = $pl;
        $model->update(false);

        if($pl == 7) {
            $searchModel = new ItemsQueSearch();
            $dataProvider = $searchModel->searchRepertoire(Yii::$app->request->queryParams);

            $this_items = Items::find()->where(['play_status' => 7])->orderBy('radio_que');
            $dataProvider2 = new ActiveDataProvider([
                'query' => $this_items,
            ]);
        }
        else {
            $searchModel = new ItemsQueSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $this_items = Items::find()->where(['play_status' => $pl])->orderBy('radio_que');
            $dataProvider2 = new ActiveDataProvider([
                'query' => $this_items,
            ]);
        }




        return $this->render('playlist', ['items' => $dataProvider, 'new_items' => $dataProvider2,  'searchModel' => $searchModel, 'pl' => $pl]);

    }

    /**
     * Удаление из плейлиста
     * @param $id
     * @param $pl
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionPlRemove($id, $pl){
        $model = $this->loadModel($id);
        $model->play_status = 1;
        $model->update(false);

        $searchModel = new ItemsQueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this_items = Items::find()->where(['play_status' => $pl])->orderBy('radio_que');
        $dataProvider2 = new ActiveDataProvider([
            'query' => $this_items,
        ]);

        return $this->render('playlist', ['items' => $dataProvider, 'new_items' => $dataProvider2,  'searchModel' => $searchModel,  'pl' => $pl]);
        
    }

    public function actionChangeq(){
        $que = 100;
        if(Yii::$app->getRequest()->getQueryParam('que') !== null && Yii::$app->getRequest()->getQueryParam('id') !== null) {
            $que = Yii::$app->getRequest()->getQueryParam('que');
            $id = Yii::$app->getRequest()->getQueryParam('id');
            $model = $this->loadModel($id);
            $model->radio_que = $que;
            $model->update(false);
        }
        
        echo $que;
    }
    

    public function loadModel($id)
    {

        $model = Items::findOne($id);

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        return $model;
    }

    
    function actionShow($id){
        if ($id) {
            $this->layout = 'rncont';
            $item = Items::findOne((int)$id);
            return $this->render('item_by_id', ['rec' => $item]);
        }
        else return 'ups';

    }

    public function actionEditItemText(){
        return 5;
        //$request = Yii::$app->request;
        //return $request->post('edited');
        //return var_dump(Yii::$app->getRequest()->getQueryParam('user'));
        if(Yii::$app->getRequest()->getQueryParam('user')) {

            $user = MarkUser::findOne(Yii::$app->getRequest()->getQueryParam('user'));

            if (!$user) return 'Доступ запрещен!';

            if(Yii::$app->getRequest()->getQueryParam('edited') && Yii::$app->getRequest()->getQueryParam('id')){
                $item = Items::findOne((int)Yii::$app->getRequest()->getQueryParam('id'));
                $item->text = Yii::$app->getRequest()->getQueryParam('edited');
                if($item->update(false)) return 'Изменено!';
                else return 'Измена!';
            }
            else return 'Ошибка сохранения';
        }

        else return 'Ошибка доступа';
    }

    /**
     * @return string
     */
    public function actionFilmPhrases(){
        $items = Items::find()
            ->where(['source_id' => 20])
            ->andWhere("tags = ''");
           // ->orderBy(['rand()' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $items,
        ]);

        return $this->render('list', ['items' => $dataProvider]);
    }

    public function actionSongPhrases(){

        $searchModel = new ItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'songs');

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionEnglishPhrases(){
        $items = Items::find()
            ->where(['cat_id' => 155])
            ->andWhere("tags = '' and audio_link <> ''");
        // ->orderBy(['rand()' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $items,
        ]);

        return $this->render('list', ['items' => $dataProvider]);
    }
    
    


}