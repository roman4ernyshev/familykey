<?php
namespace backend\controllers\rockncontroll;

use core\helpers\TranslateHelper;
use core\entities\Rockncontroll\DiaryActs;
use core\entities\Rockncontroll\RadioSource;
use core\entities\Rockncontroll\Source;
use core\entities\Rockncontroll\ArticlesContent;
use core\entities\Rockncontroll\SourceSearch;

use core\forms\rockncontroll\UploadForm;

use yii\web\Controller;
use yii\db\IntegrityException;
use yii\helpers\Url;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

class SourceController extends Controller
{
    //public $layout = 'admin';

    public function actionIndex()
    {
       /* $sources = Source::find()->orderBy('id DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $sources,
        ]);

        return $this->render('index', ['sources' => $dataProvider]);
       */

        $searchModel = new SourceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', ['sources' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionItList()
    {

        $searchModel = new SourceSearch();
        $dataProvider = $searchModel->searchIt(Yii::$app->request->queryParams);

        return $this->render('index', ['sources' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * Создание источника
     * @return string
     */
    public function actionCreate()
    {
        $model = new Source();
        
        $uploadImg = new UploadForm();

        if (Yii::$app->request->isPost) {
            
            $uploadImg->img = UploadedFile::getInstance($uploadImg, 'img');

            if($uploadImg->img && $uploadImg->validate()) {
                $uploadImg->img->saveAs('uploads/covers/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension);

            }
            else { print_r($uploadImg->getErrors()); }
        }


        if ($model->load(Yii::$app->request->post())) {
            $model->title = Yii::$app->request->post('Source')['title'];
            $model->author_id = Yii::$app->request->post('Source')['author_id'];
            $model->status = Yii::$app->request->post('Source')['status'];
            $model->cat_id = Yii::$app->request->post('Source')['cat_id'];
            if(isset($uploadImg->img))
                $model->cover = Url::base().'uploads/covers/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension;
            //$model->save(false);

            if($model->save(false)) {
                try {
                    $radio_source = new RadioSource();
                    $radio_source->id = $model->id;
                    $radio_source->author_id = $model->author_id;
                    $radio_source->title = $model->title;
                    $radio_source->save(false);
                } catch (IntegrityException $e) {
                    return $e->getMessage();
                }
            };

            $sources = Source::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $sources,
            ]);

            return $this->redirect(Url::toRoute('rockncontroll/source/index'));

        } else {

            return $this->render('_form', [
                'model' => $model,
                'uploadImg' => $uploadImg,
            ]);
        }

    }

    public function actionUpdate($id){

        $model = $this->loadModel($id);


        $uploadImg = new UploadForm();

        if (Yii::$app->request->isPost) {

            $uploadImg->img = UploadedFile::getInstance($uploadImg, 'img');

            if($uploadImg->img && $uploadImg->validate()) {
                $uploadImg->img->saveAs((Yii::getAlias('@staticRoot')).'/uploads/covers/' . Yii::$app->translater->translit($uploadImg->img->baseName) . '.' .$uploadImg->img->extension);

            }
            else { print_r($uploadImg->getErrors()); }
        }


        if ($model->load(Yii::$app->request->post())) {
            $model->title = Yii::$app->request->post('Source')['title'];
            $model->author_id = Yii::$app->request->post('Source')['author_id'];
            $model->status = Yii::$app->request->post('Source')['status'];
            $model->cat_id = Yii::$app->request->post('Source')['cat_id'];
            if(isset($uploadImg->img))
                $model->cover = Url::base().'/uploads/covers/' . $base_name . '.' .$uploadImg->img->extension;
            $model->save(false);

            $act = new DiaryActs();
            $act->model_id = 17;
            $act->mark = 1;
            $act->user_id = 8;
            $act->save(false);

            return $this->redirect(Url::toRoute('rockncontroll/source/index'));
        } else {

            return $this->render('_form', [
                'model' => $model,
                'uploadImg' => $uploadImg,
            ]);
        }

    }

    public function actionDelete($id)
    {

        if ($model = $this->loadModel($id)->delete()) {
            $source = Source::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $source,
            ]);

            return $this->render('index', ['sources' => $dataProvider]);
        } else {
            throw new \yii\web\HttpException(404, 'Cant delete record.');
        };

    }

    public function loadModel($id)
    {

        $model = Source::findOne($id);

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        return $model;
    }



}