<?php
namespace backend\controllers\rockncontroll;

use core\entities\Rockncontroll\DeutschItem;
use core\entities\Rockncontroll\DeutschItemSearch;
use core\entities\Rockncontroll\DiaryActs;
use core\helpers\CmdHelper;
use core\helpers\TranslateHelper;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;


class DeutschController extends Controller
{

    public $layout = 'main';

    public function actionIndex()
    {
        $searchModel = new DeutschItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /*
        $dataProvider = new ActiveDataProvider([
            'query' => $items,

        ]);
        */

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);
        
    }

    public function actionTicket()
    {
        $searchModel = new DeutschItemSearch();
        $dataProvider = $searchModel->searchTicket(Yii::$app->request->queryParams);

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionDubles()
    {
        $searchModel = new DeutschItemSearch();
        $dataProvider = $searchModel->searchDoubles(Yii::$app->request->queryParams);

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);

    }

    public function actionRusDoubles()
    {
        $searchModel = new DeutschItemSearch();
        $dataProvider = $searchModel->searchRusDoubles(Yii::$app->request->queryParams);

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);

    }

    public function actionWithoutAudio()
    {
        $searchModel = new DeutschItemSearch();
        $dataProvider = $searchModel->searchWithBracket(Yii::$app->request->queryParams);

        return $this->render('index', ['items' => $dataProvider, 'searchModel' => $searchModel]);

    }

    public function actionCreate()
    {

        $model = new DeutschItem();

        if ($model->load(Yii::$app->request->post())) {

            $word = Yii::$app->request->post('DeutschItem')['d_word'];
            $phrase = Yii::$app->request->post('DeutschItem')['d_phrase'];

            $model->d_word = $word;
            $model->d_phrase = $phrase;

            $model->d_word_translation = Yii::$app->request->post('DeutschItem')['d_word_translation'];
           
            $model->d_phrase_translation = Yii::$app->request->post('DeutschItem')['d_phrase_translation'];

            //$model->d_word_transcription = Yii::$app->request->post('DeutschItem')['d_word_transcription'];
            
            //$model->d_phrase_transcription = Yii::$app->request->post('DeutschItem')['d_phrase_transcription'];

            /*генерация слова и ссылки*/

            /*

            $word_for_file = str_replace(' ', '_', $word);
            $word_for_file = str_replace('?', '_', $word_for_file);

            $word_for_file = TranslateHelper::onlyLatLetters($word);
            $cmd = "espeak -v mb-de4 '".$word."' -s 100 -w /home/romanych/Музыка/Thoughts_and_klassik/deutsch/".$word_for_file.".wav";
            shell_exec($cmd);
            */
            $word_for_file = TranslateHelper::onlyLatLetters($word);
            //return var_dump(is_file("/home/romanych/Музыка/Thoughts_and_klassik/deutsch/$word_for_file.wav"));
            //$cmd = "espeak -v mb-de4 '".$word_for_file."' -s 100 -w /home/romanych/Музыка/Thoughts_and_klassik/deutsch/".$word_for_file.".wav";
            //return var_dump(CmdHelper::espeakDeutschWav($word, $word_for_file));
            //return var_dump($cmd);
            CmdHelper::espeakDeutschWav($word, $word_for_file);
            $model->audio_link = "deutsch/".$word_for_file.".wav";

            /*генерация фразы и ссылки*/
            /*

            $phrase_for_file = str_replace(' ', '_', $phrase);
            $phrase_for_file = str_replace('?', '_', $phrase_for_file);
            $phrase_for_file = str_replace('.', '_', $phrase_for_file);
            $phrase_for_file = str_replace('(', '_', $phrase_for_file);
            $phrase_for_file = str_replace(')', '_', $phrase_for_file);


            $phrase_for_file = TranslateHelper::onlyLatLetters($phrase);
            return var_dump($phrase_for_file);
            $cmd = "espeak -v mb-de4 '".$phrase."' -s 100 -w /home/romanych/Музыка/Thoughts_and_klassik/deutsch/".$phrase_for_file.".wav";
            shell_exec($cmd);
            */

            $phrase_for_file = TranslateHelper::onlyLatLetters($phrase);
            CmdHelper::espeakDeutschWav($phrase, $phrase_for_file);
            $model->audio_phrase_link = "deutsch/".$phrase_for_file.".wav";

            //$model->audio_phrase_link = "deutsch/".$phrase_for_file.".wav";


            if($model->save(false)){
                $act = new DiaryActs();
                $act->model_id = 18;
                $act->mark = 1;
                $act->user_id = 8;
                $act->save(false);
                //return $this->redirect(Url::to('http://37.192.187.83/deutschitem/index'));
                return $this->redirect(Url::toRoute('rockncontroll/deutsch/index'));
            }

                //return $this->redirect(Yii::$app->request->referrer);
            else return $this->render('_form', [
                'model' => $model,
            ]);

        } else {

            return $this->render('_form', [
                'model' => $model,
            ]);
        }

    }

    public function actionRecord($id)
    {
        $model = $this->loadModel($id);
        $word = $model->d_word;

        $word_for_file = TranslateHelper::onlyLatLetters($word).time();
        CmdHelper::espeakDeutschWav($word, $word_for_file);
        $model->audio_link = "deutsch/".$word_for_file.".wav";
        if($model->update(false))
            return $this->redirect(Url::toRoute('rockncontroll/deutsch/without-audio'));
        else return $this->render('_form', [
            'model' => $model,
        ]);

    }

    public function actionUpdate($id){
        $model = $this->loadModel($id);
        if ($model->load(Yii::$app->request->post())) {

            $model->d_word = Yii::$app->request->post('DeutschItem')['d_word'];
           
            $model->d_phrase = Yii::$app->request->post('DeutschItem')['d_phrase'];

            $model->d_word_translation = Yii::$app->request->post('DeutschItem')['d_word_translation'];
           
            $model->d_phrase_translation = Yii::$app->request->post('DeutschItem')['d_phrase_translation'];

            $model->shown = Yii::$app->request->post('DeutschItem')['shown'];

           // $model->d_word_transcription = Yii::$app->request->post('DeutschItem')['d_word_transcription'];
            
           // $model->d_phrase_transcription = Yii::$app->request->post('DeutschItem')['d_phrase_transcription'];

            if($model->update(false, ['d_word', 'd_phrase', 'd_word_translation', 'd_phrase_translation', 'shown']))
                return $this->redirect(Url::toRoute('rockncontroll/deutsch/index'));
            else return $this->render('_form', [
                'model' => $model,
            ]);

        } else {

            return $this->render('_form', [
                'model' => $model,
            ]);
        }

    }

    public function actionDelete($id){

        if ($model = $this->loadModel($id)->delete()) {
            return $this->redirect(Url::toRoute('rockncontroll/deutsch/index'));
        } else {
            throw new \yii\web\HttpException(404, 'Cant delete record.');
        }

    }

    public function loadModel($id)
    {

        $model = DeutschItem::findOne($id);

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
    
    
}