<?php
namespace backend\controllers\rockncontroll;

use app\components\TranslateHelper;
use core\entities\Rockncontroll\Author;
use core\entities\Rockncontroll\ArticlesContent;
use core\entities\Rockncontroll\AuthorSearch;
use core\entities\Rockncontroll\RadioAuthor;

use core\entities\Rockncontroll\SongText;
use core\entities\Rockncontroll\SongtextSearch;
use core\entities\Rockncontroll\Source;
use core\entities\Rockncontroll\SourceSearch;
use core\helpers\ReadMp3Helper;
use yii\web\Controller;
use yii\db\IntegrityException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;
use yii\data\ActiveDataProvider;


class AuthorController extends Controller
{
    //public $layout = 'admin';

    public function actionIndex()
    {
        /*
        $authors = Author::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $authors,
        ]);

        return $this->render('index', ['authors' => $dataProvider]);
        */

        $searchModel = new AuthorSearch();
        $rand_author = Author::find()->where(['status' => 1])->orderBy(['rand()' => SORT_DESC])->limit(2)->all();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index',
            [
                'authors' => $dataProvider,
                'searchModel' => $searchModel,
                'rand_author1' => $rand_author[0]->name,
                'rand_author2' => $rand_author[1]->name
            ]);
    }

    public function actionMusic()
    {
        $authors = Author::find()->where(['status' => 1]);
        $rand_author = Author::find()->where(['status' => 1])->orderBy(['rand()' => SORT_DESC])->limit(2)->all();
        $searchModel = new AuthorSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => $authors,
        ]);

        return $this->render('index',
            [
                'authors' => $dataProvider,
                'searchModel' => $searchModel,
                'rand_author1' => $rand_author[0]->name,
                'rand_author2' => $rand_author[1]->name
            ]
        );
    }

    /**
     * Создание автораа
     * @return string
     */
    public function actionCreate()
    {
        $model = new Author();


        if ($model->load(Yii::$app->request->post())) {
            $model->name = Yii::$app->request->post('Author')['name'];
            $model->status = Yii::$app->request->post('Author')['status'];
            $model->releases = Yii::$app->request->post('Author')['releases'];
            $model->description = Yii::$app->request->post('Author')['description'];
            $model->country_id = Yii::$app->request->post('Author')['country_id'];

            if($model->save(false)) {
                try {
                    $radio_author = new RadioAuthor();
                    $radio_author->id = $model->id;
                    $radio_author->name = $model->name;
                    $radio_author->save(false);
                } catch (IntegrityException $e) {
                    return $e->getMessage();
                }
            };

            $authors = Author::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $authors,
            ]);

            return $this->redirect(Url::toRoute('author/index'));

        } else {

            return $this->render('_form', [
                'model' => $model,

            ]);
        }

    }

    public function actionUpdate($id)
    {

        $model = $this->loadModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->name = Yii::$app->request->post('Author')['name'];
            $model->status = Yii::$app->request->post('Author')['status'];
            $model->releases = Yii::$app->request->post('Author')['releases'];
            $model->description = Yii::$app->request->post('Author')['description'];
            $model->country_id = Yii::$app->request->post('Author')['country_id'];

            $model->save(false);

            $authors = Author::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $authors,
            ]);

            return $this->redirect(Url::toRoute('rockncontroll/author/index'));

        } else {

            return $this->render('_form', [
                'model' => $model,

            ]);
        }
    }

    public function actionSourceTexts(){

        if(Yii::$app->getRequest()->getQueryParam('id')) $id = Yii::$app->getRequest()->getQueryParam('id');
        else return $this->redirect(Url::toRoute('author/index'));

        $source_texts = implode(',', ArrayHelper::map(Source::find()->where("author_id = $id")->all(), 'id', 'id'));

        $searchModel = new SongtextSearch();
        $dataProvider = $searchModel->searchSourceTexts(Yii::$app->request->queryParams, $id);

        return $this->render('song_texts', ['texts' => $dataProvider, 'searchModel' => $searchModel, 'author_id' => $id]);
    }

    public function actionDeleteText(){
        if(Yii::$app->getRequest()->getQueryParam('id')) $id = Yii::$app->getRequest()->getQueryParam('id');
        else return $this->redirect(Url::toRoute('author/index'));

        $text = SongText::findOne($id);
        $author_id = $text->source->author_id;
        if($text->delete()){
            return $this->redirect(Url::toRoute('/rockncontroll/author/sources/'.$author_id));
        }
        else return $this->redirect(Url::toRoute('author/index'));
    }

    public function actionSources()
    {
        if(Yii::$app->getRequest()->getQueryParam('id')) $id = Yii::$app->getRequest()->getQueryParam('id');
        else return $this->redirect(Url::toRoute('author/index'));

        $searchModel = new SourceSearch();
        $dataProvider = $searchModel->searchArtistSources(Yii::$app->request->queryParams, $id);

        return $this->render('sources', ['sources' => $dataProvider, 'searchModel' => $searchModel]);

    }

    public function actionAuthorSourcesBySourceId()
    {

        if(Yii::$app->getRequest()->getQueryParam('id')) $id = Yii::$app->getRequest()->getQueryParam('id');
        else return $this->redirect(Url::toRoute('author/index'));

        $searchModel = new SourceSearch();
        $dataProvider = $searchModel->searchArtistSourcesBySourceId(Yii::$app->request->queryParams, $id);

        return $this->render('sources', ['sources' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionRadio()
    {
        if(Yii::$app->getRequest()->getQueryParam('id'))$id = Yii::$app->getRequest()->getQueryParam('id');
        else return $this->redirect(Url::toRoute('author/index'));

        $author = Author::findOne($id);

        $source = Source::find()->where("author_id = $id")->one();
        //$song = SongText::find()->where(['source_id' => $source->id])->one();
        $song_link_nenor_radio = '';
        $song_link_lern_radio = '';
        $song_link_lern_radio_num = '';
        $song_link_nenor_radio_num = '';
        $meta_nenor = '';
        $meta_lern = '';

        $directories = ['musicplis/',
            'music/Music/',
            'best_alboms/',
            'stodesatmus/music/'];

        foreach ($directories as $directory){
            $lines = file('/home/romanych/radio/dio/playlist_nenor.txt');
            foreach($lines as $num_line => $line_value)
            {
                //var_dump($line_value);
                //var_dump($directory.$author->name); exit;
                /*

                if(strpos($line_value, $directory.$author->name)) {
                    $song_link_nenor_radio = $line_value;
                    $song_link_nenor_radio_num = $num_line;
                }
                */

                $author_name = str_ireplace("/", "_", $author->name);

                //if(strpos($line_value, $directory.trim($author_name).'/')) {
                if(strpos($line_value, $directory.$author_name.'/')) {

                    $song_link_nenor_radio = $line_value;
                    //$oReader = new ReadMp3Helper();
                    //var_dump($oReader->getTagsInfo(trim($song_link_nenor_radio))); exit;
                    //$meta_nenor = $oReader->getTagsInfo(trim($song_link_nenor_radio));
                    //$tag = id3_get_tag( "path/to/example.mp3" );
                    //print_r($tag);



                    $meta_nenor = shell_exec("eyeD3 '".trim($song_link_nenor_radio)."'");
                    //$meta_nenor = escapeshellcmd($meta_nenor);

                    //require_once('/usr/share/php/getid3/getid3.php');
                    //phpinfo();
                    //$tag = id3_get_tag($song_link_nenor_radio);
                    //print_r($tag);


                    //var_dump($meta_nenor); exit;
                    $song_link_nenor_radio_num = $num_line;
                }
                    //echo "$num_line $line_value<br>"; exit;
                    //echo "$line_value";
            }
        }

        foreach ($directories as $directory){
            $lines = file('/home/romanych/radio/dio/playlist.txt');
            foreach($lines as $num_line => $line_value)
            {
                //var_dump($line_value);
                //var_dump($directory.$author->name); exit;

                $author_name = str_ireplace("/", "_", $author->name);
                //return $line_value;

                //if(strpos($line_value, $directory.trim($author_name).'/')) {
                if(strpos($line_value, $directory.$author_name.'/')) {
                    $song_link_lern_radio = $line_value;
                    $meta_lern = shell_exec("eyeD3 '".trim($song_link_lern_radio)."'");
                    $song_link_lern_radio_num = $num_line;
                }
                //echo "$num_line $line_value<br>"; exit;
                //echo "$line_value";
            }
        }
        ///home/romanych/radio/dio/playlist.txt
        /// //$cmd = $event->sender->voice." '".$model->{$this->from}."' -s 120 -w ".$file;
        //$cmd = "mp3info '".trim($song_link_lern_radio)."'";
        //return var_dump(shell_exec('mp3info \'/home/romanych/Музыка/Thoughts_and_klassik/best_alboms/Warlock/1987 - Triumph and agony/04_Kiss Of Death.mp3\''));
        //return "mp3info '".trim($song_link_lern_radio)."'";
        //return var_dump(shell_exec($cmd));

        return $this->render('radio',
            [
                'song_link_nenor_radio' => $song_link_nenor_radio,
                'song_link_lern_radio' => $song_link_lern_radio,
                'song_link_nenor_radio_num' => $song_link_nenor_radio_num,
                'song_link_lern_radio_num' => $song_link_lern_radio_num,
                'meta_nenor' => $meta_nenor,
                'meta_lern' => $meta_lern,
                'author_name' => $author->name
            ]);

    }


    public function loadModel($id)
    {

        $model = Author::findOne($id);

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        return $model;
    }

    private function _curl($url)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                //'Accept-Encoding:gzip, deflate, sdch',
                //'Accept-Language:*',
                'Cache-Control:no-cache',
                'Connection:keep-alive',
                'Cookie:_dc_gtm_UA-28208502-12=1; _ga=GA1.2.1191596796.1477908016',
                'Host:d.soccerstand.com', 'Pragma:no-cache',
                'Referer:http://d.soccerstand.com/ru/x/feed/proxy-local',
                'User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
                'X-Fsign:SW9D1eZo',
                'X-GeoIP:1',
                'X-Requested-With:XMLHttpRequest',
                'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7'
            ]);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }


}