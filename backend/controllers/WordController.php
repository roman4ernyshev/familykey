<?php

namespace backend\controllers;

use core\entities\Word\Definition;
use core\entities\Word\DefinitionSearch;
use core\entities\Word\DefinitionThema;
use core\entities\Word\Example;
use core\entities\Word\ExampleSearch;
use core\entities\Word\Thema;
use core\entities\Word\ThemaSearch;
use core\entities\Word\Word;
use core\entities\Word\WordSearch;
use core\forms\rockncontroll\UploadForm;
use yii\helpers\Url;
use yii\web\Controller;
use Yii;

class WordController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@static').'/word/img',// Directory URL address, where files are stored.
                'path' => '@staticRoot/word/img/' // Or absolute path to directory where files are stored.
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/word/img/', // Directory URL address, where files are stored.
                'path' => '@static/word/img/', // Or absolute path to directory where files are stored.
                'type' => '0',
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/word/audio/', // Directory URL address, where files are stored.
                'path' => '@static/word/audio/', // Or absolute path to directory where files are stored.
                'type' => '1',//GetAction::TYPE_FILES,
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/word/audio/', // Directory URL address, where files are stored.
                'path' => '@static/word/audio/' // Or absolute path to directory where files are stored.
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new WordSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$definitions =

        return $this->render('index', [
            'searchModel' => $searchModel,
            'words' => $dataProvider,
        ]);
    }

    public function actionThemes()
    {
        $searchModel = new ThemaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('themes', [
            'searchModel' => $searchModel,
            'themes' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Word();

        if ($model->load(Yii::$app->request->post())) {
            $model->lang = Yii::$app->request->post('Word')['lang'];
            $model->title = Yii::$app->request->post('Word')['title'];

            if($model->validate()) {
                $model->save();
                return $this->redirect(Url::toRoute('word/index'));
            }
            else {
                return $this->render('_form', [
                        'model' => $model]
                );
            }
        } else {
            return $this->render('_form', [
                'model' => $model]
            );
        }
    }

    public function actionCreateThema()
    {
        $thema = new Thema();

        if ($thema->load(Yii::$app->request->post())) {
            $thema->title = Yii::$app->request->post('Thema')['title'];

            if($thema->validate()) {
                $thema->save();
                return $this->redirect(Url::toRoute('word/index'));
            }
            else {
                return $this->render('add_thema_form', [
                        'thema' => $thema]
                );
            }
        } else {
            return $this->render('add_thema_form', [
                    'thema' => $thema]
            );
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->lang = Yii::$app->request->post('Word')['lang'];
            $model->title = Yii::$app->request->post('Word')['title'];

            $model->save(false);
            return $this->redirect(Url::toRoute('word/index'));

        } else {

            return $this->render('_form', [
                    'model' => $model]
            );
        }
    }

    public function actionDelete($id)
    {
        if ($this->loadModel($id)->delete()) {
            return $this->redirect(Url::toRoute('word/index'));
        } else {
            throw new \yii\web\HttpException(404, 'Cant delete record.');
        }
    }

    public function actionAddDefinitionToThema($id){
        //echo $id; exit;
        $thema_to_definition = new DefinitionThema();
        if (Yii::$app->request->post()) {
           // var_dump(Yii::$app->request->post()); exit;
            if(Thema::find()->where(['title' => Yii::$app->request->post('DefinitionThema')['title']])->one()){
                $thema_to_definition->crossword_thema_id =
                    Thema::find()->where(['title' => Yii::$app->request->post('DefinitionThema')['title']])->one()->id;
            }
            $thema_to_definition->definition_id = (int)Yii::$app->request->post('DefinitionThema')['definition'];
            //$thema_to_definition->crossword_thema_id = Yii::$app->request->post('DefinitionThema')['id'];
            $thema_to_definition->save(false);
            return $this->redirect(Url::toRoute('/word/index'));
        }
        else{
            return $this->render('add_thema_to_definition_form', [
                'definition_id' => $id,
                'thema' => $thema_to_definition
            ]);
        }
    }

    public function actionDefinition($id){

        $definition = new Definition();

        if (Yii::$app->request->post()) {

            $definition->word_id = $id;

            $definition->tags = Yii::$app->request->post('Definition')['tags'];
            $definition->etymology = Yii::$app->request->post('Definition')['etymology'];
            $definition->forms = Yii::$app->request->post('Definition')['forms'];
            $definition->type = Yii::$app->request->post('Definition')['type'];
            $definition->synonyms = Yii::$app->request->post('Definition')['synonyms'];
            $definition->antonyms = Yii::$app->request->post('Definition')['antonyms'];
            $definition->adult_status = Yii::$app->request->post('Definition')['adult_status'];

            if(Yii::$app->request->post('Definition')['type'] === '1'){
                $definition->value = "<img src='https://static.roomab.art/"
                    .Yii::$app->request->post('Definition')['value']."' width='20px'>";
            }
            //Url::to('@static/'.$item->img)
            elseif(Yii::$app->request->post('Definition')['type'] === '2'){
                $definition->value = "<audio controls><source src='https://static.roomab.art/"
                    .Yii::$app->request->post('Definition')['value']."' type='audio/mpeg'></audio>";
            }
            else {
                $definition->value = Yii::$app->request->post('Definition')['value'];
            }

            $definition->save(false);
            return $this->redirect(Url::toRoute('/word/index'));

        } else {

            return $this->render('add_definition_form', [
                'definition' => $definition,
            ]);
        }

    }

    public function actionShowDefinitions($id)
    {
        $searchModel = new DefinitionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $word = $this->loadModel($id)->title;

        return $this->render('definitions',
            ['definition' => $dataProvider,
                'searchModel' => $searchModel,
                'word' => $word
            ]);
    }

    public function actionShowExamples($id)
    {
        $searchModel = new ExampleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $definition = Definition::find()->where(['id' => $id])->one()->value;

        //var_dump($definition); exit;

        return $this->render('examples',
            [
                'example' => $dataProvider,
                //'searchModel' => $searchModel,
                'definition' => $definition
            ]);
    }

    public function actionUpdateDefinition($id){

        $definition = Definition::findOne($id);

        if (Yii::$app->request->post()) {

            $definition->tags = Yii::$app->request->post('Definition')['tags'];
            $definition->etymology = Yii::$app->request->post('Definition')['etymology'];
            $definition->forms = Yii::$app->request->post('Definition')['forms'];
            $definition->type = Yii::$app->request->post('Definition')['type'];
            $definition->synonyms = Yii::$app->request->post('Definition')['synonyms'];
            $definition->antonyms = Yii::$app->request->post('Definition')['antonyms'];
            $definition->adult_status = Yii::$app->request->post('Definition')['adult_status'];

            if(Yii::$app->request->post('Definition')['type'] === '1'){
                $definition->value = "<img src='https://static.roomab.art/"
                    .Yii::$app->request->post('Definition')['value']."' width='20px'>";
            }
            elseif(Yii::$app->request->post('Definition')['type'] === '2'){
                $definition->value = "<audio controls><source src='https://static.roomab.art/"
                    .Yii::$app->request->post('Definition')['value']."' type='audio/mpeg'></audio>";
            }
            else {
                $definition->value = Yii::$app->request->post('Definition')['value'];
            }

            $definition->save(false);
            return $this->redirect(Url::toRoute('/word/index'));

        } else {

            return $this->render('add_definition_form', [
                'definition' => $definition,
            ]);
        }

    }

    public function actionDeleteDefinition($id)
    {
        if (Definition::findOne($id)->delete()) {
            return $this->redirect(Url::toRoute('word/index'));
        } else {
            throw new \yii\web\HttpException(404, 'Cant delete record.');
        }
    }

    public function actionExample($id){

        $example = new Example();

        if (Yii::$app->request->post()) {

            $example->definition_id = $id;

            $example->text = Yii::$app->request->post('Example')['text'];

            $example->save(false);
            return $this->redirect(Url::toRoute('/word/index'));

        } else {

            return $this->render('add_example_form', [
                'example' => $example,
            ]);
        }

    }

    public function actionUpdateExample($id){

        $example = Example::findOne($id);

        if (Yii::$app->request->post()) {

            $example->text = Yii::$app->request->post('Example')['text'];
            $example->save(false);
            return $this->redirect(Url::toRoute('/word/index'));

        } else {

            return $this->render('add_example_form', [
                'example' => $example,
            ]);
        }

    }

    public function loadModel($id)
    {

        $model = Word::findOne($id);

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        return $model;
    }

}