<?php
namespace backend\assets;

use yii\web\AssetBundle;

class RedactorAsset extends AssetBundle
{
    public $sourcePath = '@npm/ckeditor--ckeditor5-core/src/';
    public $css = [

    ];
    public $js = [
        'plagin.js',
    ];
    public $cssOptions = [

    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}