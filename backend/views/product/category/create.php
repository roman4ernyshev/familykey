<?php

/* @var $this yii\web\View */
/* @var $model core\forms\manage\Product\CategoryForm */

$this->title = 'Create Category';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-create">

  <?= $this->render('_form', [
        'model' => $model
  ])?>

</div>
