<?php

use \yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model core\forms\manage\Product\ProductConstCreateForm */

$this->title = 'Create Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]);
    ?>

    <div class="box box-default">
        <div class="box-header with-border">Product</div>
        <div class="box-body">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 20]) ?>
            <?= $form->field($model, 'carbohydrates')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'fats')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'squirrels')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'kkal')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'ferrum')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'magnesium')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'cuprum')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'iodum')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'fluorum')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'zincum')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'cobaltum')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">Photos</div>
        <div class="box-body">
            <?= $form->field($model->photos, 'files[]')->widget(\kartik\widgets\FileInput::class, [
                'options' => [
                    'accept' => 'image/*',
                    'multiple' => true,
                ]
            ]) ?>
        </div>
    </div>


    <div class="box box-default">
        <div class="box-header with-border">SEO</div>
        <div class="box-body">
            <?= $form->field($model->meta, 'title')->textInput() ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
            <?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>


    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
