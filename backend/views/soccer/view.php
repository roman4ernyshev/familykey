<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $tournament core\entities\Soccer\Tournament */

$this->title = $tournament->name;
$this->params['breadcrumbs'][] = ['label' => 'Tournaments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">

            <div class="box">
                <div class="box-header with-border">Турниры</div>
                <div class="box-body">

                    <?= DetailView::widget([
                        'model' => $tournament,
                        'attributes' => [
                            'id',
                            'name',
                            'hash',
                            'season',
                            'description',
                            'country_id',
                        ],
                    ]) ?>
                    <br />

                </div>
            </div>
        </div>

    </div>


</div>
