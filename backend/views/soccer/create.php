<?php

use \yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model core\forms\manage\Soccer\TournamentCreateForm */

$this->title = 'Create Tournament';
$this->params['breadcrumbs'][] = ['label' => 'Tournaments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]);
    ?>

    <div class="box box-default">
        <div class="box-header with-border">Турнир</div>
        <div class="box-body">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'hash')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'season')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 20]) ?>

            <div class="col-md-4">
                <?= $form->field($model, 'countryId')->dropDownList($model->countriesList()) ?>
            </div>


        </div>
    </div>

    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
