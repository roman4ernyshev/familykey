<?php

use backend\widgets\grid\RoleColumn;
use kartik\date\DatePicker;
use core\entities\User\User;
use core\helpers\UserHelper;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\Soccer\TournamentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tournaments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p>
        <?= Html::a('Create Tournament', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-sm',
                ],
                'options' => [
                    'class' => 'table-responsive',
                ],
                'columns' => [
                    'id',
                    'name',
                    'hash',
                    'season',
                    [
                        'attribute' => 'country_id',
                        //'filter' => $searchModel->categoriesList(),
                        'value' => 'country.name',
                    ],
                    //'done',
                    [
                        'attribute' => 'done',
                        'value' => function (\core\entities\Soccer\Tournament $model) {
                                        if($model->done) return 'Parsed';
                                        return Html::a('Mark Parsed',
                                            ['mark-parsed', 'id' => $model->id],
                                            ['class'=>'btn btn-primary']);
                        },
                        'format' => 'raw',
                    ],
                    /*
                    [
                        'value' => function(\core\entities\Soccer\Tournament $model)  {
                            return Html::a('0<span class="glyphicon glyphicon-hdd"></span>',
                                ['save-tournament-by-id', 'id' => $model->id, 'part' => 0]);
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align: center'],
                    ],
                    [
                        'value' => function(\core\entities\Soccer\Tournament $model)  {
                            return Html::a('1<span class="glyphicon glyphicon-hdd"></span>',
                                ['save-tournament-by-id', 'id' => $model->id, 'part' => 1]);
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align: center'],
                    ],
                    */

                    ['class' => ActionColumn::class],
                ],
            ]); ?>
        </div>
    </div>
</div>