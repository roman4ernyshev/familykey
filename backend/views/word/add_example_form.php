<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Nav;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $example \core\entities\Word\Example */

?>
<style>
    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);
    }
    label{
        color: yellow;
    }
</style>
<div class="site-login">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php

            echo Nav::widget([
                'options' => ['class' => 'nav nav-sidebar'],
                'items' => [
                ],
            ]);

            ?>

        </div>
        <div class="col-lg-10">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?=  $form->field($example, 'text')->textInput()  ?>

            <div class="form-group">
                <?= Html::submitButton($example->isNewRecord ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>


