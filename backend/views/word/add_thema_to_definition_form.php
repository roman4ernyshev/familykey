<?php

use yii\helpers\Html;
use core\entities\Word\Thema;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Nav;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $thema Thema */
/* @var $definition_id int */

?>
<style>
    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);
    }
    label{
        color: yellow;
    }
</style>
<div class="site-login">
    <h1 class="text-center"></h1>

    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php

            echo Nav::widget([
                'options' => ['class' => 'nav nav-sidebar'],
                'items' => [
                ],
            ]);

            ?>

            <?php
            //фомируем список автокомплита

            $themes = Thema::find()
                ->select(['title as label'])
                ->asArray()
                ->all();

            //$definition = \core\entities\Word\Definition::findOne($definition_id);
            //echo $definition_id;

            ?>

            <h3></h3>

        </div>
        <div class="col-lg-10">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?php echo $form->field($thema, 'title')->widget(
                AutoComplete::class, [
                'clientOptions' => [
                    'source' => $themes,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>

            <?=  $form->field($thema, 'definition')->hiddenInput(['value' => $definition_id])  ?>

            <div class="form-group">
                <?= Html::submitButton($thema->isNewRecord
                    ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>

