<?php

/*
 * @var $sources
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\assets\AppAsset;
use yii\grid\GridView;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;

//AppAsset::register($this);
?>
<div class="col-sm-3 col-md-2 sidebar">
    <?php
    echo Nav::widget([
        'options' => ['class' => 'nav nav-sidebar'],
        'items' => [
            ['label' => 'Создать', 'url' => ['rockncontroll/source/create']],
            ['label' => 'Редактировать', 'url' => ['rockncontroll/source/update']],

        ],
    ]);

    ?>

</div>

<div class="col-sm-9 col-md-10 main">



    <?= GridView::widget([
        'dataProvider' => $example,
        //'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'label' =>  'Примеры для '.$definition,

                'value' => function($example){
                    return $example->text;
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {update} {add-example}',
                'buttons' =>
                    [
                        'delete' => function ($url, $model, $example) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute(['delete-example','id' => $example]), [
                                'title' => Yii::t('yii', 'Удалить'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'get',
                                'data-pjax' => '0',
                            ]);
                        },

                        'update' => function ($url, $model, $example) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['update-example','id' => $example]), [
                                'title' => Yii::t('yii', 'Редактировать'),
                                'data-method' => 'get',
                                'data-pjax' => '0',
                            ]);
                        }
                        /*
                        'add-example' => function ($url, $model, $definition) {
                            return Html::a('<span class="glyphicon glyphicon-plus"></span>', Url::toRoute(['example','id' => $definition]), [
                                'title' => Yii::t('yii', 'Добавить пример'),
                                'data-method' => 'get',
                                'data-pjax' => '0',
                            ]);
                        },
                        */



                    ]
            ]
        ],
    ]); ?>
</div>
