<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\grid\GridView;
use yii\helpers\Url;


?>


    <div class="sidebar">

<?php

echo Nav::widget([
    'options' => ['class' => 'nav nav-sidebar'],
    'items' => [
        ['label' => 'Создать слово', 'url' => ['/word/create']],
        ['label' => 'Создать тему', 'url' => ['/word/create-thema']],
        ['label' => 'Темы', 'url' => ['/word/themes']],
    ],
]);
?>

<?php /*
   <!-- Информер RedDay.RU (Новосибирск)-->
   <a href="http://redday.ru/moon/" target="_new">
       <img src="http://redday.ru/informer/i_moon/245/bl.png" width="150" height="190" border="0" alt="Фазы Луны на RedDay.ru (Новосибирск)" />
   </a>
   <!-- / Информер RedDay.RU-->


</div>

<div class="col-sm-9 col-md-10 main">

 */
?>
    <h1 class="page-header">Words</h1>
<?php  //var_dump($articles); exit; ?>
<?= GridView::widget([
    'dataProvider' => $words,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        'title',
        [
            'label' => 'Определение',
            'format'=>'raw',
            'value' => function($words){
                return Html::a(
                        ''.count(\yii\helpers\ArrayHelper::map($words->definitions,
                            'id',
                            'value')),
                        Url::toRoute(['show-definitions', 'id' => $words->id]), [
                    'title' => Yii::t('yii', 'Посмотреть'),
                    'data-method' => 'get',
                    'data-pjax' => '0',
                ]);
            },
        ],

        ['class' => 'yii\grid\ActionColumn',
            'template' => '{delete} {update} {add-definition}',
            'buttons' =>
                [
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute(['delete','id' => $model->id]), [
                            'title' => Yii::t('yii', 'Удалить'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'get',
                            'data-pjax' => '0',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['update','id' => $model->id]), [
                            'title' => Yii::t('yii', 'Редактировать'),
                            'data-method' => 'get',
                            'data-pjax' => '0',
                        ]);
                    },
                    'add-definition' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', Url::toRoute(['definition','id' => $model->id]), [
                            'title' => Yii::t('yii', 'Добавить определение'),
                            'data-method' => 'get',
                            'data-pjax' => '0',
                        ]);
                    },

                ]
        ]
    ],
]); ?>
<?php /* </div> */ ?>