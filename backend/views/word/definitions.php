<?php

/*
 * @var $sources
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\assets\AppAsset;
use yii\grid\GridView;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;

//AppAsset::register($this);
?>
<div class="col-sm-3 col-md-2 sidebar">
    <?php



    echo Nav::widget([
        'options' => ['class' => 'nav nav-sidebar'],
        'items' => [
            ['label' => 'Создать', 'url' => ['rockncontroll/source/create']],
            ['label' => 'Редактировать', 'url' => ['rockncontroll/source/update']],

        ],
    ]);

    ?>

</div>

<div class="col-sm-9 col-md-10 main">

    <?= GridView::widget([
        'dataProvider' => $definition,
        //'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'label' =>  'Определения '.$word,

                'value' => function($definitions){
                    return $definitions->value;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Примеры',
                'format'=>'raw',
                'value' => function($example){
                    return Html::a(
                        ''.count(\yii\helpers\ArrayHelper::map($example,
                            'id',
                            'id')),
                        Url::toRoute(['show-examples', 'id' => $example->id]), [
                        'title' => Yii::t('yii', 'Посмотреть'),
                        'data-method' => 'get',
                        'data-pjax' => '0',
                    ]);
                },
            ],

            [
                'label' => 'Темы',
                'format'=>'raw',
                //'value' =>  $themen_titels,
                'value' => function($definition){
                    $titles = '';
                    if($definition->themes){
                        foreach ($definition->themes as $theme) {
                            $titles .= $theme->title. '<br>';
                        }
                    }
                    return $titles;
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {update} {add-example} {add-thema}',
                'buttons' =>
                    [
                        'delete' => function ($url, $model, $definition) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute(['delete-definition','id' => $definition]), [
                                'title' => Yii::t('yii', 'Удалить'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'get',
                                'data-pjax' => '0',
                            ]);
                        },

                        'update' => function ($url, $model, $definition) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['update-definition','id' => $definition]), [
                                'title' => Yii::t('yii', 'Редактировать'),
                                'data-method' => 'get',
                                'data-pjax' => '0',
                            ]);
                        },
                        'add-example' => function ($url, $model, $definition) {
                            return Html::a('<span class="glyphicon glyphicon-plus"></span>', Url::toRoute(['example','id' => $definition]), [
                                'title' => Yii::t('yii', 'Добавить пример'),
                                'data-method' => 'get',
                                'data-pjax' => '0',
                            ]);
                        },
                        'add-thema' => function ($url, $model, $definition) {
                            return Html::a('<span class="glyphicon glyphicon-plus"></span>',
                                Url::toRoute(['add-definition-to-thema','id' => $definition]), [
                                    'title' => Yii::t('yii', 'Добавить тему определению'),
                                    'data-method' => 'get',
                                    'data-pjax' => '0',
                                ]);
                        },


                    ]
            ]
        ],
    ]); ?>
</div>
