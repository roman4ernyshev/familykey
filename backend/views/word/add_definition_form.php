<?php

use core\entities\Word\Definition;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Nav;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $definition Definition */

?>
<style>
    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);
    }
    label{
        color: yellow;
    }
</style>
<div class="site-login">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php

            echo Nav::widget([
                'options' => ['class' => 'nav nav-sidebar'],
                'items' => [
                ],
            ]);

            ?>

        </div>
        <div class="col-lg-10">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?=  $form->field($definition, 'tags')->textInput()  ?>
            <?=  $form->field($definition, 'forms')->textInput()  ?>
            <?=  $form->field($definition, 'etymology')->textInput()  ?>
            <?=  $form->field($definition, 'value')->textInput()  ?>
            <?=  $form->field($definition, 'synonyms')->textInput()  ?>
            <?=  $form->field($definition, 'antonyms')->textInput()  ?>
            <?=  $form->field($definition, 'adult_status')->dropDownList([ '0', '+18' ]) ?>
            <?=  $form->field($definition, 'type')->dropDownList([ 'string', 'image', 'audio', 'en_str', 'rus_str', 'de_str' ]) ?>


            <div class="form-group">
                <?= Html::submitButton($definition->isNewRecord ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>

