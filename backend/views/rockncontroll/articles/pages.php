<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\grid\GridView;
use yii\helpers\Url;


?>
    <style>
        body {
            font-family: "Open Sans", sans-serif;
            line-height: 1.25;
        }

        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }

        table caption {
            font-size: 1.5em;
            margin: .5em 0 .75em;
        }

        table tr {
            background-color: #363b3f;
            color: white;
            border: 1px solid #ddd;
            padding: .35em;
        }

        table th,
        table td {
            padding: .625em;
            text-align: left;
            overflow: hidden;
        }

        table th {
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
        }

        .page-header {
            color: rgb(255, 255, 0);
        }

        thead tr th:first-child,
        tbody tr td:first-child {
            width: inherit;
            min-width: 4em;
            max-width: 4em;
            word-break: break-all;
        }

        thead tr th:last-child,
        tbody tr td:last-child {
            width: 8em;
            min-width: 8em;
            max-width: 8em;
            word-break: break-all;
        }

        @media screen and (max-width: 600px) {
            table {
                border: 0;
            }

            table caption {
                font-size: 1.3em;
            }

            table thead {
                border: none;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }

            table tr {
                border-bottom: 3px solid #ddd;
                display: block;
                margin-bottom: .625em;
            }

            table td {
                border-bottom: 1px solid #ddd;
                display: block;
                font-size: .8em;
                text-align: right;
            }

            table td::before {
                /*
                * aria-label has no advantage, it won't be read inside a table
                content: attr(aria-label);
                */
                content: attr(data-label);
                float: left;
                font-weight: bold;
                text-transform: uppercase;
            }

            table td:last-child {
                border-bottom: 0;
            }

            thead tr th:first-child,
            tbody tr td:first-child {
                width: inherit;
                min-width: inherit;
                max-width: inherit;
                word-break: normal;
            }

            thead tr th:last-child,
            tbody tr td:last-child {
                width: inherit;
                min-width: inherit;
                max-width: inherit;
                word-break: normal;
            }
        }
    </style>
    <div class="col-sm-3 col-md-2 sidebar">
        <?php

        echo Nav::widget([
            'options' => ['class' => 'nav nav-sidebar'],
            'items' => [
                ['label' => 'Создать новый контент', 'url' => ['rockncontroll/articles/create']],
                ['label' => 'Показать контент', 'url' => ['rockncontroll/articles/index']],
                //['label' => 'Добавить страницу', 'url' => ['/articles/addpage/'.$model->id]],

            ],
        ]);

        ?>

    </div>

    <div class="col-sm-9 col-md-10 main">
    <h1 class="page-header">Контент</h1>
<?php  //var_dump($articles); exit; ?>
<?php if(isset($content)) : ?>

    <?= GridView::widget([
        'dataProvider' => $content,
        //'filterModel' => $searchModel,
        'columns' => [
            'minititle',

            ['class' => 'yii\grid\ActionColumn',
                'template' => ' {update} {load-article-pictures} {delete}',
                'buttons' =>
                    [

                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute(['deletepage','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Уверены, что хотите удалить страницу?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },

                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['updatepage','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Update'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },
                        'load-article-pictures' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-camera"></span>', Url::toRoute(['load-article-pictures','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Загрузить фото страницы'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        }

                    ]
            ]
        ],
    ]); ?>

<?php endif; ?>