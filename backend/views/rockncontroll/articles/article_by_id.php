<?php

use vova07\imperavi\Widget;

?>
<style>
    .form-group{
        text-align: center;
    }

    .btn-success {
        width: 100%;
        color: rgb(29, 28, 28);
        background-color: rgb(241, 183, 10);
        border-color: rgb(50, 21, 225);
    }
    @media(min-width:320px) and (max-width:767px){.container{width: 100%; padding: 0 3px;}}
    .article, .post__text {
        display: block;
        color: rgb(192, 195, 196);
        font-size: 16px;
        border-radius: 5px;
        /*background: #ffffff url(<?=\yii\helpers\Url::to('@web/images/bg.jpg')?>) repeat-y top center;*/
        background: rgb(27, 29, 30);
        background-size: 100%;
        line-height: 20px;
        padding: 10px;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        /*color:#333333;
        font-size: 13px;
        line-height: 24px;
         font-family: 'Tinos', sans-serif;*/

    }
    .article pre {
        background-color: rgb(0, 0, 0);
        border: 1px solid rgb(82, 82, 82);
        border-radius: 4px;
        color: #01ff70;
    }

    h1, h2, h3, h4, h5, h6 {
        font-size: 25px;
        color: rgb(255, 235, 59);
        text-align: center;
        width: 100%;
    }
    p, span{
        overflow: hidden;

    }
    .article_night{
        display: block;
        color: rgb(218, 212, 195);
        font-size: 17px;
        border-radius: 5px;
        background: none;
        background-size: 100%;
        line-height: 20px;
        padding: 10px;
        text-align: left
    }
     h1, h2, {
        font-size: 25px;
    }
    img {width: 100%}
    .article {text-align: left}
    /*.article pre p {color: #0a0a0a; width: 100%}*/
    .article p {color: color: rgb(105, 102, 105); font-size: 17px; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}

    .article ul li {color: rgb(118, 128, 137); font-size: 15px;}
    a {
        color: rgb(103, 58, 183);
        text-decoration: none;
    }
</style>

<h3><hr><?=$rec->source->author->name ?><br><hr> <?=$rec->source->title?><br><hr><p><?=$rec->minititle?></p><hr> </h3>
<button type="button" class="btn btn-success" onclick="get_prev_article()" id="prev_article">PREV</button>
<!--
<p style="text-align: center;">
    <button type="button" class="btn btn-success" onclick="daySkinn()" id="day_btn" style="display: none;">День!</button>
    <button type="button" class="btn btn-success" onclick="nightSkinn()" id="night_btn" >Ночь!</button>
</p>
-->

<?php if($rec->audio) : ?>
    <audio controls="controls" class="audio">
        <source src='<?=\yii\helpers\Url::to($rec->audio)?>' type='audio/mpeg'>
    </audio>
<?php endif; ?>
<span class="article"><?=hostFilterAndCopyForEmmit($rec->body)?></span>
<button type="button" class="btn btn-success" onclick="get_next_article()" id="next_article">NEXT</button>
<?php
echo Widget::widget([
    'selector' => '#text',
    'settings' => [
        'lang' => 'ru',
        'minHeight' => 300,
        'pastePlainText' => true,
        //'buttons' => ['html', 'formatting', 'bold', 'italic'],
        'buttonSource' => true,
        'plugins' => [
            'clips',
            'fullscreen',
            'table'
        ],
        //'imageManagerJson' => Url::to(['/articles/images-get']),
        'imageUpload' => \yii\helpers\Url::to(['rockncontroll/default/image-upload']),

        //'fileManagerJson' => Url::to(['/uploads/files-get']),
        //'fileUpload' => Url::to(['/uploads/file-upload'])
    ]
]);

?>

<?php
//http://88.85.67.159:8082/uploads/imperaviim/582fa6619edb5.jpg
function hostFilterAndCopyForEmmit($text){
    $text = str_replace("http://37.192.187.83:10034/", "http://88.85.67.159:8082/", $text);
    $text = str_replace("http://37.192.187.83:8014/", "http://88.85.67.159:8082/", $text);
    $text = str_replace("http://37.192.187.83/", "http://88.85.67.159:8082/", $text);
    $text = str_replace("/uploads/", "uploads/", $text);
    $text = str_replace("uploads/imperaviim/", "http://88.85.67.159:8082/uploads/imperaviim/", $text);
    return str_replace("http://37.192.187.83:8014/", "http://88.85.67.159:8082", $text);
}
?>

<script>
    var user = <?= (isset($user->id)) ? $user->id : 8 ?>;
    var prev = <?=$rec->getPrevNextPagesOfBook($rec->id, $rec->source->id)['prev']?>;
    var next = <?=$rec->getPrevNextPagesOfBook($rec->id, $rec->source->id)['next']?>;

    function get_prev_article() {
        window.location = "http://88.85.67.159:8093/rockncontroll/articles/show/" + prev;
    }
    function get_next_article() {
        window.location = "http://88.85.67.159:8093/rockncontroll/articles/show/" + next;
    }

    $(document).ready(function() {
        var img;

        if(!prev) $('#prev_article').hide();
        if(!next) $('#next_article').hide();


        $('img').click(function(){
           //console.log(this.src);
           if(this.src.indexOf('http:37.192.187.83') === 0) img = this.src.slice(17);
           else img = this.src;

          // console.log(img);

           $(this).after(this.src +
               '<p id="res"><button type="button" class="btn btn-success" id="record">Новый илл айтем</button>' +
               '<input type="text" class="form-control" id="title" placeholder="title">' +
               '<input type="text" class="form-control" id="source" placeholder="source">' +
               '<input type="text" class="form-control" id="cat" placeholder="cat">' +
               '<input type="text" class="form-control" id="tags" placeholder="tags">' +
               '<textarea class="form-control" id="text"  placeholder="Текст" rows="10" cols="45"></textarea>' +
               '<input type="text" class="form-control" id="old_data" placeholder="old_data">' +
               '<input type="text" class="form-control" id="cens" placeholder="cens">' +
               '<input type="text" class="form-control" id="published" placeholder="published"></p>');

            $('#cat').autoComplete({
                minChars: 3,
                source: function (term, suggest) {
                    term = term.toLowerCase();
                    console.log(term);
                    $.getJSON("/rockncontroll/default/deal-cats/", function (data) {

                        choices = data;
                        var suggestions = [];
                        for (i = 0; i < choices.length; i++)
                            if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                        suggest(suggestions);

                    }, "json");

                }
            });

            $('#source').autoComplete({
                minChars: 3,
                source: function (term, suggest) {
                    term = term.toLowerCase();
                    console.log(term);
                    $.getJSON("/rockncontroll/default/sources/", function (data) {

                        choices = data;
                        var suggestions = [];
                        for (i = 0; i < choices.length; i++)
                            if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                        suggest(suggestions);

                    }, "json");

                }
            });

            $("#record").click(
                function() {

                    var title = $("#title").val();
                    var source = $("#source").val();
                    var cat = $("#cat").val();
                    var tags = $("#tags").val();
                    var txt = $("#text").val();
                    var old_data = $("#old_data").val();
                    var cens = $("#cens").val();
                    var published = $("#published").val();


                    if (title == '') {alert('Введите название!'); return;}
                    if (source == '') {alert('Введите источник!'); return;}
                    if (cat == '') {alert('Введите категорию!'); return;}
                    if (tags == '') {alert('Введите тэги!'); return;}
                    if (cens == '') {alert('Введите уровень цензуры!'); return;}
                    if (txt == '') {alert('Введите текст!'); return;}
                    if (old_data == '') {old_data = 0;}
                    if (published == '') {published = 1;}


                    rec(title, source, cat, tags, txt, user, old_data, cens, published, img);

                });


        });


    });

    function rec(title, source, cat, tags, txt, user, old_data, cens, published, img) {
        $.ajax({
            type: "GET",
            url: "/rockncontroll/default/record-item",
            //data: "title="+title+"&source="+source+"&cat="+cat+"&tags="+tags+"&txt="+txt+"&user="+user+"&old_data="+old_data+"&cens="+cens+"&published="+published,
            data:{title:title,user:user,source:source,cat:cat,tags:tags,txt:txt,old_data:old_data,cens:cens,published:published,img:img},
            success: function(html){
                $("#res").html(html);

            }

        });
    }

    function nightSkinn() {
        //document.getElementsByClassName("article")[0].removeAttribute("class");
        els = document.getElementsByClassName("article");
        for (var j=0; j<els.length; j++)  {
            els[j].setAttribute("class", "article_night");
        }
        day_btn.style.display = 'block';
        night_btn.style.display = 'none';
        codes = document.getElementsByTagName('pre');
        for (var i=0; i<codes.length; i++)  {
            codes[i].style.color = 'white';
            codes[i].style.backgroundColor = 'black';
        }


    }

    function daySkinn() {
        //document.getElementsByClassName("article")[0].removeAttribute("class");

        els = document.getElementsByClassName("article_night");
        for (var j=0; j<els.length; j++)  {
            els[j].setAttribute("class", "article");
        }
        day_btn.style.display = 'none';
        night_btn.style.display = 'block';
        codes = document.getElementsByTagName('pre');
        for (var i=0; i<codes.length; i++)  {
            codes[i].style.color = 'black';
            codes[i].style.backgroundColor = 'white';
        }
    }


</script>