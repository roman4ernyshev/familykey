<?php
/**
 * @var Event $model
 * @var $events array
 * @var $searchModel
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\grid\GridView;
use yii\helpers\Url;
use \core\entities\Rockncontroll\Event;

//AppAsset::register($this);
?>

<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header">Контент</h1>
    <?php  //var_dump($events); exit; ?>
    <?= GridView::widget([
        'dataProvider' => $events,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'text',
            [
                'attribute' => 'old_data',
                'value' => function (Event $model) {
                    return $model->old_data ? $model->old_data : date('Y-m-d', $model->act->time);
                },
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:100px'],
            ],
            [
                'attribute' => 'img',
                'value' => function (Event $model) {
                    return $model->img ? '<img src="'.Yii::getAlias('@static').'/'.$model->img.'" width="20px"/>' : '';
                },
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:50px'],
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{photo} {update}',
                'buttons' =>
                    [
                        'photo' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-camera"></span>', Url::toRoute(['add-img','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Добавить фото'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['update-text','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Редактировать'),
                                'data-method' => 'get',
                                'data-pjax' => '0',
                            ]);
                        },

                    ]
            ]
        ],
    ]); ?>
</div>