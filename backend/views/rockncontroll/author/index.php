<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\assets\AppAsset;
use yii\grid\GridView;
use yii\helpers\Url;

?>
<div class="col-sm-3 col-md-2 sidebar">
    <?php

    echo Nav::widget([
        'options' => ['class' => 'nav nav-sidebar'],
        'items' => [
            ['label' => 'Создать', 'url' => ['rockncontroll/author/create']],
            ['label' => 'Редактировать', 'url' => ['rockncontroll/author/update']],
            ['label' => 'Music', 'url' => ['rockncontroll/author/music']],
        ],
    ]);

    ?>
    <p style="color: #0b93d5;"><?= $rand_author1 .' - '. $rand_author2 ?></p>

</div>

<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header">Авторы</h1>
    <?php  //var_dump($articles); exit; ?>
    <?= GridView::widget([
        'dataProvider' => $authors,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {update} {sources} {radio}',
                'buttons' =>
                    [
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute(['delete','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Удалить'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['update','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Редактировать'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },
                        'sources' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-book"></span>', Url::toRoute(['sources','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Источники'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },
                        //glyphicon glyphicon-music
                        'radio' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-music"></span>', Url::toRoute(['radio','id' => $model->id]), [
                                'title' => Yii::t('yii', 'Источники'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },


                    ]
            ]
        ],
    ]); ?>
</div>