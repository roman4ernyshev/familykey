<?php

use core\entities\Rockncontroll\DeutschItem;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\assets\AppAsset;
use yii\grid\GridView;
use yii\helpers\Url;

//AppAsset::register($this);
?>
<div class="col-sm-3 col-md-2 sidebar">
    <?php

    echo Nav::widget([
        'options' => ['class' => 'nav nav-sidebar'],
        'items' => [
            ['label' => 'Создать', 'url' => ['rockncontroll/deutsch/create']],
            ['label' => 'Тикеты', 'url' => ['rockncontroll/deutsch/ticket']],
            ['label' => 'Дубли слов', 'url' => ['rockncontroll/deutsch/dubles']],
            ['label' => 'Дубли переводов', 'url' => ['rockncontroll/deutsch/rus-doubles']],
           // ['label' => 'Без аудио', 'url' => ['rockncontroll/deutsch/without-audio']],
        ],
    ]);

    ?>

</div>

<div class="col-sm-9 col-md-10 main">
    <h1 class="page-header">Deutsch Worte</h1>
    <?php  //var_dump($articles); exit; ?>
    <?= GridView::widget([
        'dataProvider' => $items,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'd_word',
            'd_word_translation',
            /*
            [
                'attribute' => 'audio',
                'value' => function (DeutschItem $model) {
                    return  '
                     <audio controls="controls" >
                         <source src="http://37.192.187.83:10080/'.$model->audio_link.'" >
                     </audio>';
                },

                'format' => 'raw',
                //'headerOptions' => ['style' => 'width:50px'],
            ],
            */

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {update} {record}',
                'buttons' =>
                    [
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"> </span>', Url::to('delete/'.$model->id), [
                                'title' => Yii::t('yii', 'Удалить'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"> </span>', Url::to('update/'.$model->id), [
                                'title' => Yii::t('yii', 'Редактировать'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },
                        'record' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-bullhorn"> </span>', Url::to('record/'.$model->id), [
                                'title' => Yii::t('yii', 'Перезаписать аудио'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to rewrite audio?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        },


                    ]
            ]
        ],
    ]); ?>
</div>
<style>
    .glyphicon {
        font-size: 20px;
        padding-left: 5px;
    }
</style>