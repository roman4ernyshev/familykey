<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use core\entities\Rockncontroll\Tag;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $uploadFile  */

?>
<style>
    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);
    }
</style>
<div class="site-login">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">


        </div>
        <div class="col-lg-10">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

            <?= $form->field($uploadFile, 'file')->fileInput() ?>

            <button>Submit</button>

            <?php ActiveForm::end() ?>
        </div>

    </div>
</div>

