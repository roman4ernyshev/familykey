<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use core\entities\Rockncontroll\Tag;

/* @var $this yii\web\View */
/* @var $model \core\entities\Rockncontroll\Items */

?>
<style>
    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);
    }
</style>
<div class="site-login">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">


            <?php
            //фомируем список автокомплита
            $tags = Tag::find()
                ->select(['name as label'])
                ->asArray()
                ->all();
            ?>

        </div>
        <div class="col-lg-10">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?php echo $model->tags; ?>

            <?php echo $form->field($model, 'tag')->widget(
                AutoComplete::className(), [
                'clientOptions' => [
                    'source' => $tags,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>

            <div class="form-group">
                <?= Html::submitButton('Добавить тэг', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>

