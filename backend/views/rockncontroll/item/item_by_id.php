<style>
    .form-group{
        text-align: center;
    }
    @media(min-width:320px) and (max-width:767px){.container{width: 100%; padding: 0 3px;}}
    .article {
        display: block;
        color: rgb(105, 102, 105);
        font-size: 17px;
        border-radius: 5px;
        font-family: Ubuntu;
    }
    .post__title, h1, h2{
        font-size: 25px;
    }
    img {width: 100%;  filter: opacity(0.8) drop-shadow(0 0 #d3ab33);}
    .article {
        background-color: #0a0a0a;
        text-align: left;
        padding: 10px;
    }
    .article pre p {color: #0a0a0a; width: 100%}
    .article p {
        color: rgb(211, 171, 51);
        font-size: 17px; font-family: Ubuntu;
    }
    h4, h3, h5 {color: #f3e611; overflow: hidden; text-align: center}
    .article ul li {color: rgb(23, 73, 20); font-size: 15px;}
</style>

<h3><?=$rec->title?></h3> <h4><?=$rec->source->title?></h4> <h5><?=$rec->source->author->name ?></h5>
<?php if($rec->audio_link) : ?>
<p style="text-align: center">
    <audio controls="controls" >
        <source src="<?=\yii\helpers\Url::to('@music/'.$rec->audio_link)?>" type='audio/mpeg'>
    </audio>
</p>
<?php endif; $isImgRelative = \yii\helpers\Url::isRelative($rec->img);
/*
  *  $isImgRelative = Url::isRelative($thought->img);

        if(!$isImgRelative)
            return nl2br((strip_tags($thought->text))).' '.$date.' '.$source.'<br>
                            <img style="width: 100%" src='.Url::to($thought->img).' />
                            ';
        elseif ($thought->img) return nl2br(strip_tags($thought->text)).' '.$date.' '.$source.
                '<br><img style="width: 100%" src="'.Url::to('@static/'.$thought->img).'"/>'
                // <br><img style="width: 100%" src='.Url::to($thought->img).' /><br>'
            //. var_dump(strpos($thought->img, 'http'))
        ;
        else return nl2br(strip_tags($thought->text)).' '.$date.' '.$source;
  */
?>
<?php if($rec->img && $isImgRelative) : ?>
   <img src="<?=\yii\helpers\Url::to('@static/'.$rec->img)?>">
<?php elseif ($rec->img && !$isImgRelative) : ?>
    <img src="<?=\yii\helpers\Url::to($rec->img)?>">
<?php endif; ?>
<span class="article"><?=nl2br($rec->text)?></span>