 <?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Nav;
use yii\jui\AutoComplete;
use core\entities\Rockncontroll\Tag;
use vova07\imperavi\Widget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */


$this->title = $model->isNewRecord ? 'Добавить айтем' : 'Редактировать айтем';
$this->params['breadcrumbs'][] = $this->title;

?>
 <style>
     .redactor-editor, .redactor-box {
         background: rgb(33, 32, 35);
         color: rgb(255, 255, 255);
     }
     .redactor-editor h5 {
         font-size: 18px;
         color: rgb(0, 255, 255);
     }
</style>
     <div class="site-login">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php

            echo Nav::widget([
                'options' => ['class' => 'nav nav-sidebar'],
                'items' => [

                ],
            ]);

            ?>

            <?php
            //фомируем список автокомплита
            $tags = Tag::find()
                ->select(['name as label'])
                ->asArray()
                ->all();
            $sources = core\entities\Rockncontroll\Source::find()
                ->select(['title as label'])
                ->asArray()
                ->all();
            $caters = core\entities\Rockncontroll\Categories::find()
                ->select(['name as label'])
                ->asArray()
                ->all();
            ?>

        </div>
        <div class="col-lg-10">
            <?php if ($model->audio_link) : ?>
            <audio controls="controls" >
                <source src="<?=(Yii::getAlias('@static')).$model->audio_link?>" >
            </audio>
            <?php
                $cmd = "id3v2 -l '/home/romanych/Музыка/Thoughts_and_klassik/new_ideas/".$model->audio_link."'";
                echo PHP_EOL.$cmd.PHP_EOL;
                echo shell_exec($cmd);
            ?>
            <?php endif; ?>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'title')->textInput()  ?>

            <?php echo $form->field($model, 'source_title')->widget(
                AutoComplete::className(), [
                'clientOptions' => [
                    'source' => $sources,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>
            <?php  //$form->field($model, 'cat_title')->textInput()  ?>
            <?php echo $form->field($model, 'cat_title')->widget(
                AutoComplete::className(), [
                'clientOptions' => [
                    'source' => $caters,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>

            <?= $form->field($model, 'text')->widget(Widget::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 300,
                    'pastePlainText' => true,
                    'paragraphize' => false,
                    //'buttons' => ['html', 'formatting', 'bold', 'italic'],
                    'buttonSource' => true,
                    'plugins' => [
                        'clips',
                        'fullscreen',
                        'table'
                    ],
                    //'imageManagerJson' => Url::to(['/articles/images-get']),
                    'imageUpload' => Url::to(['rockncontroll/item/image-upload']),

                    //'fileManagerJson' => Url::to(['/uploads/files-get']),
                    //'fileUpload' => Url::to(['/uploads/file-upload'])
                ]

            ]);?>
            <?=  $form->field($model, 'cens')->textInput()  ?>
            <?=  $form->field($model, 'published')->textInput()  ?>
            <?php  $form->field($model, 'tags')->textInput()  ?>
            <?php echo $form->field($model, 'tags')->widget(
                AutoComplete::className(), [
                'clientOptions' => [
                    'source' => $tags,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>
            <?= $form->field($model, 'audio_link')->textInput()  ?>
            <?= $form->field($model, 'in_work_prim')->textInput()  ?>

            <?= $form->field($uploadImg, 'img')->fileInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>
