<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $notion core\entities\Notion\Notion */
/* @var $photosForm core\forms\manage\Notion\PhotosForm */
/* @var $modificationsProvider yii\data\ActiveDataProvider */

$this->title = $notion->body;
$this->params['breadcrumbs'][] = ['label' => 'Notions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">

            <div class="box">
                <div class="box-header with-border">Common</div>
                <div class="box-body">

                    <?= DetailView::widget([
                        'model' => $notion,
                        'attributes' => [
                            'id',
                            'body',
                            'question',
                            'answer',
                            'description',
                            'category_id',
                        ],
                    ]) ?>
                    <br />

                </div>
            </div>
        </div>

    </div>

    <div class="box">
        <div class="box-header with-border">Description</div>
        <div class="box-body">
            <?= Yii::$app->formatter->asNtext($notion->description) ?>
        </div>
    </div>


    <div class="box">
        <div class="box-header with-border">SEO</div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $notion,
                'attributes' => [
                    [
                        'attribute' => 'meta.title',
                        'value' => $notion->meta->title,
                    ],
                    [
                        'attribute' => 'meta.description',
                        'value' => $notion->meta->description,
                    ],
                    [
                        'attribute' => 'meta.keywords',
                        'value' => $notion->meta->keywords,
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <div class="box" id="photos">
        <div class="box-header with-border">Photos</div>
        <div class="box-body">

            <div class="row">
                <?php if ($notion->photos) :?>
                <?php foreach ($notion->photos as $photo): ?>
                    <div class="col-md-2 col-xs-3" style="text-align: center">
                        <div class="btn-group">
                            <?= Html::a('<span class="glyphicon glyphicon-arrow-left"></span>', ['move-photo-up', 'id' => $notion->id, 'photo_id' => $photo->id], [
                                'class' => 'btn btn-default',
                                'data-method' => 'post',
                            ]); ?>
                            <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['delete-photo', 'id' => $notion->id, 'photo_id' => $photo->id], [
                                'class' => 'btn btn-default',
                                'data-method' => 'post',
                                'data-confirm' => 'Remove photo?',
                            ]); ?>
                            <?= Html::a('<span class="glyphicon glyphicon-arrow-right"></span>', ['move-photo-down', 'id' => $notion->id, 'photo_id' => $photo->id], [
                                'class' => 'btn btn-default',
                                'data-method' => 'post',
                            ]); ?>
                        </div>
                        <div>
                            <?= Html::a(
                                Html::img($photo->getThumbFileUrl('file', 'thumb')),
                                $photo->getUploadedFileUrl('file'),
                                ['class' => 'thumbnail', 'target' => '_blank']
                            ) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <?php $form = \yii\widgets\ActiveForm::begin([
                'options' => ['enctype'=>'multipart/form-data'],
            ]); ?>

            <?= $form->field($photosForm, 'files[]')->label(false)->widget(\kartik\file\FileInput::class, [
                'options' => [
                    'accept' => 'image/*',
                    'multiple' => true,
                ]
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Upload', ['class' => 'btn btn-success']) ?>
            </div>

            <?php \yii\widgets\ActiveForm::end(); ?>

        </div>
    </div>

</div>
