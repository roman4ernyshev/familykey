<?php
/* @var $this yii\web\View */
/* @var $notion core\entities\Notion\Notion */
/* @var $model core\forms\manage\Notion\NotionCreateForm */

$this->title = 'Update Notion: ' . $notion->body;
$this->params['breadcrumbs'][] = ['label' => 'Notions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $notion->body, 'url' => ['view', 'id' => $notion->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">

    <?php $form = \yii\widgets\ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-header with-border">Common</div>
        <div class="box-body">
            <div class="row">
                <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'body')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'answer')->textInput(['maxlength' => true]) ?>

                <?php /* $form->field($model, 'description')->textarea(['rows' => 20]) */?>
                <?= $form->field($model, 'description')->widget(\mihaildev\ckeditor\CKEditor::class,[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard,
                         // full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

            </div>

        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">Photos</div>
        <div class="box-body">
            <?= $form->field($model->photos, 'files[]')->widget(\kartik\widgets\FileInput::class, [
                'options' => [
                    'accept' => 'image/*',
                    'multiple' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">SEO</div>
        <div class="box-body">
            <?= $form->field($model->meta, 'title')->textInput() ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
            <?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php \yii\widgets\ActiveForm::end(); ?>

</div>