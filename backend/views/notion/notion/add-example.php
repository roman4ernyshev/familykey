<?php

use \yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model core\forms\manage\Notion\ExampleCreateForm */

$this->title = 'Create Example for "'.$model->notion->body.'"';
$this->params['breadcrumbs'][] = ['label' => 'Example', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]);
    ?>

    <div class="box box-default">
        <div class="box-header with-border">Example</div>
        <div class="box-body">

            <?= $form->field($model, 'body')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 20]) ?>


        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">Photos</div>
        <div class="box-body">
            <?= $form->field($model->photos, 'files[]')->widget(\kartik\widgets\FileInput::class, [
                'options' => [
                    'accept' => 'image/*',
                    'multiple' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">Audios</div>
        <div class="box-body">
            <?= $form->field($model->audios, 'files[]')->widget(\kartik\widgets\FileInput::class, [
                'options' => [
                    //'accept' => 'audio/*',
                    'multiple' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>