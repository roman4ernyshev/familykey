<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\entities\Notion\Notion;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\Notion\NotionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notions '.$category;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p>
        <?= Html::a('Create Notion', ['notion/'.$category.'/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                /*'rowOptions' => function (Product $model) {
                    return $model->quantity <= 0 ? ['style' => 'background: #fdc'] : [];
                },
                */
                'columns' => [
                        /*
                    [
                        'value' => function (Notion $model) {
                            return $model->mainPhoto ? Html::img($model->mainPhoto->getThumbFileUrl('file', 'admin')) : null;
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 100px'],
                    ],
                        */
                    //'id',
                    [
                        'attribute' => 'body',
                        'value' => function (Notion $model) {
                            return Html::a(Html::encode($model->body), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    /*[
                        'attribute' => 'category_id',
                        'filter' => $searchModel->categoriesList(),
                        'value' => 'category.name',
                    ],
                    */
                    [
                        'class' => \yii\grid\ActionColumn::class,
                    ],
                    /*[
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'add-example' => function (Notion $model) {
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['add-example', 'id' => $model->id]);

                            },

                        ],
                        'template' => '{add-example}',
                    ],
                    */
                    [
                        'value' => function(Notion $model)  {
                            return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['add-example', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align: center'],
                    ],

                ],
            ]); ?>
        </div>
    </div>
</div>