<?php

/* @var $this yii\web\View */
/* @var $category core\entities\Notion\Category */
/* @var $model core\forms\manage\Notion\CategoryForm */

$this->title = 'Update Categories: ' . $category->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $category->id, 'url' => ['view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="brand-update">

    <?= $this->render('_form', [
        'model' => $model
    ])?>

</div>
