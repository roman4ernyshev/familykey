<style xmlns="http://www.w3.org/1999/html">
    body{
        background-color: rgb(25, 32, 34);
    }
    td, #tournament, #form{
        text-align: center;
    }
    .container{
        background: rgba(0, 5, 212, 0.38);
        width: 100%;
    }
    #team_host, #team_guest{
        width: 40%;
        font-size: 40px;
    }
    #score_host, #score_guest{
        font-size: 80px;
    }
    .timer{

        font-size: 50px;
    }
    #score_host, #score_guest, .timer{
        cursor: pointer;
    }
    #tablo{
        margin-top: 100px;
        border-radius: 10px;
    }
    #form{
        margin-top: 10px;
        border-radius: 10px;
    }
    select[multiple], select[size] {

        width: 100%;
        background-color: rgb(210, 105, 30);
    }

    @media (max-width: 992px){
        #team_host, #team_guest {
            font-size: 20px;
        }
    }

    @media (max-width: 768px) {

        #team_host, #team_guest {
            font-size: 13px;
        }
    }


</style>

<?php
/**
 * @var $par array
 */
/*
$keys = [
        'remain_for_rul',
        'remain_to_play',
        8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21];
*/
?>


    <div class='container' id="form">
        <form class="form-inline center" role="form" id="form-ate">
            <div class="form-group">
                <h3>Расписание дня!</h3>
                <div>
                    <span style="color:#ffc82e;"> ( if b>10 Kn=6+((n-10)/10) if b<0 Kn=1 )</span>
                    <input type="text" class="form-control" id="remain_for_rul"
                           placeholder="Стадион" value="<?=$par['remain_for_rul']?>"><br>
                    <input type="text" class="form-control" id="remain_to_play"
                           placeholder="Осталось до игры" value="<?=$par['remain_to_play']?>"><br>
                    <input type="text" class="form-control" id="hour_7"  placeholder="7" value="<?=$par['hour_7']?>">
                    <select size="1" multiple name="hero[]" id="hour_7_sel" >
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>

                    <input type="text" class="form-control" id="hour_8"  placeholder="8"  value="<?=$par['hour_8']?>" >
                    <select size="1" multiple name="hero[]" id="hour_8_sel">
                        <option value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option selected="selected"  value="school">school</option>
                    </select>
                    <br>

                    <input type="text" class="form-control" id="hour_9"  placeholder="9" value="<?=$par['hour_9']?>">
                    <select size="1" multiple name="hero[]" id="hour_9_sel">
                        <option value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option selected="selected" value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_10"  placeholder="10" value="<?=$par['hour_10']?>">
                    <select size="1" multiple name="hero[]" id="hour_10_sel">
                        <option value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option selected="selected" value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_11"  placeholder="11" value="<?=$par['hour_11']?>">
                    <select size="1" multiple name="hero[]" id="hour_11_sel">
                        <option value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option selected="selected" value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_12"  placeholder="12" value="<?=$par['hour_12']?>">
                    <select size="1" multiple name="hero[]" id="hour_12_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_13"  placeholder="13" value="<?=$par['hour_13']?>" >
                    <select size="1" multiple name="hero[]" id="hour_13_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_14"  placeholder="14" value="<?=$par['hour_14']?>">
                    <select size="1" multiple name="hero[]" id="hour_14_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_15"  placeholder="15" value="<?=$par['hour_15']?>">
                    <select size="1" multiple name="hero[]" id="hour_15_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_16"  placeholder="16" value="<?=$par['hour_16']?>">
                    <select size="1" multiple name="hero[]" id="hour_16_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_17"  placeholder="17" value="<?=$par['hour_17']?>">
                    <select size="1" multiple name="hero[]" id="hour_17_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_18"  placeholder="18" value="<?=$par['hour_18']?>">
                    <select size="1" multiple name="hero[]" id="hour_18_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_19"  placeholder="19" value="<?=$par['hour_19']?>">
                    <select size="1" multiple name="hero[]" id="hour_19_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_20"  placeholder="20" value="<?=$par['hour_20']?>">
                    <select size="1" multiple name="hero[]" id="hour_20_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>
                    <input type="text" class="form-control" id="hour_21"  placeholder="21" value="<?=$par['hour_21']?>">
                    <select size="1" multiple name="hero[]" id="hour_21_sel">
                        <option selected="selected" value="free">free</option>
                        <option value="football">football</option>
                        <option value="music">music</option>
                        <option value="school">school</option>
                    </select>
                    <br>

                    <?php echo \yii\helpers\Html::button('Готово',
                        [
                            'class' => 'btn btn-primary', 'onclick' => '(
                                 function ( $event ) { sendParams(); })();'
                        ]);
                    ?>

                </div>
            </div>
        </form>
    </div>

    <div id="finish"></div>
    <?php //<audio style="display: none" src="verdy_march_aida.mp3" id="audio"></audio> ?>

</div>
<script>

    var state;

    function sendParams() {

        state = {
            remain_for_rul: $('#remain_for_rul').val(),
            remain_to_play: $('#remain_to_play').val(),
            hour_7: $('#hour_7').val(),
            hour_8: $('#hour_8').val(),
            hour_9: $('#hour_9').val(),
            hour_10: $('#hour_10').val(),
            hour_11: $('#hour_11').val(),
            hour_12: $('#hour_12').val(),
            hour_13: $('#hour_13').val(),
            hour_14: $('#hour_14').val(),
            hour_15: $('#hour_15').val(),
            hour_16: $('#hour_16').val(),
            hour_17: $('#hour_17').val(),
            hour_18: $('#hour_18').val(),
            hour_19: $('#hour_19').val(),
            hour_20: $('#hour_20').val(),
            hour_21: $('#hour_21').val(),
            hour_7_sel: $('#hour_7_sel').val(),
            hour_8_sel: $('#hour_8_sel').val(),
            hour_9_sel: $('#hour_9_sel').val(),
            hour_10_sel: $('#hour_10_sel').val(),
            hour_11_sel: $('#hour_11_sel').val(),
            hour_12_sel: $('#hour_12_sel').val(),
            hour_13_sel: $('#hour_13_sel').val(),
            hour_14_sel: $('#hour_14_sel').val(),
            hour_15_sel: $('#hour_15_sel').val(),
            hour_16_sel: $('#hour_16_sel').val(),
            hour_17_sel: $('#hour_17_sel').val(),
            hour_18_sel: $('#hour_18_sel').val(),
            hour_19_sel: $('#hour_19_sel').val(),
            hour_20_sel: $('#hour_20_sel').val(),
            hour_21_sel: $('#hour_21_sel').val(),

            /*
            9: document.getElementById('9').innerText,
            10: document.getElementById('10').innerText,
            11: document.getElementById('11').innerText,
            12: document.getElementById('12').innerText,
            13: document.getElementById('13').innerText,
            14: document.getElementById('14').innerText,
            15: document.getElementById('15').innerText,
            16: document.getElementById('16').innerText,
            17: document.getElementById('17').innerText,
            18: document.getElementById('18').innerText,
            19: document.getElementById('19').innerText,
            20: document.getElementById('20').innerText,
            21: document.getElementById('21').innerText,
            */

        };

        console.log(state);



        $.ajax({
            type: "POST",
            url: "scheme/set-params",
            data:'remain_for_rul=' + state.remain_for_rul +
                '&remain_to_play=' + state.remain_to_play +
                '&hour_7=' + state.hour_7 +
                '&hour_8=' + state.hour_8 +
                '&hour_9=' + state.hour_9 +
                '&hour_10=' + state.hour_10 +
                '&hour_11=' + state.hour_11 +
                '&hour_12=' + state.hour_12 +
                '&hour_13=' + state.hour_13 +
                '&hour_14=' + state.hour_14 +
                '&hour_15=' + state.hour_15 +
                '&hour_16=' + state.hour_16 +
                '&hour_17=' + state.hour_17 +
                '&hour_18=' + state.hour_18 +
                '&hour_19=' + state.hour_19 +
                '&hour_20=' + state.hour_20 +
                '&hour_21=' + state.hour_21 +
                '&hour_7_sel=' + state.hour_7_sel +
                '&hour_8_sel=' + state.hour_8_sel +
                '&hour_9_sel=' + state.hour_9_sel +
                '&hour_10_sel=' + state.hour_10_sel +
                '&hour_11_sel=' + state.hour_11_sel +
                '&hour_12_sel=' + state.hour_12_sel +
                '&hour_13_sel=' + state.hour_13_sel +
                '&hour_14_sel=' + state.hour_14_sel +
                '&hour_15_sel=' + state.hour_15_sel +
                '&hour_16_sel=' + state.hour_16_sel +
                '&hour_17_sel=' + state.hour_17_sel +
                '&hour_18_sel=' + state.hour_18_sel +
                '&hour_19_sel=' + state.hour_19_sel +
                '&hour_20_sel=' + state.hour_20_sel +
                '&hour_21_sel=' + state.hour_21_sel
            ,
           /*
            success: function(html){
                finish.innerHTML = html;
            }
            */


        });

    }

</script>
