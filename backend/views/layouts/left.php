<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Управление', 'options' => ['class' => 'header']],
                    ['label' => 'Notion', 'icon' => 'folder',
                        'items' => [
                            ['label' => 'Categories', 'icon' => 'file-o', 'url' => ['/notion/category/index'],
                                'active' => $this->context->id == 'notion/category'],
                            ['label' => 'Language', 'icon' => 'folder', 'url' => ['notion/language'],
                                'active' => $this->context->id == 'notion/language/index',
                                'items' => [
                                    [
                                        'label' => 'Deutsch',
                                        'icon' => 'file-o',
                                        'url' => ['notion/deutsch'],
                                        'active' => $this->context->id == '/notion/deutsch/index'
                                    ],
                                    [
                                        'label' => 'English',
                                        'icon' => 'file-o',
                                        'url' => ['notion/english'],
                                        'active' => $this->context->id == '/notion/english/index'
                                    ],
                            ]],
                            ['label' => 'Fundamental', 'icon' => 'file-o', 'url' => ['/notion/fundamental/index'],
                                'active' => $this->context->id == '/notion/fundamental/index'],

                        ],

                    ],
                    [
                        'label' => 'RocknControll',
                        'icon' => 'user',
                        'items' => [
                            ['label' => 'Article', 'icon' => 'file-o', 'url' => ['/rockncontroll/articles/index'],
                                'active' => $this->context->id == '/rockncontroll/articles/'],
                            ['label' => 'Items', 'icon' => 'file-o', 'url' => ['/rockncontroll/item/index'],
                                'active' => $this->context->id == '/rockncontroll/item/'],
                            ['label' => 'Pogoda', 'icon' => 'file-o', 'url' => ['/rockncontroll/pogxxi/index'],
                                'active' => $this->context->id == '/rockncontroll/pogxxi/'],
                            ['label' => 'SongText', 'icon' => 'file-o', 'url' => ['/rockncontroll/songtext/index'],
                                'active' => $this->context->id == '/rockncontroll/songtext/'],
                            ['label' => 'Author', 'icon' => 'file-o', 'url' => ['/rockncontroll/author/index'],
                                'active' => $this->context->id == '/rockncontroll/songtext/'],
                            ['label' => 'Source', 'icon' => 'file-o', 'url' => ['/rockncontroll/source/index'],
                                'active' => $this->context->id == '/rockncontroll/songtext/'],
                            ['label' => 'Event', 'icon' => 'file-o', 'url' => ['/rockncontroll/event/index'],
                                'active' => $this->context->id == '/rockncontroll/event/'],
                            ['label' => 'Deutsch', 'icon' => 'file-o', 'url' => ['/rockncontroll/deutsch/index'],
                                'active' => $this->context->id == '/rockncontroll/deutsch/'],
                        ]
                        /*
                        'url' => ['/rockncontroll/articles/index'],
                        'active' => $this->context->id == 'user',
                        */
                    ],
                    [
                        'label' => 'Пользователи',
                        'icon' => 'user',
                        'url' => ['/user/index'],
                        'active' => $this->context->id == 'user',
                    ],
                    [
                        'label' => 'Слова',
                        'icon' => 'word',
                        'url' => ['/word/index'],
                        'active' => $this->context->id == 'word',
                    ],
                    [
                        'label' => 'Soccer',
                        'icon' => 'file-o',
                        'url' => ['/soccer/index'],

                        'active' => $this->context->id == 'soccer',
                    ],
                    [
                        'label' => 'Schema',
                        'icon' => 'ball',
                        'url' => ['/scheme/index'],
                        'active' => $this->context->id == 'project',
                    ],
                    [
                        'label' => 'Project',
                        'icon' => 'ball',
                        'url' => ['/project/index'],
                        'active' => $this->context->id == 'project',
                    ],
                    [
                        'label' => 'Product',
                        'icon' => 'file-o',
                        'url' => ['/product/index'],
                        'active' => $this->context->id == 'project',
                    ],
                    [
                            'label' => 'Product', 'icon' => 'folder', 'items' => [
                                ['label' => 'Categories', 'icon' => 'file-o', 'url' => ['/product/category/index'],
                                    'active' => $this->context->id == 'product/category'],
                                ['label' => 'Product', 'icon' => 'folder', 'url' => ['product/product'],
                                    'active' => $this->context->id == 'product/product/index',
                                ],
                            ]

                    ],
                    [
                        'label' => 'Visit',  'icon' => 'folder',
                        'items' => [
                            ['label' => 'Visitor', 'icon' => 'file-o', 'url' => ['/visit/index'],
                                'active' => $this->context->id == 'visit'],
                            ['label' => 'Visit Block', 'icon' => 'file-o', 'url' => ['/visit/block'],
                                'active' => $this->context->id == 'block'],
                            ['label' => 'Visit Radio', 'icon' => 'file-o', 'url' => ['/visit/radio'],
                                'active' => $this->context->id == 'radio'],
                        ]
                    ],
                ],

            ]
        ) ?>

    </section>

</aside>
