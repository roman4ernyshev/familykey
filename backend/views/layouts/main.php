<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


backend\assets\AppAsset::register($this);

dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
    <style>
        .box {
            background: rgb(60, 63, 65);
            color: darkgrey;
        }
        .content-wrapper {
            background-color: rgb(49, 59, 71);
        }
        .form-control {
            background-color: rgb(0, 0, 0);
            color: rgb(255, 255, 255);
        }
        .table-striped>tbody>tr:nth-child(odd)>td,
        .table-striped>tbody>tr:nth-child(odd)>th {
            background-color: #646b7c; // Choose your own color here
        }
        a {
            color: rgb(255, 235, 59);
        }
        .main-footer {
            background: rgb(60, 141, 188);
        }

        .skin-blue .main-header .logo {
            background-color: rgb(84, 84, 84);
            color: rgb(163, 170, 165);
            border-bottom: 0 solid rgb(0, 0, 0);
        }
        .skin-blue .main-header .navbar {
            background-color: rgb(8, 8, 8);
        }

        /*gray scroll*/
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px #080808;
            border-radius: 10px;
        }


        body {
            font-family: "Open Sans", sans-serif;
            line-height: 1.25;
        }

        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }

        table caption {
            font-size: 1.5em;
            margin: .5em 0 .75em;
        }

        table tr {
            background-color: #363b3f;
            color: white;
            border: 1px solid #ddd;
            padding: .35em;
        }

        table th,
        table td {
            padding: .625em;
            text-align: left;
            overflow: hidden;
        }

        table th {
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
        }

        thead tr th:first-child,
        tbody tr td:first-child {
            width: 4em;
            min-width: 4em;
            max-width: 4em;
            word-break: break-all;
        }

        thead tr th:last-child,
        tbody tr td:last-child {
            width: 8em;
            min-width: 8em;
            max-width: 8em;
            word-break: break-all;
        }

        /**
        @media screen and (max-width: 600px) {
            table {
                border: 0;
            }

            table caption {
                font-size: 1.3em;
            }

            table thead {
                border: none;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }

            table tr {
                border-bottom: 3px solid #ddd;
                display: block;
                margin-bottom: .625em;
            }

            table td {
                border-bottom: 1px solid #ddd;
                display: block;
                font-size: .8em;
                text-align: right;
            }

            table td::before {
                /*
                * aria-label has no advantage, it won't be read inside a table
                content: attr(aria-label);

                content: attr(data-label);
                float: left;
                font-weight: bold;
                text-transform: uppercase;
            }

            table td:last-child {
                border-bottom: 0;
            }

            thead tr th:first-child,
            tbody tr td:first-child {
                width: inherit;
                min-width: inherit;
                max-width: inherit;
                word-break: normal;
            }

            thead tr th:last-child,
            tbody tr td:last-child {
                width: inherit;
                min-width: inherit;
                max-width: inherit;
                word-break: normal;
            }
            **/
        }
    </style>

    <?= $this->render(
        'header.php',
        ['directoryAsset' => $directoryAsset]
    ) ?>

    <?= $this->render(
        'left.php',
        ['directoryAsset' => $directoryAsset]
    )
    ?>

    <?= $this->render(
        'content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>

</div>

<?php $this->endBody() ?>
</body>
</html>
<script>
    <?php /*
    CKEDITOR.on('instanceReady', function(e) {
        // First time
        e.editor.document.getBody().setStyle('background-color', 'black').setStyle('color', 'white');
        // in case the user switches to source and back
        e.editor.on('contentDom', function() {
            e.editor.document.getBody().setStyle('background-color', 'black').setStyle('color', 'white').setStyle('width', '100%');
        });

    });
 */ ?>
</script>

<?php $this->endPage() ?>

