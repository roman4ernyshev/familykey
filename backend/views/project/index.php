<?php

use yii\helpers\Html;
use yii\grid\GridView;
use core\entities\Notion\Notion;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\Project\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p>
        <?= Html::a('Create Project', ['project/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                /*'rowOptions' => function (Product $model) {
                    return $model->quantity <= 0 ? ['style' => 'background: #fdc'] : [];
                },
                */
                'columns' => [
                        /*
                    [
                        'value' => function (Notion $model) {
                            return $model->mainPhoto ? Html::img($model->mainPhoto->getThumbFileUrl('file', 'admin')) : null;
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 100px'],
                    ],
                        */
                    //'id',
                    [
                        'attribute' => 'title',
                        'value' => function (\core\entities\Project\Project $model) {
                            return Html::a(Html::encode($model->title), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    /*[
                        'attribute' => 'category_id',
                        'filter' => $searchModel->categoriesList(),
                        'value' => 'category.name',
                    ],
                    */
                    [
                        'class' => \yii\grid\ActionColumn::class,
                    ],
                    /*[
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'add-example' => function (Notion $model) {
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['add-example', 'id' => $model->id]);

                            },

                        ],
                        'template' => '{add-example}',
                    ],
                    */
                ],
            ]); ?>
        </div>
    </div>
</div>