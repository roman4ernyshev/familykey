<?php
/* @var $this yii\web\View */
/* @var $project core\entities\Project\Project */
/* @var $model core\forms\manage\Project\ProjectCreateForm */

$this->title = 'Update Project: ' . $project->title;
$this->params['breadcrumbs'][] = ['label' => 'Notions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $project->title, 'url' => ['view', 'id' => $project->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">

    <?php $form = \yii\widgets\ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-header with-border">Project</div>
        <div class="box-body">
            <div class="row">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 20]) ?>
            </div>

        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">SEO</div>
        <div class="box-body">
            <?= $form->field($model->meta, 'title')->textInput() ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
            <?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php \yii\widgets\ActiveForm::end(); ?>

</div>