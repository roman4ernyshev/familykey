<?php
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\Visit\VisitorBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Visitor Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //'id',
                    [
                        'attribute' => 'time',
                        'value' => function (\core\entities\Visit\VisitBlock $model) {
                            return date('Y-m-d H:i:s', $model->time);
                        },
                        'format' => 'raw',
                    ],
                    'ip',
                    'loc',
                    'org',
                    'country',
                    [
                        'attribute' => 'hostname',
                        'value' => function (\core\entities\Visit\VisitBlock $model) {
                            return \yii\helpers\StringHelper::truncate($model->hostname, 100);
                        }
                    ],
                    [
                        'attribute' => 'visitor_id',
                        //'filter' => $searchModel->categoriesList(),
                        'value' => 'visitor.count',

                    ],
                   // 'site',

                    //['class' => ActionColumn::class],
                ],
            ]); ?>
        </div>
    </div>
</div>