<?php
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\Visit\VisitorRadioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Visitor Radio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //'id',
                    [
                        'attribute' => 'time',
                        'value' => function (\core\entities\Listener\Listener $model) {
                            return date('Y-m-d H:i:s', $model->time);
                        },
                        'format' => 'raw',
                    ],
                    'ip',
                    [
                        'attribute' => 'user_agent',
                        'value' => function (\core\entities\Listener\Listener $model) {
                            return \yii\helpers\StringHelper::truncate($model->user_agent, 100);
                        }
                    ],
                    'current_track',
                    'seconds'

                    //['class' => ActionColumn::class],
                ],
            ]); ?>
        </div>
    </div>
</div>