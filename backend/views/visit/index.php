<?php

use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\Visit\VisitorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Visitors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //'id',
                    [
                        'attribute' => 'time',
                        'value' => function (\core\entities\Visit\Visitor $model) {
                            return date('Y-m-d H:i:s', $model->time);
                        },
                        'format' => 'raw',
                    ],
                    'block',
                    [
                        'attribute' => 'user_agent',
                        'value' => function (\core\entities\Visit\Visitor $model) {
                            return \yii\helpers\StringHelper::truncate($model->user_agent, 100);
                        }
                    ],
                    [
                        'attribute' => 'visitor_id',
                        //'filter' => $searchModel->categoriesList(),
                        'value' => 'visitor.count',

                    ],
                    'visitorblock.ip',
                    'visitorblock.hostname',
                    'visitorblock.city',
                    'visitorblock.region',
                    'visitorblock.loc',
                    'site',

                    //['class' => ActionColumn::class],
                ],
            ]); ?>
        </div>
    </div>
</div>