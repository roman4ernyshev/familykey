<?php

namespace backend\forms\Visit;

use core\entities\Listener\Listener;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class VisitorRadioSearch extends Model
{
    public $id;
    public $time;
    public $ip;
    public $user_agent;
    public $current_track;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
        ];
    }

    public function search(array $params): ActiveDataProvider
    {

        $query = Listener::find()->where('ip NOT LIKE "37.192.187.83"');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        //  $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }


}