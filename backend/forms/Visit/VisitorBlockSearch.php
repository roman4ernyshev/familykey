<?php

namespace backend\forms\Visit;

use core\entities\Visit\VisitBlock;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class VisitorBlockSearch extends Model
{
    public $id;
    public $time;
    public $site;
    public $block;
    public $city;
    public $ip;
    public $hostname;
    public $visitor_id;

    public function rules(): array
    {
        return [
            [['id', 'visitor_id'], 'integer'],
        ];
    }

    public function search(array $params): ActiveDataProvider
    {
        $query = VisitBlock::find()->where('ip NOT LIKE "37.192.187.83"');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'visitor_id' => $this->visitor_id,
        ]);

        //  $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }


}