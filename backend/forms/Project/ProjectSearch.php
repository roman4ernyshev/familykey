<?php
namespace backend\forms\Project;


use core\entities\Project\Project;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ProjectSearch extends Model
{
    public $id;
    public $title;
    public $question;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['description', 'title'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Project::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }


}