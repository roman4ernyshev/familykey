<?php

namespace backend\forms\Notion;


use core\entities\Notion\Category;
use core\entities\Notion\Notion;
use core\helpers\NotionHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class NotionSearch extends Model
{
    public $id;
    public $body;
    public $question;
    public $category_id;

    public function rules(): array
    {
        return [
            [['id', 'category_id'], 'integer'],
            [['question', 'body'], 'safe'],
        ];
    }

    /**
     * @param string $category
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($category, array $params): ActiveDataProvider
    {
        $category_id = Category::find()->andWhere(['LIKE', 'name', $category])->one() ?
            Category::find()->andWhere(['LIKE', 'name', $category])->one()->id : 0;

        //var_dump($category_id); exit;

        $query = Notion::find()->andWhere(['category_id' => $category_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            /*
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
            */
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }

    public function categoriesList(): array
    {
        return ArrayHelper::map(Category::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->asArray()->all(), 'id', function (array $category) {
            return ($category['depth'] > 1 ? str_repeat('-- ', $category['depth'] - 1) . ' ' : '') . $category['name'];
        });
    }

    public function statusList(): array
    {
       // return NotionHelper::statusList();
    }


}

