<?php
namespace backend\forms\Soccer;

use core\entities\Soccer\Tournament;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class TournamentSearch extends Model
{
    public $id;
    public $name;
    public $hash;
    public $country_id;

    public function rules(): array
    {
        return [
            [['id', 'country_id'], 'integer'],
            [['description'], 'safe'],
        ];
    }

    public function search(array $params): ActiveDataProvider
    {

        $query = Tournament::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }


}