<?php
namespace backend\forms\Product;

use core\entities\Product\ProductsConst;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ProductSearch extends Model
{
    public $id;
    public $name;


    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['description'], 'safe'],
        ];
    }

    public function search(array $params): ActiveDataProvider
    {

        $query = ProductsConst::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }


}