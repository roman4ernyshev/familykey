<?php

namespace frontend\controllers;

use core\entities\Schema\DaySchema;
use core\entities\Schema\storage\RedisStorage;
use yii\web\Controller;

class SchemeController extends Controller
{
    public $layout = 'family_key';

    /*public function actionIndex()
    {
       return $this->render('index');
    }
    */

    public function actionIndex()
    {
        $tablo = new DaySchema(new RedisStorage());

        return $this->render('index', ['par' => $tablo->getParams()]);

    }

    public function actionWeek()
    {

        return $this->render('week');

    }


}