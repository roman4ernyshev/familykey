<?php

namespace frontend\controllers;

use core\entities\Article\Article;
use core\entities\Rockncontroll\DiaryActs;
use core\entities\Rockncontroll\DiaryRecDayParams;
use core\entities\Rockncontroll\Event;
use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\ManyNews;
use core\entities\Rockncontroll\Matches;
use core\entities\Rockncontroll\NsbNews;
use core\entities\Rockncontroll\PogodaXXI;
use core\entities\Rockncontroll\Weathernew;
use core\entities\RockncontrollNew\ManyNewss;
use core\entities\RockncontrollNew\NsbNewss;
use core\readModels\RadioReadRepository;
use core\readModels\Soccer\MatchReadRepository;
use core\readModels\Soccer\TournamentReadRepository;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

class CalendurController extends Controller
{
    public $layout = 'family_key';
    public $matches;
    public $tournaments;

    private $items;
    private $time_from;
    private $time_to;
    private $date;
    private $year;
    private $years_ago;
    private $cur_data_timestamp;
    private $cur_data_timestamp_tomorrow;
    private $today;
    private $tomonth;
    private $curday_acts = [];
    private $today_event_acts;

    const WEIGHT = 1;
    const ITEM=7;
    const DAY_PARAM_TEMP_MON_ID = 5;
    const DAY_PARAM_TEMP_DAY_ID = 6;
    const DAY_PARAM_TEMP_EV_ID = 7;
    const DAY_PARAM_PRESS_MON_ID = 11;
    const DAY_PARAM_PRESS_DAY_ID = 12;
    const DAY_PARAM_PRESS_EV_ID = 13;


    public function __construct(
        $id,
        $module,
        RadioReadRepository $radio_items,
        MatchReadRepository $matches, TournamentReadRepository $tournaments,
        $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->items = $radio_items;
        /*
         * $beginOfDay = strtotime("today", $timestamp);
           $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;
         */
        $this->time_from = strtotime("today", time());
        $this->today = date('d', $this->time_from);
        $this->tomonth = date('m', $this->time_from);
        $this->time_to = strtotime("tomorrow", time());
        $this->matches = $matches;
        $this->tournaments = $tournaments;
        if (Yii::$app->getRequest()->getQueryParam('year')) {
            //echo (int)date('Y', time()); exit;
            $this->years_ago = (int)date('Y', time()) - (int)Yii::$app->getRequest()->getQueryParam('year');
            $this->year = (int)Yii::$app->getRequest()->getQueryParam('year');
            //$this->date = date($this->year . '-m-d', $this->time_from - (60 * 60 * 24 * $this->years_ago * 365));
            $this->date = $this->year .'-'. $this->tomonth .'-'. $this->today;
            $this->cur_data_timestamp = mktime(0,0,0,$this->tomonth,$this->today,$this->year);
            $this->cur_data_timestamp_tomorrow = $this->cur_data_timestamp + 60*60*24;

            $from = $this->cur_data_timestamp-(7*60*60);

            $this->curday_acts = DiaryActs::find()
                ->where("time > $from  and time < $this->cur_data_timestamp_tomorrow
                                          and user_id = 8")->all();
            //var_dump($this->curday_acts); exit;
            $this->today_event_acts = implode(',', ArrayHelper::map($this->curday_acts, 'id', 'id'));
        }
        else $this->cur_data_timestamp = time();

    }

    public function actionIndex()
    {
        return $this->render('years');
    }

    public function actionGetYear()
    {
        if ($this->year) {
            /*
            $years_ago = 2019-(int)Yii::$app->getRequest()->getQueryParam('year');
            $year = (int)Yii::$app->getRequest()->getQueryParam('year');

            $date = date($year.'-m-d', $this->time_from - (60 * 60 * 24 * $years_ago*365));
            //return $date;
            */

            $old_events = Event::find()
                ->where("old_data like '".$this->date."'")
                ->orderBy('id ASC')
                ->all();

            //return var_dump($old_events->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            if($old_events){
                return $this->renderPartial('events', ['events' => $old_events]);
            }

            $time_from = $this->time_from - (60 * 60 * 24 * $this->years_ago*365)-(60*60*24);
            $time_to = $this->time_to - (60 * 60 * 24 * $this->years_ago*365)-(60*60*24);

            $today_event_acts = implode(',',
                ArrayHelper::map(
                    DiaryActs::find()->where("time > $time_from and time < $time_to
                                          and user_id = 8 and model_id = 10")
                        ->all(), 'id', 'id'));

            $today_event = [];

            if ($today_event_acts) {
                $today_event = Event::find()
                    ->where("act_id  IN (" . $today_event_acts . ")")
                    ->orderBy('id ASC')
                    ->andWhere(["old_data" => 0])
                    ->all();
            }

            //$query_items_ids = $this->items->getAllItems();

            return $this->renderPartial('events',
                [
                    'events' => $today_event,
                ]
            );
        }
    }

    function actionHead()
    {
        $article = Article::find()
            //->where('title like "%'.date('Ymd',$this->cur_data_timestamp).'%"')
            ->where("title like '%".date('Ymd',$this->cur_data_timestamp)."%'")
            ->one();
        $pogot = PogodaXXI::find()
            ->where("year = $this->year and date = $this->today and month = $this->tomonth")
            ->one();
        //return var_dump($pogot);
        if($article){
            $head = explode(' ', $article->title);
            array_shift($head);
            $header = implode(' ', $head);
            return $header;
        }
        elseif ($pogot)
            return $pogot->title;
        else
            return '';

    }

    function actionGetCurData()
    {
        if ($this->year) {
            setlocale (LC_TIME, 'ru_RU.UTF-8', 'Rus');
            return $this->date.'-'.
                strftime('%B', $this->cur_data_timestamp).'-'.strftime('%A', $this->cur_data_timestamp).'-'.date('j',$this->cur_data_timestamp);
        }
    }

    function actionGetOz()
    {
        if ($this->year) {

            $time_from = $this->time_from - (60 * 60 * 24 * $this->years_ago*365);
            $time_to = $this->time_to - (60 * 60 * 24 * $this->years_ago*365);
            $acts =
                    DiaryActs::find()->where("time > $time_from and time < $time_to
                                          and user_id = 8")
                        ->all();
            $sum_oz = 0;
            if($acts){
                foreach ($acts as $act){
                    $sum_oz += $act->mark;
                }

            }

            return 'oz '.$sum_oz;

        }

        return 'oz no';

    }

    public function actionNews()
    {
        if ($this->year) {

            $date = date('d M '.$this->year, $this->time_from - (60 * 60 * 24 * $this->years_ago*365)-(60*60*24));
            $news1 = NsbNews::find()
                ->where('pud_date like "%'.$date.'%"')
                ->orderBy(new Expression('rand()'))
                ->limit(6)
                ->all();
            $news2 = NsbNewss::find()
                ->where('pud_date like "%'.$date.'%"')
                ->orderBy(new Expression('rand()'))
                ->limit(6)
                ->all();
            $news3 = ManyNews::find()
                ->where('pud_date like "%'.$date.'%"')
                ->orderBy(new Expression('rand()'))
                ->limit(6)
                ->all();
            $news4 = ManyNewss::find()
                ->where('pud_date like "%'.$date.'%"')
                ->orderBy(new Expression('rand()'))
                ->limit(6)
                ->all();
            $news = array_merge($news1, $news2, $news3, $news4);
            //return count($news);
            if(!empty($news)){
                shuffle($news);
                $output = array_slice($news, 0, 12);
                return $this->renderPartial('news', ['news' => $output]);
            }
            return 'Новостей года нет';


        }
        return 'Новостей нет';

    }

    public function actionWeather()
    {
        if ($this->year) {
            $pogot = PogodaXXI::find()
                ->where("year = $this->year and date = $this->today and month = $this->tomonth")
                ->one();

            if($this->curday_acts){

                $day_param = [];

                $day_param['temp_mon'] = round(DiaryRecDayParams::find()
                    ->where("act_id  IN (" . $this->today_event_acts . ")")
                    ->andWhere("day_param_id=".self::DAY_PARAM_TEMP_MON_ID)
                    ->one()->value);
                $day_param['temp_day'] = round(DiaryRecDayParams::find()
                    ->where("act_id  IN (" . $this->today_event_acts . ")")
                    ->andWhere("day_param_id=".self::DAY_PARAM_TEMP_DAY_ID)
                    ->one()->value);
                $day_param['temp_ev'] = round(DiaryRecDayParams::find()
                    ->where("act_id  IN (" . $this->today_event_acts . ")")
                    ->andWhere("day_param_id=".self::DAY_PARAM_TEMP_EV_ID)
                    ->one()->value);
                $day_param['press_mon'] = round(DiaryRecDayParams::find()
                    ->where("act_id  IN (" . $this->today_event_acts . ")")
                    ->andWhere("day_param_id=".self::DAY_PARAM_PRESS_MON_ID)
                    ->one()->value);
                $day_param['press_day'] = round(DiaryRecDayParams::find()
                    ->where("act_id  IN (" . $this->today_event_acts . ")")
                    ->andWhere("day_param_id=".self::DAY_PARAM_PRESS_DAY_ID)
                    ->one()->value);
                $day_param['press_ev'] = round(DiaryRecDayParams::find()
                    ->where("act_id  IN (" . $this->today_event_acts . ")")
                    ->andWhere("day_param_id=".self::DAY_PARAM_PRESS_EV_ID)
                    ->one()->value);

                $weather = Weathernew::find()
                    ->where("time > $this->cur_data_timestamp and time < $this->cur_data_timestamp_tomorrow")
                    ->andWhere(['city_id' => 22890])
                    ->all();
                return $this->renderPartial('weather', [
                    'weather' => $weather, 'pogot' => $pogot, 'day_params' => $day_param ]);
            }

            return 'Данных о погоде в этом году нет';


        }
        return 'Данных о погоде нет';

    }

    function actionWeight()
    {
        if ($this->year) {

            if ($this->curday_acts) {

                $weight = DiaryRecDayParams::find()
                    ->where("act_id  IN (" . $this->today_event_acts . ")")
                    ->andWhere("day_param_id=" . self::WEIGHT)
                    ->one();

                if($weight) return 'weight '.round($weight->value, 1);

                return 'weight no';
            }

            return 'weight no';
        }
        return 'weight no year';
    }

    function actionItems()
    {
        if ($this->year) {
            $items = Items::find()
                ->where(['old_data'=>$this->date])
                ->orWhere("text like '%$this->date%'")
                ->orderBy(new Expression('rand()'))
                ->limit(6)
                ->all();
            if ($this->curday_acts) {

                if(!$items){
                    $items = Items::find()
                        ->where("act_id  IN (" . $this->today_event_acts . ")")
                        ->andWhere(['old_data'=>0])
                        ->orderBy(new Expression('rand()'))
                        ->limit(6)
                        ->all();
                }
            }
            if($items) return $this->renderPartial('items', ['items' => $items]);
            return 'no items';
        }
        return 'items no year';
    }

    function actionMatches()
    {
        $data = $this->today.'.'.$this->tomonth.'.'.$this->year;
        $matches = $this->matches->getMatchesTenRandomByData($data);
        return $this->renderPartial('//matches/tournament/index', [
            'matches' => $matches,
            'tournament_name' => $data,
            'tournament_id' => 0,
            'team_name' => null,
            'summary' => null
        ]);

    }

}