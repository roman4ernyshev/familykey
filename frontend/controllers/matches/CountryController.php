<?php

namespace frontend\controllers\matches;

use core\readModels\CountryReadRepository;
use core\readModels\Soccer\TournamentReadRepository;
use yii\web\Controller;

class CountryController extends Controller
{
    public $layout = 'family_key';

    public $countries;
    public $tournaments;

    public function __construct($id, $module, CountryReadRepository $countries, TournamentReadRepository $tournaments, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->countries = $countries;
        $this->tournaments = $tournaments;
    }

    public function actionIndex($id)
    {
        $tournaments = $this->tournaments->getTournamentsByCountryId($id);

        return $this->render('index', ['tournaments' => $tournaments]);
    }

}