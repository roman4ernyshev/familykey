<?php
namespace frontend\controllers\matches;

use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\RadioItem;
use core\readModels\CountryReadRepository;
use core\readModels\Soccer\TournamentReadRepository;
use yii\web\Controller;
use yii\filters\AccessControl;

class SiteController extends Controller
{

    public $countries;
    public $tournaments;

    public function __construct($id, $module, CountryReadRepository $countries, TournamentReadRepository $tournaments, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->countries = $countries;
        $this->tournaments = $tournaments;
    }

    public function actionIndex()
    {
        /*
        $countries_ids = $this->tournaments->getDistinctCountryIdsOfTournaments();
        $countries = $this->countries->getCountriesByIds($countries_ids);
        */

        $this->layout = 'family_key';

        return $this->render('index');
        // $this->layout = 'carousel_shop';
        //if (!\Yii::$app->user->isGuest) {
            //return $this->render('index', ['countries' => $countries]);
        //}
        //return $this->redirect('login');

    }

    public function actionCountries()
    {
        $country_names = $this->countries->getAllCountriesNames();
        return  json_encode($country_names);
    }

    public function actionGetTournamentsOfCountry($name)
    {
        $country_id = $this->countries->getIdByCountryName($name);
        
        return $this->renderPartial('tournaments',
            [
                'country_id' => $country_id
            ]);
    }

    public function actionTrack()
    {
        //$file = html_entity_decode(strip_tags(file("http://88.212.253.193:8000/status.xsl?mount=/test")[68]));
        //if(isset(file(\Yii::getAlias('@radio_test_tags'))[68]))
        //    $file = html_entity_decode(strip_tags(file(\Yii::getAlias('@radio_test_tags'))[68]));
        //var_dump(file('http://88.85.67.159:8000/test.xspf'));
        //var_dump($this->parseXmlTitle('http://88.85.67.159:8000/test.xspf'));

        $title = $this->parseXmlTitle(\Yii::getAlias('@radio_test_tags'));
        //exit;
        //else $file = "";
        $item = RadioItem::find()->where(['like', 'audio', trim($title)])->one();
        //var_dump($item);
        if($item) return $item->title . '<br>';
        //echo html_entity_decode(strip_tags(file("http://88.212.253.193:8000/status.xsl?mount=/test")[68])). '<br>';
        return $title;

    }

    public function actionView()
    {
        $item = Items::find()
            ->where('cat_id = 259')
            ->orderBy(['rand()' => SORT_DESC])
            ->limit(1)
            ->one();
        //\core\helpers\Helper::invertImageColor($item->img);
        return '<img class="pic css-adaptive" src="' . Url::to('@static/'.$item->img) . '?cash='.time().'"/>';
    }

}