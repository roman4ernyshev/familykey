<?php

namespace frontend\controllers;

use core\entities\Clever\CleverItemShown;
use core\entities\Clever\CleverUserItem;
use Yii;
use core\entities\Clever\CleverAnswer;
use core\entities\Rockncontroll\RadioItem;
use yii\web\Controller;
use yii\db\Expression;

class CleverController extends Controller
{
    public $layout = 'rockncontroll';

    public function actionIndex($id=0)
    {
        if(!$id){
            $item_shown = CleverItemShown::find()
                ->where(['shown'=> 0])
                //->andWhere(['cat_id' => $id])
                ->orderBy(['rand()' => SORT_DESC])
                ->one();
            if($item_shown){
                $item = RadioItem::findOne($item_shown->item_id);

                $answers = CleverAnswer::find()->where(['item_id' => $item->id])->all();
                shuffle($answers);
                return $this->render('clever',  ['item' => $item, 'ans' => $answers, 'total' => 1]);
            }
        }
        else{
            $item_shown = CleverItemShown::find()
                ->where(['shown'=> 0])
                ->andWhere(['cat_id' => $id])
                ->orderBy(['rand()' => SORT_DESC])
                ->one();
            if($item_shown){
                $item = RadioItem::findOne($item_shown->item_id);

                $answers = CleverAnswer::find()->where(['item_id' => $item->id])->all();
                shuffle($answers);
                return $this->render('clever',  ['item' => $item, 'ans' => $answers, 'total' => 0]);
            }
        }


        $items = CleverItemShown::find()->all();
        foreach ($items as $item){
            $item->shown = 0;
            $item->update();
        }

        return $this->render('game_over', []);

    }

    public function actionTestAnswer()
    {
        //return var_dump(Yii::$app->getRequest());
        if($answer_id = (int)Yii::$app->getRequest()->getQueryParam('answer_id')) {
            if ($item_id = (int)Yii::$app->getRequest()->getQueryParam('item_id')) {
                $user_id = (int)Yii::$app->getRequest()->getQueryParam('user_id');
                $answer = CleverAnswer::findOne($answer_id);
                $item = RadioItem::findOne($item_id);
                $item_show = CleverItemShown::find()->where(['item_id' => $answer->item_id])->one();
                //return var_dump($answer->item_id);
                if($item_show){
                    $item_show->shown = 1;
                    $item_show->save(false);
                    $item_user = new CleverUserItem();
                    $item_user->item_id = $item_id;
                    $item_user->user_id = $user_id;
                    if($answer->right)
                        $item_user->ball = 1;
                    else
                        $item_user->ball = 0;
                    $item_user->time = time();
                    $item_user->save(false);
                }
                else return 'errrorrr';

                $sum_ball_user = CleverUserItem::find()
                    ->select('SUM(ball) as sum')
                    ->where('user_id = ' . $user_id )
                    ->scalar();

                $users = CleverUserItem::find()
                    ->select(['user_id, COUNT(*) as cnt, SUM(ball) as sum'])
                    ->groupBy('user_id')
                    ->all();

                if ($answer->right) {
                    return $this->renderPartial('clever_klar',
                        [
                            'item' => $item,
                            'resp' => '<p style="color:green">Правильно!</p>',
                            'item_show' => $item_show,
                            'total' => (int)Yii::$app->getRequest()->getQueryParam('total'),
                            'sum_ball_user' => $sum_ball_user,
                            'users' => $users

                        ]);
                }
                return $this->renderPartial('clever_klar',
                    [
                        'item' => $item,
                        'resp' => '<p style="color:red">Неправильно!</p>',
                        'item_show' => $item_show,
                        'total' => (int)Yii::$app->getRequest()->getQueryParam('total'),
                        'sum_ball_user' => $sum_ball_user,
                        'users' => $users
                    ]);
            } else return 'ошибка';
        }

    }

}