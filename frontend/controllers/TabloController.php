<?php

namespace frontend\controllers;

use core\entities\tablo\storage\DbStorage;
use core\entities\tablo\storage\RedisStorage;
use core\entities\tablo\Tablo;
use core\entities\tablo\TabMatch;
use core\entities\tablo\TabSummary;
use core\entities\tablo\TabTeam;
use core\entities\tablo\TabTournament;
use yii\web\Controller;
use Yii;

class TabloController extends Controller
{
    public $layout = 'family_key';
    public $haveRenewData;

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->render('index');
        }
        return $this->redirect('login');
    }

    public function actionBablo()
    {
        return $this->render('bablo');
    }

    public function actionSetParams()
    {
        if($params = Yii::$app->request->post()) {

            $tablo = new Tablo(new RedisStorage());
            $tablo->saveParams($params);
        }

        return null;
    }

    public function actionGetParams()
    {
        $tablo = new Tablo(new RedisStorage());

        return  json_encode($tablo->getParams());
    }

    public function actionTeams()
    {
        $res = [];
        $tablo = new Tablo(new DbStorage());
        $teams = $tablo->getTeams();

        if($teams) {
            /**
             * @var TabTeam $team
             */
            foreach ($teams as $team){
                $res[] = $team->name;
            }

        }

        return  json_encode($res);
    }

    public function actionTournaments()
    {
        $res = [];

        $tablo = new Tablo(new DbStorage());
        $tournaments = $tablo->getTournaments();

        if($tournaments) {
            /**
             * @var TabTournament $tournament
             *
             */
            foreach ($tournaments as $tournament){
                $res[] = $tournament->name;
            }
        }


        return json_encode($res);

    }

    public function actionSave()
    {
        $matches = [];
        $summary = [];
        if($params = Yii::$app->request->post()) {

             $tablo = new Tablo(new DbStorage());
             $tablo->saveParams($params);

             $tournament = $tablo->getTournament($params['tournament']);

             if($tournament['matches']){
                 /**
                  * @var $match TabMatch
                  */
                 foreach ($tournament['matches'] as $match){
                     $matches[] = $match;
                 }
             }
             if($tournament['summary']){
                 /**
                  * @var $team_sum TabSummary
                  */
                 foreach ($tournament['summary'] as $team_sum){
                     $summary[] = $team_sum;
                 }
             }

             //return $res;
            return $this->renderPartial('matches',
                [
                    'matches' => $matches,
                    'tournament' => $params['tournament'],
                    'summary' => $summary
                ]);
        }

        return null;
    }

    public function actionShowTournament()
    {
        $matches = [];
        $summary = [];
        if($params = Yii::$app->request->post()) {
            $tablo = new Tablo(new DbStorage());
            $tournament = $tablo->getTournament($params['tournament']);

            if($tournament['summary']){
                /**
                 * @var $team_sum TabSummary
                 */
                foreach ($tournament['summary'] as $team_sum){
                    $summary[] = $team_sum;
                }
            }

            //return $res;
            return $this->renderPartial('matches',
                [
                    'matches' => $matches,
                    'tournament' => $params['tournament'],
                    'summary' => $summary
                ]);
        }

        return null;
    }
}