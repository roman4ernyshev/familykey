<?php
namespace frontend\controllers\notion;

use core\helpers\NotionHelper;
use core\readModels\NotionReadRepository;
use Yii;

class NotionController extends \yii\web\Controller
{
    public $layout = 'family_key';

    private $notions;
    private $category;

    public function __construct($id, $module, NotionReadRepository $notions, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->notions = $notions;
        $this->category =
            $this->notions->isCategoryExists(Yii::$app->request->get('category'))
            ? Yii::$app->request->get()['category']
            : null;
    }

    public function actionIndex()
    {
        return $this->render('index', ['category' => $this->category]);
    }

    public function actionWord($slug)
    {
        $word = $this->notions->getBySlug($slug, $this->notions->getCategoryIdByName($this->category));
        return $this->render('word', ['word' => $word]);
    }

    public function actionGetWordCard()
    {
        $word = $this->notions->getNextNotion(Yii::$app->user->id, $this->notions->getCategoryIdByName($this->category));

        return NotionHelper::makeJsonFromNotionObject($word);
    }


    public function actionGetBySlug($slug='')
    {
        if(!$slug) return $this->actionGetWordCard();
        $word = $this->notions->getBySlug($slug, $this->notions->getCategoryIdByName($this->category));
        return NotionHelper::makeJsonFromNotionObject($word);
    }


    public function actionGetUserStats()
    {
        $bad_words_counts =
            $this->notions->getBadOrGoodLearnedNotionLimit(
                Yii::$app->user->id,
                1,
                20,
                $this->notions->getCategoryIdByName($this->category));
        $good_words_counts =
            $this->notions->getBadOrGoodLearnedNotionLimit(
                Yii::$app->user->id,
                0,
                20,
                $this->notions->getCategoryIdByName($this->category));

        return $this->renderPartial('stats',
            [
                'bad_words' => $bad_words_counts,
                'good_words' => $good_words_counts
            ]);

    }

    public function actionTranslations()
    {
        $res = [];
        $words = $this->notions->getAllAnswers($this->notions->getCategoryIdByName($this->category));

        foreach ($words as $word){
            if($word->answer) $res[] = $word->answer;
        }

        return  json_encode($res);
    }

    function actionTestTranslation()
    {
        if($query = Yii::$app->request->post()) {

            $translation = $query['word'];
            $word_id = $query['id'];

            if ($this->notions->isTranslationRight($translation, $word_id)) {
                if($this->notions->isUserMarked(Yii::$app->user->id, (int)$word_id, 1))  return '<h3>Правильно!</h3>';
            } else {
                if($this->notions->isUserMarked(Yii::$app->user->id, (int)$word_id, -1))  return '<h3>Не правильно!</h3>';
            }
        }
        return '<h3>Произошла ошибка! Поробуйте ещё раз!</h3>';
    }

    function actionOpenTranslation(){
        if($query = Yii::$app->request->post()) {
            echo $query['id']; exit;
        }
    }



}