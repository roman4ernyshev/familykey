<?php

namespace frontend\controllers\rockncontroll;

use core\readModels\Soccer\MatchReadRepository;
use core\readModels\Soccer\TournamentReadRepository;
use yii\web\Controller;

class TournamentController extends Controller
{
    public $layout = 'family_key';

    public $matches;
    public $tournaments;

    const ALL_MATCHES = 0;
    const VICTORY_MATCHES = 1;
    const TIE_MATCHES = 2;
    const DEFEAT_MATCHES = 3;

    public function __construct($id, $module, MatchReadRepository $matches, TournamentReadRepository $tournaments, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->matches = $matches;
        $this->tournaments = $tournaments;
    }

    public function actionIndex($id)
    {
        $matches = $this->matches->getMatchesByTournamentId($id);
        //var_dump($tournament_name); exit;
        return $this->render('index', [
            'matches' => $matches,
        ]);
    }

    public function actionAll($id)
    {
        $country_names = $this->tournaments->getAllTournamentsNamesByCountryId($id);
        return  json_encode($country_names);
    }

    public function actionTeams($id)
    {
        $teams_names = $this->matches->getAllTournamentTeams($id);
        return  json_encode($teams_names);
    }

    public function actionAllTeams()
    {
        $teams_names = $this->matches->getAllTeams();
        return  json_encode($teams_names);
    }


    public function actionGetMatchesOfTwoTeams($team1, $team2)
    {
        $matches = $this->matches->getMatchesOfTwoTeams($team1, $team2);
        $tournament_matches = $this->matches->allMatchesOfTwoTeams($team1, $team2);
        $team_names = [$team1, $team2];

        return $this->renderPartial('index', [
            'matches' => $matches,
            'tournament_name' => 'Матчи команд '.$team1.' и '.$team2,
            'tournament_id' => 0,
            'team_name' => null,
            'summary' => $this->matches->getSortedTeamsSummary($team_names, $tournament_matches)
        ]);
    }

    public function actionGetTeamsOfTournament($name)
    {
        $tournament_id = $this->tournaments->getIdByTournamentName($name);

        return $this->renderPartial('teams', [
            'tournament_id' => $tournament_id,
        ]);
    }

    public function actionGetMatchesOfTeam($name, $tournament_id, $filter=self::ALL_MATCHES)
    {
        if ((int)$filter == self::VICTORY_MATCHES)
            $matches = $this->matches->getMatchesOnlyTeamVictory($name, (int)$tournament_id);
        elseif ((int)$filter == self::TIE_MATCHES)
            $matches = $this->matches->getMatchesOnlyTeamTie($name, (int)$tournament_id);
        elseif ((int)$filter == self::DEFEAT_MATCHES)
            $matches = $this->matches->getMatchesOnlyTeamDefeat($name, (int)$tournament_id);
        else
            $matches = $this->matches->getMatchesByTeamName($name, $tournament_id);

        $tournament_matches = $this->matches->allTournamentMatches($tournament_id);
        $tournament_name = $this->tournaments->getNameById($tournament_id);
        $team_names = $this->matches->getAllTournamentTeams($tournament_id);

        return $this->renderPartial('index', [
            'matches' => $matches,
            'tournament_name' => $tournament_name,
            'tournament_id' => $tournament_id,
            'team_name' => $name,
            'summary' => $this->matches->getSortedTeamsSummary($team_names, $tournament_matches)
        ]);
    }

    public function actionGetMatchesOfTour($tour, $tournament_id)
    {
        $matches = $this->matches->getMatchesByTour($tour, (int)$tournament_id);
        $tournament_matches = $this->matches->allTournamentMatchesToTour($tour, (int)$tournament_id);
        $tournament_name = $this->tournaments->getNameById($tournament_id);
        $team_names = $this->matches->getAllTournamentTeams($tournament_id);

        return $this->renderPartial('index', [
            'matches' => $matches,
            'tournament_name' => $tournament_name,
            'tournament_id' => $tournament_id,
            'team_name' => null,
            'summary' => $this->matches->getSortedTeamsSummary($team_names, $tournament_matches)
        ]);
    }

}