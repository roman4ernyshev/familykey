<?php

namespace frontend\controllers\rockncontroll;

use core\entities\Rockncontroll\Categories;
use core\entities\Rockncontroll\Products;
use core\entities\Rockncontroll\Shop;
use core\services\GraphicManager;
use yii\web\Controller;

class GrafikController extends Controller
{
    private $graphic_service;

    public function __construct(
        string $id,
        $module,
        GraphicManager $service,
        array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->graphic_service = $service;
    }

    function actionIndex(){

        $shop = Shop::find()->where(['name' => \Yii::$app->getRequest()->getQueryParam('request')])->one();
        if($shop) return $this->renderPartial('graf',
            [
                'min' => 0,
                'title' => $shop->name,
                'yAxis_title' => '€',
                'datas' => $this->graphic_service->boughtShop($shop->id)
            ]
        );

        $product = Products::find()->where(['name' => \Yii::$app->getRequest()->getQueryParam('request')])->one();
        if($product)  return $this->renderPartial('graf',
            [
                'min' => 0,
                'title' => $product->name,
                'yAxis_title' => '€',
                'datas' => $this->graphic_service->boughtWare($product->id)
            ]
        );

        $cat = Categories::find()->where(['name' => \Yii::$app->getRequest()->getQueryParam('request')])->one();
        if($cat) return $this->renderPartial('graf',
            [
                'min' => 0,
                'title' => $cat->title,
                'yAxis_title' => '€',
                'datas' => $this->graphic_service->boughtCat($cat->id)
            ]
        );

    else return 'upps';
    }

}