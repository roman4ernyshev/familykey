<?php

namespace frontend\controllers\rockncontroll;

use core\entities\Rockncontroll\TeamSum;
use yii\web\Controller;


class FootableController extends Controller
{
    public $layout = 'rockncontroll';

    public function actionTable()
    {
            $clubs = TeamSum::find()
                ->where("is_club = 1 and is_tour_visual = 1")
                ->orderBy(["cash_balls" => SORT_DESC, "cash_g_get" => SORT_DESC])
                ->all();
            $countries = TeamSum::find()
                ->where("is_club = 0 and is_tour_visual = 1")
                ->orderBy(["cash_balls" => SORT_DESC, "cash_g_get" => SORT_DESC])
                ->all();
            //return var_dump($clubs);

            return $this->render('table', ['clubs' => $clubs, 'countries' => $countries]);
    }

}