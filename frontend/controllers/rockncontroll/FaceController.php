<?php
namespace frontend\controllers\rockncontroll;

use core\entities\Coronovirus;
use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\Snapshot;
use core\helpers\UserHelper;
use core\readModels\Rockncontroll\NsbNewsReadRepository;
use yii\web\Controller;
use yii\helpers\Url;


class FaceController extends Controller
{
    public $layout = 'rockncontroll';

    private $nsb_news;

    private $cities_map = [
        'London' => 'uk',
        'Novosibirsk' => 'ru',
        'Moscow' => 'ru',
        'Muenchen' => 'de',
        'Sankt-Peterburg' => 'ru'

    ];

    public function __construct(
        $id,
        $module,
        NsbNewsReadRepository $nsb_news,
        $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->nsb_news = $nsb_news;

    }

    public function actionIndex()
    {
        $life_day = UserHelper::userLifeDayFromDayMonthYear(3,5,1970,9);
        $temp_novosib = $this->getCityCurrentTemp('Novosibirsk');
        $temp_muenchen = $this->getCityCurrentTemp('Muenchen');
        return $this->render('index',
            [
                'life_day' => $life_day,
                'temp_novosib' => (int)$temp_novosib,
                'temp_muenchen' => (int)$temp_muenchen
            ]);
    }

    public function actionRandNewNew()
    {
        return $this->nsb_news->getLast();
    }

    public function actionDeutsch()
    {
        return $this->render('deutsch');
    }

    public function getCityCurrentTemp($city_string)
    {
        foreach ($this->cities_map as $city => $country) {

            if($city_string == $city){
                $url = 'http://api.openweathermap.org/data/2.5/weather?q=' .
                    $city . ',' . $country . '&appid=1a415db9cc13a7d753bcc5fc25ee5972';
                //echo $url; exit;
                try {
                    $contents = file_get_contents($url);
                } catch (\Exception $e) {
                    return 'no';
                }

                $cont = json_decode($contents, true);
                //var_dump($cont['main']['temp']); exit;
                return $cont['main']['temp'] - 273;
            }
            //else echo 'false';
        }

    }

    public function actionCoronavirus()
    {
        $data = [];
        $data['Russia'] = Coronovirus::find()->where("country like '%Russia%'")->orderBy('id DESC')->one();
        $data['Germany'] = Coronovirus::find()->where("country like '%Germany%'")->orderBy('id DESC')->one();
        $data['Italy'] = Coronovirus::find()->where("country like '%Italy%'")->orderBy('id DESC')->one();
        $data['Spain'] = Coronovirus::find()->where("country like '%Spain%'")->orderBy('id DESC')->one();
        $data['China'] = Coronovirus::find()
            ->where("country like '%China%'")
            ->orderBy('id DESC')
            ->one();
        $data['Iran'] = Coronovirus::find()->where("country like '%Iran%'")->orderBy('id DESC')->one();
        $data['France'] = Coronovirus::find()->where("country like '%France%'")->orderBy('id DESC')->one();
        $data['SKorea'] = Coronovirus::find()->where("country like '%S. Korea%'")->orderBy('id DESC')->one();
        $data['UK'] = Coronovirus::find()->where("country like 'UK'")->orderBy('id DESC')->one();
        $data['Nether'] = Coronovirus::find()->where("country like 'Netherlands'")->orderBy('id DESC')->one();

        $data['Switz'] = Coronovirus::find()->where("country like 'Switzerland'")->orderBy('id DESC')->one();
        $data['USA'] = Coronovirus::find()->where("country like 'USA'")->orderBy('id DESC')->one();
        $data['Belgium'] = Coronovirus::find()->where("country like 'Belgium'")->orderBy('id DESC')->one();
        $data['Canada'] = Coronovirus::find()->where("country like 'Canada'")->orderBy('id DESC')->one();

        $data['Portugal'] = Coronovirus::find()->where("country like 'Portugal'")->orderBy('id DESC')->one();
        $data['Sweden'] = Coronovirus::find()->where("country like 'Sweden'")->orderBy('id DESC')->one();
        $data['Brazil'] = Coronovirus::find()->where("country like 'Brazil'")->orderBy('id DESC')->one();
        $data['Australia'] = Coronovirus::find()->where("country like 'Australia'")->orderBy('id DESC')->one();

        $data['Turkey'] = Coronovirus::find()->where("country like 'Turkey'")->orderBy('id DESC')->one();
        $data['Israel'] = Coronovirus::find()->where("country like 'Israel'")->orderBy('id DESC')->one();

       // $data['Cuba'] = Coronovirus::find()->where("country like 'Cuba'")->orderBy('id DESC')->one();
       // $data['Dominican Republic'] = Coronovirus::find()->where("country like 'Dominican Republic'")->orderBy('id DESC')->one();


        $rand_key = array_rand($data);

        return $this->renderPartial('coronavirus', ['data' => $data[$rand_key], 'key' => $rand_key]);
    }

    public function actionCurrWeatherActual()
    {
        /*
         * $ <?= \core\helpers\CurrHelper::actionGetCurrency(11)?>
        € <?= \core\helpers\CurrHelper::actionGetCurrency(12)?> Muenchen <?= $temp_muenchen ?>
            Novosibirsk <?= $temp_novosib ?>
         */
        return '$ '.\core\helpers\CurrHelper::actionGetCurrency(11).
               ' € '.\core\helpers\CurrHelper::actionGetCurrency(12).
               ' Muenchen '.round($this->getCityCurrentTemp('Muenchen')).
               ' Novosibirsk '.round($this->getCityCurrentTemp('Novosibirsk'));
    }

    public function actionInvertRandImg()
    {
        $item = Items::find()
            ->where('cat_id = 259')
            ->orderBy(['rand()' => SORT_DESC])
            ->limit(1)
            ->one();
        //\core\helpers\Helper::invertImageColor($item->img);
        return '<img class="pic css-adaptive" src="' . Url::to('@static/'.$item->img) . '?cash='.time().'"/>';
    }

    public function actionFormBalll()
    {
            $balls = Snapshot::find()
                ->select('oz, date')    
                ->orderBy('id DESC')
                ->limit(14)
                ->asArray()
                ->all();

            $res_string = '';

            foreach ($balls as $ball){
                    $day_of_week = date('w', strtotime($ball['date']));
                    if ($ball['oz'] >= 80) {
                            if ($day_of_week == 0)
                                $res_string .= ' <span class="balll" style="color:rgb(40,157,139)">'.$ball['oz'].'</span>';
                            else $res_string .= ' <span style="color:rgb(40,157,139)">'.$ball['oz'].'</span>';
                    }
                    elseif ($ball['oz'] >= 60) {
                            if ($day_of_week == 0)
                                $res_string .= ' <span class="balll" style="color:white">'.$ball['oz'].'</span>';
                            else $res_string .= ' <span style="color:white">'.$ball['oz'].'</span>';
                    }
                    elseif ($day_of_week == 0)
                        $res_string .= ' <span class="balll" style="color:orangered">'.$ball['oz'].'</span>';
                    else $res_string .= ' <span style="color:orangered">'.$ball['oz'].'</span>';
            }

            return $res_string;
 
                        
    }


}
