<?php

namespace frontend\controllers\rockncontroll;

use core\entities\Rockncontroll\Author;
use core\entities\Rockncontroll\City;
use core\entities\Rockncontroll\DeutschItem;
use core\entities\Rockncontroll\DeutschMark;
use core\entities\Rockncontroll\DeutschTournament;
use core\entities\Rockncontroll\DiaryActs;
use core\entities\Rockncontroll\DiaryDoneDeal;
use core\entities\Rockncontroll\DiaryRecDayParams;
use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\MarkUser;
use core\entities\Rockncontroll\RadioItem;
use core\entities\Rockncontroll\Snapshot;
use core\entities\Rockncontroll\SongText;
use core\entities\Rockncontroll\Source;
use app\models\UserPrint;
use app\models\VisitBlock;
use app\models\VisitError;
use app\models\Visitor;
use app\models\VisitorCount;
use core\helpers\UserHelper;
use Yii;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\LoginForm;
use app\models\ContactForm;

class DatasController extends Controller
{
    public $message = '';
    public $url = 'http://37.192.187.83:10088/status-json.xsl';
    public $layout = 'rockncontroll';

    private $cities_map = [
        'London' => 'uk',
        'Novosibirsk' => 'ru',
        'Moscow' => 'ru',
        'Muenchen' => 'de',
        'Sankt-Peterburg' => 'ru',
        'Manchester' => 'gb',
        'New York' => 'us'
    ];


    function actionShowCurrentRadioTracksTest(){
        return '<p>'.$this->getAudioTags(json_decode(file_get_contents('http://37.192.187.83:10088/status-json.xsl'))->icestats->source[2]->title).'</p>';
        //return '<p>'.$this->getAudioTags(json_decode(file_get_contents(\Yii::getAlias('@radio')))->icestats->source[2]->title).'</p>';
        //return '<p>'.$this->getAudioTags(html_entity_decode(strip_tags(file("http://37.192.187.83:10088/status.xsl?mount=/test_mp3")[68]))).'</p>';
        //return '<p>'.$this->getAudioTags($this->parseXmlTitle(\Yii::getAlias('@radio_second_tags'))).'</p>';
    }

    function actionShowCurrentRadioTracksSecond(){
        //return var_dump(file("http://37.192.187.83:10088/info.xsl"));
        return '<p>'.$this->getAudioTags(json_decode(file_get_contents('http://37.192.187.83:10088/status-json.xsl'))->icestats->source[1]->title).'</p>';
        //return '<p>'.$this->getAudioTags(html_entity_decode(strip_tags(file("http://37.192.187.83:10088/status.xsl?mount=/second_mp3")[17]))).'</p>';
        //return '<p>'.$this->getAudioTags($this->parseXmlTitle(\Yii::getAlias('@radio_second_tags'))).'</p>';
    }

    function actionShowCurrentRadioTracksBard(){
        //return var_dump(json_decode(file_get_contents('http://37.192.187.83:10088/status-json.xsl'))->icestats);
        return '<p>'.$this->getAudioTags(json_decode(file_get_contents('http://37.192.187.83:10088/status-json.xsl'))->icestats->source[0]->title).'</p>';
    }

    function actionShowCurrentTrackText($canal){

        if($canal == 'test'){
            $file = html_entity_decode(strip_tags(file("http://37.192.187.83:10088/status.xsl?mount=/test_mp3")[64]));
        }
        elseif ($canal == 'second'){
            $file = html_entity_decode(strip_tags(file("http://37.192.187.83:10088/status.xsl?mount=/second_mp3")[64]));
        }
        elseif ($canal == 'bard') $file = html_entity_decode(strip_tags(file("http://37.192.187.83:10088/status.xsl?mount=/bard_mp3")[64]));
        else return 'No information';

        //return var_dump(file("http://37.192.187.83:10088/status.xsl"));

        $item = RadioItem::find()->where(['like', 'audio', trim($file)])->one();
        if($item) return $item->text;

        $thing = SongText::find()->where(['like', 'link', trim($file)])->one();
        if($thing) return $thing->text;
        return $file;
    }

    function actionBardCurTrack()
    {
        echo $this->getRemoteCurrentTrackText();
    }

    /**
     *  'London' => 'uk',
        'Novosibirsk' => 'ru',
        'Moscow' => 'ru',
        'Muenchen' => 'de',
        'Sankt-Peterburg' => 'ru',
        'Manchester' => 'gb',
        'New York' => 'us'
     */
    function actionWeather(){
        $cities = [];

        $cities['Nsb'] = $this->getCityCurrentTemp('Novosibirsk', 'ru');
        $cities['Mue'] = $this->getCityCurrentTemp('Muenchen', 'de');
        //$cities['Manchester'] = $this->getCityCurrentTemp('Manchester', 'gb');
        $cities['Msw'] = $this->getCityCurrentTemp('Moscow', 'ru');

        $rand_city_id = rand(2699,212277);

        $rand_city = City::find()->where(['id' => $rand_city_id])->one();

        //$cut_city_name = substr($rand_city->name, 0, 5);

        $name_rand_city = $rand_city->name.' '.$rand_city->country->name;

        $cities[$name_rand_city] = $this->getCityCurrentTemp($rand_city->name, $rand_city->country->iso_code);

        asort($cities);

        //var_dump($cities); exit;
        //New York
        return $this->renderPartial('weather',
            [
                'cities' => $cities,
                //'r_cities' => $r_cities
            ]);
    }

    function getRemoteCurrentTrackText()
    {
        //$file = html_entity_decode(strip_tags(file("http://88.212.253.193:8000/status.xsl?mount=/test")[68]));
        //if(isset(file(\Yii::getAlias('@radio_test_tags'))[68]))
        //    $file = html_entity_decode(strip_tags(file(\Yii::getAlias('@radio_test_tags'))[68]));
        //var_dump(file('http://88.85.67.159:8000/test.xspf'));
        //var_dump($this->parseXmlTitle('http://88.85.67.159:8000/test.xspf'));

        $title = $this->parseXmlTitle(\Yii::getAlias('@radio_test_tags'));
        //exit;
        //else $file = "";
	$item = RadioItem::find()->where(['like', 'audio', trim($title)])->one();
	//var_dump($item);
        if($item) return $item->title . '<br>';
        //echo html_entity_decode(strip_tags(file("http://88.212.253.193:8000/status.xsl?mount=/test")[68])). '<br>';
        return $title;

    }

    function parseXmlTitle($file){
        $xml = simplexml_load_file($file);
        return $xml->trackList->track->title;
    }

    function actionRadioEnters()
    {

        //$page = $this->_conn("http://37.192.187.83:10088/admin/listclients.xsl?mount=/second_mp3");
        $page = $this->_conn("http://88.212.253.193:8000/admin/listclients.xsl?mount=/test", "admin:rhfnjxdbkb10");
        //$page = null;
        //$page_here2 = $this->_conn("http://37.192.187.83:10088/admin/listclients.xsl?mount=/second_mp3", "admin:vbifcdtnf");
        //$page_here3 = $this->_conn("http://37.192.187.83:10088/admin/listclients.xsl?mount=/test_mp3", "admin:vbifcdtnf");

        //echo ''.$this->getInformFromDom($page).'<br>';
        if($this->getInformFromDom($page)) echo '88.212.253.193 Bard<br>-----------<br>'.$this->getInformFromDom($page).'<br>';
        //if($this->getInformFromDom($page_here2))echo '<p style="color:seagreen">'.$this->getInformFromDom($page_here2).'</p><br>';
        //if($this->getInformFromDom($page_here3))echo '<p style="color:cyan">'.$this->getInformFromDom($page_here3).'</p><br>';
        //if($this->getInformFromDom($page))echo '<p style="color:chocolate">'.$this->getInformFromDomOld($page).'</p><br>';

    }

    private function getInformFromDom($page)
    {
        $res = '';
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($page);

        //echo $dom->getElementsByTagName("td")->length;
        //$div->childNodes->length

        if($dom->getElementsByTagName("td")->length) {
            for($i=5;$i<$dom->getElementsByTagName("td")->length;$i++) {
                if($dom->getElementsByTagName("td")[$i] && !(($i-3)%4)==0)
                    $res .= $dom->getElementsByTagName("td")[$i]->nodeValue.'<br>';
                //else echo '-------------<br>';
            }
            //echo $dom->getElementsByTagName("td")->length.'<br>';
        }
        return $res;

    }

    private function getInformFromDomOld($page)
    {
        $res = '';
        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($page);

        //echo $dom->getElementsByTagName("td")->length;
        //$div->childNodes->length

        if($dom->getElementsByTagName("td")->length) {
            for($i=7;$i<$dom->getElementsByTagName("td")->length;$i++) {
                if($dom->getElementsByTagName("td")[$i] && !(($i-3)%4)==0)
                    $res .= $dom->getElementsByTagName("td")[$i]->nodeValue.'<br>';
                //else echo '-------------<br>';
            }
            //echo $dom->getElementsByTagName("td")->length.'<br>';
        }
        return $res;

    }

    private function _conn($url, $auth)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        // указываем имя и пароль
        //curl_setopt($ch, CURLOPT_USERPWD, "admin:vbifcdtnf");
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                //'Accept-Encoding:gzip, deflate, sdch',
                //'Accept-Language:*',
                'Cache-Control:no-cache',
                'Connection:keep-alive',
                'Cookie:my_project=455; _dc_gtm_UA-28208502-12=1; _gat_UA-28208502-12=1; _ga=GA1.2.1191596796.1477908016',
                'Host:www.soccerstand.com', 'Pragma:no-cache',
                //'Referer:http://d.soccerstand.com/ru/x/feed/proxy-local',
                'User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
                'Upgrade-Insecure-Requests:1',
                'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                //'X-Requested-With:XMLHttpRequest',
                //'Accept-Charset: Windows-1251,utf-8;q=0.7,*;q=0.7'
            ]);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    public function actionDeutsch()
    {
        return $this->render('deutsch');
    }

    public function getCityCurrentTemp($city, $country)
    {
        //foreach ($this->cities_map as $city => $country) {

            //if($city_string == $city){
                $url = 'http://api.openweathermap.org/data/2.5/weather?q=' .
                    $city . ',' . $country . '&appid=1a415db9cc13a7d753bcc5fc25ee5972';
                //echo $url; exit;
                try {
                    $contents = file_get_contents($url);
                } catch (\Exception $e) {
                    return 'no';
                }

                $cont = json_decode($contents, true);
                //var_dump($cont['main']['temp']); exit;
                return round($cont['main']['temp'] - 273);
            //}
            //else echo 'false';
        //}

    }

    /**
     * Логирование входов на сервисы
     * @throws \Exception
     */
    function actionComeIn()
    {
        //print_r(json_decode($_POST['components'])); exit;

        if(isset($_POST['hash']) && isset($_POST['components']) && isset($_POST['site']) && isset($_POST['block'])) {


            $json_string = $_POST['components'];
            $obj = json_decode($json_string);
            //var_dump($obj); exit;

            if($visitor = VisitorCount::findOne(['hash' => $_POST['hash']])){
                $visitor->count++;
                $visitor->update(false);

                $block = new VisitBlock();
                $block->visitor_id = $visitor->id;
                $block->time = time();
                $block->site = $_POST['site'];
                $block->block = $_POST['block'];




                if(isset($_POST['ip_json'])){

                    $ip_obj = json_decode($_POST['ip_json']);

                    //var_dump($ip_obj); exit;
                    if(isset($ip_obj->ip))$block->ip = $ip_obj->ip;
                    if(isset($ip_obj->hostname))$block->hostname = $ip_obj->hostname;
                    if(isset($ip_obj->city))$block->city = $ip_obj->city;
                    if(isset($ip_obj->region))$block->region = $ip_obj->region;
                    if(isset($ip_obj->country))$block->country = $ip_obj->country;
                    if(isset($ip_obj->loc))$block->loc = $ip_obj->loc;
                    if(isset($ip_obj->org))$block->org = $ip_obj->org;
                    if(isset($ip_obj->postal))$block->postal = $ip_obj->postal;
                    //var_dump($ip_obj); exit;

                }



                if(!$block->save(false)){

                    $error = new VisitError();
                    $error->time = time();
                    $error->text = 'ошибка сохранения блока на '. $_POST['site'] . ' в блок '. $_POST['block'] . ' hash ' . $_POST['hash'];
                    $error->save(false);
                }
            }
            else{

                try {
                    $visitor = new VisitorCount();
                    $visitor->hash = $_POST['hash'];
                    $visitor->count = 0;
                } catch (ErrorException $e) {

                    $error = new VisitError();
                    $error->time = time();
                    $error->text = $e->getMessage();
                    $error->save(false);
                }


                if($visitor->save(false)) {
                    $visit = new Visitor();

                    $visit->time = time();

                    $visit->visitor_id = $visitor->id;

                    $visit->site = $_POST['site'];
                    $visit->block = $_POST['block'];


                    for ($i = 0; $i < count($obj); $i++) {
                        if ($obj[$i]->key == 'user_agent') $visit->user_agent = $obj[$i]->value;
                        if ($obj[$i]->key == 'language') $visit->language = $obj[$i]->value;
                        if ($obj[$i]->key == 'color_depth') $visit->color_depth = $obj[$i]->value;
                        if ($obj[$i]->key == 'pixel_ratio') $visit->pixel_ratio = $obj[$i]->value;
                        if ($obj[$i]->key == 'hardware_concurrency') $visit->hardware_concurrency = $obj[$i]->value;

                        if ($obj[$i]->key == 'resolution') {
                            $visit->resolution_x = $obj[$i]->value[0];
                            $visit->resolution_y = $obj[$i]->value[1];
                        }

                        if ($obj[$i]->key == 'available_resolution') {
                            $visit->available_resolution_x = $obj[$i]->value[0];
                            $visit->available_resolution_y = $obj[$i]->value[1];
                        }


                        if ($obj[$i]->key == 'timezone_offset') $visit->timezone_offset = $obj[$i]->value;
                        if ($obj[$i]->key == 'session_storage') $visit->session_storage = $obj[$i]->value;
                        if ($obj[$i]->key == 'local_storage') $visit->local_storage = $obj[$i]->value;

                        if ($obj[$i]->key == 'indexed_db') $visit->indexed_db = $obj[$i]->value;
                        if ($obj[$i]->key == 'open_database') $visit->open_database = $obj[$i]->value;
                        if ($obj[$i]->key == 'cpu_class') $visit->cpu_class = $obj[$i]->value;

                        if ($obj[$i]->key == 'navigator_platform') $visit->navigator_platform = $obj[$i]->value;
                        if ($obj[$i]->key == 'do_not_track') $visit->do_not_track = $obj[$i]->value;
                        if ($obj[$i]->key == 'regular_plugins') $visit->regular_plugins = implode('; ', $obj[$i]->value);

                        if ($obj[$i]->key == 'canvas') $visit->canvas = $obj[$i]->value;
                        if ($obj[$i]->key == 'webgl') $visit->webgl = $obj[$i]->value;
                        if ($obj[$i]->key == 'adblock') $visit->adblock = $obj[$i]->value ? 1 : 0;

                        if ($obj[$i]->key == 'has_lied_languages') $visit->has_lied_languages = $obj[$i]->value ? 1 : 0;
                        if ($obj[$i]->key == 'has_lied_resolution') $visit->has_lied_resolution = $obj[$i]->value ? 1 : 0;
                        if ($obj[$i]->key == 'has_lied_os') $visit->has_lied_os = $obj[$i]->value ? 1 : 0;

                        if ($obj[$i]->key == 'has_lied_browser') $visit->has_lied_browser = $obj[$i]->value ? 1 : 0;
                        if ($obj[$i]->key == 'touch_support') $visit->touch_support = $obj[$i]->value[0] ? 1 : 0;
                        if ($obj[$i]->key == 'js_fonts') $visit->js_fonts = implode('; ', $obj[$i]->value);

                    }

                    //var_dump($visit); exit;

                    if(!$visit->save(false)) {
                        $error = new VisitError();
                        $error->time = time();
                        $error->text = 'ошибка сохранения входа на '. $_POST['site'] . ' в блок '. $_POST['block'] . ' hash ' . $_POST['hash'];
                        $error->save(false);

                    }
                    else{
                        $block = new VisitBlock();
                        $block->visitor_id = $visitor->id;
                        $block->time = time();
                        $block->site = $_POST['site'];
                        $block->block = $_POST['block'];
                        if(!$block->save(false)){
                            $error = new VisitError();
                            $error->time = time();
                            $error->text = 'ошибка сохранения блока на '. $_POST['site'] . ' в блок '. $_POST['block'] . ' hash ' . $_POST['hash'];
                            $error->save(false);
                        }
                    }
                }

                else{
                    $error = new VisitError();
                    $error->time = time();
                    $error->text = 'ошибка сохранения пользователя - hash: '. $_POST['hash'] . 'на сайте '. $_POST['site'] . ' в блок '. $_POST['block'] . ' hash ' . $_POST['hash'];
                    $error->save(false);
                }
            }
        }

        else{
            $error = new VisitError();
            $error->time = time();
            $error->text = 'Вход без данных';
            $error->save(false);
        }


    }

    /**
     * Аудио тэги из базы
     * @param $api_string
     * @return string
     */
    function getAudioTags($api_string){
        //$str = substr($api_string, strpos($api_string, "playing:")+8);
        $item = RadioItem::find()->where(['ilike', 'audio', trim($api_string)])->one();
        //return $item->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql;

        //return substr($api_string, strpos($api_string, "playing:")+8);

        if($item) return $item->cat->name." :: ".$item->anons." :: ".$item->title;

        return trim($api_string);

    }

    /**
     * Отдаёт названия альбомов с автором
     * @return string
     */
    function actionAuthorsAlbums(){
        $res = [];

        $sources = Source::find()
            ->where('')
            ->all();

        foreach ($sources as $source){
            $res[] = $source->author->name .' ::: '.$source->title;
        }

        return  json_encode($res);
    }

    /**
     * Отдаёт немецкие слова
     * @return string
     */
    function actionDeutschWords(){
        $res = [];

        $words = DeutschItem::find()
            ->all();

        foreach ($words as $word){
            $res[] = $word->d_word;
        }

        return  json_encode($res);
    }

    /**
     * Отдаёт переводы немецких карточек
     * @return string
     */
    function actionTranslations(){
        $res = [];

        $words = DeutschItem::find()
            ->all();

        foreach ($words as $word){
            $res[] = $word->d_word_translation;
        }

        return  json_encode($res);
    }

    function actionRusTranslations(){
        $res = [];

        $words = DeutschItem::find()
            ->all();

        foreach ($words as $word){
            $res[] = $word->d_word;
        }

        return  json_encode($res);
    }


    /**
     * Отдаём альбом
     * @return string
     */
    function actionGetAlbum(){

        if(isset($_POST['album'])) {
            $album = explode(':::', $_POST['album'])[1];
            //var_dump(Source::find()->where('title like "%'.trim($album).'%"')->one()); exit;

            if(Source::find()->where('title like "%'.trim($album).'%"')->one()){
                //echo $album; exit;
                $source_id = Source::find()->where('title like "%'.trim($album).'%"')->one()->id;

                $songs = SongText::find()->where("source_id=$source_id")->all();
                $source = Source::findOne($source_id);

                return $this->renderPartial('album', ['songs' => $songs, 'source' => $source]);
                //return var_dump($items);
            }

            else echo 'uups';


        }

    }

    function actionGetAlbumById(){

        if(isset($_POST['id'])) {
            $id = (int)$_POST['id'];

            $songs = SongText::find()->where("source_id=$id")->all();
            $source = Source::findOne($id);

            return $this->renderPartial('album', ['songs' => $songs, 'source' => $source]);

        }

    }

    function actionGetAuthors()
    {
        $authors = Author::find()->where('status = 1 or status = 3')->orderBy('name')->all();
        //$authors = ArrayHelper::map(Author::find()->where(['status' => 1])->all(), 'id', 'name');

        // return var_dump($authors);
        return $this->renderPartial('authors', ['authors' => $authors]);
    }



    /**
     * Вывод карточки со словом
     * @return string
     * @throws
     */
    function actionGetWordCard(){
        $user = Yii::$app->getRequest()->getQueryParam('user');

        $word = DeutschItem::find()
            ->where(['shown' => 0])
            ->andWhere('id>50')
            ->orderBy(['rand()' => SORT_DESC])
            ->one();
        if($word) {
            $word->shown = 1;
            $word->update(false);
        }
        else {
            $words = DeutschItem::find()->all();
            foreach ($words as $word){
                $word->shown = 0;
                $word->update(false);
            }
            $word = DeutschItem::find()
                ->where(['shown' => 0])
                ->orderBy(['rand()' => SORT_DESC])
                ->one();
            $word->shown = 1;
            $word->update(false);
        }

        /*return (rand(0,1)) ?
		$this->renderPartial('word', ['word' => $word, 'user' => $user]) :*/
            return $this->renderPartial('reverse_word', ['word' => $word, 'user' => $user]);
    }

    function actionAddTicketWord(){
        if($id = (int)Yii::$app->getRequest()->getQueryParam('id')){
            $word = DeutschItem::findOne($id);
            $word->shown = 2;
            $word->update(false);
            return 'Исправим';
        }
        return 'upps';
    }

    /**
     * Проверить перевод
     * @return string
     */
    function actionTestTranslation(){
        //return var_dump($_POST);

        if(isset($_POST['word']) && isset($_POST['id']) && isset($_POST['user_id'])) {
            $word = $_POST['word'];
            $id = $_POST['id'];
            $user_id = (int)$_POST['user_id'];

            if((int)$_POST['reverse']) {
                if($obj = DeutschItem::find()->where('d_word like "'.trim($word).'"')->one()){

                    if($obj->id == $id) {
                        $mark = new DeutschMark();
                        $mark->user_id = $user_id;
                        $mark->mark = 1;
                        $mark->time = time();
                        $mark->word_id = $id;
                        if($mark->save(false))
                            return '<h3>Правильно!</h3>';
                        else return 'uups база';
                    }
                    else {
                        $mark = new DeutschMark();
                        $mark->user_id = $user_id;
                        $mark->mark = 0;
                        $mark->time = time();
                        $mark->word_id = $id;
                        if($mark->save(false))
                            return '<h3>Не правильно!</h3>';
                        else echo 'uups база';
                    }
                }

                else {
                    $mark = new DeutschMark();
                    $mark->user_id = $user_id;
                    $mark->mark = 0;
                    $mark->time = time();
                    $mark->word_id = $id;
                    if($mark->save(false))
                        return '<h3>Не правильно!</h3>';
                    else echo 'uups база';
                }

            }
            else {
                if($obj = DeutschItem::find()->where('d_word_translation like "'.trim($word).'"')->one()){

                    if($obj->id == $id) {
                        $mark = new DeutschMark();
                        $mark->user_id = $user_id;
                        $mark->mark = 1;
                        $mark->time = time();
                        $mark->word_id = $id;
                        if($mark->save(false))
                            return '<h3>Правильно!</h3>';
                        else return 'uups база';
                    }
                    else {
                        $mark = new DeutschMark();
                        $mark->user_id = $user_id;
                        $mark->mark = 0;
                        $mark->time = time();
                        $mark->word_id = $id;
                        if($mark->save(false))
                            return '<h3>Не правильно!</h3>';
                        else echo 'uups база';
                    }
                }

                else {
                    $mark = new DeutschMark();
                    $mark->user_id = $user_id;
                    $mark->mark = 0;
                    $mark->time = time();
                    $mark->word_id = $id;
                    if($mark->save(false))
                        return '<h3>Не правильно!</h3>';
                    else echo 'uups база';
                }

            }
        }
    }

    function actionTestTranslationFail(){
        if(isset($_POST['id']) && isset($_POST['user_id'])) {

            $id = $_POST['id'];
            $user_id = (int)$_POST['user_id'];

            $mark = new DeutschMark();
            $mark->user_id = $user_id;
            $mark->mark = 0;
            $mark->time = time();
            $mark->word_id = $id;
            $mark->save(false);

        }
    }

    function actionAddActBall()
    {
        $act = new DiaryActs();
        $act->model_id = 18;
        $act->mark = 1;
        $act->user_id = 8;
        $act->save(false);
    }

    /**
     * Входит пользователь
     * @return mixed|string
     */
    public function actionLogin(){

        if(Yii::$app->getRequest()->getQueryParam('name')) {

            $name = Yii::$app->getRequest()->getQueryParam('name');
            $pseudo = Yii::$app->getRequest()->getQueryParam('pseudo');

            $user = MarkUser::find()
                ->where("name like('%" . $name . "')")
                ->one();


            if($user->id == 2) {
                return $this->renderPartial('menu', ['user' => $user]);
            }

            if($user) {

                return $user->id;

            }
            else return "<p>Увы, Вас нет в базе</p>";

        }

        else {
            return $this->render('index');
        }

    }

    public function actionGetDeutschItem()
    {
        $item = Items::find()
            ->where('source_id IN (8938,8939,9469,9008,9477,9197,9589,9757,9758,9763,9751,9763,9789,9790,9791,9792,9798,9800,9801,9802,9803,9804,9805,9809,9810)')
            ->orderBy(['rand()' => SORT_DESC])
            ->one();
        $eng = Items::find()
            ->where('cat_id = 155')
            ->orderBy(['rand()' => SORT_DESC])
            ->one();
        $it = Items::find()
            ->where('cat_id = 53')
            ->orderBy(['rand()' => SORT_DESC])
            ->one();
        $enc = Items::find()
            ->where('cat_id IN (57,94,187)')
            ->orderBy(['rand()' => SORT_DESC])
            ->one();

        return $this->renderPartial('item',
            [
                'item' => $item,
                'eng' => $eng,
                'it' => $it,
                'enc' => $enc
            ]);

    }

    public function actionGetMarks(){
        $today = strtotime('today');
        if(Yii::$app->getRequest()->getQueryParam('id')) {
            $user = MarkUser::findOne((int)Yii::$app->getRequest()->getQueryParam('id'));

            $marks = DeutschMark::find()
                ->select(['user_id, mark, COUNT(*) as cnt, SUM(mark) as avg '])
                ->where("time > $today")
                ->groupBy('user_id')
                ->orderBy(['avg' => SORT_DESC])
                ->all();

            $teams = DeutschTournament::find()
                ->select(['user_id, mark, COUNT(id) as cnt, SUM(mark) as avg '])
                ->where("id > 1008")
                ->groupBy('user_id')
                ->orderBy(['avg' => SORT_DESC])
                ->all();

            $sum_mish_mark = Snapshot::find()
                ->where("id > 733")
                ->sum('mish_oz');

            /*$to_game_mish = Snapshot::find()
                ->where("id > 827")
                ->sum('mish_oz');
            */

            //Snapshot::find()->where("id > 827")->sum('column');

            //$to_game_mish_remain = 200 - $to_game_mish;


            return $this->renderPartial('teams',
                [
                    'marks' => $marks,
                    'user' => $user,
                    'teams' => $teams,
                    'sum_mish_mark' =>  $sum_mish_mark,
                    //'to_game_mish_remain'=> $to_game_mish_remain,
                ]);
        }
    }

    public function actionGetUserStats(){
        //$today = strtotime('today');

        if($user_id = Yii::$app->getRequest()->getQueryParam('id')) {


            $bad_words_counts = DeutschMark::find()
                ->select(['id, word_id, user_id, COUNT(id) as cnt, SUM(mark) as sum'])
                ->where(['user_id' => $user_id])
                ->andWhere('id > 30000')
                ->groupBy('word_id')
                ->orderBy('sum ASC')
                ->limit(20)
                ->all();

            $good_words_counts = DeutschMark::find()
                ->select(['id, word_id, user_id, COUNT(id) as cnt, SUM(mark) as sum'])
                ->where(['user_id' => $user_id])
                ->andWhere('id > 30000')
                ->groupBy('word_id')
                ->orderBy('sum DESC')
                ->limit(20)
                ->all();


            return $this->renderPartial('stats',
                [
                    'bad_words' => $bad_words_counts,
                    'good_words' => $good_words_counts,
                    'user_id' => $user_id
                ]);
        }
    }

    public function actionAddRemiderWord(){
        if($word_id = Yii::$app->getRequest()->getQueryParam('id')) {
            $user_id = Yii::$app->getRequest()->getQueryParam('user_id');
            $user = MarkUser::findOne((int)$user_id);
            $word = DeutschItem::findOne((int)$word_id);
            Yii::$app->redis->set('remide_deutch_word_'.$user->name, $word->d_word);
            Yii::$app->redis->set('deutch_trans_'.$user->name, $word->d_word_translation);
            return Yii::$app->redis->get('deutch_trans_'.$user->name).' written';
        }
        else return 'Bad';
    }


    /**
     * Валидация юзера
     * @return string
     * @throws \Exception
     */
    public function actionValidateUser(){
        if(isset($_POST['hash'])) {

            $hash = $_POST['hash'];

            if($user = UserPrint::findOne(['hash' => $hash])) {

                $user->count++;
                $user->update(false);

                if($user->name == ''){

                    return $this->renderPartial('user_form', ['message' => $this->message]);
                }
                return '<a class="navbar-brand">Привет, '.UserPrint::findOne(['hash' => $hash])->name .'!</a>';
            }

            else {
                try {
                    $visitor = new UserPrint();
                    $visitor->hash = $hash;
                    $visitor->count = 0;
                    $visitor->save(false);
                } catch (ErrorException $e) {
                    $error = new VisitError();
                    $error->time = time();
                    $error->text = $e->getMessage();
                    $error->save(false);
                }
                return $this->renderPartial('user_form', ['message' => $this->message]);
            }

        }

        else{
            return 1;
        }


    }


    /**
     * @return string
     * @throws \Exception
     */
    public function actionNameUser() {
        if (isset($_POST['hash']) && isset($_POST['name'])) {


            if($user = UserPrint::findOne(['hash' => $_POST['hash']])) {

                if(UserPrint::findOne(['name' => trim($_POST['name'])])) {
                    $this->message = 'Используйте другое имя, пожалуйста!';
                    return $this->renderPartial('user_form', ['message' => $this->message]);
                }

                $user->count++;
                $user->name = $_POST['name'];
                $user->update(false);

                return '<a class="navbar-brand">Привет, '. $user->name .'!</a>';
            }
        }

        return '<a class="navbar-brand">Привет, верменный Бармалец!</a>';

    }

    public function actionAddDeutschWord(){
        if (isset($_POST['word']) &&
            isset($_POST['word_translation']) &&
            isset($_POST['phrase']) &&
            isset($_POST['phrase_translation'])) {
            return $_POST['word'];
        }
    }

    public function actionAction(){
        return var_dump($_POST);
    }

    public function actionGetWasserReih()
    {
        $h_res = [];
        $k_res = [];
        $h_water_params =
            DiaryRecDayParams::find()
                ->select('value')
                ->where('day_param_id = 63')
                ->limit(4)
                ->orderBy('id DESC')
                ->asArray()
                ->all();
        $k_water_params =
            DiaryRecDayParams::find()
                ->select('value')
                ->where('day_param_id = 64')
                ->limit(4)
                ->orderBy('id DESC')
                ->asArray()
                ->all();

        for($i=3,$j=0;$i>=0;$i--,$j++){
            if(isset($h_water_params[$i-1]) && isset($h_water_params[$i]))
                $h_res[$j] =
                    round(((float)$h_water_params[$i-1]["value"]-(float)$h_water_params[$i]["value"]),3);
            if(isset($k_water_params[$i-1]) && isset($k_water_params[$i]))
                $k_res[$j] = round(((float)$k_water_params[$i-1]["value"]-(float)$k_water_params[$i]["value"]),3);
        }
        echo 'h:'.$h_res[2].':'.$h_res[1].':'.$h_res[0].'-k:'.$k_res[2].':'.$k_res[1].':'.$k_res[0];
    }

    public function actionGetIsEventDate(){

        $deels = DiaryDoneDeal::find()->where('deal_id = 335 or deal_id = 491')->all();

        $year_beginnig = mktime(0,0,0,1,1,2016);

        $res = [];

        for($i=0;$i<=365;$i++){
            $res[$i] = 0;
        }

        foreach ($deels as $deel){

            if($deel->act->time > $year_beginnig && $deel->act->time < ($year_beginnig + 60*60*24*365)) {

                $res[(int)(date('z',$deel->act->time))] = 1;

            }

        }

        return  json_encode($res);
    }

    public $enableCsrfValidation = false;

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] =  [
            'class' => \yii\filters\Cors::className(),
            'cors'  => [
                // restrict access to domains:
                //'Origin' => ['*'],
                'Access-Control-Request-Method'    => ['POST', 'GET','PUT','DELETE','PATCH','OPTIONS'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                'Access-Control-Expose-Headers' => ['*'],
                'Access-Control-Allow-Origin' => ['*'],
            ],
        ];
        return $behaviors;
    }

}
