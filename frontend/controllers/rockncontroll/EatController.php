<?php

namespace frontend\controllers\rockncontroll;


use core\entities\Rockncontroll\DiaryActs;
use core\entities\Rockncontroll\DiaryAte;
use core\entities\Rockncontroll\DiaryDish;
use core\entities\Rockncontroll\MarkUser;

use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

class EatController extends Controller
{
    private $dishes;

    public function __construct($id, $module, DiaryDishRepository $notions, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->notions = $notions;
        $this->category =
            $this->notions->isCategoryExists(Yii::$app->request->get('category'))
                ? Yii::$app->request->get()['category']
                : null;
    }

    public function actionDishes(){

        if(!Yii::$app->getRequest()->getQueryParam('user'))  return $this->renderPartial('error');

        $user = MarkUser::findOne(Yii::$app->getRequest()->getQueryParam('user'));

            if(Yii::$app->getRequest()->getQueryParam('dish') &&
                Yii::$app->getRequest()->getQueryParam('measure')) {

                $start_day = strtotime('now 00:00:00', time())+3*60*60;

                $dish = DiaryDish::find()->where(['name' => Yii::$app->getRequest()->getQueryParam('dish')])->one();
                $today_acts_before = implode(
                    ',',
                     ArrayHelper::map(
                         DiaryActs::find()
                             ->where("time > $start_day and user_id = $user->id and model_id = 1")
                             ->all(), 'id', 'id'));

                if($today_acts_before) {
                    $sum_kkal_before = DiaryAte::find()
                        ->select('SUM(kkal)')
                        ->where("act_id  IN (" . $today_acts_before . ")")
                        ->scalar();
                }
                else $sum_kkal_before = 0;

                $act = new DiaryActs();
                $act->model_id = 1;
                $act->user_id = Yii::$app->getRequest()->getQueryParam('user');
                $round_ate = round(Yii::$app->getRequest()->getQueryParam('measure') * $dish->kkal / 100);


                if($act->save(false)) {
                    $ate = new DiaryAte();

                    try {
                        $ate->dish_id = $dish->id;
                    } catch (\ErrorException $e) {
                        return 'Такого блюда в базе нет!';
                    }

                    $ate->act_id = $act->id;
                    $ate->user_id = $act->user_id;
                    $ate->measure = Yii::$app->getRequest()->getQueryParam('measure');
                    $ate->kkal = $round_ate;
                    //$ate->mark = $act->mark;

                    if(!$ate->validate()) {
                        return 'Данные введены некорректно';
                    }
                    else {
                        $ate->save();
                        /*метка начала текущих суток*/

                        $today_acts = implode(
                            ',',
                            ArrayHelper::map(
                                DiaryActs::find()
                                    ->where("time > $start_day and user_id = ".$user->id." and model_id = 1")
                                    ->all(),
                                'id',
                                'id'));

                        $ate_today = [];
                        $sum_kkal = 0;

                        //return var_dump($today_acts);

                        if ($today_acts) {
                            $ate_today = DiaryAte::find()
                                ->where("act_id  IN (" . $today_acts . ")")
                                ->all();
                            $sum_kkal = DiaryAte::find()
                                ->select('SUM(kkal)')
                                ->where("act_id  IN (" . $today_acts . ")")
                                ->scalar();
                        }

                        if($sum_kkal > 2000) {
                            if($sum_kkal - $round_ate < 2000) {
                                $act->mark = round(($sum_kkal-2000)/100, 0, PHP_ROUND_HALF_UP)*(-1);
                            }
                            else{
                                $act->mark = round($round_ate/100, 0, PHP_ROUND_HALF_UP)*(-1);
                            }
                            $act->update();
                            $ate->mark = $act->mark;
                            $ate->update();
                        }

                        return $this->renderPartial('ate_today', ['ate_today' => $ate_today, 'sum_kkal' => $sum_kkal, 'user' => $user]);

                    }

                }

            }


            $today_acts = implode(',', ArrayHelper::map(DiaryActs::find()->where("time > $start_day and user_id = ".$user->id." and model_id = 1")->all(), 'id', 'id'));

            //return var_dump($today_acts);

            $ate_today = [];
            $sum_kkal = 0;

            if ($today_acts) {
                $ate_today = DiaryAte::find()
                    ->where("act_id  IN (" . $today_acts . ")")
                    ->all();
                $sum_kkal = DiaryAte::find()
                    ->select('SUM(kkal)')
                    ->where("act_id  IN (" . $today_acts . ")")
                    ->scalar();
            }

            //return var_dump($ate_today);

            return $this->renderPartial('eat', ['ate_today' => $ate_today, 'sum_kkal' => $sum_kkal, 'user' => $user]);





    }

    /**
     * Удаление съеденного
     * @return string
     * @throws \Exception
     */
    public function actionDeleteAte(){
        if(Yii::$app->getRequest()->getQueryParam('user') && Yii::$app->getRequest()->getQueryParam('ate_id'))  {
            $user = Yii::$app->getRequest()->getQueryParam('user');
            $ate = DiaryAte::findOne(Yii::$app->getRequest()->getQueryParam('ate_id'));
            $act = DiaryActs::findOne($ate->act_id);
            if($ate->delete() && $act->delete())
                return "<span style='color:green; font-size: 15px;'>Запись удалена!</span>";
            else return "<span style='color:darkred; font-size: 15px;'>Ошибка удаления!</span>";

        }
    }

}