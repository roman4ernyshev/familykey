<?php

namespace frontend\controllers\rockncontroll;

use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\Tag;
use yii\db\Expression;
use yii\web\Controller;

class TagsController extends Controller
{
    function actionGet()
    {
        $res = '';
        $tags = Tag::find()
            ->where('frequency>1')
            ->orderBy(new Expression('rand()'))
            ->limit(2)
            ->all();
        foreach ($tags as $tag) {
            $res .= $tag->name .' '.$tag->frequency.' : ';
        }

        return '<span id="tag_1" onclick="showTagItems('.$tags[0]->id.')" style="cursor:pointer">'.$tags[0]->name
            .'</span> '.$tags[0]->frequency.' : '.$tags[1]->frequency.
            ' <span id="tag_2" onclick="showTagItems('.$tags[1]->id.') " style="cursor:pointer">'.$tags[1]->name.'</span>';

    }

    function actionGetItems($id)
    {
        $items = [];
        if(Tag::findOne($id)) {
            $arr_items = explode(',', Tag::findOne($id)->items);
        }
        foreach ($arr_items as $item){
            $items[] = Items::find()->where(['id' => (int)$item])->one();
        }

        return $this->renderPartial('items', ['items' => $items]);
    }

}