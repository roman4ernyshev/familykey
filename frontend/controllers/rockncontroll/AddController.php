<?php
namespace frontend\controllers\rockncontroll;

use core\helpers\UserHelper;
use yii\web\Controller;


class AddController extends Controller
{
    public $layout = 'rockncontroll';

    public function actionItem()
    {
        return $this->render('item');
    }

    public function actionEvent()
    {
        return $this->renderPartial('event');
    }
}