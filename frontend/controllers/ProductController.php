<?php

namespace frontend\controllers;

use backend\forms\Product\ProductSearch;
use core\readModels\ProductCategoryReadRepository;
use core\readModels\ProductReadRepository;
use core\readModels\RocknControllProductReadRepository;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

class ProductController extends Controller
{
    public $layout = 'family_key';

    private $products;
    private $prods_rockncontroll;
    private $categories;
    private $category;

    public function __construct(
        $id,
        $module,
        ProductReadRepository $products,
        RocknControllProductReadRepository $rockncontroll,
        ProductCategoryReadRepository $categories,
        $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->products = $products;
        $this->prods_rockncontroll = $rockncontroll;
        $this->categories = $categories;
        $this->category =
            $this->products->isCategoryExists(Yii::$app->request->get('category'))
                ? Yii::$app->request->get()['category']
                : null;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],

        ];
    }

    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {

            $dataProvider = $this->products->getAll();
            $category = $this->categories->getRoot();
            $searchModel = new ProductSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


            return $this->render('_list', [
                'category' => $category,
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);

        }
        return $this->redirect('login');
    }

    public function actionRock()
    {

        $dataProvider = $this->prods_rockncontroll->getAll();

        return $this->render('_rockncontroll', [
            'dataProvider' => $dataProvider,
            //'searchModel' => $searchModel,
        ]);

    }

}