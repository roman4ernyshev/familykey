<?php

namespace frontend\controllers;

use core\entities\Article\Source;
use core\forms\article\SearchForm;
use core\readModels\ItemReadRepository;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

class ItemController extends Controller
{
    public $layout = 'family_key';

    private $items;


    public function __construct(
        $id,
        $module,
        ItemReadRepository $items,
        $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->items = $items;

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],

        ];
    }

    public function actionIndex()
    {

        //$dataProvider = $this->articles->getAllSources();
        //var_dump(Yii::$app->request->get()); exit;

        $form = new SearchForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                if($source = $this->items->findSource($form)){
                    $this->redirect(['/item/source/'.$source->id]);
                }
                else return $this->render('search', [
                    'model' => $form,
                ]);

            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('search', [
            'model' => $form,
            'id' => ''
        ]);

    }

    public function actionSource($id)
    {

        $form = new SearchForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $source = $this->items->findSource($form);
            $contentProvider = $this->items->getProvidedContentByArticleId($source->id);
            return $this->render('_article', [
                'contentProvider' => $contentProvider,
                'model' => $this->items->getSourceById($source->id),
                'form' => $form
            ]);
        }

        return $this->render('_article', [
            'contentProvider' => $this->items->getProvidedContentByArticleId($id),
            'model' => $this->items->getSourceById($id),
            'form' => $form
        ]);
    }

    public function actionSources()
    {
        $res = [];
        $sources = $this->items->getAllSourcesTitle();

        /**
         * @var Source $source
         */
        foreach ($sources as $source){
            if($source->title) $res[] = $source->title;
        }
        //$res = [1,2];

        return  json_encode($res);
    }

}