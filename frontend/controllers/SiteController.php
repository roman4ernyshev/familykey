<?php
namespace frontend\controllers;

use yii\web\Controller;


/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
       // $this->layout = 'carousel_shop';
        $this->layout = 'family_key';
       // $this->layout = 'carousel_shop';
        if (!\Yii::$app->user->isGuest) {
            return $this->render('index');
        }
        return $this->redirect('login');

    }


    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

}
