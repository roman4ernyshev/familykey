<?php

namespace frontend\controllers;

use core\entities\Article\Article;
use core\entities\Article\Source;
use core\forms\article\SearchForm;
use core\readModels\ArticleReadRepository;
use core\readModels\ItemReadRepository;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

class ArticleController extends Controller
{
    public $layout = 'family_key';

    private $articles;
    private $items;


    public function __construct(
        $id,
        $module,
        ArticleReadRepository $articles,
        ItemReadRepository $items,
        $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->articles = $articles;
        $this->items = $items;

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],

        ];
    }

    public function actionIndex()
    {

        //$dataProvider = $this->articles->getAllSources();
        //var_dump(Yii::$app->request->get()); exit;

        $form = new SearchForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                if($source = $this->articles->findSource($form)){
                    $this->redirect(['/article/source/'.$source->id]);
                }
                else return $this->render('search', [
                    'model' => $form,
                ]);

            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('search', [
            'model' => $form,
            'id' => ''
        ]);

    }

    public function actionSource($id)
    {

        $form = new SearchForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $source = $this->articles->findSource($form);
            $contentProvider = $this->articles->getProvidedContentByArticleId($source->id);
            return $this->render('_article', [
                'contentProvider' => $contentProvider,
                'model' => $this->articles->getSourceById($source->id),
                'form' => $form
            ]);
        }

        return $this->render('_article', [
            'contentProvider' => $this->articles->getProvidedContentByArticleId($id),
            'model' => $this->articles->getSourceById($id),
            'form' => $form
        ]);
    }

    public function actionSources()
    {
        $res = [];
        $sources = $this->articles->getAllSourcesTitle();

        /**
         * @var Source $source
         */
        foreach ($sources as $source){
            if($source->title) $res[] = $source->title;
        }
        //$res = [1,2];

        return  json_encode($res);
    }

}