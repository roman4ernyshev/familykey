<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@staticRoot' => $params['staticPath'], // we may load resources as <img src="<?= Yii::getAlias('@static/1.jpg') ...
        '@static' => $params['staticHostInfo'],
        '@front' => $params['frontendHostInfo'],
        '@music' => $params['audio'],
        '@radio' => $params['radioHost'],
        '@radio_test' => $params['radio_test'],
        '@radio_second' => $params['radio_second'],
        '@radio_test_tags' => $params['radio_test_tags'],
        '@radio_second_tags' => $params['radio_second_tags'],
        '@new_site' => $params['new'],
        '@console' =>  dirname(__DIR__, 2) . '/console',
    ],
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'cookieValidationKey' => $params['cookieValidationKey']
        ],
        'user' => [
            'identityClass' => 'common\auth\Identity',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => true,
                'domain' => $params['cookieDomain'],
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => '_session',
            'cookieParams' => [
                'domain' => $params['cookieDomain'],
                'httpOnly' => true,
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            //'siteKey' => '6LdpzGUUAAAAALMKcmhWd-Q9Q00s0rYpu0IanF5M',
            //'secret' => '6LdpzGUUAAAAAGyoBca5-Sj3gAZgajs_AC9q6wsN',
            'siteKey' => $params['siteKey'],
            'secret' => $params['secret'],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                /*
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => 'google_client_id',
                    'clientSecret' => 'google_client_secret',
                ],
                */
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '6641467',
                    'clientSecret' => 'uXTeHsZPfoYtvKnTtzTG',
                ],
            ],
        ],
        // separated urlManager
        'backendUrlManager' => require __DIR__ . '/../../frontend/config/urlManager.php',
        'frontendUrlManager' => require __DIR__ . '/urlManager.php',
        'urlManager' => function() {
            return Yii::$app->get('frontendUrlManager');
        }

    ],

    'params' => $params,
];
