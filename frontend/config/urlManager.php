<?php

return [
    'class' => 'yii\web\UrlManager',
    'hostInfo' => $params['frontendHostInfo'],
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    // off cache, which breaking objects of pretty urls
    'cache' => false,
    'rules' => [
        '' => 'site/index',
        //'article/article' => '/article',
        //'<_a:about>' => 'site/<_a>',
        'contact' => 'contact/index',
        'signup' => 'auth/signup/request',
        'signup/<_a:[\w-]+>' => 'auth/signup/<_a>',
        '<_a:login|logout>' => 'auth/auth/<_a>',

        // category notion by default
        'notion/<category:[\w\-]+>' => '/notion/notion',
        // word cart by slug in layout
        'notion/<category:[\w\-]+>/<slug:[\w\-]+>' => '/notion/notion/word',
        // get notion object - one difference in url rule from coming before
        // is last slash - request to all actions must be ended by slash
        'notion/<category:[\w\-]+>/<_a:[\w-]+\/$>' => 'notion/notion/<_a>',



        //'notion/<category:[\w\-]+>/<_a:[\w-]+>' => 'notion/notion/<_a>/<slug>',


        //'/notion/notion/<_a:[\w-]+>/<category:[\w\-]+>' => '/notion/<category:[\w\-]+>/<_a:[\w-]+>',

        /*

        ['pattern' => 'sitemap', 'route' => 'sitemap/index', 'suffix' => '.xml'],
        ['pattern' => 'sitemap-<target:[a-z-]+>-<start:\d+>', 'route' => 'sitemap/<target>', 'suffix' => '.xml'],
        ['pattern' => 'sitemap-<target:[a-z-]+>', 'route' => 'sitemap/<target>', 'suffix' => '.xml'],

        ['pattern' => 'yandex-market', 'route' => 'market/index', 'suffix' => '.xml'],

        'blog' => 'blog/post/index',
        'blog/tag/<slug:[\w\-]+>' => 'blog/post/tag',
        'blog/<id:\d+>' => 'blog/post/post',
        'blog/<id:\d+>/comment' => 'blog/post/comment',
        'blog/<slug:[\w\-]+>' => 'blog/post/category',

        'catalog' => 'shop/catalog/index',
        //['class' => 'frontend\urls\CategoryUrlRule'],
        'catalog/<id:\d+>' => 'shop/catalog/product',

        'cabinet' => 'cabinet/default/index',
        'cabinet/<_c:[\w\-]+>' => 'cabinet/<_c>/index',
        'cabinet/<_c:[\w\-]+>/<id:\d+>' => 'cabinet/<_c>/view',
        'cabinet/<_c:[\w\-]+>/<_a:[\w-]+>' => 'cabinet/<_c>/<_a>',
        'cabinet/<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => 'cabinet/<_c>/<_a>',

        ['class' => 'frontend\urls\PageUrlRule'],
        */

        '<_c:[\w\-]+>' => '<_c>/index',
        '<_c:[\w\-]+>/<id:\d+>' => '<_c>/<view>',
        '<_c:[\w\-]+>/<_a:[\w-]+>' => '<_c>/<_a>',
        '<_c:[\w\-]+>/<_a:[\w-]+>/<id:\d+>' => '<_c>/<_a>',
        '<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_c>/<_a>',
        'rockncontroll/<_c:[\w\-]+>/<_a:[\w-]+>/<id:\d+>' => 'rockncontroll/<_c>/<_a>',
    ],
];