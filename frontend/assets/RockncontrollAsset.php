<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class RockncontrollAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css53/style.css',
        'js/jQuery-autoComplete-master/jquery.auto-complete.css',
        'css/jquery-ui.css',
        'js/jPlayer-2.9.2/dist/skin/blue.monday/css/jplayer.blue.monday.css',
        'css/flags.css'

    ];
    public $js = [
        'js/jQuery-autoComplete-master/jquery.auto-complete.min.js',
        /**
         * <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
         */
        'https://www.gstatic.com/charts/loader.js',
        'https://code.highcharts.com/highcharts.js',
        'https://code.highcharts.com/modules/exporting.js',
        'js/comments.js',
        'js/jquery-cookie/jquery.cookie.js',
        'js/jQuery-autoComplete-master/jquery.auto-complete.min.js',
        'js/jquery-ui.js',
        'js/jPlayer-2.9.2/dist/jplayer/jquery.jplayer.min.js',
        'js/jPlayer-2.9.2/src/javascript/add-on/jplayer.playlist.js'
    ];
    /*
     * public $js = [
        'js/comments.js',
        'js/jquery-cookie/jquery.cookie.js',
        'js/jQuery-autoComplete-master/jquery.auto-complete.min.js',
        'js/jquery-ui.js',
        'js/jPlayer-2.9.2/dist/jplayer/jquery.jplayer.min.js',
        'js/jPlayer-2.9.2/src/javascript/add-on/jplayer.playlist.js'

    ];
    <script src="js/jPlayer-2.9.2/dist/jplayer/jquery.jplayer.min.js"></script>
    <script src="js/jPlayer-2.9.2/src/javascript/add-on/jplayer.playlist.js"></script>
     */
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\web\JqueryAsset',
        'frontend\assets\AppAsset'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

}