<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class DeutschAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'js/jQuery-autoComplete-master/jquery.auto-complete.css'

    ];
    public $js = [
        'js/jQuery-autoComplete-master/jquery.auto-complete.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\AppAsset'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
