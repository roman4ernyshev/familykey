<?php
/* @var \core\entities\Notion\Notion $word */
?>

<style>
    html {
        height: 100%;
    }
    body {
        min-height: 100%;
    }
    body{
        background-color: #0e4f23;
    }

    td, th, p, h3 {
        text-align: center;
    }

    h1 {
        font-size: 50px;
        color: white;
    }
    h1 .fa {
        font-size: 50px;
    }

    .autocomplete-suggestion{
        font-size: 1.5em;
    }

    #word{
        height: 50px;
        font-size: 22px;
    }

    .container{
        background-color:#0e4332;
        /*background: rgb(30, 29, 29) url(img/col_pattern.jpg) repeat;*/
        color: white;
        /*height: 1000px;*/
    }

    #card{
        text-align: center;
        border: 5px solid rgb(89, 96, 104);
        border-radius: 4px;
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .btn-success{
        background-color: rgb(164, 180, 195);
        border-color: rgb(69, 70, 73);
        background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(rgb(168, 182, 196)), to(rgb(0, 0, 0)));

    }
    .red{
        background-color: rgb(125, 37, 27);
        border-color: rgb(255, 42, 55);
    }
    .btn-success:hover, .btn-success:focus, .btn-success:active, .btn-success.active, .open .dropdown-toggle.btn-success {
        /*color: rgb(40, 40, 47);
        background-color: rgba(236, 231, 8, 0);*/
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }
    button{
        width: 100%;
        height: 60px;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        margin-bottom: 5px;
    }

    button p{
        font-size: 15px;
    }
    .active-button{
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }


    .btn{
        height: 50px;
    }

    .btn:hover{
    //background-color: rgb(125, 37, 27);
        border-color: rgb(255, 42, 55);
        color: rgb(255, 42, 55);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(255, 42, 55)), to(rgb(0, 0, 20)));
    }

    .thin{
        height: 35px;
        color: #f5c86b;
        background-color: rgb(125, 37, 27);
    }


</style>
<body>
<div class="container">

    <div id="card">

        <?php if($word->generated_audio_link) : ?>
        <div id="play_notion" style="display: none" >
            <audio style="display: none" id="aud_play" >
                <source src="<?=  Yii::getAlias($word->generated_audio_link); ?>" >
            </audio>
        </div>
        <?php endif; ?>

        <?php if($word->example) :?>
        <div id="play_example" style="display: none" >
            <div id="play_example" style="display: none" >
                <audio style="display: none" id="aud_play_phrase" >
                    <source src="<?=  Yii::getAlias($word->example->generated_audio_link); ?>" >
                </audio>
            </div>
        </div>
        <?php endif; ?>
        <br>
        <div id="deutsch_word" onclick="onPlay()">
            <h1><?= $word->body ?>
                <?php if($word->generated_audio_link) : ?>
                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                <?php endif; ?>
            </h1>
        </div>


        <div id="translation" >
            <h1><?= $word->answer ?></h1>
        </div>

        <?php if($word->examples) : ?>
            <?php foreach ($word->examples as $example) : ?>
                <p id="example" onclick="$('#aud_play_phrase_'+<?= $example->id ?>)[0].play()"><?= $example->description ?>
                    <?php if($example->generated_audio_link) : ?>
                        <i class="fa fa-volume-up" aria-hidden="true"></i>
                    <?php endif; ?>
                </p>
                <div id="play_example" style="display: none" >
                    <div id="play_example" style="display: none" >
                        <audio style="display: none" id="aud_play_phrase_<?= $example->id ?>" >
                            <source src="<?=  Yii::getAlias($example->generated_audio_link); ?>" >
                        </audio>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>


    </div>



    <button type="submit" class="btn-success" onclick="nextCard()" id="next"><p>Следующая карточка</p></button>
    <button type="submit" class="btn-success thin" onclick="showWords()" id="show_words"><p>Посмотреть слова</p></button>
    <button type="submit" class="btn-success thin" onclick="showStats()" id="show_stats"><p>Посмотреть статистику</p></button>

    <div id="tour">

    </div>

</div>
</body>
<script id="userId"></script>

<script>
    var word_id;


    function nextCard() {

        document.location.href = "/notion/deutsch";

    }

    function onPlay() {
        $('#aud_play')[0].play()
    }



    function showWords() {
        document.location.href = "http://37.192.187.83:888/deutsch_show.html";

    }

    function showStats() {
        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:8098/datas/get-user-stats/",
            data: "id=" + user_id,
            success: function(resp){
                $("#tour").html(resp);

            }

        });
    }
</script>
