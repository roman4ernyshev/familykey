<style>
    html {
        height: 100%;
    }
    body {
        min-height: 100%;
        background-color: #0e4f23;
    }

    td, th, p, h3 {
        text-align: center;
    }

    h1 {
        font-size: 50px;
        color: white;
    }
    h1 .fa {
        font-size: 50px;
    }

    .autocomplete-suggestion{
        font-size: 1.5em;
    }

    #word{
        height: 50px;
        font-size: 22px;
    }

    .container{
        background-color:#0e4332;
        /*background: rgb(30, 29, 29) url(img/col_pattern.jpg) repeat;*/
        color: white;
        /*height: 1000px;*/
    }

    #card{
        text-align: center;
        border: 5px solid rgb(89, 96, 104);
        border-radius: 4px;
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .btn-success{
        background-color: rgb(164, 180, 195);
        border-color: rgb(69, 70, 73);
        background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(rgb(168, 182, 196)), to(rgb(0, 0, 0)));

    }
    .red{
        background-color: rgb(125, 37, 27);
        border-color: rgb(255, 42, 55);
    }
    .btn-success:hover, .btn-success:focus, .btn-success:active, .btn-success.active, .open .dropdown-toggle.btn-success {
        /*color: rgb(40, 40, 47);
        background-color: rgba(236, 231, 8, 0);*/
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }
    button{
        width: 100%;
        height: 60px;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        margin-bottom: 5px;
    }

    button p{
        font-size: 15px;
    }
    .active-button{
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }


    .btn{
        height: 50px;
    }

    .btn:hover{
    //background-color: rgb(125, 37, 27);
        border-color: rgb(255, 42, 55);
        color: rgb(255, 42, 55);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(255, 42, 55)), to(rgb(0, 0, 20)));
    }

    .thin{
        height: 35px;
        color: #f5c86b;
        background-color: rgb(125, 37, 27);
    }


</style>
<body>
<div class="container">

    <div id="card">
        <div id="play_notion" style="display: none" ></div>
        <div id="play_example" style="display: none" ></div>

        <audio id="aud_play_phrase"></audio>

        <br>
        <div onclick="onPlay()" >
            <h1 id="body"></h1>
        </div>

        <div id="translation" style="display: none"></div>

        <div onclick="onPlayPhrase()">
            <p id="example"></p>
        </div>

    </div>


    <div id="search-form">
        <p>
            <input type="text" class="form-control" id="word" placeholder="Начните вводить перевод">
        </p>
    </div>

    <button type="submit" class="btn-success" onclick="onTest()" id="test"><p>Проверить</p></button>
    <button type="submit" class="btn-success" onclick="openTranslation()" id="open_translation"><p>Открыть перевод</p></button>
    <button type="submit" class="btn-success" onclick="nextCard()" id="next"><p>Следующая карточка</p></button>
    <button type="submit" class="btn-success thin" onclick="showWords()" id="show_words"><p>Посмотреть слова</p></button>
    <button type="submit" class="btn-success thin" onclick="showStats()" id="show_stats"><p>Посмотреть статистику</p></button>

    <div id="tour">

    </div>

</div>
</body>
<script id="userId"></script>

<script>
    var word_id;

    $(document).ready(function() {

        if(Math.floor(Date.now() / 1000)%2) console.log(Math.floor(Date.now() / 1000));

        $('#word').focus(
            function () {
                $(this).select();
            });

        $('#word').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("translations/", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

        $.ajax({
            type: "GET",
            url: "<?=$category?>/get-by-slug/",
            success: function(datas){
                var obj = JSON.parse(datas);
                $('#test').show();
                $('#word').val('');
                obj.audio_link ? $('#play_notion').html('<audio id="aud_play"><source src="'+ obj.audio_link +'" ></audio>') :
                       $('#play_notion').html('');
                $('#body').html(obj.body +' <i class="fa fa-volume-up" aria-hidden="true"></i>');
                $('#translation').html('<h1>' + obj.answer + '</h1>').hide();
                obj.example ? $('#example').html('<p>' + obj.example + '</p>') : $('#example').html('');
                obj.example_audio ?
                    $('#play_example').html('<audio id="aud_play_phrase"><source src="'+ obj.example_audio +'" >') :
                    $('#play_example').html('');


                word_id = obj.id;

                window.history.pushState({urlPath:'/'},"",'<?=$category?>/'+obj.slug)

            }

        });


    });

    function onTest() {
        var word = $("#word").val();
        var auth_form = document.getElementById('auth-form');
        var test_btn = document.getElementById('test');

        if(word === '') {
            alert('Вы не выбрали ответ!');
            return;
        }

        $.ajax({
            type: "POST",
            url: "test-translation/",
            data:'word=' + word + '&id=' + word_id,
            success: function(html){
                $("#tour").html(html);
                $("body").scrollTop(0);

                translation.style.display = 'block';
                open_translation.style.display = 'none';
                test_btn.style.display = 'none';
            }

        });

    }

    function openTranslation() {
        $.ajax({
            type: "POST",
            url: "deutsch/open-translation/",
            data:  'id=' + word_id,
            success: function(){
                var translation = document.getElementById('translation');
                var word_form = document.getElementById('word');
                var open_translation = document.getElementById('open_translation');
                var test_btn = document.getElementById('test');

                translation.style.display = 'block';
                word_form.style.display = 'none';
                open_translation.style.display = 'none';
                test_btn.style.display = 'none';
                $("body").scrollTop(0);
            }

        });

    }


    function nextCard() {

        $.ajax({
            type: "GET",
            url: "get-word-card/",
            success: function(datas){
                console.log(datas);
                var obj = JSON.parse(datas);
                $('#test').show();
                $('#word').val('');
                obj.audio_link ? $('#play_notion').html('<audio id="aud_play"><source src="'+ obj.audio_link +'" ></audio>') :
                    $('#play_notion').html('');
                obj.audio_link ? $('#body').html(obj.body +' <i class="fa fa-volume-up" aria-hidden="true"></i>') :
                    $('#body').html(obj.body);
                $('#translation').html('<h1>' + obj.answer + '</h1>').hide();
                obj.example ? $('#example').html('<p>' + obj.example + ' <i class="fa fa-volume-up" aria-hidden="true"></i></p>') : $('#example').html('');
                obj.example_audio ?
                    $('#play_example').html('<audio id="aud_play_phrase"><source src="'+ obj.example_audio +'" >') :
                    $('#play_example').html('');
                word_id = obj.id;

                window.history.pushState({urlPath:'/'},"",''+obj.slug)

            }

        });

    }

    function onPlay() {
        $('#aud_play')[0].play();
    }

    function onPlayPhrase() {
        $('#aud_play_phrase')[0].play();
    }

    function showWords() {
        document.location.href = "http://37.192.187.83:888/deutsch_show.html";
    }

    function showStats() {
        $.ajax({
            type: "GET",
            url: "get-user-stats/",
            success: function(resp){
                $("#tour").html(resp);

            }

        });
    }
</script>
