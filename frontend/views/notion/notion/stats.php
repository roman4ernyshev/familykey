<?php
/***
 * @var $word->notion \core\entities\Notion\Notion
 */
?>
<table class="table" id="bad_words">
    <tr><td colspan="4" style="color: rgb(230, 153, 33);">Подучить (попытки/очки)</td></tr>
    <?php foreach ($bad_words as $word) : ?>
        <tr>
            <td style="text-align: left;"><?= $word->notion->body?>
                (<?=$word->notion->answer?>)</td>
            <td><?=$word->cnt?></td>
            <td><?=$word->sum?></td>
        </tr>
    <?php endforeach; ?>
</table>
<table class="table" id="good_words">
    <tr><td colspan="4" style="color: rgb(230, 153, 33);">Выучил (попытки/очки)</td></tr>
    <?php foreach ($good_words as $word) : ?>
        <tr>
            <td style="text-align: left;"><?=$word->notion->body?></td>
            <td><?=$word->cnt?></td>
            <td><?=$word->sum?></td>
        </tr>
    <?php endforeach; ?>
</table>
