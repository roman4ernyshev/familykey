<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
<style>
    .table-striped > tbody > tr:nth-of-type(odd) {
        background-color: rgb(173, 31, 31);
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 0;
    }
    @media(min-width:100px) and (max-width: 550px) {

        .box-body {
            font-size: 9px;
            vertical-align: baseline;
        }


    }
</style>
<div class="row">
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                /*'rowOptions' => function (Product $model) {
                    return $model->quantity <= 0 ? ['style' => 'background: #fdc'] : [];
                },
                */
                'columns' => [
                    [
                        'attribute' => 'name',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->name);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'kkal',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->kkal);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'carbohydrates',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->carbohydrates);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'squirrels',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->squirrels);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'fats',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->fats);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'ferrum',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->ferrum);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'magnesium',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->magnesium);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'cuprum',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->cuprum);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'iodum',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->iodum);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'fluorum',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->fluorum);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'zincum',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->zincum);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'cobaltum',
                        'value' => function (\core\entities\Product\RocknControllProduct $model) {
                            return Html::encode($model->cobaltum);
                        },
                        'format' => 'raw',
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?php /*foreach ($dataProvider->getModels() as $product): ?>
        <?= $this->render('_product', [
            'product' => $product
        ]) ?>
    <?php endforeach; */?>
</div>

<div class="row">
    <div class="col-sm-6 text-left">
        <?php LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
        ])  ?>
    </div>
    <div class="col-sm-6 text-right">Showing <?= $dataProvider->getCount() ?> of <?= $dataProvider->getTotalCount() ?></div>
</div>