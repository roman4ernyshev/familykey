<?php

/* @var $this yii\web\View */
/* @var $product core\entities\Product\ProductsConst */


use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

$url = Url::to(['product', 'id' =>$product->id]);

?>

<div class="product-layout product-list col-xs-12">
    <div class="product-thumb">
        <div>
            <div class="caption">
                <h4><a href="<?= Html::encode($url) ?>"><?= Html::encode($product->name) ?></a></h4>
                <p><?= Html::encode(StringHelper::truncateWords(strip_tags($product->description), 20)) ?></p>

            </div>

        </div>
    </div>
</div>
