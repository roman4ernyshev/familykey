<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
<style>
    .table-striped > tbody > tr:nth-of-type(odd) {
        background-color: rgb(173, 31, 31);
    }
</style>
<div class="row">
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                /*'rowOptions' => function (Product $model) {
                    return $model->quantity <= 0 ? ['style' => 'background: #fdc'] : [];
                },
                */
                'columns' => [
                    /*
                [
                    'value' => function (Notion $model) {
                        return $model->mainPhoto ? Html::img($model->mainPhoto->getThumbFileUrl('file', 'admin')) : null;
                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 100px'],
                ],
                    */
                    //'id',
                    [
                        'attribute' => 'name',
                        'value' => function (\core\entities\Product\ProductsConst $model) {
                            return Html::encode($model->name);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'ferrum',
                        'value' => function (\core\entities\Product\ProductsConst $model) {
                            return Html::encode($model->ferrum);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'fats',
                        'value' => function (\core\entities\Product\ProductsConst $model) {
                            return Html::encode($model->fats);
                        },
                        'format' => 'raw',
                    ],
                    /*[
                        'attribute' => 'category_id',
                        'filter' => $searchModel->categoriesList(),
                        'value' => 'category.name',
                    ],
                    */

                    /*[
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'add-example' => function (Notion $model) {
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['add-example', 'id' => $model->id]);

                            },

                        ],
                        'template' => '{add-example}',
                    ],
                    */
                ],
            ]); ?>
        </div>
    </div>
    <?php /*foreach ($dataProvider->getModels() as $product): ?>
        <?= $this->render('_product', [
            'product' => $product
        ]) ?>
    <?php endforeach; */?>
</div>

<div class="row">
    <div class="col-sm-6 text-left">
        <?php LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
        ])  ?>
    </div>
    <div class="col-sm-6 text-right">Showing <?= $dataProvider->getCount() ?> of <?= $dataProvider->getTotalCount() ?></div>
</div>