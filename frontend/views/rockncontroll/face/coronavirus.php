<?php
/**
 * @var \core\entities\Coronovirus $data;
 * @var string $key
 */
//var_dump($key);
?>
<style>
    .coronotable {
        margin-bottom: 0;
        width: 100%;
        max-width: 100%;
    }
    .coronotable > tbody > tr{
        height: 12px;
    }

    @media (max-width: 767px) and (min-width: 320px){
        table > tbody > tr > td {
            font-size: 12px;
            padding: 0;
        }
    }

</style>
<table class="coronotable" cellpadding="0" cellspacing="0" >
    <tbody>
    <tr>
        <td style="color: #0b97c4;">Country</td>
        <td style="color: rgb(40,157,139)">TotR</td>
        <td style="color: yellow">TotC</td>
        <td style="color: #f5c451">NewC</td>
        <td style="color: #ff691a">ActC</td>
        <td style="color: #ff4114">SerC</td>
        <td style="color: #ff0c1b">TotD</td>
        <td style="color: red">NewD</td>


    </tr>
    <tr>
        <td style="color: #0b97c4;"><?=$data->country?></td>
        <td style="color: rgb(40,157,139)"><?=$data->total_recovered?></td>
        <td style="color: yellow"><?=$data->total_cases?></td>
        <td style="color: #f5c451"><?=$data->new_cases?></td>
        <td style="color: #ff691a"><?=$data->active_cases?></td>
        <td style="color: #ff4114"><?=$data->serious_critical?></td>
        <td style="color: #ff0c1b"><?=$data->total_deaths?></td>
        <td style="color: red"><?=$data->new_deaths?></td>


    </tr>
    </tbody>
</table>
