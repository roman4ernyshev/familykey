<?php
/**
 * @var int $life_day
 * @var int $temp_muenchen
 * @var int $temp_novosib
 */

?>
<script>
    var arr = [];
    var user = 8;
    $(document).ready(function() {
	// alert($.cookie('the_cookie'));

        getRandNewNewInBlock('new_new');
        getBals();
        invertRandImg('footer');

	/*
        $.ajax({
            type: "GET",
            url: "datas/show-current-radio-tracks-test/",
            success: function(html){
                $("#radio_test").html(html);
            }

        });
        $.ajax({
            type: "GET",
            url: "datas/show-current-radio-tracks-second/",
            success: function(html){
                $("#radio_second").html(html);
            }

        });
        
        $.ajax({
            type: "GET",
            url: "datas/radio-enters/",
            success: function (html) {
                $("#rad_bard").html(html);
            }
        });
	 */


	$.ajax({
            type: "GET",
            url: "datas/bard-cur-track/",
            success: function (html) {
                $("#rad_cur_bard").html(html);
            }
        });

        $.ajax({
            type: "GET",
            url: "default/show-current-task/",
            data: "user=8",
            success: function(html){
                $("#current_task").html(html);
            }

        });

        $.ajax({
            type: "GET",
            url: "tags/get/",
            success: function (html) {
                $("#random_tags").html(html);
            }
        });


        $("#show_menu").hide();
        $("#preloader").hide();
        $("#stop_item_block").parent().hide();

        $("#show_menu").click(
            function() {
                $("#menu").show();
                $("#show_menu").hide();
                $("#summary").hide();
            });

    });


</script>
<style>

    #new_new{
        line-height: 14px;
        height: 61px;
        overflow: hidden;
        padding-top: 5px;
        width: 100%;
    }
    .logo button, .footer button{
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        margin-bottom: 5px;
        height: 28px;
    }
    .logo button p{
        overflow: hidden;
        text-overflow: ellipsis;
        height: 25px;
    }
    .alert{
        margin-bottom: 10px;
        padding: 7px;
    }
    #stop_item_block{
        line-height: 15px;
    }
    #stop_item_block pre{
        background: #0a0a0a;
        color: #00aa00;
        text-align: left;
    }
    .btn-success:focus, .btn-success:active:hover, .btn-success.focus , .btn-success.active,
    .btn-success:visited, .btn-success:visited:focus, .btn-success.visited {
        color: rgb(255, 255, 255);
        background-color: rgba(42, 46, 50, 0.95);
        border-color: rgb(118, 128, 137);
    }
    .active-button{
        border-color: #0f0f0f;
    }
    #stop_btn{
        background-color: #0f0f0f;
    }
    h3,h4,h5,h6,audio{
        text-align: center;
        color: white;
    }
    #menu {
        background-image: none;
        background-color: inherit;
    }
    p {
        margin: 0 0 0px;
    }
    ul > li{
        color: #449ec4;
    }
    #tags{
        text-align: center;
    }
    #tags > p{
        color: #e9e50c;
        line-height: 22px;
        font-size: 16px;
        text-align: center;
    }
    .trading {
        height: 45px;
        overflow-y: hidden;
    }
    .can-opened{
        display: none;
    }
    #footer,#random_tags{
        text-align: center;
    }
    p > img{
        width: 100%;
    }

    .pic{
        height: 80%;
        border-radius: 25px;
        max-height: 80%;
        max-width: 100%;
        background-color: rgba(84, 84, 84, 0.2);
        opacity: 0.7;
        /**/

    }
    .css-adaptive {
        max-width: 100%;
        height: 200px;
        padding: 10px;
    }
    .balll {
        border: 1px solid red;
        border-radius: 2px 2px 2px;
        background-color: rgb(34, 33, 35);
        margin: 2px;
    }
    .wrap_word{
        overflow: hidden;
        padding-left: 15px;
        padding-right: 15px;
        word-break: break-all;
    }

</style>

<header>
    <div class="logo">
        <h1 style="font-size: 25px; color: white; margin: 0">
            <?= $life_day ?>-<?=date('z-W-M-d-D', time()+3*60*60) ?>
            </h1>

        <p id="new_new"></p>
        <span style="font-size: 10px; color: orangered" id="cur_weath"> Muenchen <?= $temp_muenchen ?>
            Novosibirsk <?= $temp_novosib ?>
        </span>


        <audio id="au_test" ></audio>
        <audio id="au_second" ></audio>
	<audio id="au_bard" ></audio>
       <br> <span id="bal"></span>
<?php /*
        <button type="submit" class="btn-success" onclick="onTest()" id="radio_test"><p style="font-size: 35px;">&infin;</p></button>
        <button type="submit" class="btn-success" onclick="onSecond()" id="radio_second"><p style="font-size: 35px;">&infin;</p></button>
        <button type="submit" class="btn active-button" onclick="stopRadio()" id="stop_btn"><span class="glyphicon glyphicon-stop"></span></button>
        <span style="font-size: 7px; width: 100%" id="coronavirus"></span>
 */ ?>
    </div>
</header>


<div class="container">
    <div class="alert alert-success">
        <p><span id="curr"></span><span id="note_remind"></span></p>
        <p id="current_task"></p>
    </div>
    <div class="alert alert-success" ><p id="stop_item_block"></p><hr style="margin-top: 1px;margin-bottom: 1px;border-top-color: rgb(40,157,139);">
        <span style="color: grey">ß ç ʌ ə æ ɑ ə ɔ ɪ ʒ ü ö ä Ö Ü Ä Å œ ã ɑ ø π ° µ ‾ ÷ ± ¬ ≤ ≥
        ≈ ≠ ≡ ≢ √ ∞ ∑ ∏ ∂ ∫ ∀ ∃ Ø ∈ ∉ ∋ ⊂ ⊃ ⊄ ⊆ ⊇ ⊕ ⊗ ⊥ ∠ ∧ ∨ ∩ ∪ € ¢ £ ₽ ¤ ¥ ħ ← ↑ → ↓ ✓
        ↔ ↕ ↵ ⇐ ⇑ ⇒ ⇓ ⇔ ⇕ ∂ α β γ δ Δ ε ζ η θ Θ ι λ μ ν ξ π ρ σ ς Σ τ υ φ χ ψ Ψ ω Ω ∇ ◊ △ ▻ ► ◁ ○ ● • ⟨ ⟩ ⌈ ⌉ ⌊ ⌋ ⊔</span></div>
    <div class="trading">
        <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>
            <div class="tradingview-widget-copyright"><a href="https://ru.tradingview.com" rel="noopener" target="_blank"><span class="blue-text">Финансовые рынки</span></a> от TradingView</div>
            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
                {
                    "symbols": [
                    {
                        "proName": "FX_IDC:EURUSD",
                        "title": "EUR/USD"
                    },
                    {
                        "description": "EUR/RUB",
                        "proName": "FOREXCOM:EURRUB"
                    },
                    {
                        "description": "USD/RUB",
                        "proName": "FX_IDC:USDRUB"
                    },
                    {
                        "description": "OILBREND",
                        "proName": "CURRENCYCOM:OIL_BRENT"
                    }
                ],
                    "colorTheme": "dark",
                    "isTransparent": false,
                    "displayMode": "regular",
                    "locale": "ru"
                }
            </script>
        </div>
        <!-- TradingView Widget END -->
    </div>
    <div id="menu">
        <?php /*
        <button type="button" class="btn btn-success btn-lg btn-block" onclick="send(user,'trading')">Котировки</button>
 */ ?>


        <?php /*
        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('covid', 'covid')" id="covid">Covid-19</button>
        <button type="button" class="btn btn-success btn-lg btn-block covid can-opened" onclick="send(user,'graf-covid')">Covid-19 Cases > 8000</button>
        <button type="button" class="btn btn-success btn-lg btn-block covid can-opened" onclick="send(user,'graf-covid-death')">Covid-19 Deaths > 500</button>
        <button type="button" class="btn btn-success btn-lg btn-block covid can-opened" onclick="send(user,'graf-covid-recovered')">Covid-19 Recovered > 1000</button>
        <button type="button" class="btn btn-success btn-lg btn-block covid can-opened" onclick="send(user,'graf-covid-new-cases')">Covid-19 New Cases</button>

        */?>
        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('today', 'today')" id="today">Сегодня</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'day-params')">Параметры</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'show-task')">Задачи</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'incomes')">Incomes</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'events')">Событие</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'eat')">Съел</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'bought')">Купил</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'bought-eur')">Купил в евро</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'add-product')">Добавить</button>
        <button type="button" class="btn btn-success btn-lg btn-block today can-opened" onclick="send(user,'yesterday-params')">Вчера</button>


        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('deal', 'deal')" id="deal">Дело</button>
        <button type="button" class="btn btn-success btn-lg btn-block deal can-opened" onclick="send(user,'mishich-deals')">Оценить Мишича</button>
        <button type="button" class="btn btn-success btn-lg btn-block deal can-opened" onclick="send(user,'deals')">Сделал</button>


        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('marker', 'marker')" id="marker">Закладки</button>
        <button type="button" class="btn btn-success btn-lg btn-block marker can-opened" onclick="send(user,'markers')">Все</button>
        <button type="button" class="btn btn-success btn-lg btn-block marker can-opened" onclick="send(user,'seo-markers')">Тема</button>
        <button type="button" class="btn btn-success btn-lg btn-block marker can-opened" onclick="send(user,'ege-markers')">JS</button>
        <button type="button" class="btn btn-success btn-lg btn-block marker can-opened" onclick="send(user,'rec-remind')">Напомнить</button>


        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('stat', 'stat')" id="stat">Статистика</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'spent')">Расходы</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'boughteur-gistoram')">Spend In Deutschland</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'water')">Wasser 3 Tage</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'cold-water')">Kalt Wasser</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'warm-water')">Warm Wasser</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'heizung')">Heizung</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'graf-weith')">Вес график</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'michael-weight')">Michael's Weight</button>
        <button type="button" class="btn btn-success btn-lg btn-block stat can-opened" onclick="send(user,'year-point')">Пятна года</button>


        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('neww', 'neww')" id="neww">Новое</button>
        <button type="button" class="btn btn-success btn-lg btn-block neww can-opened" onclick="send(user,'record-item')">Записать</button>
        <button type="button" class="btn btn-success btn-lg btn-block neww can-opened" onclick="send(user,'radio')">Радио</button>
        <button type="button" class="btn btn-success btn-lg btn-block neww can-opened" onclick="send(user,'project')">Проект</button>
        <button type="button" class="btn btn-success btn-lg btn-block neww can-opened" onclick="send(user,'copyright')">Копирайт</button>

        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('oldd', 'oldd')" id="oldd">Старое</button>
        <button type="button" class="btn btn-success btn-lg btn-block oldd can-opened" onclick="send(user,'search')">Найти</button>
        <button type="button" class="btn btn-success btn-lg btn-block oldd can-opened" onclick="send(user,'article-search')">Найти статьи</button>
        <button type="button" class="btn btn-success btn-lg btn-block oldd can-opened" onclick="send(user,'news')">Новости</button>
        <button type="button" class="btn btn-success btn-lg btn-block oldd can-opened" onclick="send(user,'clever')">ItemClever</button>
        <button type="button" class="btn btn-success btn-lg btn-block oldd can-opened" onclick="send(user,'learn-it')">ItItem</button>
        <button type="button" class="btn btn-success btn-lg btn-block oldd can-opened " onclick="send(user,'item-mitem-links')">Thought Generator</button>
        <button type="button" class="btn btn-success btn-lg btn-block oldd can-opened" onclick="send(user,'enciklop')">Энциклопеция</button>
        <button type="button" class="btn btn-success btn-lg btn-block oldd can-opened" onclick="send(user,'deferred')">Отложенное</button>



        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('creative', 'creative')" id="creative">Творчество</button>
        <button type="button" class="btn btn-success btn-lg btn-block creative can-opened" onclick="send(user,'repertoire')">Репертуар</button>
        <button type="button" class="btn btn-success btn-lg btn-block creative can-opened" onclick="send(user,'ne-zevai')">Не зевай</button>
        <button type="button" class="btn btn-success btn-lg btn-block creative can-opened" onclick="send(user,'concert')">Концерт</button>
        <button type="button" class="btn btn-success btn-lg btn-block creative can-opened" onclick="send(user,'music')">Музыка</button>
        <button type="button" class="btn btn-success btn-lg btn-block creative can-opened" onclick="send(user,'shot-novels')">Роман Там Всё</button>
        <button type="button" class="btn btn-success btn-lg btn-block creative can-opened" onclick="send(user,'published')">Опубликованное</button>


        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('study', 'study')" id="study">Учиться! три раза</button>
        <button type="button" class="btn btn-success btn-lg btn-block study can-opened" onclick="send(user,'klavaro', 1)">Klavaro</button>
        <button type="button" class="btn btn-success btn-lg btn-block study can-opened" onclick="send(user,'remember')">Запомнить</button>
        <button type="button" class="btn btn-success btn-lg btn-block study can-opened" onclick="send(user,'scheme')">Расписание Миши</button>

        <!--
        <button type="button" class="btn btn-success btn-lg btn-block" id="bal"></button>
        
        <button type="button" class="btn btn-success btn-lg btn-block opened" onclick="openButtons('football', 'football')" id="football">Футбол</button>
        <button type="button" class="btn btn-success btn-lg btn-block football can-opened" onclick="send(user,'football')">Футбол 2020-2021</button>
        <button type="button" class="btn btn-success btn-lg btn-block football can-opened" onclick="send(user,'football-bets')">Футбольные ставки 2019-2020</button>


        <button type="button" class="btn btn-success btn-lg btn-block football can-opened" onclick="send(user,'football-season', 0, 1)">Футбол 2014-2020</button>
        <input list="seasons"  class="form-control football can-opened" id="foo_season"  placeholder="Сезон">

        <datalist id="seasons">
            <option value="2014">
            <option value="2015">
            <option value="2016">
            <option value="2017">
            <option value="2018">
            <option value="2019">
        </datalist>
        -->


    </div>

    <div id="show_menu">
        <button type="button" class="btn btn-success btn-lg btn-block" onclick="closeAllOpenedButtons('can-opened')">Меню</button>
    </div>

    <div id="summary"></div>
</div>
<header class="footer" style="margin-top: 5px;">
    <div id="random_tags"></div>
    <div id="footer" >
        <img  class="pic css-adaptive"  src="<?=\yii\helpers\Url::to('@static/patrik.gif')?>?cash='.time().'"/>
    </div>
    <button type="submit" class="btn-success wrap_word" onclick="onRemoteBard()" id="rad_cur_bard"></button>
    <p id="rad_bard" ></p>
  <p style="text-align: center; font-size: 18px">©Rock-N-ContRoll</p>
</header>

<script>

    setInterval(function () {
	getRandNewNewInBlock('new_new');
	bardRadio();
    }, 20000);



    setInterval(function () {
        $.ajax({
            type: "GET",
            url: "/rockncontroll/datas/weather/",
            success: function (html) {
                $("#cur_weath").html(html);
            }
        });
    }, 50000);


    /*

    setInterval(function () {
        getCoronovirus('coronavirus');
    }, 30000);
    */


    setInterval(function () {
        invertRandImg('footer');
        getTags();
    }, 100000);


    function invertRandImg(block) {
        $.ajax({
            type: "GET",
            url: "face/invert-rand-img/",
            success: function (html) {
                $("#" + block).html(html);
            }
        });
    }

    function getTags(){
        $.ajax({
            type: "GET",
            url: "tags/get/",
            success: function (html) {
                $("#random_tags").html(html);
            }
        });
    }

    function showTagItems(id){
        $.ajax({
            type: "GET",
            url: "tags/get-items",
            data: "id="+id,
            success: function(html){
                $("#summary").html(html).show();
            }
        });
    }

    function bardRadio(){
        $.ajax({
            type: "GET",
            url: "datas/bard-cur-track/",
            success: function (html) {
                $("#rad_cur_bard").html(html);
            }
        });
    }

    //CurrWeatherActual
    function getCurrWeatherActual(block) {
        $.ajax({
            type: "GET",
            url: "face/curr-weather-actual/",
            success: function (html) {
                $("#" + block).html(html);
            }
        });
    }

    function getRandNewNewInBlock(block) {
        $.ajax({
            type: "GET",
            url: "face/rand-new-new/",
            success: function (html) {
                $("#" + block).html(html);
            }
        });
    }

    function getCoronovirus(block) {
        $.ajax({
            type: "GET",
            url: "face/coronavirus/",
            success: function (html) {
                $("#" + block).html(html);
            }
        });
    }

    function getBals(){
        $.ajax({
            type: "GET",
            url: "face/form-balll/",
            success: function (html) {
                $("#bal").html(html);       
            }
        });
    }


    function markItItem(user, item) {
        $.ajax({
            type: "GET",
            url: "default/mark-it-item",
            data: "user="+user+"&item="+item,
            success: function (html) {
                $("#neww_itemm").html(html);

            }
        });
    }

    var player_second = document.getElementById('au_second');
    var player_test = document.getElementById('au_test');
    var player_bard = document.getElementById('au_bard');

    var radio_test = document.getElementById('radio_test');
    var radio_second = document.getElementById('radio_second');
    var radio_bard = document.getElementById('rad_cur_bard');

    var stop_btn = document.getElementById('stop_btn');

    var canal = '';
    stop_btn.style.display = 'none';

    // var test_button = document.getElementById('radio_test');

    var radioTestClasses = document.getElementById("radio_test").classList;
    var radioSecondClasses = document.getElementById("radio_second").classList;

    setTimeout(function run() {

	/*    
        $.ajax({
            type: "GET",
            url: "/rockncontroll/datas/show-current-radio-tracks-test/",
            success: function(html){
                if($("#radio_test").hasClass('act')) {
                    $("#radio_test").html(html).children('p').css('height', '45px');
                }
                else $("#radio_test").html(html).children('p').css('height', '20px');
            }

        });
        $.ajax({
            type: "GET",
            url: "/rockncontroll/datas/show-current-radio-tracks-second/",
            success: function(html){
                if($("#radio_second").hasClass('act')) {
                    $("#radio_second").html(html).children('p').css('height', '45px');
                }
                else $("#radio_second").html(html).children('p').css('height', '20px');
            }

        });


        $.ajax({
            type: "GET",
            url: "datas/radio-enters/",
            success: function (html) {
                $("#rad_bard").html(html);
            }
    });
	 */
        //BardCurTrack
        $.ajax({
            type: "GET",
            url: "datas/bard-cur-track/",
            success: function (html) {
                $("#rad_cur_bard").html(html);
            }
        });

        setTimeout(run, 10000);

    }, 10000);



    function onTest(){

        canal = 'test';
        player_test.src = 'http://37.192.187.83:10088/test_mp3';
        //player_test.src = '<?=\Yii::getAlias('@radio_test')?>';
        player_test.play();

        player_second.pause();
        player_bard.pause();
        player_second.src = '';
        player_bard.src = '';
        radio_second.style.display = 'none';

        radio_test.style.height = '50px';
        radio_test.setAttribute('onclick', 'stopRadio()');
        radio_test.classList.add('act');


    }

    function onSecond(){

        canal = 'second';
        player_second.src = 'http://37.192.187.83:10088/second_mp3';
        //player_second.src = '<?=\Yii::getAlias('@radio_second')?>';
        player_second.play();

        player_test.pause();
        player_test.src = '';
        player_bard.pause();
        player_bard.src = '';
        radio_test.style.display = 'none';

        radio_second.style.height = '50px';
        radio_second.setAttribute('onclick', 'stopRadio()');
        radio_second.classList.add('act');

    }

    function onRemoteBard(){

        canal = 'bard';
        //player_bard.src = 'http://88.212.253.193:8000/test';
        player_bard.src = '<?=\Yii::getAlias('@radio_test')?>';
        player_bard.play();

        player_test.pause();
        player_test.src = '';

        player_second.pause();
        player_second.src = '';

        radio_bard.setAttribute('onclick', 'stopRadio()');
        radio_bard.classList.add('act');


    }


    function stopRadio() {

        canal = '';
        player_second.pause();
        player_second.src = '';
        player_test.pause();
        player_test.src = '';
        player_bard.pause();
        player_bard.src = '';


        radio_test.style.height = '28px';
        radio_second.style.height = '28px';
        radio_bard.style.height = '28px';

        radio_test.setAttribute('onclick', 'onTest()');
        radio_test.children[0].style.height = '20px';
        radio_test.classList.remove('act');

        radio_second.setAttribute('onclick', 'onSecond()');
        radio_second.children[0].style.height = '20px';
        radio_second.classList.remove('act');

        radio_bard.setAttribute('onclick', 'onRemoteBard()');
        radio_bard.children[0].style.height = '20px';
        radio_bard.classList.remove('act');

        radio_test.style.display = 'block';
        radio_second.style.display = 'block';

    }

    function changeActiveClass(elAddClass, elRemClass_1) {
        if(elAddClass.contains('active-button')) return;
        else elAddClass.add('active-button');
        if(elRemClass_1.contains('active-button')) elRemClass_1.remove('active-button');
    }

    function siteBlockListener(site, block, ip_json) {
        // console.log(ip_json);

        new Fingerprint2().get(function(result, components){
            //console.log(result); //a hash, representing your device fingerprint
            //console.log(components); // an array of FP components

            $.ajax({
                url: "http://37.192.187.83:8098/datas/come-in/",
                type:'POST',
                data:'components=' + JSON.stringify(components) +
                    '&hash=' + result +
                    '&site='+ site +'&block=' + block +
                    '&ip_json=' + JSON.stringify(ip_json)
                // success: function(html){
                //    $("#dev_res").html(html);
                //}
            });
        });
    }

    function searchMusic() {
        var album = $("#album").val();

        if(album == '') {
            alert('Введите название альбома или исполнителя!');
            return;
        }

        stopRadio();

        $.ajax({
            type: "POST",
            url: "http://37.192.187.83:8098/datas/get-album/",
            data: "album="+album,
            success: function(html){
                $("#radio_block").html(html);
            }

        });
        radio_back.style.display = 'block';

    }

    function learned(user=8) {
        $.ajax({
            type: "GET",
            url: "default/learned/",
            data: "user="+user,
            success: function(){

                $("#menu").show();
                $("#show_menu").hide();
                $("#summary").hide();
            }

        });
    }
    function publisch(id){
        $.ajax({
            type: "GET",
            url: "default/publish-isviliny",
            data: "id=" + id,
            success: function (html) {
                $("#pub_"+id).html(html);
            }
        });
    }


    function openButtons(css_class, this_id) {
        $('#'+this_id).hide();
        $('.'+css_class).each(function(i,elem) {
            //console.log(elem);
            $(this).css({'backgroundColor' : '#000'}).toggle(true);
        });
    }

    function closeAllOpenedButtons(css_class) {
        $('.'+css_class).each(function(i,elem) {
            $(this).toggle(false);
        });
        $('.opened').each(function(i,elem) {
            $(this).toggle(true);
        });
    }


    function send(user=8, controller_str, test = 0, season = 0) {

        $("#summary").html("" +
            "<div id='preloader'> " +
            "<img  src='<?php dirname(__DIR__) ?>/image/preloader.gif'  alt='preloader' /></div>");


        if(test===1) {
            $.ajax({
                type: "GET",
                url: "test/"+controller_str+"/",
                data: "user="+user,
                success: function(html){
                    $("#summary").html(html);
                }

            });
        }

        else if(test===2) {
            $.ajax({
                type: "GET",
                url: "articles",
                data: "user="+user,
                success: function(html){
                    $("#summary").html(html);
                }

            });
        }

        else if(season){

            let seas= $("#foo_season").val();
            $.ajax({
                type: "GET",
                url: "default/"+controller_str+"/",
                data: "user="+user+"&season="+seas,
                success: function(html){
                    $("#summary").html(html);
                }

            });
        }

        else {
            $.ajax({
                type: "GET",
                url: "default/"+controller_str+"/",
                data: "user="+user,
                success: function(html){
                    $("#summary").html(html);
                }

            });
        }


        $("#show_menu").show();
        $("#summary").show();
        $("#menu").hide();

    }



</script>

