<style>
  pre {
      background: #0a0a0a;
      color: #00aa00;
      text-align: left;
  };
  p > p{
      color: #e9e50c;
  }
  ul > li{
      color: #449ec4;
  }
</style>
<div id="tags">
    <?php foreach ($items as $item) : ?>
	<hr><h3 style="color:tomato"><?= $item->title ?></h3>
	    <p style="color:#e9e50c"> <?= nl2br($item->text) ?></p>
	    <p style="color:aquamarine"> <?= $item->source->title ?> :: <?= $item->source->author->name ?></p>
	    <?php if($item->img) : ?>
            <img id="ima" style="width: 100%" src="http://88.85.67.159:8082/<?= str_replace("http://37.192.187.83:8014/", "", str_replace("http://37.192.187.83:10034/", "", $item->img)) ?>">
	    <?php endif ?>
        <?php if($item->audio_link) : ?>
            <audio controls="controls" >
                <source src="<?=\yii\helpers\Url::to('@music/'.$item->audio_link)?>" type='audio/mpeg'>
            </audio>
        <?php endif; ?>
	    <?php if($item->old_data) :  ?>
            <p style="color:orange"><?= $item->old_data ?></p>
        <?php endif ?>
    <?php endforeach; ?>
</div>
