<style>
    html {
        height: 100%;
    }
    body {
        min-height: 100%;
    }
    body{
        background-color: rgba(84, 84, 84, 0.78);
    }

    td, th, p, h3 {
        text-align: center;
    }
    a{
        cursor: pointer;
    }

    #deutsch_word > h1, #translation > h1{
        color: #9a9a9a;
    }

    #r_phrase > h4, #d_phrase > h4, #r_phrase > h3, #d_phrase > h3{
        color: #a3aaa5;
    }

    h3{
        font-size: medium;
    }

    .container{
        background-color: rgba(8, 8, 8, 0.78);
        /*background: rgb(30, 29, 29) url(img/col_pattern.jpg) repeat;*/
        color: white;
        /*height: 1000px;*/
    }

    #card{
        text-align: center;
        border: 5px solid rgb(89, 96, 104);
        border-radius: 4px;
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .btn-success{
        background-color: rgb(164, 180, 195);
        border-color: rgb(69, 70, 73);
        background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(rgb(168, 182, 196)), to(rgb(0, 0, 0)));

    }
    .red{
        background-color: rgb(220, 227, 222);
        border-color: rgb(255, 42, 55);
    }
    .btn-success:hover, .btn-success:focus, .btn-success:active, .btn-success.active, .open .dropdown-toggle.btn-success {
        /*color: rgb(40, 40, 47);
        background-color: rgba(236, 231, 8, 0);*/
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }
    button{
        width: 100%;
        height: 60px;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        margin-bottom: 5px;
    }

    button p{
        font-size: 15px;
    }
    .active-button{
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }

    pre{
        background: #0a0a0a;
        color: #00aa00;
        text-align: left;
    }

    .btn{
        height: 50px;
    }

    .btn:hover{
    //background-color: rgb(125, 37, 27);
        border-color: rgb(255, 42, 55);
        color: rgb(255, 42, 55);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(255, 42, 55)), to(rgb(0, 0, 20)));
    }

    .thin{
        height: 35px;
        color: rgb(162, 162, 163);
        background-color: rgb(220, 227, 222);
    }



</style>
<body>
<div class="container">

    <div id="card"></div>

    <div id="auth-form">
        <p id="auth">
            <input type="text" class="form-control" id="nick" placeholder="введите Ник">
        </p>
        <button type="submit" class="btn-success" onclick="logAuth()" id="log_btn"><p>Войти</p></button>
    </div>

    <div id="search-form">
        <p>
            <input type="text" class="form-control" id="word" placeholder="Начните вводить перевод">
        </p>
    </div>

    <button type="submit" class="btn-success" onclick="onTest()" id="test"><p>Проверить</p></button>
    <button type="submit" class="btn-success" onclick="openTranslation()" id="open_translation"><p>Открыть перевод</p></button>

    <div id="item"></div>

    <button type="submit" class="btn-success" onclick="nextCard()" id="next"><p>Следующая карточка</p></button>
    <button type="submit" class="btn-success thin" onclick="showWords()" id="show_words"><p>Посмотреть слова</p></button>
    <button type="submit" class="btn-success thin" onclick="showStats()" id="show_stats"><p>Посмотреть статистику</p></button>

    <div id="tour">

    </div>

</div>
</body>
<script id="userId"></script>

<script>


    $(document).ready(function() {

        //if(Math.floor(Date.now() / 1000)%2) console.log(Math.floor(Date.now() / 1000));
        $('#word').focus(
            function () {
                $(this).select();
            });

        var word_form = document.getElementById('word');
        var open_translation = document.getElementById('open_translation');
        var test_btn = document.getElementById('test');
        var card = document.getElementById('card');
        var next = document.getElementById('next');
        var auth_form = document.getElementById('auth-form');


        word_form.style.display = 'none';
        open_translation.style.display = 'none';
        test_btn.style.display = 'none';
        card.style.display = 'none';
        next.style.display = 'none';


        if($.cookie('the_cookie') == 'Рома' || $.cookie('the_cookie') == 'мама' || $.cookie('the_cookie') == 'Мишич') {
            var user = $.cookie('the_cookie');
            auth_form.style.display = 'none';
            $.ajax({
                type: "GET",
                url: "/rockncontroll/datas/login/",
                data: "name=" + user,
                success: function(resp){
                    $("#userId").html('var user_id = ' + resp );

                    $.ajax({
                        type: "GET",
                        url: "/rockncontroll/datas/get-marks/",
                        data: "id=" + resp,
                        success: function(resp){
                            $("#tour").html(resp);

                        }

                    });

                    $.ajax({
                        type: "GET",
                        url: "/rockncontroll/datas/get-deutsch-item/",
                        data: "id=" + resp,
                        success: function(resp){
                            $("#item").html(resp);

                        }

                    });

                    $.ajax({
                        type: "GET",
                        url: "/rockncontroll/datas/get-word-card/",
                        data: 'user=' + user,
                        success: function(html){
                            $("#card").html(html);
                            if(revers) {

                                $('#word').autoComplete({
                                    minChars: 3,
                                    source: function (term, suggest) {
                                        term = term.toLowerCase();

                                        $.getJSON("/rockncontroll/datas/rus-translations/", function (data) {
                                            //console.log(data);
                                            choices = data;
                                            var suggestions = [];
                                            for (i = 0; i < choices.length; i++)
                                                if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                                            suggest(suggestions);

                                        }, "json");

                                    }
                                });
                            }

                            else {

                                $('#word').autoComplete({
                                    minChars: 3,
                                    source: function (term, suggest) {
                                        term = term.toLowerCase();

                                        $.getJSON("/rockncontroll/datas/translations/", function (data) {
                                            console.log(data);
                                            choices = data;
                                            var suggestions = [];
                                            for (i = 0; i < choices.length; i++)
                                                if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                                            suggest(suggestions);

                                        }, "json");

                                    }
                                });
                            }
                        }

                    });

                }

            });

            word_form.style.display = 'block';
            open_translation.style.display = 'block';
            test_btn.style.display = 'block';
            card.style.display = 'block';
            next.style.display = 'block';
        }


    });

    function ticket(word_id) {
        var send = confirm("Хотите послать заявку?");
        if(send){
            $.ajax({
                type: "GET",
                url: "/rockncontroll/datas/add-ticket-word/",
                data: "id=" + word_id,
                success: function(resp){
                    $("#tick_"+word_id).replaceWith("<p>" + resp + "</p>");
                }
            });
        }
        else $("#tick_"+word_id).replaceWith("<p>Тикет не отправлен</p>");

    }

    function onTest() {
        var word = $("#word").val();
        var auth_form = document.getElementById('auth-form');
        var test_btn = document.getElementById('test');

        auth_form.style.display = 'none';
        translation.style.display = 'block';
        open_translation.style.display = 'none';
        test_btn.style.display = 'none';

        var r_phrase = document.getElementById('r_phrase');
        var d_phrase = document.getElementById('d_phrase');
        var deutsch_word = document.getElementById('deutsch_word');

        if(deutsch_word) deutsch_word.style.display = 'block';
        if(r_phrase) r_phrase.style.display = 'block';
        if(d_phrase) d_phrase.style.display = 'block';

        //console.log(revers);

        if(word == '') {
            alert('Вы не выбрали ответ!');
            return;
        }

        console.log(12);

        //$.post( "http://37.192.187.83:10033/rockncontroll/datas/test-translation/", { word: word, id: word_id} );

        $.ajax({
            type: "POST",
            url: "/rockncontroll/datas/test-translation",
            data:'word=' + word +
                '&id=' + word_id +
                '&reverse=' + revers +
                '&user_id=' + user_id,
            /*
              app.use(function(req, res, next) {
              res.header("Access-Control-Allow-Origin", "*");
              res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
              res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
              next();
            });
            */
            success: function(html){
                $("#tour").html(html);
                $("body").scrollTop(0);
            }
        });
    }

    function openTranslation() {
        $.ajax({
            type: "POST",
            url: "/rockncontroll/datas/test-translation-fail/",
            data:
                'id=' + word_id +
                '&user_id=' + user_id,
            success: function(){
                var translation = document.getElementById('translation');
                var word_form = document.getElementById('word');
                var open_translation = document.getElementById('open_translation');
                var test_btn = document.getElementById('test');
                var r_phrase = document.getElementById('r_phrase');
                var d_phrase = document.getElementById('d_phrase');
                var deutsch_word = document.getElementById('deutsch_word');

                if(deutsch_word) deutsch_word.style.display = 'block';
                if(r_phrase) r_phrase.style.display = 'block';
                if(d_phrase) d_phrase.style.display = 'block';

                translation.style.display = 'block';
                word_form.style.display = 'none';
                open_translation.style.display = 'none';
                test_btn.style.display = 'none';
                $("body").scrollTop(0);
            }

        });

    }

    function logAuth(){
        var nick = $("#nick").val();
        $.cookie('the_cookie', nick, { expires: 90 });
        document.location.reload();
    }

    function nextCard() {
        if( document.getElementById('test').style.display == 'block'){
            $.ajax({
                type: "POST",
                url: "/rockncontroll/datas/test-translation-fail/",
                /*
                  response.setHeader("Access-Control-Allow-Origin", "*");
                    response.setHeader("Access-Control-Allow-Credentials", "true");
                    response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                    response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers,
                    Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
                    headers: {
                        'Authorization':'Basic xxxxxxxxxxxxx',
                        'X-CSRF-TOKEN':'xxxxxxxxxxxxxxxxxxxx',
                        'Content-Type':'application/json'
                    },

                headers: {
                    'Access-Control-Allow-Origin': '*',
                    //"Access-Control-Allow-Credentials": "true",
                    //"Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
                    //"Access-Control-Allow-Headers": "Access-Control-Allow-Headers,Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
                },
                 */

                data:
                    'id=' + word_id +
                    '&user_id=' + user_id,
                success: function(){
                    //console.log('gg ');
                    document.location.reload();
                }

            });
        }

        else {
            $.ajax({
                type: "POST",
                url: "/rockncontroll/datas/add-act-ball/",
            });
            document.location.reload();
        }

    }

    function onPlay() {

        var aud_play = document.getElementById('aud_play');
        aud_play.play();

    }

    function onPlayPhrase() {
        var aud_play_phrase = document.getElementById('aud_play_phrase');
        aud_play_phrase.play();
    }

    function showWords() {
        document.location.href = "/deutsch_show.html";

    }

    function showStats() {
        $.ajax({
            type: "GET",
            url: "/rockncontroll/datas/get-user-stats/",
            data: "id=" + user_id,
            //data: 'user=' + user,
            success: function(resp){
                $("#tour").html(resp);

            }

        });
    }

</script>