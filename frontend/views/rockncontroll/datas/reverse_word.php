<?php if($word->audio_link) : ?>
    <audio style="display: none" id="aud_play" >
        <source src="http://37.192.187.83:10080/<?=$word->audio_link?>" >
    </audio>
    <audio style="display: none" id="aud_play_phrase" >
        <source src="http://37.192.187.83:10080/<?=$word->audio_phrase_link?>" >
    </audio>
<?php endif; ?>
<br>
<div id="translation">
    <h1><?= $word->d_word_translation ?></h1>
    <span style="color: rgb(162, 162, 163);"><?= \Yii::$app->redis->get('deutch_trans_'.$user);?></span>
</div>
<div id="deutsch_word" onclick="onPlay()" style="display: none">
    <h1><?= $word->d_word ?> <i class="fa fa-volume-up" aria-hidden="true"></i></h1>
    <h4><?= $word->d_word_transcription ?></h4>
    <span style="color: rgb(162, 162, 163);"><?= \Yii::$app->redis->get('remide_deutch_word_'.$user);?></span>
</div>
<div id="r_phrase">
    <h4><?= $word->d_phrase_translation ?></h4>
</div>
<div id="d_phrase" onclick="onPlayPhrase()" style="display: none">
    <h3><?= $word->d_phrase ?> <i class="fa fa-volume-up" aria-hidden="true"></i></h3>
    <h4><?= $word->d_phrase_transcription ?></h4>
</div>
<a id="tick_<?= $word->id ?>" class="btn-success thin" onclick="ticket(<?= $word->id ?>)" ><p>Тикет по карточке</p></a>
<script>
    var word_id = <?= $word->id ?>;
    var revers = 1;
</script>