<table class="table" id="bad_words">
    <tr><td colspan="4" style="color: rgb(230, 153, 33);">Подучить (попытки/очки)</td></tr>
    <?php foreach ($bad_words as $word) : ?>
        <tr>
            <td style="text-align: left;"><?=$word->word->d_word?>
            (<?=$word->word->d_word_translation?>)</td>
            <td><?=$word->cnt?></td>
            <td><?=$word->sum?></td>
            <td id="suc_<?=$word->word->id?>"><input type="radio" name="reper" onclick="addToReminder(<?= $word->word->id ?>, <?= $user_id ?>)"  /> </td>
        </tr>
    <?php endforeach; ?>
</table>
<table class="table" id="good_words">
    <tr><td colspan="4" style="color: rgb(230, 153, 33);">Выучил (попытки/очки)</td></tr>
    <?php foreach ($good_words as $word) : ?>
        <tr>
            <td style="text-align: left;"><?=$word->word->d_word?></td>
            <td><?=$word->cnt?></td>
            <td><?=$word->sum?></td>
        </tr>
    <?php endforeach; ?>
</table>
<script>
    function addToReminder(word_id, user_id) {
        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:10033/rockncontroll/datas/add-remider-word/",
            data: "id=" + word_id + "&user_id=" + user_id,
            success: function(resp){
                $("#suc_"+word_id).html(resp);
            }
        });
    }
</script>
