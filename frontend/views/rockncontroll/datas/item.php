<?php
/**
 * @var $item \core\entities\Rockncontroll\Items
 * @var $eng \core\entities\Rockncontroll\Items
 * @var $it \core\entities\Rockncontroll\Items
 * @var $enc \core\entities\Rockncontroll\Items
 */
?>
<style>
    img{
       width: 100%;
    }
    p > img{
        width: 100%;
    }

</style>
<div style="text-align: left; color: #edf6ef; line-height: normal; font-size: larger" ><?=$item->text?></div>
<hr>
<div style="text-align: left; color: #edf6ef; line-height: normal; font-size: larger"><?=nl2br($eng->text)?></div>
<hr>
<div style="text-align: left; color: #edf6ef; line-height: normal; font-size: larger">
    <p> <?= nl2br($it->text) ?></p>
    <p style="color:aquamarine"> <?= $it->source->title ?> :: <?= $it->source->author->name ?></p>
    <?php if($it->img) : ?>
        <img id="ima" style="width: 100%" src="http://88.85.67.159:8082/<?= str_replace("http://37.192.187.83:8014/", "", str_replace("http://37.192.187.83:10034/", "", $it->img)) ?>">
    <?php endif ?>
</div>
<hr>
<div style="text-align: left; color: #edf6ef; line-height: normal; font-size: larger">
    <p style="color:#e9e50c"> <?= nl2br($enc->text) ?></p>
    <p style="color:aquamarine"> <?= $enc->source->title ?> :: <?= $enc->source->author->name ?></p>
    <?php if($enc->img) : ?>
        <img id="ima" style="width: 100%" src="http://88.85.67.159:8082/<?= str_replace("http://37.192.187.83:8014/", "", str_replace("http://37.192.187.83:10034/", "", $enc->img)) ?>">
    <?php endif ?>
</div>

