<style>
    .alphabet{
        line-height: normal;
        text-align: center;
    }
    .alphabet a{
        color: #3a6baa;
        cursor: pointer;
    }
    .alphabet a:hover{
        color: #57aa39;
    }
</style>
<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function(){

        $(".accord h3:first").addClass("active");

        $(".accord p").hide();

        $(".accord h3").click(function() {

            $(this).next("p").slideToggle("slow").siblings("p:visible").slideUp("slow");

            $(this).toggleClass("active");

            $(this).siblings("h3").removeClass("active");
        });
    });

    function searchAlbumById(id) {

        $.ajax({
            type: "POST",
            url: "http://37.192.187.83:10033/rockncontroll/datas/get-album-by-id/",
            data: "id="+id,
            success: function(html){
                $("#radio_block").html(html);
                $(window).scrollTop(0);
            }

        });

    }

    function toEl(id) {
        $('body').animate({
            scrollTop: $('#'+id).offset().top
        }, 2000);
    }

    //]]>
</script>
<div class="alphabet">
    <?php  foreach ($authors as $author):  ?>
        <a id="<?=$author->id?>"
           onclick="document.getElementById('author_<?=$author->id?>').scrollIntoView()"
           > |
            <?=$author->name?>
        </a>
    <?php endforeach; ?>
</div>

<div class="accord">
    <?php  foreach ($authors as $author):  ?>
        <h3 id="author_<?=$author->id?>" style="cursor: pointer"><img src="css/blank.gif" class="flag flag-<?=$author->country->iso_code?>" alt="" />
            <?=$author->name?> </h3>
        <p>
            <?php foreach ($author->sources as $source) : ?>
                <span onclick="searchAlbumById(<?=$source->id?>)" style="cursor: pointer"><?= $source->title ?></span>  <br>
            <?php endforeach; ?>
        </p>

    <?php endforeach; ?>
</div>
