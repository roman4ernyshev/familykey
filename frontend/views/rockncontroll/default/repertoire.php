<script>
    var user = <?= (isset($user->id)) ? $user->id : 8 ?>;
    $(document).ready(function() {
        $(".accord h3:first").addClass("active");

        $(".accord span").hide();

        $(".accord h3").click(function() {

            $(this).next("span").slideToggle("slow").siblings("span:visible").slideUp("slow");

            $(this).toggleClass("active");

            $(this).siblings("h3").removeClass("active");

        });

    });

    function scrollText(id) {
        var el = document.getElementById('sn_'+id);
        el.scrollIntoView();
    }

    function scrollToBottom(id) {
        var el = document.getElementById('bot_'+id);
        el.scrollTop = el.scrollHeight;

        el.scrollIntoView({
            behavior: "smooth", // or "auto" or "instant"
            block: "end" // or "end"
        });

    }

    function autocompl(id) {

        $('#id_' + id).autoComplete({
            minChars: 5,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/songs/?term="+term, function (data) {

                    choices = data;
                    //console.log(data);
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

    }

    function autocomplConc(id) {

        $('#concert_' + id).autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/concerts", function (data) {

                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

    }

    function saveNextSong(user, id) {

        $.ajax({
            type: "GET",
            url: "default/save-next-song/",
            data: "user="+user+"&id="+id,
            success: function(html){
                $("#res").html(html);
            }

        });

    }
    
    function frash(user) {
        $.ajax({
            type: "GET",
            url: "default/frash/",
            data: "user="+user,
            success: function(html){
                $("#res_qu").html(html);
            }

        });
        
    }
    
    function add_original(id) {
        var title = $("#id_" + id).val();
        //alert(title);
        $.ajax({
            type: "GET",
            url: "default/add-original/",
            data: "user="+user+"&id="+id+"&title="+title,
            success: function(html){
                $("#res_orig_" + id).html(html);
            }

        });
    }
    
    function bind_concert(id) {
        var concert = $("#concert_" + id).val();
        //alert(title);
        $.ajax({
            type: "GET",
            url: "default/bind-concert/",
            data: "user="+user+"&id="+id+"&concert="+concert,
            success: function(html){
                $("#res_conc_" + id).html(html);
            }

        });
        
    }

    function edit(id) {

        window.location = "http://88.85.67.159:8093/rockncontroll/item/update/"+id;

        //var txt = document.getElementById("text_edited_" + id).innerHTML;

        //alert(txt); exit;

        /*$.ajax({
            type: "GET",
            url: "rockncontroll/default/edit-item-text",
            data:{edited:txt,user:user,id:id},//параметры запроса
            //data: "edited=" + txt + "&user=" + user + "&id=" + id,
            success: function (html) {
                $("#res").html(html);

            }

        });
        */
    }

    function removeFromPlaylist(song_id, concert_id) {

        $.ajax({
            type: "GET",
            url: "default/remove-from-concert/",
            data: "user="+user+"&id="+song_id+"&concert="+concert_id,
            success: function(html){
                $("#remove_req_"+song_id+concert_id).html(html);
            }

        });

    }

    function showText(id) {
        document.getElementById("txt_"+id).style.height = "auto";
    }
</script>
<style>
    .accord{
        text-align: center;
    }
    .song-name{
        font-size: 18px;
        color: rgb(255, 250, 240);
        cursor: pointer;

    }
    .song-text{
        font-size: 16px;
        color: rgb(255, 253, 150);
        width: 100%;

    }
    .block{
        border: 4px groove rgb(255, 215, 0);
        /*background: rgba(19, 19, 38, 0.94);  */
        margin-bottom: 10px;
        border-radius: 5px;
        width: 100%;
        color: rgb(225, 238, 96);
        font-size: 16px;
        line-height: normal;
    }
    h4{
        color: yellow;
    }

    .vorstel{
        width: 100%;
        overflow: hidden;
        padding: 5px 10px;
        font-size: 14px;
        color: rgb(245, 222, 179);
        text-align: center;
        line-height: 1.2;
    }
    .thougth{
        color: rgb(139, 154, 245);
        font-size: 10px;
    }
    .yell{
        cursor: pointer;
        color: rgb(245, 200, 107);
    }
    img {
        max-width: 100%;
        height: auto;
    }
</style>

<div id="res"></div>
<div class="accord">

    <?php // var_dump($thoughts);
    //echo $thoughts[rand(0,count($thoughts-1))]->text;  ?>
    <h4>Лимерик</h4>
    <hr>
    <?php if(isset($limerik)): ?>
    <h6><?=$limerik->title?></h6>
    <p style="color: white;"><?=nl2br(strip_tags($limerik->text))?></p>
    <?php endif; ?>

    <h4>Повторяем</h4>
    <hr>

    <?php if(isset($repeats)): ?>

    <?php foreach ($repeats as $repeat):  $isImgRelative = \yii\helpers\Url::isRelative($repeat->item->img); ?>

        <h3 class="song-name" id="sn_<?=$repeat->item->id?>"
            onclick="scrollText(<?=$repeat->item->id?>)">  <?= $repeat->item->title ?> <br>

        </h3>

        <span class="song-text" >
              <?php /* if($repeat->item->original_song_id) : ?>

                  <audio controls="controls" >
                     <source src="http://37.192.187.83:10081/<?=$repeat->item->original->link?>" >
                  </audio>
                  <audio controls="controls" >
                        <source src="<?=\yii\helpers\Url::to('@music/'.$repeat->item->audio_link)?>" type='audio/mpeg'>

                    </audio>
                  <br>

              <?php else : ?>

                  <form class="form-inline center" role="form" id="form-add-original">
                        <input type="text" class="form-control" id="id_<?=$repeat->item->id?>" onfocus="autocompl(<?=$repeat->item->id?>)" placeholder="Оригигнал">
                        <br>
                        <button type="button" class="btn btn-success" onclick="add_original(<?=$repeat->item->id?>)" >Добавить оригинал!</button>
                  </form>
                  <br>
                  <p id="res_orig_<?=$repeat->item->id?>"></p>

              <?php endif; */?>
            <?php if($repeat->item->audio_link) : ?>

                <audio controls="controls" >
                     <source src="http://88.85.67.159:8082/<?=$repeat->item->audio_link?>" >
                  </audio>
                <br>

            <?php endif; ?>
            <?php if($repeat->item->img) : ?>
                <?php if(!$isImgRelative): ?>
                    <img id="ima" style="width: 100%" src='<?=\yii\helpers\Url::to($repeat->item->img)?>' />
                <?php else : ?>
                    <img id="ima" style="width: 100%" src="<?=\yii\helpers\Url::to('@static/'.$repeat->item->img)?>"/>
                <?php endif; ?>
            <?php endif; ?>


            <div class="block" id="text_edited_<?=$repeat->item->id?>" onclick="scrollToBottom(<?=$repeat->item->id?>9)">
                <?=nl2br(strip_tags($repeat->item->text, '<br><img><p>'))?>
            </div>
            <button type="button" class="btn btn-success" onclick="edit(<?=$repeat->item->id?>)" id="bot_<?=$repeat->item->id?>9">Редактировать!</button><br>
            <button type="button" class="btn btn-success" onclick="removeFromPlaylist(<?=$repeat->item->id?>, 9)" >Удалить из сырого!</button>
            <div id="remove_req_<?=$repeat->item->id?>9"></div>


        </span>
        <hr>
    <?php endforeach; ?>

    <?php endif; ?>
    <hr>



    <h4>Текущее</h4>
    <hr>

    <?php foreach ($songs as $song): ?>

        <h3 class="song-name" id="sn_<?=$song->id?>" onclick="scrollText(<?=$song->id?>)">
            <input type="radio" name="reper" onclick="saveNextSong(user, <?= $song->id ?>)" value="<?= $song->id ?>" > <?= $song->title ?>
            <br>
        </h3>
        <span>
            <?php if($song->audio_link) : ?>

                <audio controls="controls" >
                    <source src="http://88.85.67.159:8082/<?=$song->audio_link?>" >
                </audio>
                <br>
            <?php endif; $isImgRelative = \yii\helpers\Url::isRelative($song->img); ?>
            <?php if($song->img) : ?>
                <?php if(!$isImgRelative): ?>
                    <img id="ima" style="width: 100%" src='<?=\yii\helpers\Url::to($song->img)?>' />
                <?php else : ?>
                    <img id="ima" style="width: 100%" src="<?=\yii\helpers\Url::to('@static/'.$song->img)?>"/>
                <?php endif; ?>
            <?php endif; ?>
            <?php if($repers = \core\entities\Rockncontroll\RepertuareItem::find()->where(['item_reper_id' => $song->id])->all()) : ?>
                    <?php foreach ($repers as $thought): ?>
                        <div class="vorstel"><?=\core\entities\Rockncontroll\Items::findOne($thought->item_phrase_id)->text ?></div>
                    <?php endforeach; ?>
            <?php endif; ?>
                <div class="block" id="text_edited_<?=$song->id?>" onclick="scrollToBottom(<?=$song->id?>)">
                    <?=nl2br(strip_tags($song->text, '<br><img><p>'))?>
                </div>

                <a href="http://88.85.67.159:8093/rockncontroll/item/update/<?=$song->id?>" target="_blank" id="bot_<?=$song->id?>">Редактировать</a><br>

                <?php $concs = \core\entities\Rockncontroll\PlistBind::find()->where("item_id=$song->id")->all();
                foreach ($concs as $pl): ?>
                    <p class="yell" id="remove_req_<?=$song->id?><?=$pl->play_list_id?>" onclick="removeFromPlaylist(<?=$song->id?>, <?=$pl->play_list_id?>)">
                        <?= \core\entities\Rockncontroll\Playlist::findOne($pl->play_list_id)->name ?>
                    </p>
                <?php endforeach; ?>
                <form class="form-inline center" role="form" id="form-bind-concert">
                    <input type="text" class="form-control" id="concert_<?=$song->id?>" onfocus="autocomplConc(<?=$song->id?>)" placeholder="Концертный статус">
                    <br>
                    <button type="button" class="btn btn-success" onclick="bind_concert(<?=$song->id?>)" >Привязать концертный статус!</button>
                </form>
                <p id="res_conc_<?=$song->id?>"></p>

        </span>


    <?php endforeach; ?>


</div>
<div id="res_qu">
    <button type="button" class="btn btn-success btn-lg btn-block" onclick="frash(user)">Сброс очереди</button>
</div>

