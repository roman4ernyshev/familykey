<?php
use \vova07\imperavi\Widget;
use yii\helpers\Url;
?>
<script>
    var user = <?= (isset($user->id)) ? $user->id : 8 ?>;
    var parent_item = 0;
    var child_item = 0;
    var song_item = 0;

    $(document).ready(function() {


        $("#make_pl").click(
            function() {

                var txt = $("#words").val();

                if (txt == '') {alert('Введите ключевые слова!'); return;}

                find(txt, user);

            });

        $("#player").click(
            function() {
                $("#silent").display = 'none';

            });

        $('#words').focus(
            function () {
                $(this).select();
            })

    });

    function find(txt, user) {
        //console.log(txt);

        $.ajax({
            type: "GET",
            url: "default/radio",
            data: "request=" + txt + "&user=" + user,
            success: function (html) {
                $("#res").html(html);

            }

        });
    }

    function autocompl_item(id) {

        $('#'+id).autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/radio-items", function (data) {

                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

    }


    function get_item(ierar) {
        var item = $('#item_' + ierar).val();

            $.ajax({
                type: "GET",
                url: "default/get-radio-item-by-title",
                data: "title=" + item + "&user=" + user,
                success: function (html) {

                    if(ierar === 'parent') {
                        parent_item = parseInt(html);

                    }
                    else {
                        child_item = parseInt(html);

                    }

                }

            });

    }

    function bindTwoItems(parent, child, song) {
        if(parent === 0 || child === 0) {alert('Обязательно заполнить Парент и Чайлд'); return;}

        $.ajax({
            type: "GET",
            url: "default/bind-two-radio-items",
            data: "parent=" + parent + "&child=" + child + "&song=" + song + "&user=" + user,
            success: function (html) {
                $("#but_success").html(html);
            }

        });
    }

    function search_item() {
        var item = $('#radio_item').val();
        $.ajax({
            type: "GET",
            url: "default/get-radio-item",
            data: "item=" + item + "&user=" + user,
            success: function (html) {
                $("#rad_it").html(html);
            }

        });
    }

    function autocompl_cat_radio() {

        $('#category_post').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/cat-post-radio", function (data) {

                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

    }

    function autocompl_clever_cat() {

        $('#clever_cat').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/clever-cats", function (data) {

                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

    }



    function addItemToRadioContent() {
        var cat = $("#category_post").val();
        var anons = $("#anons_post").val();
        var title = $("#title_post").val();
        var text = $("#item_text").val();
        var answers = $("#anwers_clever").val();
        var clever_cat = $("#clever_cat").val();
        var tags = $("#tags").val();

        $.ajax({
            type: "GET",
            url: "default/add-radio-item",
            /*
            data: "cat=" + cat +
                "&anons=" + anons +
                "&answers=" + answers +
                "&title=" + title +
                "&text=" + text +
                "&user=" + user +
                "&id=" + id +
                "&clever_cat=" + clever_cat,
            */
            data:{cat:cat,anons:anons,answers:answers,tags:tags,title:title,text:text,user:user,clever_cat:clever_cat},
            success: function (html) {
                $("#res").html(html);

            }

        });

    }



</script>
<style>

    button {
        width: 100%;
    }
    #silent{
        color: white;
        text-align: center;
    }
    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
        text-align: left;
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);

    }
    .redactor-editor pre{
        background-color: #0a0a0a;
        color: #00aa00;
    }
    .redactor-toolbar {
        background: rgb(198, 59, 9);
    }

</style>
<div class="container" id="rad_it">
    <div id="silent">Связать</div>

    <form class="form-inline center" role="form" id="form">
        <input type="text" class="form-control" id="item_parent" onfocus="autocompl_item('item_parent')" onblur="get_item('parent')" placeholder="Parent item">

        <br>
        <input type="text" class="form-control" id="item_child" onfocus="autocompl_item('item_child')" onblur="get_item('child')" placeholder="Child item">
       <button type="button" class="btn btn-success" onclick="bindTwoItems(parent_item, child_item)" id="but_success">Связать!</button>
    </form>

    <div id="silent">
        <form class="form-inline center" role="form" id="form">
            <input type="text" class="form-control" id="radio_item" onfocus="autocompl_item('radio_item')" placeholder="radio item">

            <button type="button" class="btn btn-success" onclick="search_item()" id="but_search">Найти!</button>
        </form>
    </div>

    <hr>


    <form class="form-inline center" role="form" id="form-cat-post">
        <input type="text" class="form-control" id="category_post" onfocus="autocompl_cat_radio()" placeholder="Категория">
        <input type="text" class="form-control" id="anons_post" placeholder="Анонс">
        <textarea class="form-control" id="item_text" rows="10" cols="45"></textarea>
        <?php
        echo Widget::widget([
            'selector' => '#item_text',
            'settings' => [
                'lang' => 'ru',
                'minHeight' => 300,
                'pastePlainText' => true,
                //'buttons' => ['html', 'formatting', 'bold', 'italic'],
                'buttonSource' => true,
                'plugins' => [
                    'clips',
                    'fullscreen',
                    'table'
                ],
                //'imageManagerJson' => Url::to(['/articles/images-get']),
                'imageUpload' => Url::to(['rockncontroll/default/image-upload']),

                //'fileManagerJson' => Url::to(['/uploads/files-get']),
                //'fileUpload' => Url::to(['/uploads/file-upload'])
            ]
        ]);

        ?>
        <input type="text" class="form-control" id="title_post" placeholder="Название">
        <input type="text" class="form-control" id="tags" placeholder="Тэги">
        <input type="text" class="form-control" id="anwers_clever"
               placeholder="Ответы для умного через запятую без пробелов с правильного первого">
        <br>

        <button type="button" class="btn btn-success" onclick="addItemToRadioContent()" >Добавить</button>
    </form>

    <div id="res"></div>



</div>

<script>


    /*setInterval(function () {
     $.ajax({
     type: "GET",
     url: "rockncontroll/default/rand-item/",
     success: function(html){
     $("#rand_item").html(html);
     h = html;
     }

     });

     }, 20000);
     */


    setTimeout(function run() {

     $.ajax({
     type: "GET",
     url: "default/show-current-radio-tracks/",
     success: function(html){
     $("#radio").html(html);
     }

     });
     setTimeout(run, 10000);

     }, 10000);





</script>