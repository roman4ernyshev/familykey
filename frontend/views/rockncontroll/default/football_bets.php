<?php
/**
 * @var array $summary
 *
 */
?>
<style>
.table > tbody > tr > td{
color: rgb(255, 215, 0);
font-size: 15px;
cursor: pointer;
padding-left: 3px;
}
.table > tbody > tr.firsts td{
color: rgb(255, 140, 37);
}
.table > tbody > tr.lasts td{
color: rgb(129, 236, 255);
}
h3{color: rgb(255, 215, 0); text-align: center}

.collor_match {color: #b67b69}
.fix_width {width: 10%}
span{
font-size: 13px;
}
.red{
    color: #ff3094;
}
.green{
     color: #00aa00;
}

</style>

<h3>Ставки на победу, ничью, поражение</h3>

<table class="table">
    <tbody>
    <tr>
        <td>м</td>
        <td class="fix_width">к</td>
        <td>и</td>
        <td>vic</td>
        <td>n</td>
        <td>dif</td>
    </tr>
    <?php $i=0; foreach ($summary as $team => $bet) :  $i++; ?>

            <tr class="firsts" >
                <td><?= $i ?></td>
                <td class="fix_width"> <?= $bet[4] ?></td>
                <td><?= $bet[0] ?> </td>

                <?php if($bet[1] > 0): ?>
                <td class="green"><?= $bet[1] ?></td>
                <?php else: ?>
                <td class="red"><?= $bet[1] ?></td>
                <?php endif; ?>

                <?php if($bet[2] > 0): ?>
                    <td class="green"><?= $bet[2] ?></td>
                <?php else: ?>
                    <td class="red"><?= $bet[2] ?></td>
                <?php endif; ?>

                <?php if($bet[3] > 0): ?>
                    <td class="green"><?= $bet[3] ?></td>
                <?php else: ?>
                    <td class="red"><?= $bet[3] ?></td>
                <?php endif; ?>

            </tr>

    <?php endforeach; ?>
