<script>

    $(document).ready(function() {

        $('#request').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/params-deals", function (data) {

                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });


        $("#get_date_spent").click(
            function() {

                var user = <?= (isset($user->id)) ? $user->id : 8 ?>;

                var stringyDate = $("#datepicker").val(); // mm/dd/yyyy
                var par = $("#request").val();
                var dateyDate = new Date(stringyDate);

                var ms = dateyDate.valueOf();
                var s = ms / 1000;

                get_date_spent(s);

            });


    });


    $( function() {
        $("#datepicker").datepicker({
            altFormat: "yy-mm-dd"
        });
    } );

    function get_date_spent(secs) {

        $.ajax({
            type: "GET",
            url: "default/yesterday-params",
            data: "secs="+secs,
            success: function(html){
                $("#sum_tasks").html(html);

            }

        });

    }
</script>
<style>
    h3{
        text-align: center;
        color: yellow;
    }
    button{
        width: 100%;
    }
</style>
<div id="sum_tasks">

    <div class="view">

        <form class="form-inline center" role="form" id="res">
            <div class="form-group">
                <h3>Изменить прошлое</h3>
                <p>

                    <input type="text" class="form-control" id="datepicker" placeholder="Выбрать дату"><br>

                    <button type="button" class="btn btn-success" id="get_date_spent" >Получить!</button>
                </p>
            </div>
        </form>



    </div>
</div>
