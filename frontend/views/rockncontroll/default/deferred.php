<?php


//\frontend\assets\AppAsset::register($this);

?>
<script>
    var user = <?= (isset($user->id)) ? $user->id : 8 ?>;
    $(document).ready(function() {
        $("#res").html(storage());

    });

    function timeConverter(createdAt) {
        console.log(createdAt);
        var date = new Date();
        date.setTime(createdAt);
        var year = date.getFullYear();

        var m = date.getMonth();
        if(m<9) var month = "0" + (m+1);
        else month = m+1;

        var d = date.getUTCDate();
        if(d<10) var day = "0" + d;
        else day = d;

        return year+"-"+month+"-"+day;
    }

    function clearStorage() {
        if (confirm("Очистить хранилище?")) {
            localStorage.clear();
            $("#res").html('<p>cleared</p>');
        }
    }

    function rec(item) {

        const clone = JSON.parse(localStorage.getItem(item));
        console.log(parseInt(item.slice(5),10));

        var mls = parseInt(item.slice(5),10);
        if(clone['old_data'] == 0) clone['old_data'] = timeConverter(mls);
        console.log(clone);

       // $("#res").html(JSON.stringify(clone));


        $.ajax({
            type: "GET",
            url: "default/record-item",
            data:clone,
            success: function(html){
                $("#res").html(html);
            },
            error: function (e) {
                let key = "item_"+Date.now();
                localStorage[key] = JSON.stringify(item);
                //let restored = JSON.parse(localStorage[key]);
                $("#res").html(storage()+' в хранилище <br> '+' ошибка'+ JSON.stringify(e));
            }
        });

    }


    function storage() {
        let res = '';
        for (var i = 0, length = localStorage.length; i < length; i++) {
            res +=
                'Ключ: ' + localStorage.key(i) + "; " +
                "Значение: " + localStorage.getItem(localStorage.key(i)) + ".&lt;br&gt;" +
                "<button type='button' class='btn btn-success btn-lg btn-block' onclick='rec(\""+localStorage.key(i)+"\")'>Записать!</button>";
        }
        return res;
    }

</script>

<style>
    .center, h3, table > tbody > tr > td{
        text-align: center;
    }
    .table > tbody > tr > td{
        vertical-align: middle;
        font-size: 15px;
        color: rgb(255, 215, 0);
    }
    h3{
        color: rgb(255, 215, 0);
    }


    .form-control {
        width: 100%;
    }

    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
        text-align: left;
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);

    }
    .redactor-editor pre{
        background-color: #0a0a0a;
        color: #00aa00;
    }
    .redactor-toolbar {
        background: rgb(198, 59, 9);
    }


</style>

<div class="container">
    <button type='button' class='btn btn-success btn-lg btn-block' onclick='clearStorage()'>Очистить хранилище!</button>
   <div id="res" style="color: wheat"></div>
</div>
