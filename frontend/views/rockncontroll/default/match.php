<style>


</style>
<div class="view">

    <p id="match_date">
        <?= $match->date ?>
    </p>


    <p id="match_tour"  title="Все матчи <?=$match->tournament?>">
        <?php echo $match->tournament; ?>
    </p>


    <table id="mems_match" cellpadding="0" >
        <tr>
            <td class="left"
                title="Состав: <?=$match->getSost_h()?>"
                style="cursor: pointer"
                onclick="showSost('<?=$match->getSost_h()?>', 'left_<?=$match->id?>');"><?php echo \core\helpers\Helper::cutAfterBracket($match->host); ?></td>
            <td class="center"><?php echo $match->gett; ?>:<?php echo $match->lett; ?> </td>
            <td class="right"
                title="Состав: <?=$match->getSost_g()?>"
                style="cursor: pointer"
                onclick="showSost('<?=$match->getSost_g()?>', 'right_<?=$match->id?>');"><?php echo \core\helpers\Helper::cutAfterBracket($match->guest); ?></td>

        </tr>
        <tr>
            <td class="left small-font" id="left_<?=$match->id?>" onclick="clearSost('left_<?=$match->id?>')"></td>
            <td class="center "></td>
            <td class="right small-font" id="right_<?=$match->id?>" onclick="clearSost('right_<?=$match->id?>')"></td>
        </tr>

    </table>
    <?php if($match->prim) : ?>
        <span class="prim"><?=$match->prim ?></span>
    <?php endif; ?>

        <table id="mems_goal" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->goalH_str(); ?><?php echo $match->redCardH_str(); ?><?php echo $match->penMissH_str(); ?></td>
                <td class="center"><span class="fa fa-futbol-o fa-2x"></span></td>
                <td class="right"><?php echo $match->goalG_str(); ?><?php echo $match->redCardG_str(); ?><?php echo $match->penMissG_str(); ?></td>

            </tr>
        </table>

    <?php if($match->substit_h || $match->substit_g) : ?>
        <table id="substit" cellpadding="0" >
            <tr>
                <td class="left"><?php $match->substitH_str(); ?></td>
                <td class="center"><span class="glyphicon glyphicon-resize-horizontal"></span></td>
                <td class="right"><?php $match->substitG_str(); ?></td>

            </tr>
        </table>
    <?php endif; ?>
    <?php if($match->getCoachH() != '' || $match->getCoachG() != '') : ?>

        <table id="coach" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->getCoachH(); ?></td>
                <td class="center">трен</td>
                <td class="right"><?php echo $match->getCoachG(); ?></td>

            </tr>
        </table>

    <?php endif; ?>
    <?php if($match->getKeeperH() != '' || $match->getKeeperG() != '') : ?>

        <table id="keeper" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->getKeeperH(); ?></td>
                <td class="center">врат</td>
                <td class="right"><?php echo $match->getKeeperG(); ?></td>

            </tr>
        </table>

    <?php endif; ?>

    <?php if($match->saves_h != 0 || $match->saves_g != 0) : ?>

        <table id="keeper" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->saves_h; ?></td>
                <td class="center">сейв</td>
                <td class="right"><?php echo $match->saves_g; ?> </td>

            </tr>
        </table>

    <?php endif; ?>

    <?php if($match->ud_h != 0 && $match->ud_g != 0) : ?>

        <table id="ud" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->ud_h; ?></td>
                <td class="center">удар</td>
                <td class="right"><?php echo $match->ud_g; ?></td>

            </tr>
        </table>

    <?php endif; ?>

    <?php if($match->ud_v_stv_h != 0 || $match->ud_v_stv_g != 0) : ?>

        <table id="ud" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->ud_v_stv_h; ?></td>
                <td class="center">створ</td>
                <td class="right"><?php echo $match->ud_v_stv_g; ?></td>

            </tr>
        </table>

    <?php endif; ?>

    <?php if($match->corner_h != 0 && $match->corner_g != 0) : ?>

        <table id="ud" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->corner_h; ?></td>
                <td class="center">угл</td>
                <td class="right"><?php echo $match->corner_g; ?></td>

            </tr>
        </table>

    <?php endif; ?>

    <?php if($match->outs_h != 0 && $match->outs_g != 0) : ?>

        <table id="ud" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->outs_h; ?></td>
                <td class="center">аут</td>
                <td class="right"><?php echo $match->outs_g; ?></td>

            </tr>
        </table>

    <?php endif; ?>

    <?php if($match->ballpos_h != 0 && $match->ballpos_g != 0) : ?>

        <table id="vlad" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->ballpos_h; ?></td>
                <td class="center">% влад</td>
                <td class="right"><?php echo $match->ballpos_g; ?></td>

            </tr>
        </table>

    <?php endif; ?>

    <?php if($match->falls_h != 0 && $match->falls_g != 0) : ?>

        <table id="falls" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->yellCardCount_h(); ?><?php echo $match->falls_h; ?></td>
                <td class="center">фол</td>
                <td class="right"><?php echo $match->falls_g; ?><?php echo $match->yellCardCount_g(); ?></td>

            </tr>
        </table>

    <?php endif; ?>

    <?php if($match->bet_h != 0 && $match->bet_n != 0 && $match->bet_g != 0) : ?>

        <table id="stavki" cellpadding="0" >
            <tr>
                <td class="left"></td>
                <td class="center"> ставки </td>
                <td class="right"></td>

            </tr>
        </table>
        <table id="stavki" cellpadding="0" >
            <tr>
                <td class="left"><?php echo $match->bet_h; ?></td>
                <td class="center"><?php echo $match->bet_n; ?> </td>
                <td class="right"><?php echo $match->bet_g; ?></td>

            </tr>
        </table>

    <?php endif; ?>

    <p class="info">
        <?php $match->getInfo(); ?>
    </p>


</div>
