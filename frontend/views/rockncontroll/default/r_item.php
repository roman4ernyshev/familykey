<h4><?=$item->title?></h4>
<p><?=$item->text?></p>
<textarea class="form-control" id="item_text"><?=$item->text?></textarea>
<button type="button" id="del-it" class="btn btn-success btn-lg btn-block" onclick="editItemText(8, <?=$item->id?>)">Редактировать</button>
<?php if(\core\entities\Rockncontroll\RadioItem::findOne($item->next_item)) : ?>
    <h4>Next</h4>
    <p><?= \core\entities\Rockncontroll\RadioItem::findOne($item->next_item)->title; ?></p>
<?php endif; ?>

    <?php if(\core\entities\Rockncontroll\RadioItem::isChild($item->id)) : ?>
        <h4>Is Child</h4>
        <?php foreach (\core\entities\Rockncontroll\RadioItem::isChild($item->id) as $child) : ?>
            <p><?= $child->title ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
<?php if($item->img) : ?>
    <img src="<?=\yii\helpers\Url::to('@static/'.$item->img)?>" style="width: 100%;">
<?php endif; ?>
<?php if($item->audio) : ?>
    <p><audio controls src="http://37.192.187.83:10081/<?=$item->audio?>"></audio></p>
<?php endif; ?>
<?php if($item->alias) : ?>
    <p><a href="http://radiorooma.ru/<?=$item->alias?>.html" target="_blank">http://radiorooma.loc/<?=$item->alias?>.html</a></p>
<?php endif; ?>
<?php if($item->published === 1) : ?>
    <button type="button" id="del-it" class="btn btn-success btn-lg btn-block" onclick="delItem(8, <?=$item->id?>, 1)">Удалить</button>
<?php else : ?>
    <button type="button" id="del-it" class="btn btn-success btn-lg btn-block" onclick="delItem(8, <?=$item->id?>, 0)">Опубликовать</button>
<?php endif; ?>
<script>
    function delItem(user, item_id, flag) {
        $.ajax({
            type: "GET",
            url: "default/del-radio-item",
            data: "item_id=" + item_id + "&user=" + user + "&flag=" + flag,
            success: function (html) {
                $("#del-it").html(html);
            }

        });
    }
    function editItemText(user, item_id) {
        var text = $("#item_text").val();
        $.ajax({
            type: "GET",
            url: "default/edit-radio-item",
            //data: "item_id=" + item_id + "&user=" + user + "&text=" + text,
            data:{item_id:item_id,user:user,text:text},
            success: function (html) {
                $("#del-it").html(html);
            }

        });
    }
</script>
