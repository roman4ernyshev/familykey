<?php
/**
 * @var $item \core\entities\Rockncontroll\Items
 */
?>
<h4><?=$item->title?></h4>
    <span><?=$item->source->title?></span>
<p><?=$item->text?></p>
<?php if($item->img) : ?>
    <?php if(!$isImgRelative): ?>
        <img style="width: 100%" src='<?=\yii\helpers\Url::to($item->img)?>' />
    <?php else : ?>
        <img style="width: 100%" src="<?=\yii\helpers\Url::to('@static/'.$item->img)?>"/>
    <?php endif; ?>
<?php endif; ?>
<button type="button" class="btn btn-success" onclick="publisch(<?=$item->id?>)" id="pub_<?=$item->id?>" >Опубликовать в извилинах</button>

