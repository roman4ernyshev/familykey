
<?php  use miloschuman\highcharts\Highcharts; ?>

<script>
    var arr = [];
    $(document).ready(function() {
       // alert($.cookie('the_cookie'));
        getRandNewNewInBlock('new_new');

        if($.cookie('the_cookie') === 'the_value') {
            $.ajax({
                type: "GET",
                url: "/rockncontroll/default/login/",
                data: "name=рома&pseudo=папапа",
                success: function(html){
                    $("#rechange").html(html);
                }

            });
        }

        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:8098/datas/show-current-radio-tracks-test/",
            success: function(html){
                $("#radio_test").html(html);
            }

        });
        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:8098/datas/show-current-radio-tracks-second/",
            success: function(html){
                $("#radio_second").html(html);
            }

        });

        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:8098/datas/radio-enters/",
            success: function (html) {
                $("#rad_bard").html(html);
            }
        });


      
    });

   
    function login(){
        //$.cookie('the_cookie', 'the_value', { expires: 90 });

        var name = $("#logg").val();
        var pseudo = $("#pswd").val();

        //Валидация
        if (name  === "" ) {
            alert("Введите имя");
            return false;
        }

        if (pseudo === "") {
            alert("Введите псевдоним");
            return false;
        }
        //$("#login").hide();


        $.ajax({
            type: "GET",
            url: "/rockncontroll/default/login/",
            data: "name="+name+"&pseudo="+pseudo,
            success: function(html){
                    $("#rechange").html(html);
                    $.cookie('name', name+pseudo, { expires: 7 });

                }

            });

    }

</script>
<style>

    #new_new{
        line-height: 14px;
        height: 61px;
        overflow: hidden;
        padding-top: 5px;
    }
    .logo button{
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        margin-bottom: 5px;
        height: 25px;
    }
    .logo button p{
        overflow: hidden;
        text-overflow: ellipsis;
        height: 25px;
    }
    .alert{
        margin-bottom: 10px;
        padding: 7px;
    }
    #stop_item_block{
        line-height: 15px;
    }
    .btn-success:focus, .btn-success:active:hover, .btn-success.focus , .btn-success.active,
    .btn-success:visited, .btn-success:visited:focus, .btn-success.visited {
        color: rgb(255, 255, 255);
        background-color: rgba(42, 46, 50, 0.95);
        border-color: rgb(118, 128, 137);
    }
    .active-button{
        border-color: #0f0f0f;
    }
    #stop_btn{
        background-color: #0f0f0f;
    }
    h4,h5,h6{
        text-align: center;
        color: white;
    }


</style>

<header>
    <div class="logo">
       <h1 style="font-size: 25px; color: white; margin: 0"> <?=$denzhisni?>-<?=date('z-W-M-d-D', time()+3*60*60) ?></h1>
        <p id="new_new"></p>
        <audio id="au_test" ></audio>
        <audio id="au_second" ></audio>

        <button type="submit" class="btn-success" onclick="onTest()" id="radio_test"><p style="font-size: 35px;">&infin;</p></button>
        <button type="submit" class="btn-success" onclick="onSecond()" id="radio_second"><p style="font-size: 35px;">&infin;</p></button>
         <button type="submit" class="btn active-button" onclick="stopRadio()" id="stop_btn"><span class="glyphicon glyphicon-stop"></span></button>

    </div>
</header>
<div class="container">
    <div id="rechange">
        <div class="col-md-4 col-md-offset-4">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <?php
                    /* $form = ActiveForm::begin([
                        'id'                     => 'login-form',
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                        'validateOnBlur'         => false,
                        'validateOnType'         => false,
                        'validateOnChange'       => false,
                    ]) ?>

                    <?= $form->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]) ?>

                    <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2', 'id' => 'pswd']])->passwordInput()->label(Yii::t('user', 'Пароль') . ($module->enablePasswordRecovery ? ' (' . Html::a(Yii::t('user', 'Забыли пароль?'), ['/user/recovery/request'], ['tabindex' => '5']) . ')' : '')) ?>

                    <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>

                    <?= Html::submitButton(Yii::t('user', 'Вход'), ['class' => 'btn btn-primary btn-block', 'tabindex' => '3', 'onclick' => 'login()']) ?>

                    <?php ActiveForm::end();

                    */
                    ?>
                    <p class="form">Имя</p>
                    <input type='text' class="form-control" id="logg" placeholder="Введите имя"/>
                    <p class="form">Псевдоимя</p>
                    <input type='text' class="form-control" id="pswd" placeholder="Введите псевдоним"/>

                    <button class="btn" id="login" onclick="login()" >Войти</button>
                    <div id="res"></div>
                </div>
            </div>
        </div>
        
    </div>

    <div id="summary"></div>

    
    
</div>

<script>

    setInterval(function () {
            getRandNewNewInBlock('new_new');
        }, 20000);


    function getRandNewNewInBlock(block) {
        $.ajax({
            type: "GET",
            url: "rockncontroll/default/rand-new-new/",
            success: function (html) {
                $("#" + block).html(html);
            }
        });
    }

    function markItItem(user, item) {
        $.ajax({
            type: "GET",
            url: "rockncontroll/default/mark-it-item",
            data: "user="+user+"&item="+item,
            success: function (html) {
                $("#mark-it").html(html);

            }
        });
    }

    var player_second = document.getElementById('au_second');
    var player_test = document.getElementById('au_test');
    var radio_test = document.getElementById('radio_test');
    var radio_second = document.getElementById('radio_second');
    var stop_btn = document.getElementById('stop_btn');

    var canal = '';
    stop_btn.style.display = 'none';

    // var test_button = document.getElementById('radio_test');

    var radioTestClasses = document.getElementById("radio_test").classList;
    var radioSecondClasses = document.getElementById("radio_second").classList;

    setTimeout(function run() {

        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:8098/datas/show-current-radio-tracks-test/",
            success: function(html){
                if($("#radio_test").hasClass('act')) {
                    $("#radio_test").html(html).children('p').css('height', '45px');
                }
                else $("#radio_test").html(html).children('p').css('height', '20px');
            }

        });
        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:8098/datas/show-current-radio-tracks-second/",
            success: function(html){
                if($("#radio_second").hasClass('act')) {
                    $("#radio_second").html(html).children('p').css('height', '45px');
                }
                else $("#radio_second").html(html).children('p').css('height', '20px');
            }

        });

        $.ajax({
            type: "GET",
            //url: "http://37.192.187.83:8098/datas/radio-enters/",
            url: "http://37.192.187.83:8098/datas/radio-enters/",
            //\Yii::getAlias('@statdir')
            success: function (html) {
                $("#rad_bard").html(html);
            }
        });


        setTimeout(run, 10000);

    }, 10000);



    function onTest(){

        canal = 'test';
        //player_test.src = 'http://37.192.187.83:10088/test_mp3';
        player_test.src = <?=\Yii::getAlias('@radio_test')?>;
        player_test.play();
        player_second.pause();
        player_second.src = '';
        radio_second.style.display = 'none';

        radio_test.style.height = '50px';
        radio_test.setAttribute('onclick', 'stopRadio()');
        radio_test.classList.add('act');


    }

    function onSecond(){

        canal = 'second';
        player_second.src = 'http://37.192.187.83:10088/second_mp3';
        player_second.play();
        player_test.pause();
        player_test.src = '';
        radio_test.style.display = 'none';

        radio_second.style.height = '50px';

        radio_second.setAttribute('onclick', 'stopRadio()');
        radio_second.classList.add('act');


    }


    function stopRadio() {

        canal = '';
        player_second.pause();
        player_second.src = '';
        player_test.pause();
        player_test.src = '';


        radio_test.style.height = '25px';
        radio_second.style.height = '25px';

        radio_test.setAttribute('onclick', 'onTest()');
        radio_test.children[0].style.height = '20px';
        radio_test.classList.remove('act');

        radio_second.setAttribute('onclick', 'onSecond()');
        radio_second.children[0].style.height = '20px';
        radio_second.classList.remove('act');

        radio_test.style.display = 'block';
        radio_second.style.display = 'block';


    }

    function changeActiveClass(elAddClass, elRemClass_1) {
        if(elAddClass.contains('active-button')) return;
        else elAddClass.add('active-button');
        if(elRemClass_1.contains('active-button')) elRemClass_1.remove('active-button');
    }

    function siteBlockListener(site, block, ip_json) {
        // console.log(ip_json);

        new Fingerprint2().get(function(result, components){
            //console.log(result); //a hash, representing your device fingerprint
            //console.log(components); // an array of FP components

            $.ajax({
                url: "http://37.192.187.83:8098/datas/come-in/",
                type:'POST',
                data:'components=' + JSON.stringify(components) +
                '&hash=' + result +
                '&site='+ site +'&block=' + block +
                '&ip_json=' + JSON.stringify(ip_json)
                // success: function(html){
                //    $("#dev_res").html(html);
                //}
            });
        });
    }

    function searchMusic() {
        var album = $("#album").val();

        if(album == '') {
            alert('Введите название альбома или исполнителя!');
            return;
        }

        stopRadio();

        $.ajax({
            type: "POST",
            url: "http://37.192.187.83:8098/datas/get-album/",
            data: "album="+album,
            success: function(html){
                $("#radio_block").html(html);
            }

        });
        radio_back.style.display = 'block';

    }


</script>
