<style>
    .music > td{
        color: black;
        background-color: yellow;
        text-align: center;

    }
    .school > td{
        color: black;
        background-color:  #0b93d5;
        text-align: center;
    }
    .football > td{
        color: wheat;
        background-color: green;
        text-align: center;
    }
    .free > td{
        color: black;
        background-color: #363b3f;
        text-align: center;
    }
    .bordered > td{
        border-bottom:1pt solid black;
    }

    h3, p, #do_calc{
        text-align: center;
    }
    h3{
        color: white;
    }




</style>
<h3>Понедельник</h3>
<table>
    <tr align="left" class="school bordered">
        <td>8</td>
        <td>8:50-9:30</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>9</td>
        <td>9:40-10:20</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>10</td>
        <td>10:30-11:10</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>11</td>
        <td>11:20-12:00</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>12</td>
        <td>12:10-12:50</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>13</td>
        <td>13:00-13:40</td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>14</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>15</td>
        <td>15:45-16:30</td>
        <td>Фортепьяно</td>
    </tr>
    <tr align="left" class="school bordered">
        <td>16</td>
        <td>15:45-16:30</td>
        <td>Фортепьяно</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>17</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>18</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="football bordered">
        <td>19</td>
        <td>19:00-20:15</td>
        <td>Футбол</td>
    </tr>
    <tr align="left" class="football bordered">
        <td>20</td>
        <td>19:00-20:15</td>
        <td>Футбол</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>21</td>
        <td></td>
        <td></td>
    </tr>
</table>
<h3>Вторник</h3>
<table>
    <tr align="left" class="school bordered">
        <td>8</td>
        <td>8:50-9:30</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>9</td>
        <td>9:40-10:20</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>10</td>
        <td>10:30-11:10</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>11</td>
        <td>11:20-12:00</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>12</td>
        <td>12:10-12:50</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>13</td>
        <td>13:00-13:40</td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>14</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="music bordered">
        <td>15</td>
        <td>15:10-16:40</td>
        <td>Хор</td>
    </tr>
    <tr align="left" class="music bordered">
        <td>16</td>
        <td>15:10-16:40</td>
        <td>Хор</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>17</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>18</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>19</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>20</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>21</td>
        <td></td>
        <td></td>
    </tr>
</table>
<h3>Среда</h3>
<table>
    <tr align="left" class="school bordered">
        <td>8</td>
        <td>8:50-9:30</td>
        <td>Английский язык</td>
    </tr>
    <tr align="left" class="school bordered">
        <td>9</td>
        <td>9:40-10:20</td>
        <td>Математика</td>
    </tr>
    <tr align="left" class="school bordered">
        <td>10</td>
        <td>10:30-11:10</td>
        <td>История</td>
    </tr>
    <tr align="left" class="school bordered">
        <td>11</td>
        <td>11:20-12:00</td>
        <td>Русский язык</td>
    </tr>
    <tr align="left" class="school bordered">
        <td>12</td>
        <td>12:10-12:50</td>
        <td>Русский язык</td>
    </tr>
    <tr align="left" class="school bordered">
        <td>13</td>
        <td>13:00-13:40</td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>14</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>15</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>16</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>17</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>18</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="football bordered">
        <td>19</td>
        <td>19:00-20:30</td>
        <td>Футбол</td>
    </tr>
    <tr align="left" class="football bordered">
        <td>20</td>
        <td>19:00-20:30</td>
        <td>Футбол</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>21</td>
        <td></td>
        <td></td>
    </tr>
</table>
<h3>Четверг</h3>
<table>
    <tr align="left" class="school bordered">
        <td>8</td>
        <td>8:50-9:30</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>9</td>
        <td>9:40-10:20</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>10</td>
        <td>10:30-11:10</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>11</td>
        <td>11:20-12:00</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>12</td>
        <td>12:10-12:50</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>13</td>
        <td>13:00-13:40</td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>14</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>15</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>16</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>17</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="music bordered">
        <td>18</td>
        <td>18:45-20:00</td>
        <td>Сольфеджио</td>
    </tr>
    <tr align="left" class="music bordered">
        <td>19</td>
        <td>18:45-20:00</td>
        <td>Сольфеджио</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>20</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>21</td>
        <td></td>
        <td></td>
    </tr>
</table>
<h3>Пятница</h3>
<table>
    <tr align="left" class="school bordered">
        <td>8</td>
        <td>8:50-9:30</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>9</td>
        <td>9:40-10:20</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>10</td>
        <td>10:30-11:10</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>11</td>
        <td>11:20-12:00</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>12</td>
        <td>12:10-12:50</td>
        <td></td>
    </tr>
    <tr align="left" class="school bordered">
        <td>13</td>
        <td>13:00-13:40</td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>14</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="music bordered">
        <td>15</td>
        <td>15:10-16:40</td>
        <td>Хор</td>
    </tr>
    <tr align="left" class="music bordered">
        <td>16</td>
        <td>15:10-16:40</td>
        <td>Хор</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>17</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>18</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="football bordered">
        <td>19</td>
        <td>19:00-20:15</td>
        <td>Футбол</td>
    </tr>
    <tr align="left" class="football bordered">
        <td>20</td>
        <td>19:00-20:15</td>
        <td>Футбол</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>21</td>
        <td></td>
        <td></td>
    </tr>
</table>
<h3>Суббота</h3>
<table>
    <tr align="left" class="free bordered">
        <td>8</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>9</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>10</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>11</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>12</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>13</td>
        <td></td>
        <td>Хор</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>14</td>
        <td></td>
        <td>Хор</td>
    </tr>
    <tr align="left" class="free bordered">
        <td>15</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>16</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>17</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>18</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>19</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>20</td>
        <td></td>
        <td></td>
    </tr>
    <tr align="left" class="free bordered">
        <td>21</td>
        <td></td>
        <td></td>
    </tr>
</table>
