<?php
/**
 * @var $params array
 * @var $recorded_params array
 * @var $time integer
 */

?>
<script>

    function recParam(param_id, i, user){

        var val = $("#val_"+i).val();

        //Валидация
        if (val  === "") {
            alert("Введите значение");
            return false;
        }

        $.ajax({
            type: "GET",
            url: "default/yesterday-params",
            data: "val="+val+"&user="+user+"&param_id="+param_id+"&time="+<?=$time?>,
            success: function(res){
                $("#param_"+i).hide();
                $("#val_"+i).hide();
                $("#res_"+i).html(res);
            }

        });

        return true;

    }

    function changeParam(param_id, i, user){

        var val = $("#rec_val_"+i).val();

        //Валидация
        if (val  === "") {
            alert("Введите значение");
            return false;
        }


        $.ajax({
            type: "GET",
            url: "default/change-param",
            data: "val="+val+"&user="+user+"&param_id="+param_id,
            success: function(res){
                $("#summary").html(res);
            }

        });


        return true;

    }

</script>

<style>
    .form-control{
        padding: 0;
        height: 33px;
        text-align: center;
        width: 100%;
    }

    #task_name, #task_description{
        width: 100%;
    }

    table > tbody > tr > td{
        padding: 0;
        font-size: 20px;
    }


    @media (min-width:320px) and (max-width:767px) {
        table > tbody > tr > td{
            font-size: 14px;
            color: white;
        }


    }

</style>

<div id="sum_tasks">
    <?php  $i = 0; ?>

    <div class="view">
        <h3><?=date('d-m-Y',$time)?></h3>

        <table class="table">
            <tbody>

            <?php foreach ($recorded_params as $param) : ?>

                <tr>
                    <td>
                        <?= $param->dayparam->name ?>
                    </td>

                    <td width="80">

                        <input type='text' class="form-control" id="rec_val_<?=$i ?>" value="<?= $param->value ?>" />
                        <button class="btn btn-success" id="rec_param_<?=$i ?>" onclick="changeParam(<?=$param->id?>, <?=$i?>, 8)" >Изменить!</button>
                        <p id="res_<?=$i?>"></p>

                    </td>


                </tr>

                <?php $i++; endforeach; ?>



            <?php foreach ($params as $param) :
                //var_dump($param); exit; ?>


                <tr>
                    <td>
                        <?= $param->name ?>
                    </td>

                    <td width="80">
                        <input type='text' class="form-control" id="val_<?=$i ?>" placeholder="Значение" />
                        <button class="btn btn-success" id="param_<?=$i ?>" onclick="recParam(<?=$param->id?>, <?=$i?>, 8)" >Записать!</button>
                        <p id="res_<?=$i?>"></p>
                    </td>
                </tr>

                <?php $i++; endforeach; ?>


            </tbody>
        </table>
    </div>
</div>


