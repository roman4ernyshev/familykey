<?php

use \vova07\imperavi\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\helpers\Html;

//\frontend\assets\AppAsset::register($this);

?>
<script>
    var user = <?= (isset($user->id)) ? $user->id : 8 ?>;
    $(document).ready(function() {

        $('#cat').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/deal-cats", function (data) {

                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

        $('#published').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/deal-theme", function (data) {

                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

        $('#source').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                term = term.toLowerCase();
                console.log(term);
                $.getJSON("default/sources", function (data) {

                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

        $("#record").click(
            function() {

                var title = $("#title").val();
                var source = $("#source").val();
                var cat = $("#cat").val();
                var tags = $("#tags").val();
                var txt = $("#text").val();
                var old_data = $("#old_data").val();
                var cens = $("#cens").val();
                var published = $("#published").val();
                

                if (title == '') {alert('Введите название!'); return;}
                if (source == '') {alert('Введите источник!'); return;}
                if (cat == '') {alert('Введите категорию!'); return;}
                if (tags == '') {alert('Введите тэги!'); return;}
                if (cens == '') {alert('Введите уровень цензуры!'); return;}
                if (txt == '') {alert('Введите текст!'); return;}
                if (old_data == '') {old_data = 0;}
                if (published == '') {published = 1;}

                rec(title, source, cat, tags, txt, user, old_data, cens, published);

            });

        $('#dish').focus(
            function () {
                $(this).select();
            })
        $('#measure').focus(
            function () {
                $(this).select();
            })

    });

    $('.fileinput').change(function(){
        var fd = new FormData();

        console.log(this.files);

        fd.append("userpic", this.files[0]);

        console.log(this.files);

        $.ajax({
            url: "default/upload",
            type: "POST",
            data: fd,
            dataType: 'json',
            //maxFileSize: 256 * 1014,
            allowedTypes: 'image/*',
            cache: false,
            contentType: false,
            processData: false,
            forceSync: false,
            success: function(html){
                $("#res").html(html);

            }
        });
    });


    function rec(title, source, cat, tags, txt, user, old_data, cens, published) {

        let item = {
            title: title,
            source: source,
            cat: cat,
            tags: tags,
            txt: txt,
            user: user,
            old_data: old_data,
            cens: cens,
            published: published
        };

        $.ajax({
            type: "GET",
            url: "default/record-item",
            data:item,
            success: function(html){
                $("#res").html(html);
            },
            error: function (e) {
                let key = "item_"+Date.now();
                localStorage[key] = JSON.stringify(item);
                //let restored = JSON.parse(localStorage[key]);
                $("#res").html(storage()+'<br> в хранилище <br> '+' ошибка'+ JSON.stringify(e));
            }
        });

    }

    function storage() {
        let res = '';
        for (var i = 0, length = localStorage.length; i < length; i++) {
            res += 'Ключ: ' + localStorage.key(i) + "; Значение: " + localStorage.getItem(localStorage.key(i)) + ".&lt;br&gt;";
        }
        return res;
    }


    /**
    function rec(title, source, cat, tags, txt, user, old_data, cens, published) {


        $.ajax({
            url: "default/record-item",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data:{title:title,user:user,source:source,cat:cat,tags:tags,txt:txt,old_data:old_data,cens:cens,published:published},
            success: function(html){
                $("#res").html(html);

            },
            dataType: 'html',
            processData: false,

        });

    }

     **/
    


</script>

<style>
    .center, h3, table > tbody > tr > td{
        text-align: center;
    }
    .table > tbody > tr > td{
        vertical-align: middle;
        font-size: 15px;
        color: rgb(255, 215, 0);
    }
    h3{
        color: rgb(255, 215, 0);
    }
    

    .form-control {
        width: 100%;
    }

    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
        text-align: left;
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);

    }
    .redactor-editor pre{
        background-color: #0a0a0a;
        color: #00aa00;
    }
    .redactor-toolbar {
        background: rgb(198, 59, 9);
    }
    #tags{
        text-align: left;
    }


</style>

<div class="container">
    <form class="form-inline center" role="form" id="form-ate">
        <div class="form-group">
            <h3>Есть что?</h3>
            <p>
                <input type="text" class="form-control" id="title"  placeholder="Титул" autocomplete="false"  spellcheck="false">
                <input type="text" class="form-control" id="source"  placeholder="Источник" autocomplete="false"  spellcheck="false">
                <input type="text" class="form-control" id="cat"  placeholder="Категория" autocomplete="false"  spellcheck="false">
                <input type="text" class="form-control" id="tags"  placeholder="Тэги" autocomplete="false"  spellcheck="false">
                <input type="text" class="form-control" id="published"  placeholder="Тема" autocomplete="off"  spellcheck="false">
                <input type="text" class="form-control" id="cens"  placeholder="Уровень цензуры, 0 - +6" value="0" autocomplete="off"  spellcheck="false">
                <input type="text" class="form-control" id="old_data"  placeholder="Дата в формате ГГГГ-ММ-ДД">
                <textarea class="form-control" id="text"  placeholder="Текст" rows="10" cols="45"></textarea>

                <?php
                echo Widget::widget([
                    'selector' => '#text',
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 300,
                        'pastePlainText' => true,

                        'buttonSource' => true,
                        //'buttons' => ['html', 'formatting', 'bold', 'italic'],
                        'plugins' => [
                            'clips',
                            'fullscreen',
                            'table',
                            //'textexpander',
                            'fontcolor'
                        ],
                        'clips' => [
                            ['Lorem ipsum...', 'Lorem...'],
                            ['red', 't<sup>2</sup>'],
                            ['green', '<span class="label-green">green</span>'],
                            ['blue', '<span class="label-blue">blue</span>'],
                        ],
                        //'imageManagerJson' => Url::to(['/articles/images-get']),
                        'imageUpload' => Url::to(['rockncontroll/default/image-upload']),

                        //'fileManagerJson' => Url::to(['/uploads/files-get']),
                        //'fileUpload' => Url::to(['/uploads/file-upload'])
                    ]
                ]);

               ?>
                <button type="button" class="btn btn-success" id="record" >Записать!</button>
            </p>
            <?php /*
            $tags = core\entities\Rockncontroll\Tag::find()
                ->select(['name as label'])
                ->asArray()
                ->all();
            $sources = core\entities\Rockncontroll\Source::find()
                ->select(['title as label'])
                ->asArray()
                ->all();
            $caters = core\entities\Rockncontroll\Categories::find()
                ->select(['title as label'])
                ->asArray()
                ->all();
            ?>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'title')->textInput()  ?>

            <?php echo $form->field($model, 'source_title')->widget(
                AutoComplete::class, [
                'clientOptions' => [
                    'source' => $sources,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>
            <?php  //$form->field($model, 'cat_title')->textInput()  ?>
            <?php echo $form->field($model, 'cat_title')->widget(
                AutoComplete::className(), [
                'clientOptions' => [
                    'source' => $caters,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>

            <?= $form->field($model, 'text')->widget(Widget::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 300,
                    'pastePlainText' => true,
                    //'buttons' => ['html', 'formatting', 'bold', 'italic'],
                    'buttonSource' => true,
                    'plugins' => [
                        'clips',
                        'fullscreen'
                    ],
                    //'imageManagerJson' => Url::to(['/articles/images-get']),
                    'imageUpload' => Url::to(['rockncontroll/default/image-upload']),

                    //'fileManagerJson' => Url::to(['/uploads/files-get']),
                    //'fileUpload' => Url::to(['/uploads/file-upload'])
                ]

            ]);?>
            <?=  $form->field($model, 'cens')->textInput()  ?>
            <?=  $form->field($model, 'published')->textInput()  ?>
            <?php  $form->field($model, 'tags')->textInput()  ?>
            <?php echo $form->field($model, 'tags')->widget(
                AutoComplete::className(), [
                'clientOptions' => [
                    'source' => $tags,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>
            <?= $form->field($model, 'audio_link')->textInput()  ?>
            <?= $form->field($model, 'in_work_prim')->textInput()  ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
            </div>

            <?php ActiveForm::end(); */
            ?>

            <div id="res" style="color: wheat"></div>
        </div>
    </form>
   <?php // <input class="fileinput" name="userpic" type="file" /> ?>
</div>