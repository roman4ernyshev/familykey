<style>
    #tradingview_13994-wrapper{
        width: 100%;
    }
    .js-hidden {
        display: block!important;
    }
</style>
<div class="content">


    <div id="tradingview_13994-wrapper"
         style="
            position: relative;
            box-sizing: content-box;
            width: 100%;
            height: 515px;
            margin: 0 auto !important;
            padding: 0 !important;
            font-family:Arial,sans-serif;">
        <div style="width: 100%;height: 515px;background: transparent;padding: 0 !important;">
            <iframe id="tradingview_13994" src="https://s.tradingview.com/widgetembed/?frameElementId=tradingview_13994&amp;symbol=FOREXCOM%3AUSDRUB&amp;interval=5&amp;hidesidetoolbar=1&amp;symboledit=1&amp;saveimage=0&amp;toolbarbg=E4E8EB&amp;watchlist=FX_IDC%3AUSDRUB%1FFX_IDC%3AEURRUB%1FBCOUSD%1FSPX500USD%1FUSDOLLAR%1FEURUSD&amp;details=1&amp;studies=%5B%5D&amp;widgetbarwidth=243&amp;hideideas=1&amp;theme=Dark&amp;style=1&amp;timezone=Europe%2FMoscow&amp;hideideasbutton=1&amp;hidevolume=1&amp;studies_overrides=%7B%7D&amp;overrides=%7B%7D&amp;enabled_features=%5B%5D&amp;disabled_features=%5B%5D&amp;utm_source=xn----dtbfdbwspgnceulm.xn--p1ai&amp;utm_medium=widget&amp;utm_campaign=chart&amp;utm_term=FOREXCOM%3AUSDRUB" style="width: 100%; height: 100%; margin: 0 !important; padding: 0 !important;" frameborder="0" allowtransparency="true" scrolling="no" allowfullscreen="">
            </iframe>
        </div>
    </div>

    <script type="text/javascript">

        $(function(){
            $('.js-hidden').removeClass('js-hidden');
        });

    </script>



</div>
