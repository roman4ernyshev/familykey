<style>
    .red{
        color: orangered!important;
    }
    .green{
        color: rgb(40,157,139) !important;
    }
    #rand_item{
        line-height: 15px;
        height: 45px;
        overflow: hidden;
        padding-left: 15px;
        padding-right: 15px;
    }
    #preloader{
        text-align: center;
    }

</style>
<table class="table_data">
    <tbody>
    <tr id="wasser">
        <td>W</td>
        <td>R</td>
        <td>WR</td>
        <td>RM</td>
        <td>ISD</td>
    </tr>
    <tr id="actual">
        <td class="<?= $we < 80 ? 'green' : 'red' ?>"><?= round($we, 2) ?> </td>
        <td class="<?= $avg_oz >= 80 ? 'green' : 'red' ?>"><?= round($avg_oz, 2) ?> </td>
        <td class="<?= $cur_form >= 40 ? 'green' : 'red' ?>"><?= round($cur_form, 2) ?> </td>
        <td class="<?= $avg_oz >= 100 ? 'green' : 'red' ?>"><?= round($avg_oz, 2) ?> </td>
        <td class="<?= $avg_spent_day <= $avg_incomes_day ? 'green' : 'red' ?>"><?= round($avg_spent_day) ?>:<?= round($avg_incomes_day) ?></td>
    </tr>
    </tbody>
</table>
<div id="rand_item"></div>
<script>
        var h;
        /*setInterval(function () {
            $.ajax({
                type: "GET",
                url: "rockncontroll/default/rand-item/",
                success: function(html){
                    $("#rand_item").html(html);
                    h = html;
                }

            });

        }, 20000);
        */


        setTimeout(function run() {

            $.ajax({
                type: "GET",
                url: "default/rand-item/",
                success: function(html){
                    $("#rand_item").html(html);
                    h = html;
                }

            });
            setTimeout(run, 20000);

        }, 20000);

        setTimeout(function run() {

            $.ajax({
                type: "GET",
                url: "default/actual/",
                success: function(html){
                    $("#actual").html(html);
                }

            });
            setTimeout(run, 20000);

        }, 20000);

        /*

        setTimeout(function run() {

            $.ajax({
                type: "GET",
                url: "datas/get-wasser-reih/",
                success: function(html){
                    $("#wasser").html(html);
                }

            });
            setTimeout(run, 5000);

        }, 5000);

         */

        $('#current_task').mouseover(function() {

            $("#stop_item_block").parent().show();
            $("#stop_item_block").html(h);
        });
        $('#stop_item_block').mouseover(function() {
            $("#stop_item_block").parent().hide();

        });

</script>
