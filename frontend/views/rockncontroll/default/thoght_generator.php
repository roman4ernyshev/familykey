<style>
    .gener h3, h6{
        color: yellow;
        text-align: center;
    }
    .gener p{
        color: #fbcc75e6;
        line-height: normal;
        font-size: 15px;
        font-family: sans-serif;
        padding-bottom: 20px;
    }
    blockquote{
        color: aliceblue;
    }
    pre {
        color: #48d931 !important;
        background-color: #3d3434 !important;
    }
    ul{
        color: antiquewhite;

    }
    .gener {
        background-color: #0a0a0a;
    }

</style>
<div class="gener">
    <?php
    /**
     * @var $item \core\entities\Rockncontroll\Items
     */
    //var_dump($items);
    foreach ($items as $item): $isImgRelative = \yii\helpers\Url::isRelative($item->img); ?>
        <hr>
        <h3><?=$item->title?></h3>
        <h6><?=$item->source->author->name?> - <?=$item->source->title?></h6>
        <p><?=hostFilterAndCopyForEmmit(nl2br($item->text))?></p>
        <?php if($item->audio_link) : ?>
            <audio controls="controls" >
                <source src="<?=\yii\helpers\Url::to('@music/'.$item->audio_link)?>" type='audio/mpeg'>
            </audio><br>
        <?php endif; ?>

        <?php if($item->img) : ?>
            <?php if(!$isImgRelative): ?>
                <img style="width: 100%" src='<?=\yii\helpers\Url::to($item->img)?>' />
            <?php else : ?>
                <img style="width: 100%" src="<?=\yii\helpers\Url::to('@static/'.altHostWeg($item->img))?>"/>
            <?php endif; ?>
        <?php endif; ?>
        <button type="button" class="btn btn-success" onclick="publisch(<?=$item->id?>)" id="pub_<?=$item->id?>" >Опубликовать в извилинах</button>
    <?php endforeach; ?>
</div>

<button type="button" class="btn btn-success" onclick="window.scrollTo({ top: 0, behavior: 'smooth' });">Up</button>

<?php
//http://88.85.67.159:8082/http://37.192.187.83/uploads/imperaviim/5ccfa39715348.jpg
function hostFilterAndCopyForEmmit($text){
    $text = str_replace("http://37.192.187.83:10034/", "http://88.85.67.159:8082/", $text);
    $text = str_replace("http://37.192.187.83:8014/", "http://88.85.67.159:8082/", $text);
    $text = str_replace("http://37.192.187.83/", "http://88.85.67.159:8082/", $text);
    $text = str_replace("/uploads/", "uploads/", $text);
    return str_replace("http://37.192.187.83:8014/", "http://88.85.67.159:8082", $text);
}
function altHostWeg($text){
    return str_replace("http://37.192.187.83/", "", $text);
}
?>

