<?php
/**
 * @var $items
 */
?>
<script>
    $(document).ready(function() {
        $(".accord h5:first").addClass("active");

        $(".accord span").hide();

        $(".accord h5").click(function() {

            $(this).next("span").slideToggle("slow").siblings("span:visible").slideUp("slow");

            $(this).toggleClass("active");

            $(this).siblings("h5").removeClass("active");

        });

    });
    function text_edit(id) {
        window.open('http://88.85.67.159:8093/rockncontroll/item/update/'+id, '_blank');
    }
    function publisch(id){
        $.ajax({
            type: "GET",
            url: "default/publish-isviliny",
            data: "id=" + id,
            success: function (html) {
                $("#pub_"+id).html(html);
            }
        });
    }
</script>
<style>
    h1 {
        font-size: 33px;
        color: rgb(255, 100, 0);
        text-align: center;
    }
    h5 {
        font-size: 20px;
        color: rgb(255, 255, 0);
        text-align: center;
    }
    .nov_text {
        font-size: 17px;
        line-height: 1.2;
        color: rgb(160, 173, 184);
        text-align: left;
    }
    #ima {
        width: 100%;
    }
    pre {
        background-color: rgb(0, 0, 0);
        border: 1px solid rgb(82, 82, 82);
        border-radius: 4px;
        color: #01ff70;
    }

</style>


<div class="accord">
    <?php foreach ($items as $item) : $isImgRelative = \yii\helpers\Url::isRelative($item->img);?>
    <h5><?= $item->title ?></h5>
    <span class="nov_text">
        <?=$item->text?><br>
        <?php if($item->audio_link) : ?>
            <audio controls="controls" >
                <source src="<?=\yii\helpers\Url::to('@music/'.$item->audio_link)?>" type='audio/mpeg'>
            </audio><br>
        <?php endif; ?>
        <?php if($item->img) : ?>
            <?php if(!$isImgRelative): ?>
                <img id="ima" style="width: 100%" src='<?=\yii\helpers\Url::to($item->img)?>' />
            <?php else : ?>
                <img id="ima" style="width: 100%" src="<?=\yii\helpers\Url::to('@static/'.$item->img)?>"/>
            <?php endif; ?>
        <?php endif; ?>
        <button type="button" class="btn btn-success" onclick="text_edit(<?=$item->id?>)" id="red_button_<?=$item->id?>" >Редактировать!</button>
        <button type="button" class="btn btn-success" onclick="publisch(<?=$item->id?>)" id="pub_<?=$item->id?>" >Опубликовать</button>
        <button type="button" class="btn btn-success" onclick="showw(<?=$item->id?>)" id="red_button_<?=$item->id?>" >Показать</button>
        <a href="http://88.85.67.159:8093/rockncontroll/item/show/<?=$item->id?>" target="_blank">Поделиться ссылкой</a><br>
    </span>
    <?php endforeach; ?>
</div>
