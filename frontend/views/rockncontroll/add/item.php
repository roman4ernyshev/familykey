<?php

use \vova07\imperavi\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\helpers\Html;

//\frontend\assets\AppAsset::register($this);
header("Cache-Control: max-age=2592020");
?>
<script>
    var user = <?= (isset($user->id)) ? $user->id : 8 ?>;
    $(document).ready(function() {

        $("#record").click(
            function() {

                var title = $("#title").val();
                var source = $("#source").val();
                var cat = $("#cat").val();
                var tags = $("#tags").val();
                var txt = $("#text").val();
                var old_data = $("#old_data").val();
                var cens = $("#cens").val();
                var published = $("#published").val();


                if (title == '') {alert('Введите название!'); return;}
                if (source == '') {alert('Введите источник!'); return;}
                if (cat == '') {alert('Введите категорию!'); return;}
                if (tags == '') {alert('Введите тэги!'); return;}
                if (cens == '') {alert('Введите уровень цензуры!'); return;}
                if (txt == '') {alert('Введите текст!'); return;}
                if (old_data == '') {old_data = 0;}
                if (published == '') {published = 1;}

                rec(title, source, cat, tags, txt, user, old_data, cens, published);

            });

        $('#dish').focus(
            function () {
                $(this).select();
            })
        $('#measure').focus(
            function () {
                $(this).select();
            })

    });


    function rec(title, source, cat, tags, txt, user, old_data, cens, published) {
        //localStorage.clear();

        let item = {
            title: title,
            source: source,
            cat: cat,
            tags: tags,
            txt: txt,
            user: user,
            old_data: old_data,
            cens: cens,
            published: published
        };

        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:10033/rockncontroll/default/record-item",
            data:item,
            success: function(html){
                $("#res").html(html);
                $('#form-ate')[0].reset();
                $('.redactor-editor').html('');
            },
            error: function (e) {
                let key = "item_"+Date.now();
                localStorage[key] = JSON.stringify(item);
                //let restored = JSON.parse(localStorage[key]);
                $("#res").html(storage()+' в хранилище <br> '+' ошибка'+ JSON.stringify(e));
            }
        });

    }

    function storage() {
        let res = '';
        for (var i = 0, length = localStorage.length; i < length; i++) {
            res += 'Ключ: ' + localStorage.key(i) + "; Значение: " + localStorage.getItem(localStorage.key(i)) + ".&lt;br&gt;";
        }
        return res;
    }

    function cleanImp() {
        $('.redactor-editor').html('');
    }

</script>

<style>
    .center, h3, table > tbody > tr > td{
        text-align: center;
    }
    .table > tbody > tr > td{
        vertical-align: middle;
        font-size: 15px;
        color: rgb(255, 215, 0);
    }
    h3{
        color: rgb(255, 215, 0);
    }


    .form-control {
        width: 100%;
    }

    .redactor-editor, .redactor-box {
        background: rgb(33, 32, 35);
        color: rgb(255, 255, 255);
        text-align: left;
    }
    .redactor-editor h5 {
        font-size: 18px;
        color: rgb(0, 255, 255);

    }
    .redactor-editor pre{
        background-color: #0a0a0a;
        color: #00aa00;
    }
    .redactor-toolbar {
        background: rgb(198, 59, 9);
    }


</style>

<div class="container">
    <form class="form-inline center" role="form" id="form-ate">
        <div class="form-group">
            <h3>Есть что?</h3>
            <p>
                <input type="text" class="form-control" id="title"  placeholder="Титул">

                <select type="text" class="form-control" id="source"  >
                    <option>Толковая энциклопедия</option>
                    <option>Идиотский мозг</option>
                    <option>Омофон</option>
                    <option>https://habrahabr.ru/</option>
                    <option>Утопия </option>
                    <option>Немецкая грамматика с человеческим лицом</option>
                    <option>Стихотворения Шандора Петефи</option>
                    <option>Спорт-Экспресс</option>
                    <option>Сборник фраз из песен</option>
                    <option>Энциклопедия символов</option>
                    <option>Немецкая грамматика с человеческим лицом</option>
                    <option>Стихотворения Шандора Петефи</option>
                </select>

                <select type="text" class="form-control" id="cat"  >
                    <option>Энциклопеция</option>
                    <option>Короткие рассказы</option>
                    <option>Созвучности</option>
                    <option>IT Информационные технологии</option>
                    <option>Лимерик</option>
                    <option>Афоризмы своё</option>
                    <option>Афоризмы чужие</option>
                    <option>Стихотворения свои</option>
                    <option>Стихотворения чужие</option>
                    <option>English</option>
                    <option>Фразы из песен</option>
                </select>

                <input type="text" class="form-control" id="tags"  placeholder="Тэги">
                <input type="text" class="form-control" id="published"  placeholder="Опубликовать 1, нет 0" value="0">
                <input type="text" class="form-control" id="cens"  placeholder="Уровень цензуры, 0 - +6" value="0">
                <input type="text" class="form-control" id="old_data"  value="<?=date('Y-m-d', time())?>">
                <textarea class="form-control" id="text"  placeholder="Текст" rows="10" cols="45"></textarea>

                <?php
                echo Widget::widget([
                    'selector' => '#text',
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 300,
                        'pastePlainText' => true,
                        //'buttons' => ['html', 'formatting', 'bold', 'italic'],
                        'buttonSource' => true,
                        'plugins' => [
                            'clips',
                            'fullscreen',
                            'table'
                        ],
                        //'imageManagerJson' => Url::to(['/articles/images-get']),
                        'imageUpload' => Url::to(['rockncontroll/default/image-upload']),

                        //'fileManagerJson' => Url::to(['/uploads/files-get']),
                        //'fileUpload' => Url::to(['/uploads/file-upload'])
                    ]
                ]);

                ?>
            <?php /*
                <button type="button" class="btn btn-success btn-lg btn-block" onclick="cleanImp()" >Очистить редактор!</button><hr>
            */?>
                <button type="button" class="btn btn-success btn-lg btn-block" id="record" >Записать!</button>


            </p>
            <?php /*
            $tags = core\entities\Rockncontroll\Tag::find()
                ->select(['name as label'])
                ->asArray()
                ->all();
            $sources = core\entities\Rockncontroll\Source::find()
                ->select(['title as label'])
                ->asArray()
                ->all();
            $caters = core\entities\Rockncontroll\Categories::find()
                ->select(['title as label'])
                ->asArray()
                ->all();
            ?>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'title')->textInput()  ?>

            <?php echo $form->field($model, 'source_title')->widget(
                AutoComplete::class, [
                'clientOptions' => [
                    'source' => $sources,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>
            <?php  //$form->field($model, 'cat_title')->textInput()  ?>
            <?php echo $form->field($model, 'cat_title')->widget(
                AutoComplete::className(), [
                'clientOptions' => [
                    'source' => $caters,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>

            <?= $form->field($model, 'text')->widget(Widget::class, [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 300,
                    'pastePlainText' => true,
                    //'buttons' => ['html', 'formatting', 'bold', 'italic'],
                    'buttonSource' => true,
                    'plugins' => [
                        'clips',
                        'fullscreen'
                    ],
                    //'imageManagerJson' => Url::to(['/articles/images-get']),
                    'imageUpload' => Url::to(['rockncontroll/default/image-upload']),

                    //'fileManagerJson' => Url::to(['/uploads/files-get']),
                    //'fileUpload' => Url::to(['/uploads/file-upload'])
                ]

            ]);?>
            <?=  $form->field($model, 'cens')->textInput()  ?>
            <?=  $form->field($model, 'published')->textInput()  ?>
            <?php  $form->field($model, 'tags')->textInput()  ?>
            <?php echo $form->field($model, 'tags')->widget(
                AutoComplete::className(), [
                'clientOptions' => [
                    'source' => $tags,
                    'minLength'=>'3',
                    'autoFill'=>true
                ],
                'options'=>[
                    'class'=>'form-control'
                ]
            ]);
            ?>
            <?= $form->field($model, 'audio_link')->textInput()  ?>
            <?= $form->field($model, 'in_work_prim')->textInput()  ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
            </div>

            <?php ActiveForm::end(); */
            ?>

            <div id="res" style="color: wheat"></div>
        </div>
    </form>
    <?php // <input class="fileinput" name="userpic" type="file" /> ?>

