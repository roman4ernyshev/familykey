<?php
/**
 * @var \app\models\RadioItem $item
 * @var \app\models\CleverAnswer $ans
 * @var integer $total
 */
?>
<style>
    .pic, img{
        width: 80%;
    }

    h2{
        margin: 0;
    }
    h2 img {
        vertical-align: bottom;
    }
    h3 span{
        font-size: 30px;
    }
    .cat{
        color: white;
    }
    .anons{
        color: rgb(255, 200, 46);
        font-size: 20px;
    }
    .title{
        color: rgb(244, 134, 104);
    }
    a {
        color: rgb(244, 134, 104);
        text-decoration: none;
    }
    .pic{
        border-radius: 25px;
    }
    p{
        font-size: 16px;
    }
    button p {
        height: 80px;
    }
    #hidden_audio{
        display: none;
    }
    .text{
        font-size: 18px;
        color: rgb(210, 105, 30);
    }
    h2{
        color: red;
    }
    h3 span{
        font-size: 15px;
    }

    .btn-success {

        background-color: rgb(36, 46, 56);
        border-color: rgb(36, 46, 56);
        background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(rgb(50, 49, 52)), to(rgb(0, 0, 20)));
    }
    #text{
        max-height: 0px;
        overflow: hidden;
    }
    #answers{
        color: yellow;
    }

    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid #dce3de;
    }
</style>
<div class="container" style="text-align: center;background-color: rgba(31, 28, 28, 0.81);">
    <hr style="border: 2px solid rgb(244, 134, 104);">
    <?php /*
    <h2>
        <span class="title"><?=$item->title?></span>
    </h2>
    */?>

    <h6 class="text"><?=$item->text?></h6>
    <?php if($item->img) : ?>
        <img src="<?=$item->img ?>" class="pic"/>
    <?php endif; ?>
    <h6 class="anons"><?=$item->anons?></h6>
    <?php if($item->audio) : ?>
        <audio controls controlsList="nodownload" >
            <source src="/uploads/<?=$item->audio?>">
        </audio>
    <?php endif; ?>
    <h3 id="answers">
        <?php foreach ($ans as $an) :?>
            <p onclick="testAnswer(<?=$an->id?>,<?=$item->next_item?>)"><?=$an->ans?></p>
        <?php endforeach; ?>
    </h3>

    <hr style="border: 2px solid rgb(244, 134, 104);">
</div>
<script>

    function testAnswer(answer_id, klar_item_id)
    {
        $.ajax({
            type: "GET",
            url: "/clever/test-answer/",
            data: "answer_id=" + answer_id + "&total=" + <?=$total?> + "&item_id=" + klar_item_id + "&user_id=" + <?=Yii::$app->user->id?>,
            success: function(html){
                $("#answers").html(html);
            }

        });

    }

    $(".content_toggle").click(function(){
        $('#text').slideDown(300, function(){
            $(this).css({"display":"block", "max-height":"none"});
            $('.content_toggle').hide();
        });

        return false;
    });

</script>
