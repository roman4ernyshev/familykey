<?php
/**
 * @var \app\models\RadioItem $item
 * @var string $resp
 * @var CleverItemShown $item_show
 * @var integer $total
 * @var CleverUserItem $users
 */
?>

<style>
    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid #dce3de;
    }
</style>

<div class="container" style="text-align: center">
    <hr style="border: 2px solid rgb(244, 134, 104);">
    <h2><?=$resp ?></h2>
    <h2>
        <span class="title"><?=$item->anons?></span>
    </h2>

    <h6 class="text"><?=$item->text?></h6>
    <?php if($item->img) : ?>
        <img src="<?=$item->img ?>" class="pic"/>
    <?php endif; ?>

    <?php if($item->audio) : ?>
        <audio controls controlsList="nodownload" >
            <source src="/uploads/<?=$item->audio?>">
        </audio>
    <?php endif; ?>

    <hr style="border: 2px solid rgb(244, 134, 104);">

    <table>
        <tr>
            <td>Юзер</td>
            <td>Попытки</td>
            <td>Очки</td>
            <td>Рейтинг</td>
        </tr>
        <?php foreach ($users as $user) : ?>
        <tr>
            <td><?=$user->user->username?></td>
            <td><?=$user->cnt?></td>
            <td><?=$user->sum?></td>
            <td><?=round($user->sum/$user->cnt, 2)?></td>
        </tr>
        <?php endforeach; ?>
    </table>


    <hr style="border: 2px solid rgb(244, 134, 104);">
    <button type="submit" class="btn-success" onclick="getMore(<?=$total?>)" id="butt" style="height: 55px;width: 100%;">
        <p style="font-size: 35px;">Ещё</p>
    </button>
</div>

<script>
    console.log(<?=$total?>);
    function getMore(total){
        if(total !== 1) window.location.href = '/clever/index/<?=$item_show->cat_id?>';
        else window.location.href = '/clever/index';
    }
</script>
