<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $source core\entities\Rockncontroll\Source */
/* @var $contentProvider yii\data\DataProviderInterface */
?>


<?= $this->render('_article', [
    'model' => $source,
    'contentProvider' => $contentProvider,
]) ?>

<?= $this->render('search', [
    'model' => $form,
    'id' => ' '
]) ?>

