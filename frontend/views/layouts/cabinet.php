<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php $this->beginContent('@frontend/views/layouts/family_key.php') ?>


        <div id="content" class="col-sm-12">
            <?= $content ?>
        </div>


<?php $this->endContent() ?>