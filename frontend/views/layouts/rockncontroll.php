<?php
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\RockncontrollAsset::register($this);
//\frontend\assets\KrokodileAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en">
<!--<![endif]-->
<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title)?></title>
    <link href="<?= Html::encode(Url::canonical()) ?>" rel="canonical"/>
    <?= $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<style type="text/css">
    .btn-success {
        color: rgb(255, 255, 255);
        background-color: rgba(42, 46, 50, 0.95);
        border-color: rgba(63, 68, 72, 0.95);
        background-image: none;
    }
    .btn-success:hover{
        background-color: rgb(60, 63, 65);
        border-color: rgb(32, 27, 29);
    }

    .alert-success {
        color: rgb(231, 228, 214);
        background-color: rgb(34, 33, 35);
        border-color: rgb(175, 172, 173);
    }
    .form-control{
        background-color: rgb(8, 10, 12);
        color: white;
    }

    .form-inline .form-control {
        display: inline-block;
        width: 100%;
        vertical-align: middle;
    }

    /*gray scroll*/
    ::-webkit-scrollbar {
        width: 10px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 10px;
    }

    body{
        background: rgb(70, 74, 79);
        /*font-family: "Verdana", "sans-serif";*/
    }

    table{
        width: 100%;
    }

    .table_data > tbody > tr > td{
        padding: 0;
        width: 16.66%;
        color: currentColor;
        font-size: 14px;
    }

    header{
        background-color: rgb(43, 43, 43);
        border-radius: 5%;
        color: #b7b7b7;
    }
    .alert > p {
        text-align: center;
    }
    .alert > pre{
        background: #0a0a0a;
        color: #00aa00;
        text-align: left;
    }
    #rad_bard{
        text-align: center;
    }

    @media(min-width:1199px) {
        .container {
            width: 710px;

        }
    }
    @media (min-width: 768px){
        .form-inline .form-group {
            display: block;
            margin-bottom: 0;
            vertical-align: middle;
            width: 100%;
        }
    }

</style>

<div class="container" style="margin-top: 5px">

    <div class="container" style="display: none">

        <div id="top-links" class="nav pull-right">
            <ul class="list-inline">

                <li class="dropdown"><a href="" title="My Account" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"></span>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <?php if (Yii::$app->user->isGuest): ?>
                            <li><a href="<?= Html::encode(Url::to(['/auth/auth/login'])) ?>">Вход</a></li>
                            <li><a href="<?= Html::encode(Url::to(['/auth/signup/request'])) ?>">Регистрация</a></li>
                        <?php else: ?>
                            <li><a href="<?= Html::encode(Url::to(['/cabinet/default/index'])) ?>">Кабинет</a></li>
                            <li><a href="<?= Html::encode(Url::to(['/auth/auth/logout'])) ?>" data-method="post">Выход</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <?= Alert::widget() ?>
    <?= $content ?>


</div>
<?php $this->endBody() ?>


</body></html>
<?php $this->endPage() ?>
