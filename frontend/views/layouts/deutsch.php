<!DOCTYPE html>
<head>
    <meta charset='utf-8'>
    <title>Deutsch</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>

    <!--[if lt IE 9]>
    <script src='../html5shim.googlecode.com/svn/trunk/html5.js' tppabs='http://html5shim.googlecode.com/svn/trunk/html5.js'></script>
    <![endif]-->
    <!-- Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/ua-parser.js"></script>
    <script src="js/fingerprint2.min.js"></script>
    <script src="js/jQuery-autoComplete-master/jquery.auto-complete.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jPlayer-2.9.2/dist/jplayer/jquery.jplayer.min.js"></script>
    <script src="js/jPlayer-2.9.2/src/javascript/add-on/jplayer.playlist.js"></script>
    <script src="js/jquery-cookie/jquery.cookie.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,100italic,100,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <!-- Stylesheets -->
    <link href='css/normalize.min.css' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css">
    <link href='css/style.css' rel='stylesheet'>
    <link href='js/jQuery-autoComplete-master/jquery.auto-complete.css' rel='stylesheet'>
    <link href='css/jquery-ui.css' rel='stylesheet'>

</head>
<style>
    body{
        background-color: rgb(125, 75, 15);
    }

    td, th, p, h3 {
        text-align: center;
    }

    .container{
        background-color:rgb(67, 37, 14);
        /*background: rgb(30, 29, 29) url(img/col_pattern.jpg) repeat;*/
        color: white;
        height: 1000px;
    }

    #card{
        text-align: center;
        border: 5px solid rgb(89, 96, 104);
        border-radius: 4px;
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .btn-success{
        background-color: rgb(164, 180, 195);
        border-color: rgb(69, 70, 73);
        background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(rgb(168, 182, 196)), to(rgb(0, 0, 0)));

    }
    .red{
        background-color: rgb(125, 37, 27);
        border-color: rgb(255, 42, 55);
    }
    .btn-success:hover, .btn-success:focus, .btn-success:active, .btn-success.active, .open .dropdown-toggle.btn-success {
        /*color: rgb(40, 40, 47);
        background-color: rgba(236, 231, 8, 0);*/
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }
    button{
        width: 100%;
        height: 60px;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        margin-bottom: 5px;
    }

    button p{
        font-size: 15px;
    }
    .active-button{
        border-color: rgb(162, 162, 163);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(162, 162, 163)), to(rgb(0, 0, 20)));
    }


    .btn{
        height: 50px;
    }

    .btn:hover{
    //background-color: rgb(125, 37, 27);
        border-color: rgb(255, 42, 55);
        color: rgb(255, 42, 55);
        background: -webkit-gradient(linear, 0% 0%, 0% 79%, from(rgb(255, 42, 55)), to(rgb(0, 0, 20)));
    }


</style>
<body>
<div class="container">

    <div id="card"></div>

    <button type="submit" class="btn-success" onclick="nextCard()" id="next"><p>Следующая карточка</p></button>
    <button type="submit" class="btn-success" onclick="inTest()" id="in_test"><p>Проверить себя</p></button>

    <div id="tour">

    </div>

</div>
</body>
<script id="userId"></script>

<script>


    $(document).ready(function() {

        $.ajax({
            type: "GET",
            url: "http://37.192.187.83:8098/datas/get-word-card/",
            success: function(html){
                $("#card").html(html);
                var translation = document.getElementById('translation');
                translation.style.display = 'block';
            }

        });


        var word_form = document.getElementById('word');
        var test_btn = document.getElementById('in_test');
        var card = document.getElementById('card');
        var next = document.getElementById('next');


    });

    function inTest() {
        document.location.href = "http://37.192.187.83:888/deutsch.html";
    }



    function nextCard() {

        document.location.reload();

    }

    function onPlay() {

        var aud_play = document.getElementById('aud_play');
        aud_play.play();

    }

    function onPlayPhrase() {
        var aud_play_phrase = document.getElementById('aud_play_phrase');
        aud_play_phrase.play();
    }
</script>
