<?php

?>
<style>
    .music > td{
        color: black;
        background-color: yellow;
        text-align: center;

    }
    .school > td{
        color: black;
        background-color:  #0b93d5;
        text-align: center;
    }
    .football > td{
        color: wheat;
        background-color: green;
        text-align: center;
    }
    .free > td{
        color: black;
        background-color: #363b3f;
        text-align: center;
    }
    .bordered > td{
        border-bottom:1pt solid black;
    }

    h3, p, h1{
        text-align: center;
        color: yellow;
    }
    h3{
        color: white;
        cursor: pointer;
    }
    table{
        width: 100%;
        font-size: 20px;
    }
    tr{
        height: 30px;
    }
    thead tr th:first-child,
    tbody tr td:first-child {
        width: 10px;
    }
    .btn-block{
        height: 50px;
    }
    .btn-success {
        background-color: rgba(98, 123, 149, 0.95);
    }

</style>
<h1>Расписание на неделю</h1>
<hr>
<div class="accord">
    <h3>Понедельник</h3>
    <table>
        <tr align="left" class="school bordered">
            <td>8</td>
            <td>8:50-9:30</td>
            <td>Английский язык</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>9</td>
            <td>9:40-10:20</td>
            <td>Русский язык</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>10</td>
            <td>10:30-11:10</td>
            <td>Литература</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>11</td>
            <td>11:20-12:00</td>
            <td>Технология</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>12</td>
            <td>12:10-12:50</td>
            <td>Технология</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>13</td>
            <td>13:00-13:40</td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>14</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="music bordered">
            <td>15</td>
            <td>15:45-16:30</td>
            <td>Фортепьяно</td>
        </tr>
        <tr align="left" class="music bordered">
            <td>16</td>
            <td>15:45-16:30</td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>17</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>18</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="football bordered">
            <td>19</td>
            <td>19:00-20:15</td>
            <td>Футбол</td>
        </tr>
        <tr align="left" class="football bordered">
            <td>20</td>
            <td>19:00-20:15</td>
            <td>Футбол</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>21</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <h3>Вторник</h3>
    <table>
        <tr align="left" class="school bordered">
            <td>8</td>
            <td>8:50-9:30</td>
            <td>Информатика</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>9</td>
            <td>9:40-10:20</td>
            <td>Математика</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>10</td>
            <td>10:30-11:10</td>
            <td>Русский язык</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>11</td>
            <td>11:20-12:00</td>
            <td>Литература</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>12</td>
            <td>12:10-12:50</td>
            <td>Физкультура</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>13</td>
            <td>13:00-13:40</td>
            <td>География</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>14</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="music bordered">
            <td>15</td>
            <td>15:10-16:40</td>
            <td>Хор</td>
        </tr>
        <tr align="left" class="music bordered">
            <td>16</td>
            <td>15:10-16:40</td>
            <td>Хор</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>17</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>18</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>19</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>20</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>21</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <h3>Среда</h3>
    <table>
        <tr align="left" class="school bordered">
            <td>8</td>
            <td>8:50-9:30</td>
            <td>Английский язык</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>9</td>
            <td>9:40-10:20</td>
            <td>Математика</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>10</td>
            <td>10:30-11:10</td>
            <td>История</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>11</td>
            <td>11:20-12:00</td>
            <td>Русский язык</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>12</td>
            <td>12:10-12:50</td>
            <td>Литература</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>13</td>
            <td>13:00-13:40</td>
            <td>Классный час</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>14</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>15</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="music bordered">
            <td>16</td>
            <td>16:20-17:05</td>
            <td>Музыкальная литература</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>17</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>18</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="football bordered">
            <td>19</td>
            <td>19:00-20:30</td>
            <td>Футбол</td>
        </tr>
        <tr align="left" class="football bordered">
            <td>20</td>
            <td>19:00-20:30</td>
            <td>Футбол</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>21</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <h3>Четверг</h3>
    <table>
        <tr align="left" class="school bordered">
            <td>8</td>
            <td>8:50-9:30</td>
            <td>Русский язык</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>9</td>
            <td>9:40-10:20</td>
            <td>Литература</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>10</td>
            <td>10:30-11:10</td>
            <td>Математика</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>11</td>
            <td>11:20-12:00</td>
            <td>Математика</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>12</td>
            <td>12:10-12:50</td>
            <td>Биология</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>13</td>
            <td>13:00-13:40</td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>14</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>15</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>16</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>17</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="music bordered">
            <td>18</td>
            <td>18:45-20:00</td>
            <td>Сольфеджио</td>
        </tr>
        <tr align="left" class="music bordered">
            <td>19</td>
            <td>18:45-20:00</td>
            <td>Сольфеджио</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>20</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>21</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <h3>Пятница</h3>
    <table>
        <tr align="left" class="school bordered">
            <td>7</td>
            <td>8:00-8:40</td>
            <td>Английский язык</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>8</td>
            <td>8:50-9:30</td>
            <td>Физкультура</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>9</td>
            <td>9:40-10:20</td>
            <td>История</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>10</td>
            <td>10:30-11:10</td>
            <td>Математика</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>11</td>
            <td>11:20-12:00</td>
            <td>Математика</td>
        </tr>
        <tr align="left" class="school bordered">
            <td>12</td>
            <td>12:10-12:50</td>
            <td></td>
        </tr>
        <tr align="left" class="school bordered">
            <td>13</td>
            <td>13:00-13:40</td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>14</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="music bordered">
            <td>15</td>
            <td>15:10-16:40</td>
            <td>Хор</td>
        </tr>
        <tr align="left" class="music bordered">
            <td>16</td>
            <td>15:10-16:40</td>
            <td>Хор</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>17</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>18</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="football bordered">
            <td>19</td>
            <td>19:00-20:15</td>
            <td>Футбол</td>
        </tr>
        <tr align="left" class="football bordered">
            <td>20</td>
            <td>19:00-20:15</td>
            <td>Футбол</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>21</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <h3>Суббота</h3>
    <table>
        <tr align="left" class="free bordered">
            <td>8</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>9</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>10</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>11</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>12</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>13</td>
            <td></td>
            <td>Хор</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>14</td>
            <td></td>
            <td>Хор</td>
        </tr>
        <tr align="left" class="free bordered">
            <td>15</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>16</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>17</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>18</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>19</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>20</td>
            <td></td>
            <td></td>
        </tr>
        <tr align="left" class="free bordered">
            <td>21</td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>
<?php echo
\yii\helpers\Html::a('Сегодня', ['index'], ['class' => 'btn btn-success btn-lg btn-block']);
?>
<script>
    $(document).ready(function() {
        $(".accord h3:first").addClass("active");

        $(".accord table").hide();

        $(".accord h3").click(function() {


            $(this).next("table").slideToggle("slow").siblings("table:visible").slideUp("slow");

            $(this).toggleClass("active");

            $(this).siblings("h3").removeClass("active");

        });

    });
</script>
