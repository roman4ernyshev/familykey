<?php
/**
 * @var $par array
 */
?>
<style>
    .music > td{
        color: black;
        background-color: yellow;
        text-align: center;

    }
    .school > td{
        color: black;
        background-color:  #0b93d5;
        text-align: center;
    }
    .football > td{
        color: wheat;
        background-color: green;
        text-align: center;
    }
    .free > td{
        color: white;
        background-color: #363b3f;
        text-align: center;
    }
    .bordered > td{
        border-bottom:1pt solid black;
    }

    h3, p, #do_calc{
        text-align: center;
    }
    h3{
        color: white;
    }
    table{
        width: 100%;
        font-size: 20px;
    }
    tr{
        height: 30px;
    }
    thead tr th:first-child,
    tbody tr td:first-child {
        width: 10px;
    }
    .btn-block{
        height: 50px;
    }
    .btn-success {
        background-color: rgba(98, 123, 149, 0.95);
    }
</style>
<h3>Расписание на <?=date('d-m-Y', time()) ?></h3>
<h4>Накоплено <?=$par['remain_for_rul']?> р</h4>
<h4>До игры осталось <?=$par['remain_to_play']?> о</h4>
<table>
    <tr align="left" class="<?=$par['hour_7_sel']?> bordered">
        <td>7</td>
        <td><?=$par['hour_7']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_8_sel']?> bordered">
        <td>8</td>
        <td><?=$par['hour_8']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_9_sel']?> bordered">
        <td>9</td>
        <td><?=$par['hour_9']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_10_sel']?> bordered">
        <td>10</td>
        <td><?=$par['hour_10']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_11_sel']?> bordered">
        <td>11</td>
        <td><?=$par['hour_11']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_12_sel']?> bordered">
        <td>12</td>
        <td><?=$par['hour_12']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_13_sel']?> bordered">
        <td>13</td>
        <td><?=$par['hour_13']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_14_sel']?> bordered">
        <td>14</td>
        <td><?=$par['hour_14']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_15_sel']?> bordered">
        <td>15</td>
        <td><?=$par['hour_15']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_16_sel']?> bordered">
        <td>16</td>
        <td><?=$par['hour_16']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_17_sel']?> bordered">
        <td>17</td>
        <td><?=$par['hour_17']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_18_sel']?> bordered">
        <td>18</td>
        <td><?=$par['hour_18']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_19_sel']?> bordered">
        <td>19</td>
        <td><?=$par['hour_19']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_20_sel']?> bordered">
        <td>20</td>
        <td><?=$par['hour_20']?></td>
    </tr>
    <tr align="left" class="<?=$par['hour_21_sel']?> bordered">
        <td>21</td>
        <td><?=$par['hour_21']?></td>
    </tr>
</table>
<?php echo
    \yii\helpers\Html::a('Расписание на неделю', ['week'], ['class' => 'btn btn-success btn-lg btn-block']);
?>
<script>
    setInterval(function () {
        location.reload() // window.location.reload()
    }, 20000);
</script>

