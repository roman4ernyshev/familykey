<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\article\SearchForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Title';
$this->params['breadcrumbs'][] = $this->title;
?>


<script>
    $(document).ready(function() {
        $('#source').focus(
            function () {
                $(this).select();
            });

        $('#source').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("<?=Url::toRoute(['article/sources/']); ?>", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });
    });

</script>

<style>
    .container {
        background-color: rgb(26, 30, 33);
        padding: 5px;
    }
    .well {
        background-color: rgba(42, 46, 50, 0.95);
        padding: 10px;
    }
    .well h2, h3{
        font-size: 30px;
        text-align: center;
        color: rgb(255, 255, 255);
    }
    .well .has-error, .has-error {
        color: rgb(232, 172, 196);
    }
    .row {
        margin-right: 5px;
        margin-left:  5px;
    }
    .btn-primary {
        color: rgb(35, 42, 255);
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .25);
        background-color: rgb(153, 141, 29);
        background-image: linear-gradient(to bottom, rgb(255, 121, 40), rgb(234, 255, 85));
        background-repeat: repeat-x;
        border-color: rgb(255, 121, 40) rgb(255, 121, 40) rgb(255, 121, 40);
    }

    .btn-primary:hover, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled] {
        background-position: 0;
    }
</style>

<div class="row">

    <h2>Поиск ресурса</h2>

    <?php $form = ActiveForm::begin(['id' => 'login-form', 'action' => Url::to(['article/index/'.$id])]); ?>

    <?= $form->field($model, 'title')
        ->textInput(['id' => 'source'])
    ?>

    <div>
        <?= Html::submitButton('Найти',
            ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>



