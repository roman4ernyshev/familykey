<?php

/* @var $this yii\web\View */
/* @var $model core\entities\Article\Source */
/* @var $contentProvider yii\data\DataProviderInterface */

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = 'Cabinet';
$this->params['breadcrumbs'][] = $this->title;
?>
<script>
    $(document).ready(function() {
        $(".accord h5:first").addClass("active");

        $(".accord span").hide();

        $(".accord h5").click(function() {

            $(this).next("span").slideToggle("slow").siblings("span:visible").slideUp("slow");

            $(this).toggleClass("active");

            $(this).siblings("h5").removeClass("active");

        });

    });
</script>
<style>
    h1 {
        font-size: 33px;
        color: rgb(255, 100, 0);
        text-align: center;
    }
    h5 {
        font-size: 20px;
        color: rgb(255, 255, 0);
        text-align: center;
    }
    span p{
        font-size: 17px;
        line-height: 1.2;
        color: rgb(160, 173, 184);
        text-align: left;
    }
    img {
        width: 100%;
    }
    pre {
        background-color: rgb(0, 0, 0);
        border: 1px solid rgb(82, 82, 82);
        border-radius: 4px;
        color: #01ff70;
    }

</style>


<?= $this->render('search', [
    'model' => $form,
    'id' => $model->id
]) ?>

<div class="accord">
    <h1><?= Html::encode($model->title) ?></h1>

    <?= \yii\widgets\ListView::widget( [
        'dataProvider' => $contentProvider,
        'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('_list_item',['model' => $model]);
        },
    ]);
    ?>
    <?php /*foreach ($model->articles as $article) : ?>
    <h5><?= $article->minititle ?></h5>
    <span><?= $article->body ?></span>
    <?php endforeach;*/ ?>
</div>
