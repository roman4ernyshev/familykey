<?php
/**
 * @var $events
 */
?>
<div class="container">

    <?= $this->render('events', ['events' => $events]) ?>

    <div id="matches"></div>
    <div id="sun_moon"></div>
    <div id="event"></div>
    <div id="pic"></div>
    <div id="history"></div>
    <div id="bds"></div>
    <div id="humor"></div>

</div>
