<style>
    .center, h3, table > tbody > tr > td{
        text-align: center;
    }
    .table > tbody > tr > td{
        color: rgb(255, 215, 0);
        padding: 0;
    }
    h3{
        color: rgb(255, 215, 0);
    }


    .form-control {
        width: 100%;
    }


</style>
<table class="table">
    <tbody>
        <tr>
            <td></td>
            <td>temp, C</td>
            <td>press, mm Hg</td>
        </tr>
        <?php if($pogot): ?>
        <tr>
            <td>Температура минимальная-максимальная</td>
            <td><?= $pogot->min_temp ?></td>
            <td><?= $pogot->max_temp ?></td>
        </tr>
        <?php endif; ?>
        <?php if($day_params): ?>
        <tr>
            <td>Утро метеостанция</td>
            <td><?=$day_params['temp_mon']?></td>
            <td><?=$day_params['press_mon']?></td>
        </tr>
        <tr>
            <td>День метеостанция</td>
            <td><?=$day_params['temp_day']?></td>
            <td><?=$day_params['press_day']?></td>
        </tr>
        <tr>
            <td>Вечер метео станция</td>
            <td><?=$day_params['temp_ev']?></td>
            <td><?=$day_params['press_ev']?></td>
        </tr>
        <?php endif; ?>
    </tbody>
    <tbody>
    <?php foreach ($weather as $city) : ?>
    <tr>
        <td><?= $city->city->name ?> <?= date('Y-m-d H:i', $city->time) ?></td>
        <td><?= (int)$city->temp - 273 ?> </td>
        <td><?= round($city->pressure * 100 /  133.3224) ?> </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>

