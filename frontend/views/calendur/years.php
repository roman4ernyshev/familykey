<?php
/**
 * @var $events
 * @var $header string
 */
$locale_time = setlocale (LC_TIME, 'ru_RU.UTF-8', 'Rus');
?>
<style>
    .container {
        background-color: rgb(45, 46, 47);

    }
    .top-cal{
        font-size: 30px;
        height: 30px;
    }
    .month-day{
        font-size: 20px;
        height: 20px;
        font-family: cursive;
    }
    .big-cifer{
        font-size: 100px;
        font-family: fantasy;
        height: 130px;
    }
    #cal{

        width: 50%;
    }
    .big-cifer p {
        line-height: 1;
    }
    .container{
        text-align: center;
    }
    .heighter{
        height: 500px;
        overflow: auto;
    }
    img {
        width: 100%;
    }
    table {
        border-collapse: separate;
        width: 100%;
        margin-bottom: 5px;
    }
    .bordered-block{
        border: 2px solid rgb(255, 152, 0);
        border-radius: 7px;

       /* margin: 5px;*/
    }
    .side_colums
    {
        background-color: rgb(33, 33, 33);
        width: 25%;
    }
    #years{
        cursor: pointer;
        padding: 5px;
    }

    #years span {
        display: inline-block;
        /*margin-top: 10px;*/
        margin-bottom: 5px;
        font-size: 30px;
    }
    h5{
        color: yellow;
    }

</style>
<div class="container">
    <table >

        <tbody>

        <tr>
            <td class="side_colums bordered-block">

            </td>
            <td id="cal" >

                    <p id="year" class="top-cal"><?= date('Y')?></p>
                    <p id="month" class="month-day"><?= strftime('%B')?></p>
                    <p id="cifer" class="big-cifer"><?= date('j')?></p>
                    <p id="day" class="month-day"><?= strftime('%A')?></p>
                    <h3 id="header" ></h3>

            </td>

            <td class="side_colums bordered-block">
                <p id="oz"></p>
                <p id="weight"></p>
            </td>

        </tr>

        </tbody>
    </table>

    <div id="years">
        <span onclick="send('1992')">1992 </span>
        <span onclick="send('1993')">1993 </span>
        <span onclick="send('1994')">1994 </span>
        <span onclick="send('1995')">1995 </span>
        <span onclick="send('1996')">1996 </span>
	    <span onclick="send('1997')">1997 </span>
        <span onclick="send('1998')">1998 </span>
        <span onclick="send('1999')">1999 </span>
        <span onclick="send('2000')">2000 </span>
        <span onclick="send('2001')">2001 </span>
        <span onclick="send('2002')">2002 </span>
        <span onclick="send('2003')">2003 </span>
        <span onclick="send('2004')">2004 </span>
        <span onclick="send('2005')">2005 </span>
        <span onclick="send('2006')">2006 </span>
        <span onclick="send('2007')">2007 </span>
        <span onclick="send('2008')">2008 </span>
        <span onclick="send('2009')">2009 </span>
        <span onclick="send('2010')">2010 </span>
        <span onclick="send('2011')">2011 </span>
        <span onclick="send('2012')">2012 </span>
        <span onclick="send('2013')">2013 </span>
        <span onclick="send('2014')">2014 </span>
        <span onclick="send('2015')">2015 </span>
        <span onclick="send('2016')">2016 </span>
        <span onclick="send('2017')">2017 </span>
        <span onclick="send('2018')">2018 </span>
        <span onclick="send('2019')">2019 </span>
	    <span onclick="send('2020')">2020 </span>
	    <span onclick="send('2021')">2021 </span>
	    <span onclick="send('2022')">2022 </span>
	    <span onclick="send('2023')">2023 </span>
        <span onclick="send('2024')">2024 </span>
    </div>




</div>
<div>
    <div class="bordered-block">
        <h5>Погода</h5>
        <div id="weather" ></div>
    </div>

    <div class="col-sm-6 heighter bordered-block">
        <h5>События</h5>
        <div id="summary_event" ></div>
    </div>

    <div class="col-sm-6 heighter bordered-block">
        <h5>Новости</h5>
        <div id="summary_news" ></div>
    </div>

    <div class="col-sm-6 heighter bordered-block">
        <h5>Матчи</h5>
        <div id="summary_matches" ></div>
    </div>

    <div class="col-sm-6 heighter bordered-block">
        <h5>Айтемы</h5>
        <div id="summary_items" ></div>
    </div>

</div>
<div id="summary"></div>

<script>
    $(document).ready(function() {
        let res = getRandomArbitrary(1992,2024);
        send(res);
    });
    function getRandomArbitrary(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    function send(year){
        $.ajax({
            type: "GET",
            url: "items/",
            data: "year="+year,
            success: function(html){
                $("#summary_items").html(html);
            }
        });
        $.ajax({
            type: "GET",
            url: "get-year/",
            data: "year="+year,
            success: function(html){
                $("#summary_event").html(html);
            }
        });
        $.ajax({
            type: "GET",
            url: "weather/",
            data: "year="+year,
            success: function(html){
                $("#weather").html(html);
            }
        });
        $.ajax({
            type: "GET",
            url: "head/",
            data: "year="+year,
            success: function(html){
                $("#header").html(html);
            }
        });
        $.ajax({
            type: "GET",
            url: "get-cur-data/",
            data: "year="+year,
            success: function(html){
                var arr = html.split('-');
                $("#year").html(arr[0]);
                $("#month").html(arr[3]);
                $("#cifer").html(arr[5]);
                $("#day").html(arr[4]);
            }
        });
        $.ajax({
            type: "GET",
            url: "news/",
            data: "year="+year,
            success: function(html){
                $("#summary_news").html(html);
                //setTimeout(function run() {
                //setTimeout(run, 30000);
                //}, 30000);
            }
        });
        $.ajax({
            type: "GET",
            url: "matches/",
            data: "year="+year,
            success: function(html){
                $("#summary_matches").html(html);
            }
        });
        /**
         * left block
         */
        $.ajax({
            type: "GET",
            url: "get-oz/",
            data: "year="+year,
            success: function(html){
                $("#oz").html(html);
            }
        });
        $.ajax({
            type: "GET",
            url: "weight/",
            data: "year="+year,
            success: function(html){
                $("#weight").html(html);
            }
        });
    }
</script>



