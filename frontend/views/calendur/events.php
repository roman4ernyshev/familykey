<style>
    .green-ev{
        color: green;
    }
    .greener-ev{
        color: #6daa89;
    }
    .red-ev{
        color: #ff691a;
    }
    .blue-ev{
        color: #9ed2ea;
    }
    .yell-ev{
        color: #ffe982;
    }
    .gray-ev{
        color: #545454;
    }
</style>
<div>
    <?php foreach ($events as $event) : ?>
        <?php if ($event->cat->id == 257) : ?>
            <p class="green-ev"><?= $event->text ?> <?= date('Y-m-d H:i', $event->act->time )?></p>
        <?php elseif ($event->cat->id == 145 || $event->cat->id == 120): ?>
            <p class="greener-ev"><?= $event->text ?> <?= date('Y-m-d H:i', $event->act->time )?></p>
        <?php elseif ($event->cat->id == 118) : ?>
            <p class="blue-ev"><?= $event->text ?> <?= date('Y-m-d H:i', $event->act->time )?></p>
        <?php elseif ($event->cat->id == 122) : ?>
            <p class="yell-ev"><?= $event->text ?> <?= date('Y-m-d H:i', $event->act->time )?></p>
        <?php elseif ($event->cat->id == 175) : ?>
            <p class="green-ev"><?= $event->text ?> <?= date('Y-m-d H:i', $event->act->time )?></p>
        <?php else: ?>
            <p class="red-ev"><?= $event->text ?> <?= date('Y-m-d H:i', $event->act->time )?></p>
        <?php endif; ?>
        <?php if ($event->img) : ?>
            <img src="<?=\yii\helpers\Url::to('@static/'.$event->img) ?>">
        <?php endif; ?>
    <?php endforeach; ?>
</div>

