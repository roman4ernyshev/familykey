<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $countries \core\entities\Country[] */
$this->title = 'Футбол';
?>
<style>
    .half_width{
        width: 48%;
        display:inline-block;
    }
</style>
    <div>

        <div id="search-form">
            <p>
                <input type="text" class="form-control half_width" id="team1" placeholder="Команда 1"> -
                <input type="text" class="form-control half_width" id="team2" placeholder="Команда 2">
                <button type="submit" class="btn-success" onclick="getMatchesOfTwoTeams()" id="get-two-teams">Матчи команд</button>
            </p>
            <p>
                <input type="text" class="form-control" id="data" placeholder="Дата в формате DD.MM.YYYY">
            </p>

        </div>
        <button type="submit" class="btn-success" onclick="getMatchesByData()" id="get-matches-by_data">Дата</button>
        <!--
        <button type="submit" class="btn-success" onclick="getTournamentsOfCountry()" id="get-tournament">Турнир</button>
        -->
        <div id="tournaments"></div>
        <div id="matches"></div>


    </div>

<script>
    $(document).ready(function() {

        $('#country').focus(
            function () {
                $(this).select();
            });
        $('#team1').focus(
            function () {
                $(this).select();
            });
        $('#team2').focus(
            function () {
                $(this).select();
            });

        $('#country').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("site/countries", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

        $('#team1').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("tournament/all-teams", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

        $('#team2').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("tournament/all-teams", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

    });

    function getTournamentsOfCountry() {

        var country_name = $("#country").val();
        $.ajax({
            type: "GET",
            url: "site/get-tournaments-of-country",
            data:  'name=' + country_name,
            success: function(res){
                $("#tournaments").html(res);
                $("#get-tournament").hide();
            }
        });
    }

    function getMatchesOfTwoTeams() {
        var team1 = $("#team1").val();
        var team2 = $("#team2").val();

        if(!team1 || !team2) {
            alert('Выберите из предлагаемого списка команду');
            return;
        }

        $.ajax({
            type: "GET",
            url: "tournament/get-matches-of-two-teams",
            data:  'team1=' + team1 + '&team2=' + team2,
            success: function(res){
                $("#matches").html(res);
            }
        });
    }

    function getMatchesByData() {
        const escapeRegex = str => str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
        var data = $("#data").val();
        console.log(escapeRegex(data));
        $.ajax({
            type: "GET",
            url: "tournament/get-matches-by-data",
            data:  'data=' + data,
            success: function(res){
                $("#matches").html(res);
            }
        });
    }

</script>

