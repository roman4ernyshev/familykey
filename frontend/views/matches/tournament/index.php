<?php
/* @var \core\entities\Soccer\Match[] $matches*/
/* @var  string $tournament_name */
/* @var \core\entities\Soccer\TeamSummary[] $summary*/
/* @var  integer $tournament_id*/
/* @var string $team_name */

use \yii\widgets\Pjax;
?>

    <style>

        .table > tbody > tr > td{
            color: rgb(255, 215, 0);
            font-size: 15px;
            cursor: pointer;
            padding-left: 3px;
        }
        .table > tbody > tr.firsts td{
            color: rgb(255, 140, 37);
        }
        .table > tbody > tr.lasts td{
            color: rgb(129, 236, 255);
        }
        h3{color: rgb(255, 215, 0); text-align: center}

        .collor_match {color: #b67b69}
        .fix_width {width: 10%}
        span{
            font-size: 13px;
        }

        .empty{
            text-align: center;
        }

        .view {
            width: 97%;
            max-width:900px;
            margin: 1em auto;
            font-size: 18px;
            padding: 1em;
            /*border-radius: 10px;*/
            /*background: rgb(16, 56, 10) radial-gradient(circle at 0 0, rgba(8, 21, 11, 0.65), rgba(6, 47, 7, 0.35));*/
            box-shadow:
                    inset rgba(250,250,250,.9) -8px 8px 8px,
                    inset rgba(255,255,255,.9) 8px 3px 8px,
                    rgba(0,0,0,.8) 3px 3px 8px -3px;
            background:
                    repeating-linear-gradient(
                            transparent,transparent 21px, rgb(100,100,100) 22px
                    ),
                    repeating-linear-gradient(
                            90deg,
                            transparent,transparent 21px, rgb(100,100,100) 22px
                    );

        }
        #match_date {
            color:#009900;
            font-size: 15px;
            float: right;
            padding-top: 3px;
            margin: 0;
            padding-right: 10px;
        }

        #match_tour{
            color:#009900;
            font-size: 18px;
            margin: 0;
            padding-left: 10px;

        }

        #match_tour:hover{
            cursor: pointer;
            color: #879971;
        }

        #mems_match {
            color:#e0bd69;
            font-size: 18px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }

        #bet {
            width: 750px;
            margin-left: 70px;
        }

        .left_bet_cyph{

            text-align: right;
            width: 250px;
            color: #0088e4;
            font-size: 30px;
        }

        .center_bet_cyph{
            text-align: center;
            width: 250px;
            color: #0088e4;
            font-size: 30px;

        }

        .right_bet_cyph{
            text-align: left;
            width: 250px;
            color: #0088e4;
            font-size: 30px;

        }

        .left_bet{

            text-align: right;
            width: 250px;
            color: #0088e4;

        }

        .center_bet{
            text-align: center;
            width: 250px;
            color: #0088e4;


        }

        .right_bet{
            text-align: left;
            width: 250px;
            color: #0088e4;

        }


        #ud{
            color: #7db0e0;
            font-size: 15px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }

        #vlad{
            color: #e08129;
            font-size: 15px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }

        #coach{
            color: #24e08d;
            font-size: 15px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }

        #keeper{
            color: #a2e0d1;
            font-size: 15px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }

        #falls{
            color: #e05d1d;
            font-size: 15px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }


        #stavki{
            color: #e0c400;
            font-size: 15px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }

        .left{

            text-align: right;
            width: 40%;
            padding-left: 20px;
            /*word-wrap: break-word;*/
        }

        .small-font{
            font-size: 13px;
            color: rgb(255, 235, 59);
        }

        .center{
            text-align: center;
            width: 50px;
            padding: 5px;

        }

        .right{
            text-align: left;
            width: 40%;
            padding-right: 20px;
            /*word-wrap: break-word;*/
        }

        #teams_data{
            width: 100%;
        }

        .info{
            font-size: 12px;
            color: #cf4b25;
            text-align: center;
        }

        #mems_goal {
            color:#BCE774;
            font-size: 14px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }

        #mems_goal p{
            text-align: center;
        }

        #substit{
            color: #9E771D;
            font-size: 15px;

            margin: 0;
            /*width: 700px;*/
            table-layout: fixed;
        }

        td img{
            width: 10%;
        }

        .prim, .fa, .glyphicon{
            font-size: 13px;
        }


        @media(min-width:100px) and (max-width: 550px) {
            #planet {
                display: none;
            }

            .news {
                font-size: 10px;
            }

            #blinkingText1 {

                font-size: 12px;
            }

            h3 {
                font-size: 17px;
            }

            h2 {
                font-size: 20px;
            }

            .head_champ {
                font-size: 15px;
            }
            .timer {
                font-size: 15px;
            }
            .emb {
                width: 18%;
            }
            /*.left, .right, .center{
                font-size: 13px;
            }
            #mems_match .left,  #mems_match .right {

                font-size: 17px;

            }
            */


        }

    </style>
<h3><?=$tournament_name?></h3>

<?php if($summary): ?>

    <?= $this->render('_summary', [
        'summary' => $summary,
        'tournament_id' => $tournament_id,
        'team_name' => $team_name
    ]) ?>

<?php endif; ?>

<?php Pjax::begin();
echo \yii\widgets\ListView::widget( [
    'dataProvider' => $matches,
    'itemOptions' => ['class' => 'item'],
    'summary' => false,
    'emptyText' => 'Данных нет',
    'itemView' => function ($model, $key, $index, $widget) use ($team_name){
        return $this->render('match',
            ['match' => $model, 'team_name' =>  $team_name]);
    },
    //'pager' => ['class' => \kop\y2sp\ScrollPager::class]
]);
Pjax::end();
?>
