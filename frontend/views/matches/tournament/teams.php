<?php
/**
 * @var $tournament_id integer
 */
?>
<div id="search-form">
    <p>
        <input type="text" class="form-control" id="team" placeholder="Команда">
    </p>

</div>

<button type="submit" class="btn-success" onclick="getMatches()" id="get-matches-of-team">Матчи</button>

<div id="matches"></div>


<script>
    $(document).ready(function() {

        $('#team').focus(
            function () {
                $(this).select();
            });

        $('#team').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("tournament/teams/<?= $tournament_id ?>", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

    });

    function getMatches() {

        var team = $("#team").val();
        $.ajax({
            type: "GET",
            url: "tournament/get-matches-of-team",
            data:  'name=' + team + '&tournament_id=' + <?= $tournament_id ?>,
            success: function(res){
                $("#matches").html(res);
                $("#get-matches").hide();
                $("#tournament").hide();
                $("#get-tournament").show();
            }
        });
    }
</script>

