<?php
/**
 * @var \core\entities\tablo\TabMatch[] $matches
 * @var \core\entities\tablo\TabSummary[] $summary
 * @var string $tournament
 */
?>
<style>
    .tbl{
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }
</style>
<h4><?= $tournament ?></h4>

<h4>Турнирная таблица</h4>
<table class="tbl">
    <tr>
        <td>м</td>
        <td>команда</td>
        <td>в</td>
        <td>н</td>
        <td>п</td>
        <td>заб</td>
        <td>:</td>
        <td>проп</td>
        <td>о</td>
    </tr>
    <?php $i=0; foreach ($summary as $team_sum): $i++; ?>

        <tr>
            <td><?= $i ?></td>
            <td><?= $team_sum->getTeamName() ?></td>
            <td><?= $team_sum->victories ?></td>
            <td><?= $team_sum->ties ?></td>
            <td><?= $team_sum->defeats ?></td>
            <td><?= $team_sum->gets ?></td>
            <td>:</td>
            <td><?= $team_sum->lets ?></td>
            <td><?= $team_sum->balls ?></td>
        </tr>

    <?php endforeach; ?>
</table>

<table class="tbl">
    <?php foreach ($matches as $match): ?>

        <tr>
            <td><?= $match->team1->name ?></td>
            <td>-</td>
            <td><?= $match->team2->name ?></td>
            <td><?= $match->goals_first ?></td>
            <td>:</td>
            <td><?= $match->goals_second ?></td>
        </tr>

    <?php endforeach; ?>
</table>

