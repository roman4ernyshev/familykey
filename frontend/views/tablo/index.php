<style xmlns="http://www.w3.org/1999/html">
    body{
        background-color: rgb(25, 32, 34);
    }
    td, #tournament, #form{
        text-align: center;
    }
    .container{
        background: rgba(0, 5, 212, 0.38);
        width: 100%;
    }
    #team_host, #team_guest{
        width: 40%;
        font-size: 40px;
    }
    #score_host, #score_guest{
        font-size: 80px;
    }
    .timer{

        font-size: 50px;
    }
    #score_host, #score_guest, .timer{
        cursor: pointer;
    }
    #tablo{
        margin-top: 100px;
        border-radius: 10px;
    }
    #form{
        margin-top: 10px;
        border-radius: 10px;
    }

    @media (max-width: 992px){
        #team_host, #team_guest {
            font-size: 20px;
        }
    }

    @media (max-width: 768px) {

        #team_host, #team_guest {
            font-size: 13px;
        }
    }


</style>
<div id="block">
    <div class='container' id="tablo">
        <h2 id="tournament">Чемпионат</h2>
        <div class="table-responsive">
            <table class="table">
                <tr class="teams">
                    <td id="team_host">Команда 1</td>
                    <td id="part"></td>
                    <td id="team_guest">Команда 2</td>
                </tr>
                <tr class="score">
                    <td id="score_host" onclick="scoreIter('score_host')">0</td>
                    <td onclick="time()" class="timer"><span id="minute">2</span>:<span id="second">00</span></td>
                    <td id="score_guest" onclick="scoreIter('score_guest')">0</td>
                </tr>
            </table>
        </div>
    </div>


    <div class='container' id="form">
        <form class="form-inline center" role="form" id="form-ate">
            <div class="form-group">
                <h3>Заполни форму!</h3>
                <div>
                    <input type="text" class="form-control" id="set_tournament"  placeholder="Турнир"><br>
                    <button type="button" class="btn btn-success" id="show_tournament" onclick="showTournament()">
                        Показать положение команд в турнире!
                    </button>
                    <input type="text" class="form-control" id="set_host"  placeholder="Хозяин"><br>
                    <input type="text" class="form-control" id="set_guest"  placeholder="Гость"><br>
                    <div id="two_time">
                        <span>Один или два тайма по</span>
                        <input type="text" class="form-control" id="set_minute"  placeholder="Минут(ы)"><br>
                        <button type="button" class="btn btn-success" id="set_button_1" onclick="setField()">
                            Сыгать в один тайм
                        </button>
                        <button type="button" class="btn btn-success" id="set_button_2" onclick="setField(1)">
                            Сыграть в два тайма!
                        </button>
                    </div>
                    <div id="two_extra_time" style="display: none">
                        <span>Один или два дополнительных тайма по</span>
                        <input type="text" class="form-control" id="set_minute"  placeholder="Минут(ы)"><br>
                        <button type="button" class="btn btn-success" id="set_button_3" onclick="setField(3)">
                            Сыгать в один тайм
                        </button>
                        <button type="button" class="btn btn-success" id="set_button_4" onclick="setField(4)">
                            Сыграть в два тайма!
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div id="finish"></div>
    <?php //<audio style="display: none" src="verdy_march_aida.mp3" id="audio"></audio> ?>

</div>
<script>

    var timer;
    var timeUp = 0;
    var state;
    var parted;
    var time_minute;

    $(document).ready(function() {

        $('#set_host').focus(
            function () {
                $(this).select();
            });
        $('#set_guest').focus(
            function () {
                $(this).select();
            });
        $('#set_tournament').focus(
            function () {
                $(this).select();
            });

        $('#set_host').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("teams/", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

        $('#set_guest').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("teams/", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });

        $('#set_tournament').autoComplete({
            minChars: 3,
            source: function (term, suggest) {
                console.log(term);
                term = term.toLowerCase();

                $.getJSON("tournaments/", function (data) {
                    console.log(data);
                    choices = data;
                    var suggestions = [];
                    for (i = 0; i < choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                    suggest(suggestions);

                }, "json");

            }
        });
    });

    function stopClock() {
        window.clearInterval(timer);
        sendParams();
    }

    function time() {
        if(timeUp == 0) timeUp = 1;
        else {
            timeUp = 0;
            stopClock();
        }

        if(timeUp) {
            var second = document.getElementById('second');
            var minute = document.getElementById('minute');
            var part_block = document.getElementById('part');
            var set_form = document.getElementById('form');
            var two_time = document.getElementById('two_time');
            var two_extra_time = document.getElementById('two_extra_time');

            timer = setTimeout(function run() {

                if(second.innerHTML == 0) {
                    minute.innerHTML--;
                    second.innerHTML = 59;
                }
                else second.innerHTML--;
                timer = setTimeout(run, 1000);

                if(second.innerHTML<10) second.innerHTML = '0' + second.innerHTML;
                if(second.innerHTML == 0 && minute.innerHTML == 0) {

                    if(hasValue(state, "parted", 0)){
                        saveMatch();
                    }

                    else if(hasValue(state, "parted", 1)){
                        parted = 2;
                        part_block.innerHTML = 'Второй тайм';
                        minute.innerHTML = time_minute;
                        second.innerHTML = '00';
                        timeUp = 0;
                    }
                    else if (hasValue(state, "parted", 2)) {
                        if(state.sc_host == state.sc_guest && confirm("Сыграть дополнительное время?")){
                            parted = 3;
                            part_block.innerHTML = 'Дополнительное время. Первый тайм';
                            minute.innerHTML = '0';
                            second.innerHTML = '00';
                            timeUp = 0;
                            two_extra_time.style.display = 'block';
                            two_time.style.display = 'none';
                            set_form.style.display = 'block';
                        }
                        else {
                          saveMatch();
                        }
                    }
                    else if (hasValue(state, "parted", 3)) {
                        parted = 5;
                        part_block.innerHTML = 'Дополнительное время. Второй тайм';
                        minute.innerHTML = time_minute;
                        second.innerHTML = '00';
                        timeUp = 0;
                    }
                    else if (hasValue(state, "parted", 4)) {
                        parted = 5;
                        part_block.innerHTML = 'Дополнительное время';
                        minute.innerHTML = time_minute;
                        second.innerHTML = '00';
                        timeUp = 0;
                    }
                    else if (hasValue(state, "parted", 5)) {
                        if(state.sc_host == state.sc_guest && confirm("Серия пенальти?")){
                            parted = 4;
                            part_block.innerHTML = 'Дополнительное время. Первый тайм';
                            minute.innerHTML = '0';
                            second.innerHTML = '00';
                            timeUp = 0;
                        }
                        else {
                            saveMatch();
                        }
                    }
                    stopClock();

                }
                sendParams();

            }, 1000);
        }

    }

    function scoreIter(el) {

        var score = document.getElementById(el);

        if(el === 'score_host'){
            score.innerHTML++;
        }
        else if (el === 'score_guest'){
            score.innerHTML++;
        }

        sendParams();

    }

    function setField(part=null) {
        var tournament = document.getElementById('tournament');
        var host = document.getElementById('team_host');
        var guest = document.getElementById('team_guest');
        var minute = document.getElementById('minute');
        var second = document.getElementById('second');

        var tournament_field = document.getElementById('set_tournament');
        var host_field = document.getElementById('set_host');
        var guest_field = document.getElementById('set_guest');

        var score_host = document.getElementById('score_host');
        var score_guest = document.getElementById('score_guest');

        var minute_field = document.getElementById('set_minute');

        var set_form = document.getElementById('form');
        var part_block = document.getElementById('part');

        if(part === 1) {
            part_block.innerHTML = 'Первый тайм';
            parted = 1;
        }
        else {
            part_block.innerHTML = 'Один тайм';
            parted = 0;
        }

       // console.log(score_host.innerText);
        tournament.innerHTML = tournament_field.value;
        host.innerHTML = host_field.value;
        guest.innerHTML = guest_field.value;
        minute.innerHTML = minute_field.value;
        time_minute = minute_field.value;
        second.innerHTML = '00';
        set_form.style.display = 'none';

        sendParams();

    }

    function sendParams() {

        state = {
            tournament: document.getElementById('tournament').innerText,
            host: document.getElementById('team_host').innerText,
            guest: document.getElementById('team_guest').innerText,
            sc_host: document.getElementById('score_host').innerText,
            sc_guest: document.getElementById('score_guest').innerText,
            minute: document.getElementById('minute').innerText,
            second: document.getElementById('second').innerText,
            timeUp: timeUp,
            parted: parted
        };


        $.ajax({
            type: "POST",
            url: "set-params/",
            data:'host=' + state.host +
            '&guest=' + state.guest +
            '&tournament=' + state.tournament +
            '&sc_guest=' + state.sc_guest +
            '&sc_host=' + state.sc_host +
            '&minute=' + state.minute +
            '&second=' + state.second +
            '&timeUp=' + state.timeUp +
            '&parted=' + document.getElementById('part').innerHTML,
            success: function(html){
                console.log(html);
            }

        });
    }

    function hasValue(obj, key, value) {
        return obj.hasOwnProperty(key) && obj[key] === value;
    }

    function saveMatch() {
        $.ajax({
            url: "save/",
            type: "POST",
            data:'host=' + state.host +
            '&guest=' + state.guest +
            '&tournament=' + state.tournament +
            '&sc_guest=' + state.sc_guest +
            '&sc_host=' + state.sc_host +
            '&minute=' + state.minute +
            '&second=' + state.second +
            '&timeUp=' + state.timeUp,
            success: function(html) {
                finish.innerHTML = html;
            }
        });

    }

    function showTournament() {
        var tournament_field = document.getElementById('set_tournament');
        if(!tournament_field.value) {
            alert('Введите турнир');
            return;
        }
        $.ajax({
            url: "show-tournament/",
            type: "POST",
            data:'tournament=' + tournament_field.value,
            success: function(html) {
                finish.innerHTML = html;
            }
        });
    }

</script>
