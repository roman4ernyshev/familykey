
<style>
    body{
        background-color: rgb(25, 32, 34);
    }
    td, #tournament, #form{
        text-align: center;
    }
    .container{
        background: rgba(0, 5, 212, 0.38);
        width: 100%;
    }
    #team_host, #team_guest{
        width: 40%;
        font-size: 40px;
    }
    #score_host, #score_guest{
        font-size: 80px;
    }
    .timer{

        font-size: 50px;
    }
    #score_host, #score_guest, .timer{
        cursor: pointer;
    }
    #tablo{
        margin-top: 100px;
        border-radius: 10px;
    }
    #form{
        margin-top: 10px;
        border-radius: 10px;
    }


    @media (max-width: 992px){
        #team_host, #team_guest {
            font-size: 20px;
        }
    }

    @media (max-width: 768px) {

        #team_host, #team_guest {
            font-size: 13px;
        }
    }

</style>
<div id="block">
    <div class='container' id="tablo">
        <h2 id="tournament"><?= Yii::$app->redis->get('tournament')?></h2>
        <div class="table-responsive">
            <table class="table">
                <tr class="teams">
                    <td id="team_host"><?= Yii::$app->redis->get('host')?></td>
                    <td id="part"><?= Yii::$app->redis->get('parted')?></td>
                    <td id="team_guest"><?= Yii::$app->redis->get('guest')?></td>
                </tr>
                <tr class="score">
                    <td id="score_host" >0</td>
                    <td class="timer"><span id="minute"><?= Yii::$app->redis->get('minute')?></span>:<span id="second">00</span><p id="finish"></p></td>
                    <td id="score_guest" >0</td>
                </tr>
            </table>
        </div>
    </div>


</div>
<script>

    var timer;
    var timeUp = 0;


    timer = setTimeout(function run() {

        newDatas();
        setTimeout(run, 1000);

    }, 1000);

    function newDatas() {
        var tournament = document.getElementById('tournament');
        var host = document.getElementById('team_host');
        var guest = document.getElementById('team_guest');

        var minute = document.getElementById('minute');
        var second = document.getElementById('second');
        var finish = document.getElementById('finish');
        var part = document.getElementById('part');
        /*

        var tournament_field = document.getElementById('set_tournament');
        var host_field = document.getElementById('set_host');
        var guest_field = document.getElementById('set_guest');
        */


        $.ajax({
            url: "get-params/",
            success: function(html){
                var data = JSON.parse(html);
                console.log(data);
                tournament.innerText = data.tournament;
                host.innerHTML = data.host;
                guest.innerHTML = data.guest;
                score_host.innerHTML = data.sc_host;
                score_guest.innerHTML = data.sc_guest;
                console.log(data.sc_host);
                minute.innerHTML = data.minute;
                second.innerHTML = data.second;
                part.innerHTML = data.parted;
                finish.innerHTML = '';
                if(data.second === '00' && data.minute === '0') {
                    console.log('матч ок');
                    stopClock();
                    finish.innerHTML = 'Матч окончен!';
                }

            }

        });
    }


    function stopClock() {
        window.clearInterval(timer);
    }



</script>
