<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\auth\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .well {
        background-color: rgba(42, 46, 50, 0.95);
        padding: 10px;
    }
    .well h2, h3{
        font-size: 30px;
        text-align: center;
        color: rgb(255, 255, 255);
    }
    .well .has-error, .has-error {
        color: rgb(232, 172, 196);
    }
</style>


<div class="row">
    <div class="col-sm-6">
        <div class="well">
            <h2>Нет аккаунта?</h2>
            <p><strong><a href="<?= Html::encode(Url::to(['/auth/signup/request'])) ?>" class="btn btn-primary">Зарегистрируйтесь!</a></strong></p>
        </div>
        <div class="well">

            <h2>Есть аккаунт в соцсетях?</h2>

            <?=
            yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['auth/network/auth']
            ]);
            ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="well">
            <h2>У вас уже есть аккаунт?</h2>
            <h3><strong>Добро пожаловать!</strong></h3>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput() ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <?= $form->field($model, 'reCaptcha')->widget(
                \himiklab\yii2\recaptcha\ReCaptcha::className(), ['siteKey' => \Yii::$app->params['siteKey']]
                //['siteKey' => '6LeSnG0UAAAAAK9YosWX3IS7ZSA39OFXsBzuJIfU']
            ) ?>

            <div style="color:#999;margin:1em 0">
                Если вы забыли пароль, вы можете <?= Html::a('восстановить его', ['auth/reset/request']) ?>.
            </div>

            <div>
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div id="guest_menu">
    <button type="button" class="btn btn-success btn-lg btn-block" onclick="send(user,'day-params')">Сегодня</button>
    <button type="button" class="btn btn-success btn-lg btn-block" onclick="send(user,'show-task')">Задачи</button>
</div>
