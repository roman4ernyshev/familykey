<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\auth\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    h2, h3{
        font-size: 30px;
        text-align: center;
        color: rgb(255, 255, 255);
    }
</style>
<div class="site-signup">

    <h3>Заполните форму регистрации и нажмите кнопку</h3>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>


                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'reCaptcha')->widget(
                    \himiklab\yii2\recaptcha\ReCaptcha::className(),
                    //['siteKey' => '6LdpzGUUAAAAALMKcmhWd-Q9Q00s0rYpu0IanF5M']
                        ['siteKey' => \Yii::$app->params['siteKey']]
                ) ?>

                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
