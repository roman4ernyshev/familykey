<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@staticRoot' => $params['staticPath'],
        '@static'   => $params['staticHostInfo'],
    ],
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => [
        'log',
        'common\bootstrap\SetUp',
        // for different queries formats
        [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => 'json',
                'application/xml' => 'xml',
            ],
        ],
    ],

    'modules' => [

        'oauth2' => [
            'class' => \filsh\yii2\oauth2server\Module::class,

            'components' => [
                'request' => function () {
                    return \filsh\yii2\oauth2server\Request::createFromGlobals();
                },
                'response' => [
                    'class' => \filsh\yii2\oauth2server\Response::class,
                ],
            ],

            'tokenParamName' => 'accessToken',
            'tokenAccessLifetime' => 3600 * 24,
            'storageMap' => [
                'user_credentials' => 'common\auth\Identity',
            ],
            'grantTypes' => [
                'user_credentials' => [
                    'class' => 'OAuth2\GrantType\UserCredentials',
                ],
                'refresh_token' => [
                    'class' => 'OAuth2\GrantType\RefreshToken',
                    'always_issue_new_refresh_token' => true
                ]
            ]
        ]

    ],

    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
        ],
        'response' => [
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
        ],
        // login only by token
        'user' => [
            'identityClass' => 'common\auth\Identity',
            'enableAutoLogin' => true,
            'enableSession' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                'GET site' => 'site',
                'GET book' => 'book',
                'word/<id:\d+>' => 'site/index',
                'words' => 'site/words',
                'lat-words' =>'site/lat-words',
                'wordobs' => 'site/wordobs',
                'webobs' => 'site/webobs',
                'crossword' => 'site/crossword',
                'web-crossword' =>  'site/web-crossword',
                'rus-crossword' =>  'site/rus-crossword',
                'rand-img' => 'site/rand-img',
                'rand-word' => 'site/rand-word',
                'cur-track' => 'site/cur-track',
                'rand-word/<lang:\w+>' => 'site/rand-word',
                '<_c:[\w\-]+>/<_a:[\w-]+>/<id:\d+>' => '<_c>/<_a>',
            ],
        ],
    ],

    'as authenticator' => [
        'class' => 'filsh\yii2\oauth2server\filters\auth\CompositeAuth',
        'except' => [
            'site/index',
            'book/index',
            'book/add',
            'book/update',
            'book/delete',
            'oauth2/rest/token',
            'site/words',
            'site/lat-words',
            'site/wordobs',
            'site/webobs',
            'site/crossword',
            'site/web-crossword',
            'site/rus-crossword',
            'site/rand-word',
            'site/rand-word-de',
            'site/rand-img',
            'site/cur-track'
        ],
        'authMethods' => [
            ['class' => 'yii\filters\auth\HttpBearerAuth'],
            ['class' => 'yii\filters\auth\QueryParamAuth', 'tokenParam' => 'accessToken'],
        ]
    ],
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'except' => [
            'site/index',
            'book/index',
            'book/add',
            'book/update',
            'book/delete',
            'oauth2/rest/token',
            'site/cur-track',
            'site/words',
            'site/lat-words',
            'site/wordobs',
            'site/webobs',
            'site/crossword',
            'site/web-crossword',
            'site/rus-crossword',
            'site/rand-word',
            'site/rand-word-de',
            'site/rand-img'
        ],
        'rules' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
    'as exceptionFilter' => [
        'class' => 'filsh\yii2\oauth2server\filters\ErrorToExceptionFilter',
    ],

    'params' => $params,
];