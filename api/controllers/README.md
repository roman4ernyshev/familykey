


Задание на вакансию php-разработчика
------
Реализовал на своей тестовой платфоме noravbo.ru

Создать две связанных модели: книга и автор и реализовать методы GET, POST, PUT, DELETE
-----------------------------
Код сущностей https://bitbucket.org/roman4ernyshev/familykey/src/master/core/entities/Books/

Миграция https://bitbucket.org/roman4ernyshev/familykey/src/master/console/migrations/m190803_011617_books_tables.php

Необходимые методы реализованы здесь https://bitbucket.org/roman4ernyshev/familykey/src/master/api/controllers/BookController.php

roman.4ernyshev на связи в Skype для вопросов