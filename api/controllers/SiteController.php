<?php

namespace api\controllers;

use core\entities\Rockncontroll\Items;
use core\entities\Rockncontroll\RadioItem;
use core\entities\Word\Definition;
use core\entities\Word\DefinitionThema;
use core\entities\Word\Example;
use core\entities\Word\Word;
use core\helpers\DisplayCrossword;
use yii\db\Expression;
use yii\helpers\Url;
use yii\rest\Controller;


class SiteController extends Controller
{
    private $display_crossword;

    public function __construct(
        string $id,
               $module,
        DisplayCrossword $display_crossword_service,
        array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->display_crossword = $display_crossword_service;
    }

    public function actionIndex($id): array
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $word = Word::findOne($id);

        if($word)
        {
            return [
                'status' => true,
                'id' => $word->id,
                'word' => $word->title,
                'lang' => $word->lang,
                'definitions' => $this->getAllWordDefinitions($word->id)
            ];
        }
        else {
            return ['status' => false, 'data' => 'Not Found'];
        }

    }

    public function actionRandWordDe(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $word = Word::find()
            ->where('lang = 2')
            ->orderBy(new Expression('random()'))
            ->one();

        if($word)
        {
            return [
                'status' => true,
                'id' => $word->id,
                'word' => $word->title,
                'lang' => $word->lang,
                'definitions' => $this->getAllWordDefinitions($word->id)
            ];
        }
        else {
            return ['status' => false, 'data' => 'Not Found'];
        }
    }

    public function actionRandImg(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $item = Items::find()
            ->where('cat_id = 259')
            ->andWhere(['not in','id',
                [204,12381,12804,16607,16623,16640,41553,43760,
                    44383,46960,47519,47744,47865,48041,48085,
                    50417,50419,53050,54193,54705,54744,57154,57159,59213,59291,59717]])
            ->orderBy(['rand()' => SORT_DESC])
            ->limit(1)
            ->one();

        return [
            'status' => true,
            'data' => '<img alt="Радио Комната С Мехом" class="pic css-adaptive" src="' . Url::to('@static/'.$item->img) . '?cash='.time().'"/>'];
    }

    public function actionCurTrack(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $xml = simplexml_load_file('http://88.85.67.159:8000/test.xspf');
        $title = $xml->trackList->track->title;
        $item = RadioItem::find()->where(['like', 'audio', trim($title)])->one();
        if(!$item){
            $item = strstr($xml->trackList->track->title, ' - ');
            $title = substr($item, 2);
            $item = RadioItem::find()->where(['like', 'audio', trim($title)])->one();
        }

        if($item)
        {
            return [
                'status' => true,
                'id' => $item->id,
                'title' => $item->title,
                'anons' => $item->anons,
            ];
        }
        else {
            return ['status' => false, 'data' => 'Not Found'];
        }

    }

    public function actionRandWord($lang){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        switch ($lang) {
            case 'de':
                $word = Word::find()
                    //->where(['lang' => 2])
                    ->where(['like', 'lang', '1'])
                    ->orderBy(new Expression('random()'))
                    //->limit(1)
                    ->one();
                break;
            case 'en':
                $word = Word::find()
                    //->where(['lang' => 1])
                    ->where(['like', 'lang', '0'])
                    ->orderBy(new Expression('random()'))
                    //->limit(1)
                    ->one();
                break;
            case 'ru':
                $word = Word::find()
                    //->where(['lang' => 0])
                    ->where(['like', 'lang', '2'])
                    ->orderBy(new Expression('random()'))
                    //->limit(1)
                    ->one();
                break;
            case 'it':
                $word = Word::find()
                    //->where(['lang' => 0])
                    ->where(['like', 'lang', '3'])
                    ->orderBy(new Expression('random()'))
                    //->limit(1)
                    ->one();
                break;
            default:
                $word = Word::find()
                   // ->where(['lang' => 3])
                    //->where(['like', 'lang', ''])
                    ->orderBy(new Expression('random()'))
                    //->limit(1)
                    ->one();
        }


        if($word)
        {
            return [
                'status' => true,
                'id' => $word->id,
                'word' => $word->title,
                'lang' => $word->lang,
                'definitions' => $this->getAllWordDefinitions($word->id)
            ];
        }
        else {
            return ['status' => false, 'data' => 'Not Found'];
        }

    }

    function getAllWordDefinitions($word_id): array
    {

        $result_arr = [];

        $definitions = Definition::find()
            ->where(['word_id' => $word_id])
            ->with('examples')
            ->all();

        for($i=0; $i<count($definitions); $i++){
            $result_arr[$i]['id'] = $definitions[$i]->id;
            $result_arr[$i]['word_id'] = $definitions[$i]->word_id;
            $result_arr[$i]['value'] = $definitions[$i]->value;
            $result_arr[$i]['synonyms'] = $definitions[$i]->synonyms;
            $result_arr[$i]['antonyms'] = $definitions[$i]->antonyms;
            $result_arr[$i]['tags'] = $definitions[$i]->tags;
            $result_arr[$i]['etymology'] = $definitions[$i]->etymology;
            $result_arr[$i]['forms'] = $definitions[$i]->forms;
            $result_arr[$i]['adult_status'] = $definitions[$i]->adult_status;
            $result_arr[$i]['type'] = $definitions[$i]->type;
            $result_arr[$i]['examples'] = $this->getAllDefinitionsExamples($definitions[$i]->id);
        }

        return $result_arr;

    }

    function getAllDefinitionsExamples($def_id): array
    {
        return Example::find()
            ->where(['definition_id' => $def_id])
            ->all();
    }


    public function actionWords(): array
    {
        $res = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $words = Word::find()->all();
        foreach ($words as $word){
            $res[] = $word->title;
        }
        return $res;
    }

    public function actionLatWords(): array
    {
        $res = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $words = Word::find()
            ->where(['lang' => 0])
            ->orWhere(['lang' => 1])
            ->all();
        foreach ($words as $word){
            $res[] = $word->title;
        }
        return $res;
    }

    public function actionWordobs(): array
    {
        $res = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $words = Word::find()->all();
        shuffle($words);
        foreach ($words as $key=>$word){
            $res[$key]['id'] = $word->id;
            $res[$key]['titel'] = $word->title;
        }
        return $res;
    }

    public function actionWebobs(): array
    {
        $res = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $definitions = DefinitionThema::find()
            ->select('definitions.*, word.*, definition_crossword_thema.*')
            ->where('crossword_thema_id = 1')
            ->leftJoin('definitions', 'definitions.id = definition_crossword_thema.definition_id')
            ->leftJoin('word', 'word.id = definitions.word_id')
            ->asArray()
            ->all();

      // var_dump($definitions); exit;

        shuffle($definitions);
        foreach ($definitions as $key=>$definition){
            $res[$key]['id'] = $definition['word_id'];
            $res[$key]['titel'] = Word::findOne($definition['word_id'])->title;
        }
        return $res;
    }

    public function actionWebCrossword(): array
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $this->display_crossword->getAxleCrossword(1);
    }

    public function actionRusCrossword()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //$crossword = new DisplayCrossword();
        return $this->display_crossword->getAxleCrossword(5);
    }

    public function actionCrossword()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //$crossword = new DisplayCrossword();
        return $this->display_crossword->getAxleCrossword();
    }

    private static function getNextCol($index, $shift, $word, &$grid)
    {
        for ($i=$index; $i<100; $i=$i+10){
            if ($i === $shift) {
                for($k=0;$k<strlen($word['string']);$k++){
                    if(isset($grid[$i+$k]['blank']) && $grid[$i+$k]['blank'] === true) {
                        $grid[$i+$k] = [
                            'blank' => false,
                            'liter' => mb_substr($word['string'], $k, 1),
                            'word' => $word['string'],
                            'i' => mb_strlen($word['string']),
                            'id' => $word['id']];
                    } else continue;
                }
            } else continue;
        }
    }

    private static function getWorter(): array
    {
        $res = [];
        $words = Word::find()
            ->where(['lang'=>1])
            ->all();
        foreach ($words as $word){
            $res[] = $word->title;
        }
        return $res;
    }

    private static function getRandomWord(): array
    {
        $words = self::getWorter();
        $random_word = rand(0,count($words)-1);
        return [
            'id' => $random_word,
            'string' => $words[$random_word]
        ];
    }
}