<?php

namespace api\controllers;

use api\providers\MapDataProvider;
use core\entities\Books\Book;
use core\entities\Rockncontroll\Items;
use core\repositories\BookRepository;
use core\useCases\manage\BookManageService;
use yii\data\DataProviderInterface;
use yii\rest\Controller;

/**
 * Class BookController
 *
 * В данном контроллере реализованы методы для тестового задания
 * на вакансию PHP-разработчика
 *
 *
 * @package api\controllers
 */
class BookController extends Controller
{
    private $service;
    private $books;

    public function __construct(
        $id,
        $module,
        BookManageService $service,
        BookRepository $books,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->books = $books;
    }

    protected function verbs(): array
    {
        return [
            'index' => ['GET'],
            'add' => ['POST'],
            'update' => ['PUT'],
            'delete' => ['DELETE'],
        ];
    }

    /**
     * Получение всех книг данного автора (в формате json)
     * по его айдишнику
     * @param $author_id
     *
     * @return array
     *
     * Можно протестировать запрос в HTTP клиенте
     * GET http://api.noravbo.ru/book/1
     * Accept: application/json
     * Cache-Control: no-cache
     * Content-Type: application/json; charset=UTF-8
     *
     * Респонс:
     * {"title": "PHP. Объекты, шаблоны и методики программирования", "release_year": "2005", "edition": "IT" , "author_id" : "1"}
     */
    public function actionIndex(): array
    {
        // to see JSON in browser
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $student = Items::find()->where('id<10')->all();
        if(count($student) > 0 )
        {
            return array('status' => true, 'data'=> $student);
        }
        else {
            return array('status' => false, 'data' => 'No Student Found');
        }

    }

    /**
     * Добавление книги
     *
     * Можно протестировать запрос в HTTP клиенте
     * POST http://api.noravbo.ru/book/add
     * Accept: application/json
     * Cache-Control: no-cache
     * Content-Type: application/json; charset=UTF-8
     * POST Request Body format
     * {"title": "", "release_year": "", "edition": "" , "author_id" : ""}
     *
     * @TODO The better way to pass in this method
     * @TODO some object BookCreateForm, where we can validate data
     */
    public function actionAdd()
    {
        return $this->service->create(\Yii::$app->request->post());
    }

    /**
     * Изменение автора книги
     * (допущение: автор и книга в базе есть и запрос производится по айдишникам)
     * @return array|mixed
     *
     * в HTTP клиенте
     * PUT http://api.noravbo.ru/book/update
     * Accept: application/json
     * Cache-Control: no-cache
     * Content-Type: application/json; charset=UTF-8
     *
     * PUT Request Body format
     * {"author_id" : "", "book_id": ""}
     * @TODO some object BookUpdateForm to validate POST params
     */
    public function actionUpdate()
    {
        return $this->service->updateBookAuthor(\Yii::$app->request->post());
    }


    /**
     * Каскадное удаление автора и всех книг принадлежащих ему
     *
     * @TODO Данный метод правильнее было реализовать в AuthorController
     * @param $author_id
     *
     * в HTTP клиенте
     * DELETE http://api.noravbo.ru/book/delete/1
     * Accept: application/json
     * Cache-Control: no-cache
     * Content-Type: application/json; charset=UTF-8
     */
    public function actionDelete($author_id): void
    {
        $this->service->deleteAuthor($author_id);
    }


    public function serializeListItem(Book $book): array
    {
        return [
            'title' => $book->title,
            'edition' => $book->edition,
            'release_year' => $book->release_year,
            'author' => $book->author->name,
        ];
    }

}