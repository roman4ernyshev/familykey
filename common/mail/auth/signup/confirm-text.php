<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user core\entities\User\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['auth/signup/confirm', 'token' => $user->email_confirm_token]);
?>

Hello <?= $user->username?>,
 follow the link bellow to confirm your email
<?= $confirmLink?>

